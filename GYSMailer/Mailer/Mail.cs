﻿namespace Mailer
{
    using Microsoft.SqlServer.Server;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Data.SqlTypes;
    using System.Net;
    using System.Net.Mail;

    public class Mail
    {
        public Mail()
        {

        }

        [SqlFunction(DataAccess = DataAccessKind.Read, TableDefinition = "EventId INT, EventInvitationId INT, MemberId INT, EmailWasSent BIT, Message NVARCHAR(2000)", FillRowMethodName = "FillRow")]
        public static IEnumerable SendEmail(string SMTPHost, int SMTPPort, string SMTPUser, string SMTPPassword, string ReplyToList, string NameEmailAddress, string SQLSentenceToSend,
            bool IsBodyHtml = true,
            bool EnableSSL = true)
        {
            DataTable EventsDT = new DataTable();
            DataTable EmailsDT = new DataTable();
            List<EmailStatus> ResultEmails = new List<EmailStatus>();

            try
            {
                FillDataTables(EventsDT, EmailsDT, SQLSentenceToSend);
            }
            catch (Exception ex)
            {
                return new List<EmailStatus>()
                {
                    new EmailStatus() { EmailWasSent = false, EventId = 0, EventInvitationId = 0, MemberId = 0, Message = ex.Message }
                };
            }

            using (SmtpClient Client = new SmtpClient(SMTPHost, SMTPPort))
            {
                Client.Credentials = new NetworkCredential(SMTPUser, SMTPPassword);
                Client.EnableSsl = EnableSSL;
                foreach (DataRow RowEvent in EventsDT.Rows)
                {
                    string Body = RowEvent["EmailBody"].ToString();
                    foreach (DataColumn ColumnEvent in EventsDT.Columns)
                    {
                        Body = Body.Replace($"[[{ColumnEvent.ColumnName}]]", RowEvent[ColumnEvent.ColumnName].ToString());
                    }
                    foreach (DataRow RowEmail in EmailsDT.Rows)
                    {
                        using (MailMessage Message = new MailMessage()
                        {
                            From = new MailAddress(SMTPUser, NameEmailAddress),
                            IsBodyHtml = IsBodyHtml,
                            Subject = RowEvent["Name"].ToString()
                        })
                        {
                            Message.To.Add(RowEmail["Email"].ToString());
                            Message.Body = Body.Replace("[[FullName]]", RowEmail["FullName"].ToString());
                            Message.ReplyToList.Add(ReplyToList);
                            try
                            {
                                Client.Send(Message);
                                ResultEmails.Add(new EmailStatus()
                                {
                                    EventId = Convert.ToInt32(RowEmail["EventId"]),
                                    MemberId = Convert.ToInt32(RowEmail["MemberId"]),
                                    EventInvitationId = Convert.ToInt32(RowEmail["EventInvitationId"]),
                                    Message = "Mensaje enviado con éxito",
                                    EmailWasSent = true
                                });
                            }
                            catch (Exception ex)
                            {
                                ResultEmails.Add(new EmailStatus()
                                {
                                    EventId = Convert.ToInt32(RowEmail["EventId"]),
                                    MemberId = Convert.ToInt32(RowEmail["MemberId"]),
                                    EventInvitationId = Convert.ToInt32(RowEmail["EventInvitationId"]),
                                    Message = ex.Message,
                                    EmailWasSent = false
                                });
                            }
                        }
                    }
                }
            }
            return ResultEmails;
        }

        [SqlFunction(DataAccess = DataAccessKind.Read)]
        public static void SendCustomEmail()
        {

        }

        public static void FillRow(object EmailStatusObject, out SqlInt32 EventId, out SqlInt32 EventInvitationId, out SqlInt32 MemberId, out SqlBoolean EmailWasSent, out SqlString Message)
        {
            var EmStatus = (EmailStatus)EmailStatusObject;
            EventId = EmStatus.EventId;
            EventInvitationId = EmStatus.EventInvitationId;
            MemberId = EmStatus.MemberId;
            EmailWasSent = EmStatus.EmailWasSent;
            Message = EmStatus.Message;
        }

        public static void FillDataTables(DataTable EventsDT, DataTable EmailsDT, string SQLSentenceToSend)
        {
            using (SqlConnection sqlConn = new SqlConnection("context connection=true"))
            {
                sqlConn.Open();
                using (SqlCommand cmd = new SqlCommand(SQLSentenceToSend, sqlConn))
                {
                    using (DbDataReader dataReader = cmd.ExecuteReader())
                    {
                        EventsDT.Load(dataReader);
                        EmailsDT.Load(dataReader);
                    }
                }
                sqlConn.Close();
            }
        }
    }

    public class EmailStatus
    {
        public int EventId { get; set; }

        public int EventInvitationId { get; set; }

        public int MemberId { get; set; }

        public bool EmailWasSent { get; set; }

        public string Message { get; set; }
    }
}
