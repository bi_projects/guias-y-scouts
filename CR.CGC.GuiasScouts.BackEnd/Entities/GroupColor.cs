﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("GroupColor")]
    public class GroupColor
    {
        [XmlElement("GroupColorId")]
        public int? GroupColorId { get; set; }

        [Required]
        [XmlElement("GroupId")]
        public int GroupId { get; set; }

        [MaxLength(7)]
        [Required]
        [XmlElement("Color")]
        public string Color { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
