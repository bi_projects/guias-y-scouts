﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberAlert")]
    public class MemberAlert
    {
        [XmlElement("MemberAlertId")]
        public int? MemberAlertId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [Required]
        [XmlElement("AlertTypeId")]
        public int AlertTypeId { get; set; }

        [Required]
        [MaxLength(1000)]
        [XmlElement("Message")]
        public string Message { get; set; }

        [Required]
        [MaxLength(100)]
        [XmlElement("Url")]
        public string Url { get; set; }

        [Required]
        [MaxLength(140)]
        [XmlElement("SMSMessage")]
        public string SMSMessage { get; set; }

        [Required]
        [XmlElement("CreationDate")]
        public DateTime CreationDate { get; set; }


        [Required]
        [XmlElement("Received")]
        public bool Received { get; set; }

        [Required]
        [XmlElement("DateReceived")]
        public DateTime? DateReceived { get; set; }


        [Required]
        [XmlElement("Reading")]
        public bool Reading { get; set; }

        [Required]
        [XmlElement("ReadingDate")]
        public DateTime? ReadingDate { get; set; }


        [Required]
        [XmlElement("SmsSent")]
        public bool SmsSent { get; set; }

        [Required]
        [XmlElement("DateSendingSms")]
        public DateTime? DateSendingSms { get; set; }


        [Required]
        [XmlElement("DueDate")]
        public DateTime DueDate { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        //----------------------------------------------------

        [IgnoreProperty]
        [XmlElement("AlertType")]
        public AlertType AlertType { get; set; }

        //----------------------------------------------------

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordMemberAlert";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetMemberAlert";

        public MemberAlert()
        {
            CreationDate = DateTime.Now;
            DueDate = DateTime.Now;
        }
    }
}
