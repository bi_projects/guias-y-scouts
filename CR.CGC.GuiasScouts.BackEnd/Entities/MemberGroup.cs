﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberGroup")]
    public class MemberGroup
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberGroupId")]
        public int? MemberGroupId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Grupo")]
        [DisplayForeignBinnacleProperty("Group", "Name", "Groups", "GroupId")]
        [Required]
        [XmlElement("GroupId")]
        public int GroupId { get; set; }

        [DisplayNameBinnacleProperty("Sección")]
        [DisplayForeignBinnacleProperty("Section", "Name", "Sections", "SectionId")]
        [Required]
        [XmlElement("SectionId")]
        public int SectionId { get; set; }

        [DisplayNameBinnacleProperty("Progresión")]
        [DisplayForeignBinnacleProperty("Progression", "Name", "Progressions", "ProgressionId")]
        [Required]
        [XmlElement("ProgressionId")]
        public int ProgressionId { get; set; }

        [DisplayNameBinnacleProperty("Fecha de inicio")]
        [Required]
        [XmlElement("StartDate")]
        public DateTime StartDate { get; set; }

        [DisplayNameBinnacleProperty("Fecha de finalización")]
        [XmlElement("EndingDate")]
        public DateTime? EndingDate { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Group")]
        public Group Group { get; set; }

        [IgnoreProperty]
        [XmlElement("Section")]
        public Section Section { get; set; }

        [IgnoreProperty]
        [XmlElement("Progression")]
        public Progression Progression { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetMemberGroup";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordMemberGroup";
    }
}
