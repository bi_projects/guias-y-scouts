﻿namespace Entities
{
    using System.Net.Mail;

    public class EmailContainer
    {
        public MailMessage mailMessage { get; set; }
        public string ListToSend { get; set; }
    }
}
