﻿namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using System.Xml.Serialization;

    [XmlRoot("Publication")]
    public class Publication
    {
        [XmlElement("PublicationId")]
        public int? PublicationId { get; set; }

        [Required]
        [MaxLength(100)]
        [XmlElement("Title")]
        public string Title { get; set; }

        [Required]
        [XmlElement("InterestThemeId")]
        public int InterestThemeId { get; set; }

        [Required]
        [XmlElement("StartDate")]
        public DateTime StartDate { get; set; }

        [Required]
        [XmlElement("EndDate")]
        public DateTime EndDate { get; set; }

        [Required]
        [XmlElement("Description")]
        [MaxLength(250)]
        public string Description { get; set; }

        [XmlElement("FileLocation")]
        [MaxLength(250)]
        public string FileLocation { get; set; }

        [XmlIgnore]
        [IgnoreProperty]
        public HttpPostedFileWrapper SendFile { get; set; }

        [Required]
        [XmlElement("Body")]
        //[AllowHtml]
        public string Body { get; set; }

        [Required]
        [MaxLength(250)]
        [XmlElement("Keywords")]
        public string Keywords { get; set; }

        [Required]
        [XmlElement("Assessment")]
        public decimal Assessment { get; set; }

        [Required]
        [XmlElement("ViewsNumber")]
        public int ViewsNumber { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("PublicationAgeRanges")]
        public List<PublicationAgeRange> PublicationAgeRanges { get; set; } = new List<PublicationAgeRange>();

        [XmlElement("PublicationFiles")]
        public List<PublicationFile> PublicationFiles { get; set; } = new List<PublicationFile>();



        [XmlElement("InterestTheme")]
        [IgnoreProperty]
        public InterestTheme InterestTheme { get; set; }

        [IgnoreProperty]
        [XmlElement("PublicationAssessments")]
        public List<PublicationAssessment> PublicationAssessments { get; set; } = new List<PublicationAssessment>();

        [IgnoreProperty]
        [XmlIgnore]
        public List<HttpPostedFileWrapper> PublicationSendFiles { get; set; } = new List<HttpPostedFileWrapper>();



        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Publications";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetPublication";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordPublication";

        public Publication()
        {
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
        }
    }
}
