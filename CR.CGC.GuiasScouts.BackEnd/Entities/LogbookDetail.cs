﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("LogbookDetail")]
    public class LogbookDetail
    {
        [XmlElement("LogbookDetailId")]
        public int? LogbookDetailId { get; set; }

        [Required]
        [XmlElement("LogbookId")]
        public int LogbookId { get; set; }

        [MaxLength(6)]
        [Required]
        [XmlElement("ActionType")]
        public string ActionType { get; set; }

        [MaxLength(15)]
        [Required]
        [XmlElement("ActionSubType")]
        public string ActionSubType { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Entity")]
        public string Entity { get; set; }

        [XmlElement("OldData")]
        public ERContainer OldData { get; set; }

        [XmlElement("NewData")]
        public ERContainer NewData { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "History";
    }
}
