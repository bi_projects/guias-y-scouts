﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberLanguage")]
    public class MemberLanguage
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberLanguageId")]
        public int? MemberLanguageId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Idioma")]
        [DisplayForeignBinnacleProperty("Language", "Name", "Languages", "BaseCatalogId")]
        [Required]
        [XmlElement("LanguageId")]
        public int LanguageId { get; set; }

        [DisplayNameBinnacleProperty("Nivel de lectura")]
        [DisplayForeignBinnacleProperty("LanguageReading", "Name", "LanguageLevels", "BaseCatalogId")]
        [Required]
        [XmlElement("LanguageReadingId")]
        public int LanguageReadingId { get; set; }

        [DisplayNameBinnacleProperty("Nivel de escritura")]
        [DisplayForeignBinnacleProperty("LanguageWriting", "Name", "LanguageLevels", "BaseCatalogId")]
        [Required]
        [XmlElement("LanguageWritingId")]
        public int LanguageWritingId { get; set; }

        [DisplayNameBinnacleProperty("Nivel de conversación")]
        [DisplayForeignBinnacleProperty("LanguageConversation", "Name", "LanguageLevels", "BaseCatalogId")]
        [Required]
        [XmlElement("LanguageConversationId")]
        public int LanguageConversationId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Language")]
        public BaseCatalog Language { get; set; }

        [IgnoreProperty]
        [XmlElement("LanguageReading")]
        public BaseCatalog LanguageReading { get; set; }

        [IgnoreProperty]
        [XmlElement("LanguageWriting")]
        public BaseCatalog LanguageWriting { get; set; }

        [IgnoreProperty]
        [XmlElement("LanguageConversation")]
        public BaseCatalog LanguageConversation { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
