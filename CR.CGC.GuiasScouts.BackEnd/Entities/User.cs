﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Xml.Serialization;

    [XmlRoot("User")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [XmlElement("UserId")]
        public int UserId { get; set; }

        [XmlElement("RoleId")]
        public int? RoleId { get; set; }

        [MaxLength(20)]
        [Required]
        [XmlElement("UserName")]
        public string UserName { get; set; }

        [MaxLength(255)]
        [XmlElement("Email")]
        public string Email { get; set; }

        [MaxLength(12)]
        [Required]
        [XmlElement("Password")]
        public string Password { get; set; }

        [XmlElement("CurrentAccess")]
        public DateTime? CurrentAccess { get; set; }

        [XmlElement("CurrentIp")]
        [MaxLength(45)]
        public string CurrentIp { get; set; }

        [MaxLength(12)]
        [XmlElement("PasswordResetToken")]
        [Required]
        public string PasswordResetToken { get; set; }

        [XmlElement("DateLastAccess")]
        public DateTime? DateLastAccess { get; set; }

        [MaxLength(45)]
        [XmlElement("LastIp")]
        public string LastIp { get; set; }

        [MaxLength(50)]
        [XmlElement("Theme")]
        public string Theme { get; set; }

        [XmlElement("FontSize")]
        public int? FontSize { get; set; }

        [Required]
        [XmlElement("Active")]
        public bool Active { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("CreationDate")]
        public DateTime? CreationDate { get; set; }

        [XmlElement("ModifiedDate")]
        public DateTime? ModifiedDate { get; set; }

        [XmlElement("IsFirstLogin")]
        public bool IsFirstLogin { get; set; }

        [IgnoreProperty]
        [XmlElement("Role")]
        public Role Role { get; set; }

        /// <summary>
        /// Variable que contiene datos de cuando se solicita un reseteo de contrasena
        /// </summary>
        [XmlIgnore]
        public string Source { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "dbSecurity";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetUser";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordUser";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string LoginProcedure { get; set; } = "usp_VerifyUserIdentity";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string LogOutProcedure { get; set; } = "usp_LogOut";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string ValidatePasswordResetRequestProcedure { get; set; } = "usp_ValidatePasswordResetRequest";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string ResetPasswordProcedure { get; set; } = "usp_ResetPassword";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        [IgnoreProperty]
        public string ActivateProcedure { get; set; } = "usp_ActivateUser";

        [XmlIgnore]
        [JsonIgnoreSerialization]
        public string ChangePasswordProcedure { get; set; } = "usp_ChangePassword";
    }
}
