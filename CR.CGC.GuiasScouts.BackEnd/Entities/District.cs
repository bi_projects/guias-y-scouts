﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("District")]
    public class District
    {
        [XmlElement("DistrictId")]
        public int? DistrictId { get; set; }

        [Required]
        [XmlElement("CantonId")]
        public int CantonId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(6)]
        [Required]
        [XmlElement("PostalCode")]
        public string PostalCode { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("Canton")]
        public Canton Canton { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Location";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetDistrict";
    }
}
