﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("EventSectionInvitation")]
    public class EventSectionInvitation
    {
        [XmlElement("EventSectionInvitationId")]
        public int? EventSectionInvitationId { get; set; }

        [Required]
        [XmlElement("EventId")]
        public int EventId { get; set; }

        [Required]
        [XmlElement("SectionId")]
        public int SectionId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        //----------------------------------------------------

        [IgnoreProperty]
        [XmlElement("Section")]
        public Section Section { get; set; }

        //----------------------------------------------------

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Activities";
    }
}
