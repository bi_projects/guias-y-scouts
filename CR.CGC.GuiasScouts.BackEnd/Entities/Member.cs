﻿namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using System.Xml.Serialization;

    [XmlRoot("Member")]
    public class Member
    {
        [IgnoreBinnacleProperty]
        [XmlElement("MemberId")]
        public int? MemberId { get; set; }

        [DisplayNameBinnacleProperty("Nombres")]
        [MaxLength(100)]
        [Required]
        [XmlElement("Names")]
        public string Names { get; set; }

        [DisplayNameBinnacleProperty("Apellidos")]
        [MaxLength(100)]
        [Required]
        [XmlElement("Surnames")]
        public string Surnames { get; set; }

        [DisplayNameBinnacleProperty("Foto de perfil")]
        [ImageFileProperty]
        [MaxLength(250)]
        [XmlElement("Photo")]
        public string Photo { get; set; }

        [DisplayNameBinnacleProperty("Tipo de Identificación")]
        [DisplayForeignBinnacleProperty("Type", "Name", "IdentificationTypes", "BaseCatalogId")]
        [Required]
        [XmlElement("IDType")]
        public int IDType { get; set; }

        [DisplayNameBinnacleProperty("Número de Identificación")]
        [MaxLength(50)]
        [XmlElement("IDNumber")]
        public string IDNumber { get; set; }

        [DisplayNameBinnacleProperty("Nacionalidad")]
        [DisplayForeignBinnacleProperty("Nationality", "Name", "Nationalities", "BaseCatalogId")]
        [Required]
        [XmlElement("NationalityId")]
        public int NationalityId { get; set; }

        [DisplayNameBinnacleProperty("Fecha de nacimiento")]
        [Required]
        [XmlElement("BirthDate")]
        public DateTime BirthDate { get; set; }

        [DisplayNameBinnacleProperty("Género")]
        [DisplayForeignBinnacleProperty("Gender", "Name", "Genders", "BaseCatalogId")]
        [Required]
        [XmlElement("GenderId")]
        public int GenderId { get; set; }

        [DisplayNameBinnacleProperty("Fecha de inicio")]
        [XmlElement("StartDate")]
        public DateTime? StartDate { get; set; }

        [DisplayNameBinnacleProperty("Fecha de promesa")]
        [XmlElement("PromiseDate")]
        public DateTime? PromiseDate { get; set; }

        [DisplayNameBinnacleProperty("Correo electrónico")]
        [MaxLength(100)]
        [XmlElement("Email")]
        public string Email { get; set; }

        [XmlElement("UserId")]
        public int? UserId { get; set; }

        [DisplayNameBinnacleProperty("Es Guía o Scout")]
        [Required]
        [XmlElement("IsGorS")]
        public bool IsGorS { get; set; }

        [Required]
        [XmlElement("IsTutor")]
        public bool IsTutor { get; set; }

        [XmlElement("RegistrationDate")]
        public DateTime? RegistrationDate { get; set; }

        [DisplayNameBinnacleProperty("Estado de miembro")]
        [DisplayForeignBinnacleProperty("MemberStatus", "Name", "MemberStatuses", "MemberStatusId")]
        [Required]
        [XmlElement("MemberStatusId")]
        public int MemberStatusId { get; set; }

        [DisplayNameBinnacleProperty("Dirección")]
        [MaxLength(250)]
        [XmlElement("Address")]
        public string Address { get; set; }

        [DisplayNameBinnacleProperty("Distrito")]
        [DisplayForeignBinnacleProperty("District", "Name", "Districts", "DistrictId")]
        [Required]
        [XmlElement("DistrictId")]
        public int DistrictId { get; set; }

        [DisplayNameBinnacleProperty("Código postal")]
        [MaxLength(5)]
        [XmlElement("PostalCode")]
        public string PostalCode { get; set; }

        [DisplayNameBinnacleProperty("Religión")]
        [DisplayForeignBinnacleProperty("Religion", "Name", "Religions", "BaseCatalogId")]
        [XmlElement("ReligionId")]
        public int? ReligionId { get; set; }

        [DisplayNameBinnacleProperty("Nivel de educativo")]
        [DisplayForeignBinnacleProperty("EducationLevel", "Name", "EducationLevels", "BaseCatalogId")]
        [XmlElement("EducationLevelId")]
        public int? EducationLevelId { get; set; }

        [DisplayNameBinnacleProperty("Centro escolar")]
        [DisplayForeignBinnacleProperty("School", "Name", "Schools", "BaseCatalogId")]
        [XmlElement("SchoolId")]
        public int? SchoolId { get; set; }

        [DisplayNameBinnacleProperty("Profesión")]
        [DisplayForeignBinnacleProperty("Profession", "Name", "Professions", "BaseCatalogId")]
        [XmlElement("ProfessionId")]
        public int? ProfessionId { get; set; }

        [DisplayNameBinnacleProperty("Lugar de trabajo")]
        [MaxLength(100)]
        [XmlElement("Workplace")]
        public string Workplace { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlIgnore]
        [IgnoreProperty]
        public HttpPostedFileWrapper SendFile { get; set; }

        //----------------------------------------------------

        [IgnoreProperty]
        [XmlElement("Type")]
        public BaseCatalog Type { get; set; }

        [IgnoreProperty]
        [XmlElement("User")]
        public User User { get; set; }

        [IgnoreProperty]
        [XmlElement("MemberStatus")]
        public MemberStatus MemberStatus { get; set; }

        [IgnoreProperty]
        [XmlElement("Nationality")]
        public BaseCatalog Nationality { get; set; }

        [IgnoreProperty]
        [XmlElement("Gender")]
        public BaseCatalog Gender { get; set; }

        [IgnoreProperty]
        [XmlElement("District")]
        public District District { get; set; }

        [IgnoreProperty]
        [XmlElement("Religion")]
        public BaseCatalog Religion { get; set; }

        [IgnoreProperty]
        [XmlElement("EducationLevel")]
        public BaseCatalog EducationLevel { get; set; }

        [IgnoreProperty]
        [XmlElement("School")]
        public BaseCatalog School { get; set; }

        [IgnoreProperty]
        [XmlElement("Profession")]
        public BaseCatalog Profession { get; set; }

        //----------------------------------------------------

        [DisplayNameBinnacleProperty("Cargos")]
        [XmlElement("MemberPositions")]
        public List<MemberPosition> MemberPositions { get; set; } = new List<MemberPosition>();

        [DisplayNameBinnacleProperty("Parientes")]
        [XmlElement("MemberKinships")]
        public List<MemberKinship> MemberKinships { get; set; } = new List<MemberKinship>();

        [DisplayNameBinnacleProperty("Redes Sociales")]
        [XmlElement("MemberSocialNetworks")]
        public List<MemberSocialNetwork> MemberSocialNetworks { get; set; } = new List<MemberSocialNetwork>();

        [DisplayNameBinnacleProperty("Números telefónicos")]
        [XmlElement("MemberPhoneNumbers")]
        public List<MemberPhoneNumber> MemberPhoneNumbers { get; set; } = new List<MemberPhoneNumber>();

        [DisplayNameBinnacleProperty("Idiomas")]
        [XmlElement("MemberLanguages")]
        public List<MemberLanguage> MemberLanguages { get; set; } = new List<MemberLanguage>();

        [DisplayNameBinnacleProperty("Cursos")]
        [XmlElement("MemberCourses")]
        public List<MemberCourse> MemberCourses { get; set; } = new List<MemberCourse>();

        [DisplayNameBinnacleProperty("Talleres")]
        [XmlElement("MemberWorkshops")]
        public List<MemberWorkshop> MemberWorkshops { get; set; } = new List<MemberWorkshop>();

        [DisplayNameBinnacleProperty("Actividades")]
        [XmlElement("MemberActivities")]
        public List<MemberActivity> MemberActivities { get; set; } = new List<MemberActivity>();

        [DisplayNameBinnacleProperty("Evento nacional")]
        [XmlElement("MemberNationalEvents")]
        public List<MemberNationalEvent> MemberNationalEvents { get; set; } = new List<MemberNationalEvent>();

        [DisplayNameBinnacleProperty("Evento internacional")]
        [XmlElement("MemberInternationalEvents")]
        public List<MemberInternationalEvent> MemberInternationalEvents { get; set; } = new List<MemberInternationalEvent>();

        [DisplayNameBinnacleProperty("Otros estudios")]
        [XmlElement("MemberOtherStudies")]
        public List<MemberOtherStudy> MemberOtherStudies { get; set; } = new List<MemberOtherStudy>();

        [DisplayNameBinnacleProperty("Tutores")]
        [XmlElement("MemberTutors")]
        public List<MemberTutor> MemberTutors { get; set; } = new List<MemberTutor>();

        [DisplayNameBinnacleProperty("Reconocimientos")]
        [IgnoreProperty]
        [XmlElement("MemberRecognitions")]
        public List<MemberRecognition> MemberRecognitions { get; set; } = new List<MemberRecognition>();

        [DisplayNameBinnacleProperty("Etapas / Sección / Grupos")]
        [IgnoreProperty]
        [XmlElement("MemberGroups")]
        public List<MemberGroup> MemberGroups { get; set; } = new List<MemberGroup>();

        [DisplayNameBinnacleProperty("Evaluaciones")]
        [IgnoreProperty]
        [XmlElement("MemberCompetenceEvaluations")]
        public List<MemberCompetenceEvaluation> MemberCompetenceEvaluations { get; set; } = new List<MemberCompetenceEvaluation>();

        //----------------------------------------------------

        [JsonIgnoreSerialization]
        [IgnoreProperty]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        [IgnoreProperty]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetMember";

        [JsonIgnoreSerialization]
        [IgnoreProperty]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordMember";

        [JsonIgnoreSerialization]
        [IgnoreProperty]
        [XmlIgnore]
        public string GetListsForMembersProcedure { get; set; } = "usp_GetListsForMembers";

        [JsonIgnoreSerialization]
        [IgnoreProperty]
        [XmlIgnore]
        public string GetBirthdayOfTheDayProcedure { get; set; } = "usp_GetBirthdayOfTheDay";

        [JsonIgnoreSerialization]
        [IgnoreProperty]
        [XmlIgnore]
        public string GetDataGreetingMessageAPProcedure { get; set; } = "usp_GetDataGreetingMessageAP";

        [JsonIgnoreSerialization]
        [IgnoreProperty]
        [XmlIgnore]
        public string GetMemberByPermissionsProcedure { get; set; } = "usp_GetMemberByPermissions";


        public Member()
        {
            BirthDate = DateTime.Now.AddYears(-4);
            RegistrationDate = DateTime.Now;
        }
    }
}
