﻿namespace Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using System.Xml.Serialization;

    [XmlRoot("InterestTheme")]
    public class InterestTheme
    {
        [XmlElement("InterestThemeId")]
        public int? InterestThemeId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("FileLocation")]
        public string FileLocation { get; set; }

        [MaxLength(250)]
        [XmlElement("BackgroundFileLocation")]
        public string BackgroundFileLocation { get; set; }

        [MaxLength(250)]
        [Required]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlIgnore]
        [IgnoreProperty]
        public HttpPostedFileWrapper SendFile { get; set; }

        [XmlIgnore]
        [IgnoreProperty]
        public HttpPostedFileWrapper BackgroundSendFile { get; set; }

        [IgnoreProperty]
        [XmlElement("MemberInterestThemes")]
        public List<MemberInterestTheme> MemberInterestThemes { get; set; }

        [IgnoreProperty]
        [XmlElement("Publications")]
        public List<Publication> Publications { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Publications";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetInterestTheme";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordInterestTheme";
    }
}
