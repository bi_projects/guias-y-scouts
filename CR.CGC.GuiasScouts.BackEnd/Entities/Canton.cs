﻿namespace Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("Canton")]
    public class Canton
    {
        [XmlElement("CantonId")]
        public int? CantonId { get; set; }

        [Required]
        [XmlElement("ProvinceId")]
        public int ProvinceId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(6)]
        [Required]
        [XmlElement("PostalCode")]
        public string PostalCode { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("Province")]
        public Province Province { get; set; }

        [XmlElement("Districts")]
        public List<District> Districts { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Location";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetCanton";

        public Canton()
        {
            Districts = new List<District>();
        }
    }
}
