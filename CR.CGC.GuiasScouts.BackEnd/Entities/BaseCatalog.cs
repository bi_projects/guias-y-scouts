﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("BaseCatalog")]
    public class BaseCatalog
    {
        [XmlElement("BaseCatalogId")]
        public int? BaseCatalogId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [XmlElement("ItemOf")]
        public int? ItemOf { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Configuration";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordBaseCatalog";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetBaseCatalog";

        [IgnoreProperty]
        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedureItemsOf { get; set; } = "usp_GetItemsOf";

        [IgnoreProperty]
        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedureGetAnItem { get; set; } = "usp_GetAnItem";
    }
}
