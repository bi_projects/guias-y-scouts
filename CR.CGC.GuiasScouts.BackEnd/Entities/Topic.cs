﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("Topic")]
    public class Topic
    {
        [XmlElement("TopicId")]
        public int? TopicId { get; set; }

        [Required]
        [XmlElement("TypeId")]
        public int TypeId { get; set; }

        [Required]
        [MaxLength(100)]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("TopicType")]
        [IgnoreProperty]
        public BaseCatalog TopicType { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Requests";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetTopic";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordTopic";
    }
}
