﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberOtherStudy")]
    public class MemberOtherStudy
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberOtherStudyId")]
        public int? MemberOtherStudyId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Nombre del curso")]
        [Required]
        [MaxLength(100)]
        [XmlElement("StudyName")]
        public string StudyName { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
