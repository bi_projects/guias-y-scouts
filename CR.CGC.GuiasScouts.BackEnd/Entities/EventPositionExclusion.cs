﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("EventPositionExclusion")]
    public class EventPositionExclusion
    {
        [XmlElement("EventPositionExclusionId")]
        public int? EventPositionExclusionId { get; set; }

        [Required]
        [XmlElement("EventId")]
        public int EventId { get; set; }

        [Required]
        [XmlElement("PositionId")]
        public int PositionId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        //----------------------------------------------------

        [IgnoreProperty]
        [XmlElement("Position")]
        public Position Position { get; set; }

        //----------------------------------------------------

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Activities";
    }
}
