﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberWorkshop")]
    public class MemberWorkshop
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberWorkshopId")]
        public int? MemberWorkshopId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Taller")]
        [DisplayForeignBinnacleProperty("Workshop", "Name", "Workshops", "BaseCatalogId")]
        [Required]
        [XmlElement("WorkshopId")]
        public int WorkshopId { get; set; }

        [DisplayNameBinnacleProperty("Fecha del taller")]
        [Required]
        [XmlElement("WorkshopDate")]
        public DateTime WorkshopDate { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Workshop")]
        public BaseCatalog Workshop { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
