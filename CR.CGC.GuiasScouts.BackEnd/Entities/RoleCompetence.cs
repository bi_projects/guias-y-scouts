﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("RoleCompetence")]   
    public class RoleCompetence
    {
        [XmlElement("RoleCompetenceId")]
        public int? RoleCompetenceId { get; set; }

        [Required]
        [XmlElement("RoleId")]
        public int RoleId { get; set; }

        [Required]
        [XmlElement("CompetenceId")]
        public int CompetenceId { get; set; }

        [Required]
        [XmlElement("LevelId")]
        public int LevelId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("Competence")]
        public Competence Competence { get; set; }

        [XmlElement("CompetenceLevel")]
        public CompetenceLevel CompetenceLevel { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "dbSecurity";
    }
}
