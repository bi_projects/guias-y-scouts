﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("GroupEmail")]
    public class GroupEmail
    {
        [XmlElement("GroupEmailId")]
        public int? GroupEmailId { get; set; }

        [Required]
        [XmlElement("GroupId")]
        public int GroupId { get; set; }

        [MaxLength(100)]
        [XmlElement("Email")]
        [Required]
        public string Email { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
