﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("PositionType")]
    public class PositionType
    {
        [XmlElement("PositionTypeId")]
        public int? PositionTypeId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [Required]
        [XmlElement("CategoryId")]
        public int CategoryId { get; set; }

        [Required]
        [XmlElement("GenderId")]
        public int GenderId { get; set; }

        [Required]
        [XmlElement("AgeRangeId")]
        public int AgeRangeId { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("BaseCatalog")]
        [IgnoreProperty]
        public BaseCatalog BaseCatalog { get; set; }

        [XmlElement("Gender")]
        [IgnoreProperty]
        public BaseCatalog Gender { get; set; }

        [XmlElement("AgeRange")]
        [IgnoreProperty]
        public AgeRange AgeRange { get; set; }
        
        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Configuration";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetPositionType";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordPositionType";
    }
}
