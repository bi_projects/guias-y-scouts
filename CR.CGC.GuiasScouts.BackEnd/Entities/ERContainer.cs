﻿namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class JsonIgnoreSerializationAttribute : Attribute { }
    public class IgnorePropertyAttribute : Attribute { }

    public class AttachmentFilePropertyAttribute : Attribute { }
    public class ImageFilePropertyAttribute : Attribute { }
    public class IgnoreBinnaclePropertyAttribute : Attribute { }
    public class ComparatorFieldBinnaclePropertyAttribute : Attribute { }
    public class DisplayNameBinnaclePropertyAttribute : Attribute
    {
        public string DisplayName { get; set; }
        public DisplayNameBinnaclePropertyAttribute(string DisplayName)
        {
            this.DisplayName = DisplayName;
        }
    }
    public class DisplayForeignBinnaclePropertyAttribute : Attribute
    {
        public string DisplayObject { get; set; }
        public string DisplayField { get; set; }
        public string ContainerObject { get; set; }
        public string ConteinerObjectId { get; set; }

        public DisplayForeignBinnaclePropertyAttribute(string DisplayObject, string DisplayField, string ContainerObject, string ConteinerObjectId)
        {
            this.DisplayObject = DisplayObject;
            this.DisplayField = DisplayField;
            this.ContainerObject = ContainerObject;
            this.ConteinerObjectId = ConteinerObjectId;
        }
    }

    [XmlRoot("ERContainer")]
    public class ERContainer
    {
        [XmlElement("AgeRanges")]
        public List<AgeRange> AgeRanges { get; set; } = new List<AgeRange>();

        [XmlElement("BaseCatalogs")]
        public List<BaseCatalog> BaseCatalogs { get; set; } = new List<BaseCatalog>();

        [XmlElement("Competences")]
        public List<Competence> Competences { get; set; } = new List<Competence>();

        [XmlElement("CompetenceLevels")]
        public List<CompetenceLevel> CompetenceLevels { get; set; } = new List<CompetenceLevel>();

        [XmlElement("MemberStatuses")]
        public List<MemberStatus> MemberStatuses { get; set; } = new List<MemberStatus>();

        [XmlElement("RequestStatuses")]
        public List<RequestStatus> RequestStatuses { get; set; } = new List<RequestStatus>();

        [XmlElement("PositionTypes")]
        public List<PositionType> PositionTypes { get; set; } = new List<PositionType>();

        [XmlElement("SectionTypes")]
        public List<SectionType> SectionTypes { get; set; } = new List<SectionType>();

        [XmlElement("SystemStatuses")]
        public List<SystemStatus> SystemStatuses { get; set; } = new List<SystemStatus>();

        [XmlElement("Sections")]
        public List<Section> Sections { get; set; } = new List<Section>();

        [XmlElement("InterestThemes")]
        public List<InterestTheme> InterestThemes { get; set; } = new List<InterestTheme>();

        [XmlElement("Recognitions")]
        public List<Recognition> Recognitions { get; set; } = new List<Recognition>();

        [XmlElement("Progressions")]
        public List<Progression> Progressions { get; set; } = new List<Progression>();

        [XmlElement("Positions")]
        public List<Position> Positions { get; set; } = new List<Position>();

        [XmlElement("Roles")]
        public List<Role> Roles { get; set; } = new List<Role>();

        [XmlElement("Permissions")]
        public List<Permission> Permissions { get; set; } = new List<Permission>();

        [XmlElement("User")]
        public User User { get; set; } = new User();

        [XmlElement("Users")]
        public List<User> Users { get; set; } = new List<User>();

        [XmlElement("Faqs")]
        public List<Faq> Faqs { get; set; } = new List<Faq>();

        [XmlElement("Cantons")]
        public List<Canton> Cantons { get; set; } = new List<Canton>();

        [XmlElement("Districts")]
        public List<District> Districts { get; set; } = new List<District>();

        [XmlElement("Provinces")]
        public List<Province> Provinces { get; set; } = new List<Province>();

        [XmlElement("Members")]
        public List<Member> Members { get; set; } = new List<Member>();

        [XmlElement("MemberRecognitions")]
        public List<MemberRecognition> MemberRecognitions { get; set; } = new List<MemberRecognition>();

        [XmlElement("MemberGroups")]
        public List<MemberGroup> MemberGroups { get; set; } = new List<MemberGroup>();

        [XmlElement("MemberCompetenceEvaluations")]
        public List<MemberCompetenceEvaluation> MemberCompetenceEvaluations { get; set; } = new List<MemberCompetenceEvaluation>();

        [XmlElement("Groups")]
        public List<Group> Groups { get; set; } = new List<Group>();

        [XmlElement("Publications")]
        public List<Publication> Publications { get; set; } = new List<Publication>();

        [XmlElement("MemberInterestThemes")]
        public List<MemberInterestTheme> MemberInterestThemes { get; set; } = new List<MemberInterestTheme>();

        [XmlElement("PublicationAssessments")]
        public List<PublicationAssessment> PublicationAssessments { get; set; } = new List<PublicationAssessment>();

        [XmlElement("Complexities")]
        public List<Complexity> Complexities { get; set; } = new List<Complexity>();

        [XmlElement("Priorities")]
        public List<Priority> Priorities { get; set; } = new List<Priority>();

        [XmlElement("Topics")]
        public List<Topic> Topics { get; set; } = new List<Topic>();

        [XmlElement("Requests")]
        public List<Request> Requests { get; set; } = new List<Request>();

        [XmlElement("RequestComments")]
        public List<RequestComment> RequestComments { get; set; } = new List<RequestComment>();

        [XmlElement("Templates")]
        public List<Template> Templates { get; set; } = new List<Template>();

        [XmlElement("EventStatuses")]
        public List<EventStatus> EventStatuses { get; set; } = new List<EventStatus>();

        [XmlElement("Events")]
        public List<Event> Events { get; set; } = new List<Event>();

        [XmlElement("MemberEvents")]
        public List<MemberEvent> MemberEvents { get; set; } = new List<MemberEvent>();

        [XmlElement("EventAssessments")]
        public List<EventAssessment> EventAssessments { get; set; } = new List<EventAssessment>();

        [XmlElement("EventInvitations")]
        public List<EventInvitationContainer> EventInvitationContainers { get; set; } = new List<EventInvitationContainer>();

        [XmlElement("AlertTypes")]
        public List<AlertType> AlertTypes { get; set; } = new List<AlertType>();

        [XmlElement("MemberAlertSettings")]
        public List<MemberAlertSetting> MemberAlertSettings { get; set; } = new List<MemberAlertSetting>();

        [XmlElement("MemberAlerts")]
        public List<MemberAlert> MemberAlerts { get; set; } = new List<MemberAlert>();

        [XmlElement("Logbooks")]
        public List<Logbook> Logbooks { get; set; } = new List<Logbook>();

        //List for members
        #region List for members

        [XmlElement("Genders")]
        public List<BaseCatalog> Genders { get; set; } = new List<BaseCatalog>();

        [XmlElement("Kinships")]
        public List<BaseCatalog> Kinships { get; set; } = new List<BaseCatalog>();

        [XmlElement("LanguageLevels")]
        public List<BaseCatalog> LanguageLevels { get; set; } = new List<BaseCatalog>();

        [XmlElement("EducationLevels")]
        public List<BaseCatalog> EducationLevels { get; set; } = new List<BaseCatalog>();

        [XmlElement("ActivityTypes")]
        public List<BaseCatalog> ActivityTypes { get; set; } = new List<BaseCatalog>();

        [XmlElement("IdentificationTypes")]
        public List<BaseCatalog> IdentificationTypes { get; set; } = new List<BaseCatalog>();

        [XmlElement("SocialNetworks")]
        public List<BaseCatalog> SocialNetworks { get; set; } = new List<BaseCatalog>();

        [XmlElement("Nationalities")]
        public List<BaseCatalog> Nationalities { get; set; } = new List<BaseCatalog>();

        [XmlElement("Religions")]
        public List<BaseCatalog> Religions { get; set; } = new List<BaseCatalog>();

        [XmlElement("Schools")]
        public List<BaseCatalog> Schools { get; set; } = new List<BaseCatalog>();

        [XmlElement("Languages")]
        public List<BaseCatalog> Languages { get; set; } = new List<BaseCatalog>();

        [XmlElement("NationalEvents")]
        public List<BaseCatalog> NationalEvents { get; set; } = new List<BaseCatalog>();

        [XmlElement("InternationalEvents")]
        public List<BaseCatalog> InternationalEvents { get; set; } = new List<BaseCatalog>();

        [XmlElement("Courses")]
        public List<BaseCatalog> Courses { get; set; } = new List<BaseCatalog>();

        [XmlElement("Workshops")]
        public List<BaseCatalog> Workshops { get; set; } = new List<BaseCatalog>();

        [XmlElement("Activities")]
        public List<BaseCatalog> Activities { get; set; } = new List<BaseCatalog>();

        [XmlElement("Professions")]
        public List<BaseCatalog> Professions { get; set; } = new List<BaseCatalog>();

        #endregion

        //util properties
        [XmlIgnore]
        public string Model { get; set; }

        [XmlIgnore]
        public int? CurrentUser { get; set; }

        //pagination properties
        [XmlIgnore]
        public string SearchText { get; set; }

        [XmlIgnore]
        public int RowCount { get; set; }

        [XmlIgnore]
        public int PageNumber { get; set; }

        [XmlIgnore]
        public int PageSize { get; set; }

    }
}