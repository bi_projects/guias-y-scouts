﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("SectionType")]
    public class SectionType
    {
        [XmlElement("SectionTypeId")]
        public int? SectionTypeId { get; set; }

        [MaxLength(100)]
        [XmlElement("Name")]
        [Required]
        public string Name { get; set; }

        [Required]
        [XmlElement("AgeRangeId")]
        public int AgeRangeId { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("AgeRange")]
        [IgnoreProperty]
        public AgeRange AgeRange { get; set; }
        
        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Configuration";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetSectionType";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordSectionType";
    }
}
