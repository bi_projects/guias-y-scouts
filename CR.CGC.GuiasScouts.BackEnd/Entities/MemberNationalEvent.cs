﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberNationalEvent")]
    public class MemberNationalEvent
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberNationalEventId")]
        public int? MemberNationalEventId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Evento")]
        [DisplayForeignBinnacleProperty("NationalEvent", "Name", "NationalEvents", "BaseCatalogId")]
        [Required]
        [XmlElement("NationalEventId")]
        public int NationalEventId { get; set; }

        [DisplayNameBinnacleProperty("Fecha del evento")]
        [Required]
        [XmlElement("NationalEventDate")]
        public DateTime NationalEventDate { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("NationalEvent")]
        public BaseCatalog NationalEvent { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
