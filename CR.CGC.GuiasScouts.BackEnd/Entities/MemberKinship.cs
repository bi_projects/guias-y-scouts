﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberKinship")]
    public class MemberKinship
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberKinshipId")]
        public int? MemberKinshipId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Parentesco")]
        [DisplayForeignBinnacleProperty("Kinship", "Name", "Kinships", "BaseCatalogId")]
        [Required]
        [XmlElement("KinshipId")]
        public int KinshipId { get; set; }

        [DisplayNameBinnacleProperty("Pariente")]
        [DisplayForeignBinnacleProperty("Kin", "Names", "Members", "MemberId")]
        [Required]
        [XmlElement("KinId")]
        public int KinId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Kinship")]
        public BaseCatalog Kinship { get; set; }

        [IgnoreProperty]
        [XmlElement("Kin")]
        public Member Kin { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
