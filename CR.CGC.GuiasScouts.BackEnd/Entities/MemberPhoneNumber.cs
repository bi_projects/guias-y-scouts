﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberPhoneNumber")]
    public class MemberPhoneNumber
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberPhoneNumberId")]
        public int? MemberPhoneNumberId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Teléfono")]
        [MaxLength(15)]
        [Required]
        [XmlElement("PhoneNumber")]
        public string PhoneNumber { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
