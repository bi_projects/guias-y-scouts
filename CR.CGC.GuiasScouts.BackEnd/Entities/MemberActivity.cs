﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberActivity")]
    public class MemberActivity
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberActivityId")]
        public int? MemberActivityId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Actividad")]
        [DisplayForeignBinnacleProperty("Activity", "Name", "Activities", "BaseCatalogId")]
        [Required]
        [XmlElement("ActivityId")]
        public int ActivityId { get; set; }

        [DisplayNameBinnacleProperty("Fecha de la actividad")]
        [Required]
        [XmlElement("ActivityDate")]
        public DateTime ActivityDate { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Activity")]
        public BaseCatalog Activity { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
