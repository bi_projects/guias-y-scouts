﻿namespace Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("Province")]
    public class Province
    {
        [XmlElement("ProvinceId")]
        public int? ProvinceId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(6)]
        [Required]
        [XmlElement("PostalCode")]
        public string PostalCode { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("Cantons")]
        public List<Canton> Cantons { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Location";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetProvince";

        public Province()
        {
            Cantons = new List<Canton>();
        }
    }
}
