﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberRecognition")]
    public class MemberRecognition
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberRecognitionId")]
        public int? MemberRecognitionId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Reconocimiento")]
        [DisplayForeignBinnacleProperty("Recognition", "Name", "MemberRecognitions", "RecognitionId")]
        [Required]
        [XmlElement("RecognitionId")]
        public int RecognitionId { get; set; }

        [DisplayNameBinnacleProperty("Fecha de asignación")]
        [Required]
        [XmlElement("DateAssignment")]
        public DateTime DateAssignment { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Recognition")]
        public Recognition Recognition { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetMemberRecognition";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordMemberRecognition";
    }
}
