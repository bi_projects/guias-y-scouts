﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberInternationalEvent")]
    public class MemberInternationalEvent
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberInternationalEventId")]
        public int? MemberInternationalEventId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Evento")]
        [DisplayForeignBinnacleProperty("InternationalEvent", "Name", "InternationalEvents", "BaseCatalogId")]
        [Required]
        [XmlElement("InternationalEventId")]
        public int InternationalEventId { get; set; }

        [DisplayNameBinnacleProperty("Fecha del evento")]
        [Required]
        [XmlElement("InternationalEventDate")]
        public DateTime InternationalEventDate { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("InternationalEvent")]
        public BaseCatalog InternationalEvent { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
