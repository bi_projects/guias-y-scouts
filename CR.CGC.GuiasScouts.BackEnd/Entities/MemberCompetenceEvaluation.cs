﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberCompetenceEvaluation")]
    public class MemberCompetenceEvaluation
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberCompetenceEvaluationId")]
        public int? MemberCompetenceEvaluationId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Competencia")]
        [DisplayForeignBinnacleProperty("Competence", "Name", "Competences", "CompetenceId")]
        [Required]
        [XmlElement("CompetenceId")]
        public int CompetenceId { get; set; }

        [DisplayNameBinnacleProperty("Nivel")]
        [DisplayForeignBinnacleProperty("CompetenceLevel", "Level", "CompetenceLevels", "LevelId")]
        [Required]
        [XmlElement("LevelId")]
        public int LevelId { get; set; }

        [DisplayNameBinnacleProperty("Fecha de evaluación")]
        [Required]
        [XmlElement("EvaluationDate")]
        public DateTime EvaluationDate { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Competence")]
        public Competence Competence { get; set; }

        [IgnoreProperty]
        [XmlElement("CompetenceLevel")]
        public CompetenceLevel CompetenceLevel { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetMemberCompetenceEvaluation";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordMemberCompetenceEvaluation";
    }
}
