﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("AgeRange")]
    public class AgeRange
    {
        [XmlElement("AgeRangeId")]
        public int? AgeRangeId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("MinimumAge")]
        public int MinimumAge { get; set; }

        [Required]
        [XmlElement("MaximumAge")]
        public int MaximumAge { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Configuration";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetAgeRange";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordAgeRange";

        public AgeRange()
        {
            MinimumAge = 6;
            MaximumAge = 6;
        }
    }
}
