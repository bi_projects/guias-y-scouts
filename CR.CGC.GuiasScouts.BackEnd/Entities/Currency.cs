﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("Currency")]
    public class Currency
    {
        [XmlElement("CurrencyId")]
        public int? CurrencyId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(2)]
        [Required]
        [XmlElement("Symbol")]
        public string Symbol { get; set; }

        [MaxLength(3)]
        [Required]
        [XmlElement("ISOCode")]
        public string ISOCode { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Configuration";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetCurrency";
    }
}
