﻿namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlRoot("EventInvitationContainer")]
    public class EventInvitationContainer
    {
        [XmlElement("EventId")]
        public int? EventId { get; set; }

        [IgnoreProperty]
        public bool Send { get; set; }

        #region NotUsed

        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("EventImage")]
        public string EventImage { get; set; }

        [XmlElement("Description")]
        public string Description { get; set; }

        [XmlElement("StartDateTime")]
        public DateTime StartDateTime { get; set; }

        [XmlElement("EndDateTime")]
        public DateTime EndDateTime { get; set; }

        [XmlElement("QuotaLimit")]
        public int QuotaLimit { get; set; }

        [XmlElement("ScopeId")]
        public int ScopeId { get; set; }

        [XmlElement("ActivityTypeId")]
        public int ActivityTypeId { get; set; }

        [XmlElement("CurrencyId")]
        public int CurrencyId { get; set; }

        [XmlElement("Cost")]
        public decimal Cost { get; set; }

        [XmlElement("ItIsFundable")]
        public bool ItIsFundable { get; set; }

        [XmlElement("InscriptionStart")]
        public DateTime InscriptionStart { get; set; }

        [XmlElement("InscriptionEnd")]
        public DateTime InscriptionEnd { get; set; }

        [XmlElement("Requirements")]
        public string Requirements { get; set; }

        [XmlElement("Place")]
        public string Place { get; set; }

        [XmlElement("Address")]
        public string Address { get; set; }

        [XmlElement("Longitude")]
        public decimal? Longitude { get; set; }

        [XmlElement("Latitude")]
        public decimal? Latitude { get; set; }

        [XmlElement("TemplateId")]
        public int? TemplateId { get; set; }

        [XmlElement("TemplateCode")]
        public string TemplateCode { get; set; }

        [XmlElement("EmailBody")]
        public string EmailBody { get; set; }

        [XmlElement("EventStatusId")]
        public int EventStatusId { get; set; }

        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        //----------------------------------------------------

        [IgnoreProperty]
        [XmlElement("Scope")]
        public BaseCatalog Scope { get; set; }

        [IgnoreProperty]
        [XmlElement("ActivityType")]
        public BaseCatalog ActivityType { get; set; }

        [IgnoreProperty]
        [XmlElement("Template")]
        public Template Template { get; set; }

        [IgnoreProperty]
        [XmlElement("EventStatus")]
        public EventStatus EventStatus { get; set; }

        [IgnoreProperty]
        [XmlElement("EventAgeRanges")]
        public List<EventAgeRange> EventAgeRanges { get; set; } = new List<EventAgeRange>();

        [IgnoreProperty]
        [XmlElement("MemberEvents")]
        public List<MemberEvent> MemberEvents { get; set; } = new List<MemberEvent>();

        [IgnoreProperty]
        [XmlElement("EventAssessments")]
        public List<EventAssessment> EventAssessments { get; set; } = new List<EventAssessment>();

        [IgnoreProperty]
        [XmlElement("EventRoles")]
        public List<EventRole> EventRoles { get; set; } = new List<EventRole>();

        #endregion

        //----------------------------------------------------

        [XmlElement("EventMemberInvitations")]
        public List<EventMemberInvitation> EventMemberInvitations { get; set; } = new List<EventMemberInvitation>();

        [XmlElement("EventGroupInvitations")]
        public List<EventGroupInvitation> EventGroupInvitations { get; set; } = new List<EventGroupInvitation>();

        [XmlElement("EventSectionInvitations")]
        public List<EventSectionInvitation> EventSectionInvitations { get; set; } = new List<EventSectionInvitation>();

        [XmlElement("EventPositionInvitations")]
        public List<EventPositionInvitation> EventPositionInvitations { get; set; } = new List<EventPositionInvitation>();

        //----------------------------------------------------

        [XmlElement("EventMemberExclusions")]
        public List<EventMemberExclusion> EventMemberExclusions { get; set; } = new List<EventMemberExclusion>();

        [XmlElement("EventGroupExclusions")]
        public List<EventGroupExclusion> EventGroupExclusions { get; set; } = new List<EventGroupExclusion>();

        [XmlElement("EventSectionExclusions")]
        public List<EventSectionExclusion> EventSectionExclusions { get; set; } = new List<EventSectionExclusion>();

        [XmlElement("EventPositionExclusions")]
        public List<EventPositionExclusion> EventPositionExclusions { get; set; } = new List<EventPositionExclusion>();

        //----------------------------------------------------

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Activities";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetEventInvitation";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordEventInvitation";

        public EventInvitationContainer()
        {
            StartDateTime = DateTime.Now;
            EndDateTime = DateTime.Now;
            InscriptionStart = DateTime.Now;
            InscriptionEnd = DateTime.Now;
        }
    }
}
