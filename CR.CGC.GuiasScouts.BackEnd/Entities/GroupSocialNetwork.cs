﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("GroupSocialNetwork")]
    public class GroupSocialNetwork
    {
        [XmlElement("GroupSocialNetworkId")]
        public int? GroupSocialNetworkId { get; set; }

        [Required]
        [XmlElement("GroupId")]
        public int GroupId { get; set; }

        [Required]
        [XmlElement("Type")]
        public int Type { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Nickname")]
        public string Nickname { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("BaseCatalog")]
        public BaseCatalog BaseCatalog { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
