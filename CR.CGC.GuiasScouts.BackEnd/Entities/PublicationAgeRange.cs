﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("PublicationAgeRange")]
    public class PublicationAgeRange
    {
        [XmlElement("PublicationAgeRangeId")]
        public int? PublicationAgeRangeId { get; set; }

        [Required]
        [XmlElement("PublicationId")]
        public int PublicationId { get; set; }

        [Required]
        [XmlElement("AgeRangeId")]
        public int AgeRangeId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("AgeRange")]
        [IgnoreProperty]
        public AgeRange AgeRange { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Publications";
    }
}
