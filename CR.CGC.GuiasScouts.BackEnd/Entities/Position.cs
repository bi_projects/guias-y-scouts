﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("Position")]
    public class Position
    {
        [XmlElement("PositionId")]
        public int? PositionId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("Quantity")]
        public int? Quantity { get; set; }

        [Required]
        [XmlElement("PositionTypeId")]
        public int PositionTypeId { get; set; }

        [MaxLength(250)]
        [Required]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("PositionType")]
        [IgnoreProperty]
        public PositionType PositionType { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetPosition";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordPosition";
    }
}
