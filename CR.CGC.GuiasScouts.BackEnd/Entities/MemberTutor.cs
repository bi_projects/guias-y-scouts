﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberTutor")]
    public class MemberTutor
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberTutorId")]
        public int? MemberTutorId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Tutor")]
        [DisplayForeignBinnacleProperty("Tutor", "Names", "Members", "MemberId")]
        [Required]
        [XmlElement("TutorId")]
        public int TutorId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Tutor")]
        public Member Tutor { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
