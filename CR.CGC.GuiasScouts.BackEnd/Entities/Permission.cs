﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("Permission")]
    public class Permission
    {
        [XmlElement("PermissionId")]
        public int? PermissionId { get; set; }

        [XmlElement("RoleId")]
        [Required]
        public int RoleId { get; set; }

        [MaxLength(100)]
        [XmlElement("Controller")]
        [Required]
        public string Controller { get; set; }

        [MaxLength(100)]
        [XmlElement("Action")]
        [Required]
        public string Action { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        [Required]
        public string Description { get; set; }

        [IgnoreProperty]
        [XmlIgnore]
        public bool ToSave { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "dbSecurity";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetPermission";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordPermission";
    }
}
