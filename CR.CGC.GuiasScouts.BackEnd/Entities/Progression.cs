﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using System.Xml.Serialization;

    [XmlRoot("Progression")]
    public class Progression
    {
        [XmlElement("ProgressionId")]
        public int? ProgressionId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("FileLocation")]
        public string FileLocation { get; set; }

        [Required]
        [XmlElement("SectionId")]
        public int SectionId { get; set; }

        [Required]
        [XmlElement("SystemStatusId")]
        public int SystemStatusId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlIgnore]
        [IgnoreProperty]
        public HttpPostedFileWrapper SendFile { get; set; }

        [XmlElement("Section")]
        [IgnoreProperty]
        public Section Section { get; set; }

        [XmlElement("SystemStatus")]
        [IgnoreProperty]
        public SystemStatus SystemStatus { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetProgression";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordProgression";
    }
}
