﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using System.Xml.Serialization;

    [XmlRoot("Section")]
    public class Section
    {
        [XmlElement("SectionId")]
        public int? SectionId { get; set; }

        [MaxLength(100)]
        [XmlElement("Name")]
        [Required]
        public string Name { get; set; }

        [Required]
        [XmlElement("SectionTypeId")]
        public int SectionTypeId { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [MaxLength(250)]
        [XmlElement("FileLocation")]
        public string FileLocation { get; set; }

        [Required]
        [XmlElement("SystemStatusId")]
        public int SystemStatusId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlIgnore]
        [IgnoreProperty]
        public HttpPostedFileWrapper SendFile { get; set; }

        [XmlElement("SectionType")]
        [IgnoreProperty]
        public SectionType SectionType { get; set; }

        [XmlElement("SystemStatus")]
        [IgnoreProperty]
        public SystemStatus SystemStatus { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetSection";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordSection";
    }
}
