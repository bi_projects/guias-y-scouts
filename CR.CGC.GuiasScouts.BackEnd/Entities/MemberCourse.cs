﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberCourse")]
    public class MemberCourse
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberCourseId")]
        public int? MemberCourseId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Curso")]
        [DisplayForeignBinnacleProperty("Course", "Name", "Courses", "BaseCatalogId")]
        [Required]
        [XmlElement("CourseId")]
        public int CourseId { get; set; }

        [DisplayNameBinnacleProperty("Fecha del curso")]
        [Required]
        [XmlElement("CourseDate")]
        public DateTime CourseDate { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Course")]
        public BaseCatalog Course { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
