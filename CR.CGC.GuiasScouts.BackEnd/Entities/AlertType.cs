﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("AlertType")]
    public class AlertType
    {
        [XmlElement("AlertTypeId")]
        public int? AlertTypeId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("Other")]
        public bool Other { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Configuration";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordAlertType";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetAlertType";
    }
}
