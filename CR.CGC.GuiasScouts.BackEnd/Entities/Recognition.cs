﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using System.Xml.Serialization;

    [XmlRoot("Recognition")]
    public class Recognition
    {
        [XmlElement("RecognitionId")]
        public int? RecognitionId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [Required]
        [XmlElement("Description")]
        public string Description { get; set; }

        [MaxLength(250)]
        [XmlElement("FileLocation")]
        public string FileLocation { get; set; }

        [Required]
        [XmlElement("SystemStatusId")]
        public int SystemStatusId { get; set; }

        [XmlElement("Enabled")]
        [Required]
        public bool Enabled { get; set; }

        [XmlIgnore]
        [IgnoreProperty]
        public HttpPostedFileWrapper SendFile { get; set; }

        [XmlElement("SystemStatus")]
        [IgnoreProperty]
        public SystemStatus SystemStatus { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetRecognition";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordRecognition";
    }
}
