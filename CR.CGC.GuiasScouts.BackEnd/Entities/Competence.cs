﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("Competence")]
    public class Competence
    {
        [XmlElement("CompetenceId")]
        public int? CompetenceId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [Required]
        [XmlElement("Type")]
        public int Type { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("BaseCatalog")]
        [IgnoreProperty]
        public BaseCatalog BaseCatalog { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Configuration";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetCompetence";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordCompetence";
    }
}
