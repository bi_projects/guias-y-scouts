﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberSocialNetwork")]
    public class MemberSocialNetwork
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberSocialNetworkId")]
        public int? MemberSocialNetworkId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Red Social")]
        [DisplayForeignBinnacleProperty("Type", "Name", "SocialNetworks", "BaseCatalogId")]
        [Required]
        [XmlElement("TypeId")]
        public int TypeId { get; set; }

        [DisplayNameBinnacleProperty("Nombre de Usuario")]
        [MaxLength(100)]
        [Required]
        [XmlElement("Nickname")]
        public string Nickname { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Type")]
        public BaseCatalog Type { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
