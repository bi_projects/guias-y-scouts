﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("Faq")]
    public class Faq
    {
        [XmlElement("FaqId")]
        public int? FaqId { get; set; }

        [Required]
        [XmlElement("Type")]
        public int Type { get; set; }

        [MaxLength(250)]
        [Required]
        [XmlElement("Question")]
        public string Question { get; set; }

        [MaxLength(500)]
        [Required]
        [XmlElement("Answer")]
        public string Answer { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("BaseCatalog")]
        [IgnoreProperty]
        public BaseCatalog BaseCatalog { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Help";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetFaq";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordFaq";
    }
}
