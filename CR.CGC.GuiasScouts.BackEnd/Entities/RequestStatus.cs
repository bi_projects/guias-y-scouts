﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("RequestStatus")]
    public class RequestStatus
    {
        [XmlElement("RequestStatusId")]
        public int? RequestStatusId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [MaxLength(7)]
        [Required]
        [XmlElement("Color")]
        public string Color { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Configuration";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetRequestStatus";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordRequestStatus";
    }
}
