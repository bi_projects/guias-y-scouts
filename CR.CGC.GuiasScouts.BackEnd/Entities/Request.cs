﻿namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using System.Xml.Serialization;

    [XmlRoot("Request")]
    public class Request : IDisposable
    {
        [IgnoreBinnacleProperty]
        [XmlElement("RequestId")]
        public int? RequestId { get; set; }

        [DisplayNameBinnacleProperty("Temática")]
        [DisplayForeignBinnacleProperty("Topic", "Name", "Topics", "TopicId")]
        [Required]
        [XmlElement("TopicId")]
        public int TopicId { get; set; }

        [DisplayNameBinnacleProperty("Nombre / Título")]
        [Required]
        [MaxLength(100)]
        [XmlElement("Name")]
        public string Name { get; set; }

        [DisplayNameBinnacleProperty("Descripción")]
        [DataType(DataType.MultilineText)]
        [Required]
        [MaxLength(2000)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("CreationDate")]
        public DateTime CreationDate { get; set; }

        [DisplayNameBinnacleProperty("Fecha de atención")]
        [XmlElement("AttentionDate")]
        public DateTime? AttentionDate { get; set; }

        [DisplayNameBinnacleProperty("Prioridad")]
        [DisplayForeignBinnacleProperty("Priority", "Name", "Priorities", "PriorityId")]
        [Required]
        [XmlElement("PriorityId")]
        public int PriorityId { get; set; }

        [DisplayNameBinnacleProperty("Complejidad")]
        [DisplayForeignBinnacleProperty("Complexity", "Name", "Complexities", "ComplexityId")]
        [XmlElement("ComplexityId")]
        public int? ComplexityId { get; set; }

        [Required]
        [XmlElement("RequestingUser")]
        public int RequestingUser { get; set; }

        [DisplayNameBinnacleProperty("Asignado a")]
        [DisplayForeignBinnacleProperty("Assigned", "Names", "Members", "UserId")]
        [XmlElement("AssignedTo")]
        public int? AssignedTo { get; set; }

        [DisplayNameBinnacleProperty("Estado")]
        [DisplayForeignBinnacleProperty("RequestStatus", "Name", "RequestStatuses", "RequestStatusId")]
        [Required]
        [XmlElement("RequestStatusId")]
        public int RequestStatusId { get; set; }

        [XmlElement("Progress")]
        public int Progress { get; set; }

        [XmlElement("Qualification")]
        public int Qualification { get; set; }

        [MaxLength(1000)]
        [XmlElement("QualifyingComment")]
        public string QualifyingComment { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [DisplayNameBinnacleProperty("Archivos Adjuntos")]
        [XmlElement("RequestFiles")]
        public List<RequestFile> RequestFiles { get; set; } = new List<RequestFile>();


        [IgnoreProperty]
        [XmlElement("Topic")]
        public Topic Topic { get; set; }

        [IgnoreProperty]
        [XmlElement("Priority")]
        public Priority Priority { get; set; }

        [IgnoreProperty]
        [XmlElement("Complexity")]
        public Complexity Complexity { get; set; }

        [IgnoreProperty]
        [XmlElement("Assigned")]
        public Member Assigned { get; set; }

        [IgnoreProperty]
        [XmlElement("Requesting")]
        public Member Requesting { get; set; }

        [IgnoreProperty]
        [XmlElement("RequestStatus")]
        public RequestStatus RequestStatus { get; set; }

        [DisplayNameBinnacleProperty("Comentarios")]
        [IgnoreProperty]
        [XmlElement("RequestComments")]
        public List<RequestComment> RequestComments { get; set; } = new List<RequestComment>();


        [IgnoreProperty]
        [XmlIgnore]
        public List<HttpPostedFileWrapper> SendFiles { get; set; } = new List<HttpPostedFileWrapper>();



        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Requests";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetRequest";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordRequest";

        [JsonIgnoreSerialization]
        [IgnoreProperty]
        [XmlIgnore]
        public string GetListsForRequestsProcedure { get; set; } = "usp_GetListsForRequest";

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
