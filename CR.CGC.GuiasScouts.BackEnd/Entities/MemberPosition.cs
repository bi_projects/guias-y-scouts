﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberPosition")]
    public class MemberPosition
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("MemberPositionId")]
        public int? MemberPositionId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [DisplayNameBinnacleProperty("Cargo")]
        [DisplayForeignBinnacleProperty("Position", "Name", "Positions", "PositionId")]
        [Required]
        [XmlElement("PositionId")]
        public int PositionId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Position")]
        public Position Position { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";
    }
}
