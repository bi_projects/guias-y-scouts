﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("EventGroupInvitation")]
    public class EventGroupInvitation
    {
        [XmlElement("EventGroupInvitationId")]
        public int? EventGroupInvitationId { get; set; }

        [Required]
        [XmlElement("EventId")]
        public int EventId { get; set; }

        [Required]
        [XmlElement("GroupId")]
        public int GroupId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        //----------------------------------------------------

        [IgnoreProperty]
        [XmlElement("Group")]
        public Group Group { get; set; }

        //----------------------------------------------------

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Activities";
    }
}
