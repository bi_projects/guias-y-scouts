﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("MemberAlertSetting")]
    public class MemberAlertSetting
    {
        [XmlElement("MemberAlertSettingId")]
        public int? MemberAlertSettingId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [Required]
        [XmlElement("AlertTypeId")]
        public int AlertTypeId { get; set; }

        [Required]
        [XmlElement("Sms")]
        public bool Sms { get; set; }

        [Required]
        [XmlElement("Email")]
        public bool Email { get; set; }

        [Required]
        [XmlElement("Portal")]
        public bool Portal { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("AlertType")]
        public AlertType AlertType { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordMemberAlertSetting";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetMemberAlertSetting";
    }
}
