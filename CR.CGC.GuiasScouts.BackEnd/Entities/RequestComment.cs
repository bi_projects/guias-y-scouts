﻿namespace Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("RequestComment")]
    public class RequestComment
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("RequestCommentId")]
        public int? RequestCommentId { get; set; }

        [Required]
        [XmlElement("RequestId")]
        public int RequestId { get; set; }

        [DisplayNameBinnacleProperty("Comentario")]
        [Required]
        [MaxLength(1000)]
        [XmlElement("Comment")]
        public string Comment { get; set; }

        [Required]
        [XmlElement("Commentator")]
        public int Commentator { get; set; }

        [DisplayNameBinnacleProperty("Fecha de creación")]
        [Required]
        [XmlElement("DateTimeComment")]
        public DateTime DateTimeComment { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Requests";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetRequestComment";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordRequestComment";
    }
}
