﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("PublicationAssessment")]
    public class PublicationAssessment
    {
        [XmlElement("PublicationAssessmentId")]
        public int? PublicationAssessmentId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [Required]
        [XmlElement("PublicationId")]
        public int PublicationId { get; set; }

        [Required]
        [XmlElement("Assessment")]
        public decimal Assessment { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Publications";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetPublicationAssessment";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordPublicationAssessment";
    }
}
