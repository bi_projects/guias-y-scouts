﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("EventMemberInvitation")]
    public class EventMemberInvitation
    {
        [XmlElement("EventMemberInvitationId")]
        public int? EventMemberInvitationId { get; set; }

        [Required]
        [XmlElement("EventId")]
        public int EventId { get; set; }

        [Required]
        [XmlElement("MemberId")]
        public int MemberId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        //----------------------------------------------------

        [IgnoreProperty]
        [XmlElement("Member")]
        public Member Member { get; set; }

        //----------------------------------------------------

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Activities";
    }
}
