﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("EventAgeRange")]
    public class EventAgeRange
    {
        [XmlElement("EventAgeRangeId")]
        public int? EventAgeRangeId { get; set; }

        [Required]
        [XmlElement("EventId")]
        public int EventId { get; set; }

        [Required]
        [XmlElement("AgeRangeId")]
        public int AgeRangeId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("AgeRange")]
        [IgnoreProperty]
        public AgeRange AgeRange { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Configuration";
    }
}
