﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;

    public class UpdPassModel
    {
        [MaxLength(255)]
        public string Email { get; set; }

        [MaxLength(20)]
        [Required]
        public string UserName { get; set; }

        [MaxLength(64)]
        [Required]
        public string CurrentPassword { get; set; }

        [MaxLength(64)]
        [Required]
        public string NewPassword { get; set; }

        public int? CurrentUser { get; set; }
    }
}
