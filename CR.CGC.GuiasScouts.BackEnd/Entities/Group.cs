﻿namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using System.Xml.Serialization;

    [XmlRoot("Group")]
    public class Group
    {
        [XmlElement("GroupId")]
        public int? GroupId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [Required]
        [XmlElement("Number")]
        public int Number { get; set; }

        [Required]
        [XmlElement("FoundationDate")]
        public DateTime FoundationDate { get; set; }

        [MaxLength(250)]
        [XmlElement("HeadkerchiefImage")]
        public string HeadkerchiefImage { get; set; }

        [MaxLength(250)]
        [Required]
        [XmlElement("HeadkerchiefMeaning")]
        public string HeadkerchiefMeaning { get; set; }

        [MaxLength(250)]
        [Required]
        [XmlElement("HeadkerchiefDescription")]
        public string HeadkerchiefDescription { get; set; }

        [XmlElement("ChiefMember")]
        public int? ChiefMember { get; set; }

        [Required]
        [XmlElement("HasABuilding")]
        public bool HasABuilding { get; set; }

        [Required]
        [XmlElement("DistrictId")]
        public int DistrictId { get; set; }

        [Required]
        [XmlElement("LocalId")]
        public int LocalId { get; set; }

        [MaxLength(250)]
        [Required]
        [XmlElement("Address")]
        public string Address { get; set; }

        [Required]
        [XmlElement("SectorId")]
        public int SectorId { get; set; }

        [Required]
        [XmlElement("Latitude")]
        public decimal Latitude { get; set; }

        [Required]
        [XmlElement("Longitude")]
        public decimal Longitude { get; set; }

        [Required]
        [XmlElement("SystemStatusId")]
        public int SystemStatusId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [IgnoreProperty]
        [XmlElement("Member")]
        public Member Member { get; set; }

        [IgnoreProperty]
        [XmlElement("District")]
        public District District { get; set; }

        [IgnoreProperty]
        [XmlElement("Local")]
        public BaseCatalog Local { get; set; }

        [IgnoreProperty]
        [XmlElement("Sector")]
        public BaseCatalog Sector { get; set; }

        [IgnoreProperty]
        [XmlElement("SystemStatus")]
        public SystemStatus SystemStatus { get; set; }

        [XmlElement("GroupColors")]
        public List<GroupColor> GroupColors { get; set; } = new List<GroupColor>();

        [XmlElement("GroupEmails")]
        public List<GroupEmail> GroupEmails { get; set; } = new List<GroupEmail>();

        [XmlElement("GroupSocialNetworks")]
        public List<GroupSocialNetwork> GroupSocialNetworks { get; set; } = new List<GroupSocialNetwork>();

        [IgnoreProperty]
        [XmlIgnore]
        public HttpPostedFileWrapper SendFile { get; set; }

        [JsonIgnoreSerialization]
        public string Schema { get; set; } = "Members";

        [JsonIgnoreSerialization]
        public string GetProcedure { get; set; } = "usp_GetGroup";

        [JsonIgnoreSerialization]
        public string InsertionProcedure { get; set; } = "usp_RecordGroup";

        public Group()
        {
            FoundationDate = DateTime.Now;
        }

    }
}
