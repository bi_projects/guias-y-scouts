﻿namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using System.Xml.Serialization;

    [XmlRoot("Event")]
    public class Event
    {
        [XmlElement("EventId")]
        public int? EventId { get; set; }

        [MaxLength(100)]
        [Required]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("EventImage")]
        public string EventImage { get; set; }

        [XmlIgnore]
        [IgnoreProperty]
        public HttpPostedFileWrapper SendFile { get; set; }

        [Required]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("StartDateTime")]
        public DateTime StartDateTime { get; set; }

        [Required]
        [XmlElement("EndDateTime")]
        public DateTime EndDateTime { get; set; }

        [Required]
        [XmlElement("QuotaLimit")]
        public int QuotaLimit { get; set; }

        [Required]
        [XmlElement("ScopeId")]
        public int ScopeId { get; set; }

        [Required]
        [XmlElement("ActivityTypeId")]
        public int ActivityTypeId { get; set; }

        [Required]
        [XmlElement("CurrencyId")]
        public int CurrencyId { get; set; }

        [Required]
        [XmlElement("Cost")]
        public decimal Cost { get; set; }

        [Required]
        [XmlElement("ItIsFundable")]
        public bool ItIsFundable { get; set; }

        [Required]
        [XmlElement("InscriptionStart")]
        public DateTime InscriptionStart { get; set; }

        [Required]
        [XmlElement("InscriptionEnd")]
        public DateTime InscriptionEnd { get; set; }

        [MaxLength(2000)]
        [XmlElement("Requirements")]
        public string Requirements { get; set; }

        [Required]
        [MaxLength(250)]
        [XmlElement("Place")]
        public string Place { get; set; }

        [Required]
        [MaxLength(250)]
        [XmlElement("Address")]
        public string Address { get; set; }

        [XmlElement("Longitude")]
        public decimal? Longitude { get; set; }

        [XmlElement("Latitude")]
        public decimal? Latitude { get; set; }

        [XmlElement("TemplateId")]
        public int? TemplateId { get; set; }

        [MaxLength(100)]
        [XmlElement("TemplateCode")]
        public string TemplateCode { get; set; }

        [Required]
        [XmlElement("EmailBody")]
        public string EmailBody { get; set; }

        [Required]
        [XmlElement("EventStatusId")]
        public int EventStatusId { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("EventAgeRanges")]
        public List<EventAgeRange> EventAgeRanges { get; set; } = new List<EventAgeRange>();

        [XmlElement("EventRoles")]
        public List<EventRole> EventRoles { get; set; } = new List<EventRole>();

        //----------------------------------------------------

        [IgnoreProperty]
        [XmlElement("Scope")]
        public BaseCatalog Scope { get; set; }

        [IgnoreProperty]
        [XmlElement("ActivityType")]
        public BaseCatalog ActivityType { get; set; }

        [IgnoreProperty]
        [XmlElement("Currency")]
        public Currency Currency { get; set; }

        [IgnoreProperty]
        [XmlElement("Template")]
        public Template Template { get; set; }

        [IgnoreProperty]
        [XmlElement("EventStatus")]
        public EventStatus EventStatus { get; set; }

        [IgnoreProperty]
        [XmlElement("EventAssessments")]
        public List<EventAssessment> EventAssessments { get; set; } = new List<EventAssessment>();

        [IgnoreProperty]
        [XmlElement("MemberEvents")]
        public List<MemberEvent> MemberEvents { get; set; } = new List<MemberEvent>();

        [IgnoreProperty]
        [XmlElement("Guests")]
        public int Guests { get; set; }

        //----------------------------------------------------

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Activities";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetEvent";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordEvent";

        public Event()
        {
            StartDateTime = DateTime.Now;
            EndDateTime = DateTime.Now;
            InscriptionStart = DateTime.Now;
            InscriptionEnd = DateTime.Now;
        }
    }
}
