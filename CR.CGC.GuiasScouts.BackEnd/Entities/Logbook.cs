﻿namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("Logbook")]
    public class Logbook
    {
        [XmlElement("LogbookId")]
        public int? LogbookId { get; set; }

        [Required]
        [XmlElement("LogTypeId")]
        public int LogTypeId { get; set; }

        [MaxLength(1000)]
        [Required]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [XmlElement("ExecutingUser")]
        public int ExecutingUser { get; set; }

        [Required]
        [XmlElement("ActionDate")]
        public DateTime ActionDate { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        //----------------------------------------------------

        [IgnoreProperty]
        [XmlElement("LogType")]
        public LogType LogType { get; set; }

        [IgnoreProperty]
        [XmlElement("LogbookDetails")]
        public List<LogbookDetail> LogbookDetails { get; set; } = new List<LogbookDetail>();

        //----------------------------------------------------

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "History";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetLogbook";

        [JsonIgnoreSerialization]
        [IgnoreProperty]
        [XmlIgnore]
        public string GetAdminProcedure { get; set; } = "usp_GetLogbookAdmin";

        public Logbook()
        {
            ActionDate = DateTime.Now;
        }
    }
}
