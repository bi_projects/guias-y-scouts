﻿namespace Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("Role")]
    public class Role
    {
        [XmlElement("RoleId")]
        public int? RoleId { get; set; }

        [MaxLength(256)]
        [XmlElement("Name")]
        [Required]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        [Required]
        public string Description { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("RoleCompetencies")]
        public List<RoleCompetence> RoleCompetencies { get; set; } = new List<RoleCompetence>();

        [IgnoreProperty]
        [XmlElement("Permissions")]
        public List<Permission> Permissions { get; set; } = new List<Permission>();

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "dbSecurity";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string GetProcedure { get; set; } = "usp_GetRole";

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string InsertionProcedure { get; set; } = "usp_RecordRole";
    }
}
