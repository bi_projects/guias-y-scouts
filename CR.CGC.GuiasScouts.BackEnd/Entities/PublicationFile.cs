﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("PublicationFile")]
    public class PublicationFile
    {
        [XmlElement("PublicationFileId")]
        public int? PublicationFileId { get; set; }

        [Required]
        [XmlElement("PublicationId")]
        public int PublicationId { get; set; }

        [Required]
        [MaxLength(100)]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [Required]
        [MaxLength(250)]        
        [XmlElement("FileLocation")]
        public string FileLocation { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Publications";
    }
}
