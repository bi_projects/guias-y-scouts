﻿namespace Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Xml.Serialization;

    [XmlRoot("RequestFile")]
    public class RequestFile
    {
        [ComparatorFieldBinnacleProperty]
        [XmlElement("RequestFileId")]
        public int? RequestFileId { get; set; }

        [Required]
        [XmlElement("RequestId")]
        public int RequestId { get; set; }

        [DisplayNameBinnacleProperty("Nombre del archivo")]
        [Required]
        [MaxLength(100)]
        [XmlElement("Name")]
        public string Name { get; set; }

        [MaxLength(250)]
        [XmlElement("Description")]
        public string Description { get; set; }

        [MaxLength(250)]
        [Required]
        [XmlElement("FileLocation")]
        public string FileLocation { get; set; }

        [Required]
        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [JsonIgnoreSerialization]
        [XmlIgnore]
        public string Schema { get; set; } = "Requests";
    }
}
