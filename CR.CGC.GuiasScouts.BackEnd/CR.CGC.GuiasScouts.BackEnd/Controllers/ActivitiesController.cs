﻿namespace CR.CGC.GuiasScouts.BackEnd.Controllers
{
    using CR.CGC.GuiasScouts.BackEnd.Models;
    using Entities;
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Web.Http;
    using System.Web.Script.Serialization;
    using static System.Net.HttpStatusCode;

    [RoutePrefix("api/Activities")]
    public class ActivitiesController : CRUDController
    {
        Func<string, string, ERContainer, bool, (HttpStatusCode, string, object)> WritingMethods;
        [HttpPost]
        [Route("Model/SendEmails")]
        public HttpResponseMessage SendEmails([FromBody]ERContainer container)
        {
            WritingMethods = DALBas.InsertCustomProcedure;
            var (ResponseCode, ResponseText, ResultingObject) = WritingMethods(container.Model, "usp_SendEventInvitation", container, true);
            if (ResponseCode == OK)
            {
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(new JavaScriptSerializer().Serialize(ResultingObject), Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseCode, ResultingObject);
            }
            else
            {
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(ResponseText, Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseCode, ResponseText);
            }
        }
    }
}