﻿namespace CR.CGC.GuiasScouts.BackEnd.Controllers
{
    using CR.CGC.GuiasScouts.BackEnd.Models;
    using DALBase;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Script.Serialization;
    using static System.Net.HttpStatusCode;

    [ApplicationController]
    [RoutePrefix("api/CRUD")]
    public class CRUDController : ApiController, ICrud
    {
        Func<string, ERContainer, bool, (HttpStatusCode, string, object)> WritingMethods;

        [HttpPost]
        [Route("Model/NewObject")]
        public async Task<HttpResponseMessage> Insert([FromBody]ERContainer container)
        {
            WritingMethods = DALBas.InsertObject;
            Request oldRequest = new Request();
            switch (container.Model)
            {
                case "Request":
                    if (container.Requests.First().RequestId.HasValue && container.Requests.First().RequestId != 0)
                    {
                        var (ResponseCodeRequest, ContainerTextRequest) = DALBas.GetObjectsFromModel("Request", new Dictionary<string, object>() { { "RequestId", container.Requests.First().RequestId.Value } });
                        if (ResponseCodeRequest == OK)
                            oldRequest = new JavaScriptSerializer().Deserialize<ERContainer>(Regex.Unescape(ContainerTextRequest)).Requests.First();
                    }
                    break;
                default:
                    break;
            }

            var (ResponseCode, ResponseText, ResultingObject) = WritingMethods(container.Model, container, true);
            if (ResponseCode == OK)
            {
                switch (container.Model)
                {
                    case "MemberRecognition":
                    case "MemberCompetenceEvaluation":
                    case "MemberGroup":
                        using (MembersController memberController = new MembersController())
                        {
                            await memberController.MemberManagementAlert(container.Model, container);
                        }
                        break;
                    case "Request":
                        using (RequestsController requestController = new RequestsController())
                        {
                            await requestController.RequestManagementAlert(container.Model, oldRequest, ResultingObject);
                        }
                        break;
                    default:
                        break;
                }
                oldRequest.Dispose();
                if (Request == null)
                {
                    return await Task.FromResult(new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(new JavaScriptSerializer().Serialize(ResultingObject), Encoding.UTF8)
                    });
                }
                return await Task.FromResult(Request.CreateResponse(ResponseCode, ResultingObject));
            }
            else
            {
                oldRequest.Dispose();
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(ResponseText, Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseCode, ResponseText);
            }
        }

        [HttpGet]
        [Route("Model/GetObjects")]
        public HttpResponseMessage GetObjects(params object[] obj)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            IEnumerable<KeyValuePair<string, string>> temp;
            if (obj != null)
            {
                temp = Utilities.TryCast(obj);
                parameters = Utilities.ToDictionary(temp);
            }
            else
                parameters = Utilities.ToDictionary(Request.GetQueryNameValuePairs());

            if (!parameters.ContainsKey("Model"))
            {
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NotFound,
                        Content = new StringContent("Se debe especificar el modelo que se desea obtener, por favor intenta mas tarde. Si el problema persiste comunicate con el administrador del sistema.", Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, "Se debe especificar el modelo que se desea obtener, por favor intenta mas tarde. Si el problema persiste comunicate con el administrador del sistema.");
            }
            var (ResponseCode, ResponseText) = DALBas.GetObjectsFromModel(parameters["Model"].ToString(), parameters);
            if (Request == null)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = ResponseCode,
                    Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                };
            }
            return Request.CreateResponse(ResponseCode, ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""));
        }
    }
}