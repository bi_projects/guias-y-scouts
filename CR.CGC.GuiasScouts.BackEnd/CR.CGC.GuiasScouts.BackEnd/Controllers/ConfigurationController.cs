﻿namespace CR.CGC.GuiasScouts.BackEnd.Controllers
{
    using CR.CGC.GuiasScouts.BackEnd.Models;
    using DALBase;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    [RoutePrefix("api/Config")]
    public class ConfigurationController : CRUDController
    {

        [Route("Catalog/GetAnItem")]
        [HttpGet]
        public HttpResponseMessage BaseCatalogAnItem()
        {
            Dictionary<string, object> parameters = Utilities.ToDictionary(Request.GetQueryNameValuePairs());
            if (!parameters.ContainsKey("Model"))
                return Request.CreateResponse(HttpStatusCode.NotFound, "Se debe especificar el modelo que se desea obtener, por favor intenta mas tarde. Si el problema persiste comunicate con el administrador del sistema.");
            var (ResponseCode, ResponseText, ResponseObject) = DALBas.GetCustomProcedure(parameters["Model"].ToString(), "GetProcedureGetAnItem", parameters);
            return Request.CreateResponse(ResponseCode, ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""));
        }
    }
}
