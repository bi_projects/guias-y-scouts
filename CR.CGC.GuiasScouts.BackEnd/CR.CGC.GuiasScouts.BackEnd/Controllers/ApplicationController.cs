﻿namespace CR.CGC.GuiasScouts.BackEnd.Controllers
{
    using DALBase;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    public class ApplicationController : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext context)
        {
            base.OnActionExecuting(context);
            var UserSession = HttpUtility.ParseQueryString(context.Request.RequestUri.Query)["UserSession"] ?? (context.Request.GetRouteData().Values.ContainsKey("UserSession") ? context.Request.GetRouteData().Values["UserSession"] : null);
            if (UserSession == null)
            {
                //context.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);//Si el id de la sesion no existe automaticamente se rechaza la peticion
            }
            else//Verificar si la sesion aun esta activa
            {
                if (string.IsNullOrEmpty(""))//El tiempo de la sesion ya expiro
                {
                    context.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
            }
        }

        public override void OnActionExecuted(HttpActionExecutedContext context)
        {
            //if (context.ActionContext.Request.Method.Method == "POST" && context.ActionContext.Response.StatusCode == HttpStatusCode.OK)
            //{

            //}
        }
    }
}
