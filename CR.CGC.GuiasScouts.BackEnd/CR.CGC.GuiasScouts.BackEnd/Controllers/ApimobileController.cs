﻿using CR.CGC.GuiasScouts.BackEnd.Models;
using DALBase;
using Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace CR.CGC.GuiasScouts.BackEnd.Controllers
{
    [RoutePrefix("api/Apimobile")]
    public class ApimobileController : ApiController
    {
        // GET: api/Apimobile
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        // GET: api/Apimobile/5
        public string Get(int id)
        {
            return "value";
        }
        // POST: api/Apimobile
        public void Post([FromBody]string value)
        {
        }
        // PUT: api/Apimobile/5
        public void Put(int id, [FromBody]string value)
        {
        }

        Func<string, ERContainer, bool, (HttpStatusCode, string, object)> WritingMethods;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("Account/LogIn")]
        [HttpPost]
        public HttpResponseMessage LogIn([FromBody]User user)
        {
            ERContainer container = new ERContainer() { User = user };
            var (ResponseCode, ResponseText, ResultingObject) = container.LoginUser();
            if (ResponseCode == HttpStatusCode.OK)
            {
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(new JavaScriptSerializer().Serialize(ResultingObject).Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseCode, ResultingObject);
            }
            else
            {
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseCode, ResponseText);
            }
        }

        [Route("Account/LogOut")]
        [HttpPost]
        public HttpResponseMessage LogOut([FromBody]User user)
        {
            ERContainer container = new ERContainer() { User = user };
            var (ResponseCode, ResponseText) = container.LogOutUser();
            if (ResponseCode == HttpStatusCode.OK)
            {
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(new JavaScriptSerializer().Serialize(user).Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseCode, user);
            }
            else
            {
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseCode, ResponseText);
            }
        }

        [Route("Account/UpdatePassword")]
        [HttpPost]
        public HttpResponseMessage UpdatePassword([FromBody]UpdPassModel CurrentInfo)
        {
            ERContainer container = new ERContainer() { User = new User() { UserName = CurrentInfo.UserName, Password = CurrentInfo.CurrentPassword } };
            User NewInfo = new User() { UserName = CurrentInfo.UserName, Password = CurrentInfo.NewPassword };
            var (ResponseCode, ResponseText, ResultingObject) = container.ChangePassword(NewInfo);
            if (ResponseCode == HttpStatusCode.OK)
            {
                MailMessage message = new MailMessage()
                {
                    From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                    IsBodyHtml = true,
                    Subject = "Actualización de contraseña"
                };
                message.To.Add(CurrentInfo.Email);
                StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/ChangePasswordConfirmation.html")));

                bodyNotificaction = SetDefaultValuesToEmail(bodyNotificaction, CurrentInfo.Email);
                message.Body = bodyNotificaction.ToString();
                var (ResponseEmailCode, ResponseEmailText) = message.SendEmail();
                if (ResponseEmailCode != HttpStatusCode.OK)
                {
                    if (Request == null)
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = ResponseEmailCode,
                            Content = new StringContent("Ocurrio un error inesperado al enviar el correo para confirmar el cambio de contraseña, sin embargo la contraseña logró cambiarse.", Encoding.UTF8)
                        };
                    }
                    return Request.CreateResponse(ResponseEmailCode, "Ocurrio un error inesperado al enviar el correo para confirmar el cambio de contraseña, sin embargo la contraseña logró cambiarse.");
                }
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(new JavaScriptSerializer().Serialize(ResultingObject).Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseCode, ResultingObject);
            }
            if (Request == null)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = ResponseCode,
                    Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                };
            }
            return Request.CreateResponse(ResponseCode, ResponseText);
        }

        [Route("Account/ForgotPassword")]
        [HttpPost]
        public HttpResponseMessage ForgotPassword([FromBody]User user)
        {
            user.PasswordResetToken = Utilities.GetToken();
            ERContainer container = new ERContainer() { User = user };
            var (ResponseCode, ResponseText) = container.ValidatePasswordResetRequest();
            if (ResponseCode == HttpStatusCode.OK)
            {
                MailMessage message = new MailMessage()
                {
                    From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                    IsBodyHtml = true,
                    Subject = "Restablecer contraseña"
                };
                message.To.Add(user.Email);
                message.Body = GetBodyToForgotPassword(user);
                var (ResponseEmailCode, ResponseEmailText) = message.SendEmail();
                if (ResponseEmailCode != HttpStatusCode.OK)
                {
                    if (Request == null)
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = ResponseEmailCode,
                            Content = new StringContent("Ocurrio un error inesperado al enviar el correo para restablecer la contraseña, por favor intenta mas tarde. Si el problema persiste comunicate con el administrador del sistema.", Encoding.UTF8)
                        };
                    }
                    return Request.CreateResponse(ResponseEmailCode, "Ocurrio un error inesperado al enviar el correo para restablecer la contraseña, por favor intenta mas tarde. Si el problema persiste comunicate con el administrador del sistema.");
                }
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(new JavaScriptSerializer().Serialize(user).Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            if (Request == null)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = ResponseCode,
                    Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                };
            }
            return Request.CreateResponse(ResponseCode, ResponseText);
        }

        [Route("Account/ConfirmForgotPassword")]
        [HttpPost]
        public HttpResponseMessage ConfirmForgotPassword([FromBody]User user)
        {
            ERContainer container = new ERContainer() { User = user };
            var (ResponseCode, ResponseText, ResultingObject) = container.ResetPassword(false);
            if (ResponseCode == HttpStatusCode.OK)
            {
                MailMessage message = new MailMessage()
                {
                    From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                    IsBodyHtml = true,
                    Subject = "Contraseña restablecida"
                };
                message.To.Add(ResultingObject.Members.First().User.Email);
                StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/ForgotPasswordConfirmation.html")));

                //Seteo de variables en el cuerpo del correo
                bodyNotificaction = Utilities.SetDefaultValuesToEmail(bodyNotificaction, ResultingObject.Members.First().User, ResultingObject.Members.First());
                message.Body = bodyNotificaction.ToString();
                var (ResponseEmailCode, ResponseEmailText) = message.SendEmail();
                if (ResponseEmailCode != HttpStatusCode.OK)
                {
                    if (Request == null)
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = ResponseEmailCode,
                            Content = new StringContent("Ocurrio un error inesperado al enviar el correo de confirmación, sin embargo la contraseña logró restablecerse con éxito.", Encoding.UTF8)
                        };
                    }
                    return Request.CreateResponse(ResponseEmailCode, "Ocurrio un error inesperado al enviar el correo de confirmación, sin embargo la contraseña logró restablecerse con éxito.");
                }
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(new JavaScriptSerializer().Serialize(user).Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            if (Request == null)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = ResponseCode,
                    Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                };
            }
            return Request.CreateResponse(ResponseCode, ResponseText);
        }

        /// <summary>
        /// Metodo para armar el cuerpo del correo para restablecer la contrasena
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string GetBodyToForgotPassword(User user)
        {
            StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/ForgotPassword.html")));
            Dictionary<string, object> _Object = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(user.Source);

            //Seteo de variables en el cuerpo del correo
            bodyNotificaction = bodyNotificaction.Replace("[HREF]", Utilities.UrlWebSite + "Account/ForgotPasswordToken?token=" + user.PasswordResetToken);
            if (_Object is null || (!_Object.ContainsKey("Browser") && !_Object.ContainsKey("OSName")))
                bodyNotificaction = bodyNotificaction.Replace("[SECURITYMESSAGE]", "");
            else
            {
                string SecurityMessage = string.Empty;
                if (_Object.ContainsKey("Browser") && !string.IsNullOrEmpty(_Object["Browser"].ToString()))
                {
                    SecurityMessage = $"Por razones de seguridad, esta solicitud fue recibida de un navegador <strong>{_Object["Browser"]}</strong>";
                    if (_Object.ContainsKey("OSName") && !string.IsNullOrEmpty(_Object["OSName"].ToString()))
                        SecurityMessage += $" en un dispositivo <strong>{_Object["OSName"]}</strong>.";
                    else
                        SecurityMessage += ".";
                }
                else if (_Object.ContainsKey("OSName") && !string.IsNullOrEmpty(_Object["OSName"].ToString()))
                    SecurityMessage = $"Por razones de seguridad, esta solicitud fue recibida de un dispositivo <strong>{_Object["OSName"]}</strong>.";
                bodyNotificaction = bodyNotificaction.Replace("[SECURITYMESSAGE]", SecurityMessage);
            }
            bodyNotificaction = SetDefaultValuesToEmail(bodyNotificaction, user.Email);
            return bodyNotificaction.ToString();
        }

        private StringBuilder SetDefaultValuesToEmail(StringBuilder bodyNotificaction, string email)
        {
            //Seteo de variables en el cuerpo del correo
            bodyNotificaction = bodyNotificaction.Replace("[EMAIL]", email);
            bodyNotificaction = bodyNotificaction.Replace("[YEAR]", DateTime.Now.ToString("yyyy"));
            bodyNotificaction = bodyNotificaction.Replace("[URLSUPPORT]", Utilities.UrlSuppport);
            bodyNotificaction = bodyNotificaction.Replace("[URLWEBSITE]", Utilities.UrlWebSite);
            bodyNotificaction = bodyNotificaction.Replace("[SUPPORTMAIL]", Utilities.SupportMail);
            return bodyNotificaction;
        }

        private string MapPath(string filePath)
        {
            string temp;
            filePath = filePath.Replace('/', '\\');
            var hostingRoot = System.Web.Hosting.HostingEnvironment.IsHosted

            ? System.Web.Hosting.HostingEnvironment.MapPath("~/")

            : Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

            temp = hostingRoot + filePath;
            return temp;
        }

        [HttpGet]
        [Route("Model/GetObjects")]
        public HttpResponseMessage GetObjects(params object[] obj)
        {
            //"SMS-test Guías y Scouts".SendSMS(new List<string>() { "88374753", "89256432", "86820349" });
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            IEnumerable<KeyValuePair<string, string>> temp;
            if (obj != null)
            {
                temp = Utilities.TryCast(obj);
                parameters = Utilities.ToDictionary(temp);
            }
            else
                parameters = Utilities.ToDictionary(Request.GetQueryNameValuePairs());

            if (!parameters.ContainsKey("Model"))
            {
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NotFound,
                        Content = new StringContent("Se debe especificar el modelo que se desea obtener, por favor intenta mas tarde. Si el problema persiste comunicate con el administrador del sistema.", Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, "Se debe especificar el modelo que se desea obtener, por favor intenta mas tarde. Si el problema persiste comunicate con el administrador del sistema.");
            }
            var (ResponseCode, ResponseText) = DALBas.GetObjectsFromModel(parameters["Model"].ToString(), parameters);
            if (Request == null)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = ResponseCode,
                    Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                };
            }
            return Request.CreateResponse(ResponseCode, ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""));
        }
    }
}
