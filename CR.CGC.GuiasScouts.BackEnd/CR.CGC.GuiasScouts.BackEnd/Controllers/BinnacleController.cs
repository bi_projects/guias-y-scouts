﻿namespace CR.CGC.GuiasScouts.BackEnd.Controllers
{
    using CR.CGC.GuiasScouts.BackEnd.Models;
    using DALBase;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Web.Http;
    using Newtonsoft.Json;

    [RoutePrefix("api/Binnacle")]
    public class BinnacleController : CRUDController
    {
        Func<string, string, Dictionary<string, object>, (HttpStatusCode, string, ERContainer)> ReadMethods;

        [Route("List/ForAdmin")]
        public HttpResponseMessage GetAdminBinnacle(params object[] obj)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            IEnumerable<KeyValuePair<string, string>> temp;
            if (obj != null)
            {
                temp = Utilities.TryCast(obj);
                parameters = Utilities.ToDictionary(temp);
            }
            else
                parameters = Utilities.ToDictionary(Request.GetQueryNameValuePairs());

            ReadMethods = DALBas.GetCustomProcedure;
            var (ResponseCode, ResponseText, ResponseObject) = ReadMethods("Logbook", "GetAdminProcedure", parameters);
            //ResponseText = JsonConvert.SerializeObject(ResponseObject.Logbooks);
            if (Request == null)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = ResponseCode,
                    Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                };
            }
            return Request.CreateResponse(ResponseCode, ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""));
        }

    }
}
