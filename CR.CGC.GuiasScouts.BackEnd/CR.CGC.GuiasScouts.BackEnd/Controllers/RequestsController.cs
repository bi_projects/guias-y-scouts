﻿namespace CR.CGC.GuiasScouts.BackEnd.Controllers
{
    using CR.CGC.GuiasScouts.BackEnd.Models;
    using CR.CGC.GuiasScouts.BackEnd.Tasks;
    using DALBase;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Mail;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Script.Serialization;
    using static System.Net.HttpStatusCode;

    [RoutePrefix("api/Requests")]
    public class RequestsController : CRUDController
    {
        [Route("Email/SendEmail")]
        [HttpPost]
        public HttpResponseMessage SendMail([FromBody]EmailContainer message)
        {
            MailMessage messageToSend = new MailMessage()
            {
                From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                IsBodyHtml = true,
                Subject = message.mailMessage.Subject,
                Body = message.mailMessage.Body,
            };
            messageToSend.To.Add(message.ListToSend);

            var (ResponseEmailCode, ResponseEmailText) = messageToSend.SendEmail();
            if (ResponseEmailCode != OK)
            {
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseEmailCode,
                        Content = new StringContent("Ocurrió un error inesperado al enviar el correo de confirmación, sin embargo la contraseña logró restablecerse con éxito.", Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseEmailCode, "Ocurrió un error inesperado al enviar el correo de confirmación, sin embargo la contraseña logró restablecerse con éxito.");
            }
            if (Request == null)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = ResponseEmailCode,
                    Content = new StringContent(new JavaScriptSerializer().Serialize(message).Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                };
            }
            return Request.CreateResponse(OK, message);
        }

        public async Task RequestManagementAlert(string model, Request oldRequest, object ResultingObject)
        {
            Request currentRequest = ((List<Request>)ResultingObject).First();
            var (ResponseCodeMember, ContainerText) = DALBas.GetObjectsFromModel("Member", new Dictionary<string, object>() { { "UserId", currentRequest.RequestingUser } });
            Member memberInfo = new JavaScriptSerializer().Deserialize<ERContainer>(ContainerText).Members.First();
            if (oldRequest.RequestId.HasValue && oldRequest.RequestId.Value != 0)
            {
                MailMessage message = new MailMessage()
                {
                    From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                    IsBodyHtml = true,
                    Subject = "Gestión de solicitudes"
                };
                message.To.Add(memberInfo.Email);
                StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/CustomAlert.html")));
                if ((!oldRequest.AssignedTo.HasValue && currentRequest.AssignedTo.HasValue) || (oldRequest.AssignedTo.HasValue && currentRequest.AssignedTo.HasValue && oldRequest.AssignedTo.Value != currentRequest.AssignedTo.Value))
                {
                    (ResponseCodeMember, ContainerText) = DALBas.GetObjectsFromModel("Member", new Dictionary<string, object>() { { "UserId", currentRequest.AssignedTo.Value } });
                    Member memberAsigned = new JavaScriptSerializer().Deserialize<ERContainer>(ContainerText).Members.First();
                    bodyNotificaction = bodyNotificaction.Replace("[MESSAGE]", $"Su solicitud ha sido asignada a: {memberAsigned.Names + (string.IsNullOrEmpty(memberAsigned.Surnames) ? string.Empty : " " + memberAsigned.Surnames)}, número de referencia: <strong>{currentRequest.RequestId.Value}</strong>.");

                    MailMessage messageMemberAsigned = new MailMessage()
                    {
                        From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                        IsBodyHtml = true,
                        Subject = "Gestión de solicitudes"
                    };
                    messageMemberAsigned.To.Add(memberAsigned.Email);
                    StringBuilder bodyNotificactionMemberAsigned = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/CustomAlert.html")));
                    bodyNotificactionMemberAsigned = bodyNotificactionMemberAsigned.Replace("[MESSAGE]", $"Una nueva solicitud te ha sido asignada, número de referencia: <strong>{currentRequest.RequestId.Value}</strong>.");
                    bodyNotificactionMemberAsigned = Utilities.SetDefaultValuesToEmail(bodyNotificactionMemberAsigned, memberAsigned.User, memberAsigned);
                    messageMemberAsigned.Body = bodyNotificactionMemberAsigned.ToString();
                    EmailAsync.SendEmailAsync(messageMemberAsigned);
                }
                else if (currentRequest.RequestStatusId != oldRequest.RequestStatusId)
                    bodyNotificaction = bodyNotificaction.Replace("[MESSAGE]", $"El estado de su solicitud cambió a: <strong>{currentRequest.RequestStatus.Name}</strong>, número de referencia: <strong>{currentRequest.RequestId.Value}</strong>.");
                else
                    bodyNotificaction = bodyNotificaction.Replace("[MESSAGE]", $"Su solicitud fue actualizada, número de referencia: <strong>{currentRequest.RequestId.Value}</strong>.");
                bodyNotificaction = Utilities.SetDefaultValuesToEmail(bodyNotificaction, memberInfo.User, memberInfo);
                message.Body = bodyNotificaction.ToString();
                EmailAsync.SendEmailAsync(message);
            }
            else
            {
                RquestManagerAlertAsync(memberInfo, currentRequest.RequestId.Value);
                List<MemberAlertSetting> settings = DALBas.GetMemberAlert(memberInfo.MemberId.Value);
                MemberAlertSetting setting = settings.Find(f => f.AlertType.Name.Equals("Solicitudes"));
                if (setting != null && setting.Email)
                {
                    MailMessage message = new MailMessage()
                    {
                        From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                        IsBodyHtml = true,
                        Subject = "Gestión de solicitudes"
                    };
                    message.To.Add(memberInfo.Email);
                    StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/CustomAlert.html")));
                    bodyNotificaction = bodyNotificaction.Replace("[MESSAGE]", $"Su solicitud ha sido creada con éxito, con el número de referencia: <strong>{currentRequest.RequestId.Value}</strong>");
                    bodyNotificaction = Utilities.SetDefaultValuesToEmail(bodyNotificaction, memberInfo.User, memberInfo);
                    message.Body = bodyNotificaction.ToString();
                    EmailAsync.SendEmailAsync(message);
                }
                if (setting != null && setting.Sms && !string.IsNullOrEmpty(Utilities.GetPhoneNumber(memberInfo)))
                    await $"Solicitud creada exitosamente, número: {currentRequest.RequestId.Value}".SendSMSAsync(Utilities.GetPhoneNumber(memberInfo)).ConfigureAwait(false);
            }
        }

        private void RquestManagerAlertAsync(Member memberInfo, int reference)
        {
            List<Permission> permissions = new List<Permission>() {
                    new Permission() {
                        RoleId = 0,
                        Action = "Management"
                    },
                    new Permission(){
                        RoleId = 0,
                        Action = "AssignRequest"
                    }
                };
            var (ResponseCodeManagement, ResponseTextManagement, ResponseObject) = DALBas.GetCustomProcedure("Member", "GetMemberByPermissionsProcedure", new Dictionary<string, object>() { { "Controller", "CRM" }, { "Permission", permissions.ListToDataTable() } });
            List<Member> requestManagers = new JavaScriptSerializer().Deserialize<ERContainer>(ResponseTextManagement).Members;
            foreach (Member requestManager in requestManagers)
            {
                MailMessage message = new MailMessage()
                {
                    From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                    IsBodyHtml = true,
                    Subject = "Gestión de solicitudes"
                };

                message.To.Add(requestManager.Email);
                StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/CustomAlert.html")));
                bodyNotificaction = bodyNotificaction.Replace("[MESSAGE]", $"{memberInfo.Names + (string.IsNullOrEmpty(memberInfo.Surnames) ? string.Empty : " " + memberInfo.Surnames)} ha creado una solicitud, número de referencia: <strong>{reference}</strong>");
                bodyNotificaction = Utilities.SetDefaultValuesToEmail(bodyNotificaction, requestManager.User, requestManager);
                message.Body = bodyNotificaction.ToString();
                EmailAsync.SendEmailAsync(message);
            }
        }

        private string MapPath(string filePath)
        {
            string temp;
            filePath = filePath.Replace('/', '\\');
            var hostingRoot = System.Web.Hosting.HostingEnvironment.IsHosted

            ? System.Web.Hosting.HostingEnvironment.MapPath("~/")

            : Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

            temp = hostingRoot + filePath;
            return temp;
        }
    }
}
