﻿using System.Web.Script.Serialization;

namespace CR.CGC.GuiasScouts.BackEnd.Controllers
{
    using CR.CGC.GuiasScouts.BackEnd.Models;
    using DALBase;
    using Entities;
    using System.Linq;
    using static System.Net.HttpStatusCode;
    using System.Net.Mail;
    using System.Web.Http;
    using System.Net.Http;
    using System.Text;
    using System;
    using System.Net;
    using System.IO;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        Func<string, ERContainer, bool, (HttpStatusCode, string, object)> WritingMethods;

        [Route("Account/ForcePasswordChange")]
        [HttpPost]
        public async Task<HttpResponseMessage> ForcePasswordChange([FromBody]User user)
        {
            user.PasswordResetToken = Utilities.GetToken();
            ERContainer container = new ERContainer() { User = user };
            var (ResponseCodeReset, ResponseTextReset) = container.ValidatePasswordResetRequest();
            if (ResponseCodeReset == OK)
            {
                var (ResponseCode, ResponseText, ResultingObject) = container.ResetPassword(true);
                if (ResponseCode == OK)
                {
                    List<MemberAlertSetting> settings = DALBas.GetMemberAlert(ResultingObject.Members.First().MemberId.Value);
                    MemberAlertSetting setting = settings.Find(f => f.AlertType.Name.Equals("Gestión de usuario"));
                    if (setting != null && setting.Sms && !string.IsNullOrEmpty(Utilities.GetPhoneNumber(ResultingObject.Members.First())))
                        await "Contraseña actualizada, detalles en tu correo electrónico".SendSMSAsync(Utilities.GetPhoneNumber(ResultingObject.Members.First()));
                    MailMessage message = new MailMessage()
                    {
                        From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                        IsBodyHtml = true,
                        Subject = "Cambio de contraseña"
                    };
                    message.To.Add(user.Email);
                    StringBuilder bodyNotification = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/ForcePasswordChange.html")));

                    //Reemplazo de variables dentro del correo por valores reales
                    bodyNotification = bodyNotification.Replace("[PASSWORDTEMP]", user.Password);
                    bodyNotification = Utilities.SetDefaultValuesToEmail(bodyNotification, ResultingObject.Members.First().User, ResultingObject.Members.First());
                    message.Body = bodyNotification.ToString();
                    var (ResponseEmailCode, ResponseEmailText) = message.SendEmail();
                    if (ResponseEmailCode == OK)
                    {
                        if (Request == null)
                            return new HttpResponseMessage()
                            {
                                StatusCode = ResponseCode,
                                Content = new StringContent(new JavaScriptSerializer().Serialize(ResultingObject).Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                            };
                        return Request.CreateResponse(ResponseEmailCode, ResultingObject);
                    }
                    else
                    {
                        if (Request == null)
                            return new HttpResponseMessage()
                            {
                                StatusCode = ResponseEmailCode,
                                Content = new StringContent("La contraseña logró cambiarse de manera satisfactoria, lamentablemente el correo de confirmación no logro enviarse", Encoding.UTF8)
                            };
                        return Request.CreateResponse(ResponseEmailCode, "La contraseña logró cambiarse de manera satisfactoria, lamentablemente el correo de confirmación no logro enviarse");
                    }
                }
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseCode, ResponseText);
            }
            if (Request == null)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = ResponseCodeReset,
                    Content = new StringContent(ResponseTextReset.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                };
            }
            return Request.CreateResponse(ResponseCodeReset, ResponseTextReset);
        }

        [HttpPost]
        [Route("Account/EmailUpdate")]
        public async Task<HttpResponseMessage> EmailUpdate(ERContainer container)
        {
            WritingMethods = DALBas.InsertObject;
            container.Users.Add(container.Members.First().User);
            var (ResponseCode, ResponseText, ResultingObject) = WritingMethods("Users", container, true);
            if (ResponseCode == OK)
            {
                (ResponseCode, ResponseText, ResultingObject) = DALBas.InsertMember(container);
                if (ResponseCode == OK)
                {
                    List<MemberAlertSetting> settings = DALBas.GetMemberAlert(container.Members.First().MemberId.Value);
                    MemberAlertSetting setting = settings.Find(f => f.AlertType.Name.Equals("Gestión de usuario"));
                    if (setting != null && setting.Sms && !string.IsNullOrEmpty(Utilities.GetPhoneNumber(container.Members.First())))
                        await "Su correo ha sido actualizado exitosamente".SendSMSAsync(Utilities.GetPhoneNumber(container.Members.First()));
                    if (setting != null && setting.Email)
                    {
                        MailMessage message = new MailMessage()
                        {
                            From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                            IsBodyHtml = true,
                            Subject = "Atención"
                        };
                        message.To.Add(container.Members.First().Email);
                        StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/CustomAlert.html")));

                        bodyNotificaction = Utilities.SetDefaultValuesToEmail(bodyNotificaction, container.Users.First(), container.Members.First());
                        bodyNotificaction = bodyNotificaction.Replace("[MESSAGE]", "Su correo ha sido actualizado exitosamente.");
                        message.Body = bodyNotificaction.ToString();
                        var (ResponseCodeEmail, ResponseTextEmail) = message.SendEmail();
                    }
                    if (Request == null)
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = ResponseCode,
                            Content = new StringContent(new JavaScriptSerializer().Serialize(ResultingObject).Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                        };
                    }
                    return Request.CreateResponse(ResponseCode, ResultingObject);
                }
                else
                {
                    if (Request == null)
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = ResponseCode,
                            Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                        };
                    }
                    return Request.CreateResponse(ResponseCode, ResponseText);
                }
            }
            else
            {
                if (Request == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                    };
                }
                return Request.CreateResponse(ResponseCode, ResponseText);
            }
        }

        [HttpPost]
        [Route("Account/Activation")]
        public HttpResponseMessage Activation([FromBody]ERContainer container)
        {
            WritingMethods = DALBas.InsertObject;
            User _user = container.Users.First();
            _user.PasswordResetToken = string.Empty;
            _user.Password = RandomPassword.GetPassword();
            container.User = _user;
            int? currentUser = container.CurrentUser;
            container.CurrentUser = null;
            var (ResponseCode, ResponseText, ResultingObject) = DALBas.ActivateUser(container);
            if (ResponseCode == OK)
            {
                object insert;
                container.CurrentUser = currentUser;
                (ResponseCode, ResponseText, insert) = WritingMethods("User", container, true);
                if (ResponseCode == OK)
                {
                    Member member = ResultingObject.Members.First();
                    MailMessage message = new MailMessage()
                    {
                        From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                        IsBodyHtml = true,
                        Subject = "Bienvenido a Guías y Scouts"
                    };
                    message.To.Add(_user.Email);
                    StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/Welcome.html")));

                    //Reemplazo de variables dentro del correo por valores reales
                    bodyNotificaction = bodyNotificaction.Replace("[PASSWORDTEMP]", _user.Password);
                    bodyNotificaction = Utilities.SetDefaultValuesToEmail(bodyNotificaction, _user, member);
                    message.Body = bodyNotificaction.ToString();
                    var (ResponseEmailCode, ResponseEmailText) = message.SendEmail();
                    if (ResponseEmailCode == OK)
                    {
                        if (Request == null)
                            return new HttpResponseMessage()
                            {
                                StatusCode = ResponseCode,
                                Content = new StringContent(new JavaScriptSerializer().Serialize(ResultingObject).Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                            };
                        return Request.CreateResponse(ResponseEmailCode, ResultingObject);
                    }
                    else
                    {
                        if (Request == null)
                            return new HttpResponseMessage()
                            {
                                StatusCode = ResponseEmailCode,
                                Content = new StringContent("El usuario logró activarse de manera satisfactoria, lamentablemente el correo de confirmación no logró enviarse", Encoding.UTF8)
                            };
                        return Request.CreateResponse(ResponseEmailCode, "El usuario logró activarse de manera satisfactoria, lamentablemente el correo de confirmación no logró enviarse");
                    }
                }
                if (Request == null)
                    return new HttpResponseMessage()
                    {
                        StatusCode = ResponseCode,
                        Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                    };
                return Request.CreateResponse(ResponseCode, ResponseText);
            }
            if (Request == null)
                return new HttpResponseMessage()
                {
                    StatusCode = ResponseCode,
                    Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                };
            return Request.CreateResponse(ResponseCode, ResponseText);
        }

        private string MapPath(string filePath)
        {
            string temp;
            filePath = filePath.Replace('/', '\\');
            var hostingRoot = System.Web.Hosting.HostingEnvironment.IsHosted

            ? System.Web.Hosting.HostingEnvironment.MapPath("~/")

            : Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

            temp = hostingRoot + filePath;
            return temp;
        }
    }
}
