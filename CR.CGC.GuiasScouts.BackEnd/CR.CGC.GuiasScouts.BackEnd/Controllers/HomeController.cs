﻿namespace CR.CGC.GuiasScouts.BackEnd.Controllers
{
    using System.Web.Mvc;

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
