﻿namespace CR.CGC.GuiasScouts.BackEnd.Controllers
{
    using DALBase;
    using Entities;
    using System.Net;
    using static System.Net.HttpStatusCode;
    using System.Net.Mail;
    using System.Text;
    using System.Web.Http;
    using System.Net.Http;
    using CR.CGC.GuiasScouts.BackEnd.Models;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.IO;
    using System.Web.Script.Serialization;
    using System.Threading.Tasks;

    [RoutePrefix("api/Members")]
    public class MembersController : CRUDController
    {
        Func<ERContainer, (HttpStatusCode, string, object)> WritingMethods;
        Func<string, string, Dictionary<string, object>, (HttpStatusCode, string, ERContainer)> ReadMethods;

        [Route("Record/New")]
        [HttpPost]
        public HttpResponseMessage CreateAccount([FromBody]ERContainer container)
        {
            string email = container.Members.FirstOrDefault().Email;
            container.User = new User()
            {
                Email = email,
                Password = RandomPassword.GetPassword()
            };

            WritingMethods = DALBas.InsertMember;
            var (ResponseUserCode, ResponseUserText, ResultingObject) = WritingMethods(container);
            if (ResponseUserCode == OK)
                return Request.CreateResponse(OK, ResultingObject);
            return Request.CreateResponse(ResponseUserCode, ResponseUserText);
        }

        [Route("List/ForMembers")]
        public HttpResponseMessage GetListForMembers(params object[] obj)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            IEnumerable<KeyValuePair<string, string>> temp;
            if (obj != null)
            {
                temp = Utilities.TryCast(obj);
                parameters = Utilities.ToDictionary(temp);
            }
            else
                parameters = Utilities.ToDictionary(Request.GetQueryNameValuePairs());

            ReadMethods = DALBas.GetCustomProcedure;
            var (ResponseCode, ResponseText, ResponseObject) = ReadMethods("Member", "GetListsForMembersProcedure", parameters);
            if (Request == null)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = ResponseCode,
                    Content = new StringContent(ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""), Encoding.UTF8)
                };
            }
            return Request.CreateResponse(ResponseCode, ResponseText.Replace(Environment.NewLine, "").Replace("\\\"", "\""));
        }

        public async Task<(HttpStatusCode ResponseCode, string ResponseText)> MemberManagementAlert(string model, ERContainer container)
        {
            try
            {
                MailMessage message = new MailMessage()
                {
                    From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                    IsBodyHtml = true,
                    Subject = "Actualización en tu información de miembro GyS"
                };
                string SmsMessage = string.Empty;
                Member memberInfo = new Member();
                StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/CustomAlert.html")));
                var (ResponseCodeMember, ContainerText) = new Tuple<HttpStatusCode, string>(OK, string.Empty);
                switch (model)
                {
                    case "MemberRecognition":
                        SmsMessage = "Se han registrado cambios en tus reconocimientos";
                        (ResponseCodeMember, ContainerText) = DALBas.GetObjectsFromModel("Member", new Dictionary<string, object>() { { "MemberId", container.MemberRecognitions.First().MemberId } });
                        memberInfo = new JavaScriptSerializer().Deserialize<ERContainer>(ContainerText).Members.First();
                        bodyNotificaction = bodyNotificaction.Replace("[MESSAGE]", "Se han registrado cambios en tus reconocimientos y condecoraciones, ingresa al sistema para verificarlos.");
                        break;
                    case "MemberCompetenceEvaluation":
                        SmsMessage = "Se han registrado cambios en tus evaluaciones";
                        (ResponseCodeMember, ContainerText) = DALBas.GetObjectsFromModel("Member", new Dictionary<string, object>() { { "MemberId", container.MemberCompetenceEvaluations.First().MemberId } });
                        memberInfo = new JavaScriptSerializer().Deserialize<ERContainer>(ContainerText).Members.First();
                        bodyNotificaction = bodyNotificaction.Replace("[MESSAGE]", "Se han registrado cambios en tus evaluaciones de competencias, ingresa al sistema para verificarlos.");
                        break;
                    case "MemberGroup":
                        SmsMessage = "Se han registrado cambios en tu información de grupo";
                        (ResponseCodeMember, ContainerText) = DALBas.GetObjectsFromModel("Member", new Dictionary<string, object>() { { "MemberId", container.MemberGroups.First().MemberId } });
                        memberInfo = new JavaScriptSerializer().Deserialize<ERContainer>(ContainerText).Members.First();
                        bodyNotificaction = bodyNotificaction.Replace("[MESSAGE]", "Se han registrado cambios en tu información de grupo, ingresa al sistema para verificarlos.");
                        break;

                    default:
                        break;
                }
                message.To.Add(memberInfo.Email);
                bodyNotificaction = Utilities.SetDefaultValuesToEmail(bodyNotificaction, memberInfo.User, memberInfo);
                message.Body = bodyNotificaction.ToString();
                List<MemberAlertSetting> settings = DALBas.GetMemberAlert(memberInfo.MemberId.Value);
                MemberAlertSetting setting = settings.Find(f => f.AlertType.Name.Equals("Gestión de miembro"));
                if (setting != null && setting.Sms && !string.IsNullOrEmpty(Utilities.GetPhoneNumber(memberInfo)))
                    await SmsMessage.SendSMSAsync(Utilities.GetPhoneNumber(memberInfo));
                if (setting != null && setting.Email)
                    var (ResponseCodeEmail, ResponseTextEmail) = message.SendEmail();
                return (OK, string.Empty);
            }
            catch (Exception ex)
            {
                return (HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private string MapPath(string filePath)
        {
            string temp;
            filePath = filePath.Replace('/', '\\');
            var hostingRoot = System.Web.Hosting.HostingEnvironment.IsHosted

            ? System.Web.Hosting.HostingEnvironment.MapPath("~/")

            : Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

            temp = hostingRoot + filePath;
            return temp;
        }
    }
}
