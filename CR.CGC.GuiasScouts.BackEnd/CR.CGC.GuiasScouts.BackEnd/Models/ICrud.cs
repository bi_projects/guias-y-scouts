﻿using Entities;
using System.Net.Http;
using System.Threading.Tasks;

namespace CR.CGC.GuiasScouts.BackEnd.Models
{
    interface ICrud
    {
        Task<HttpResponseMessage> Insert(ERContainer model);
        HttpResponseMessage GetObjects(params object[] obj);
        //HttpResponseMessage GetObject();
        //HttpResponseMessage UpdateObjects(ERContainer model);
        //HttpResponseMessage DeleteObjects(ERContainer model);
    }
}
