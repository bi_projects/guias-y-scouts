﻿namespace CR.CGC.GuiasScouts.BackEnd.Tasks
{
    using CR.CGC.GuiasScouts.BackEnd.Models;
    using DALBase;
    using Entities;
    using Newtonsoft.Json;
    using Quartz;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using static System.Net.HttpStatusCode;

    public class ScheduledMails : IJob
    {
        Func<string, string, Dictionary<string, object>, (HttpStatusCode, string, ERContainer)> ReadMethods;
        /// <summary>
        /// Accion de la tarea programada para realizar el envio de correos por defecto del sistema: Cumpleaños proximos, Cambio en el cargo del miembro
        /// <para>La tarea programada se ejecuta todos los dias a las 8 AM</para>
        /// </summary>
        /// <param name="context">Contexto de la tarea programada</param>
        public void Execute(IJobExecutionContext context)
        {
            ReadMethods = DALBas.GetCustomProcedure;

            var (ResponseCode, ResponseText, ResponseObject) = ReadMethods("Member", "GetBirthdayOfTheDayProcedure", new Dictionary<string, object>() { { "Model", "Member" } });
            if (ResponseCode == OK)
            {
                ERContainer container = JsonConvert.DeserializeObject<ERContainer>(Regex.Unescape(ResponseText));
                foreach (Member MemberInfo in container.Members)
                {
                    List<MemberAlertSetting> settings = DALBas.GetMemberAlert(MemberInfo.MemberId.Value);
                    MemberAlertSetting setting = settings.Find(f => f.AlertType.Name.Equals("Cumpleaños"));
                    if (setting != null && setting.Sms && !string.IsNullOrEmpty(Utilities.GetPhoneNumber(MemberInfo)))
                        @"El equipo de Guías y Scouts te desea un Feliz Cumpleaños".SendSMS(Utilities.GetPhoneNumber(MemberInfo));
                    if (setting != null && setting.Email)
                    {
                        MailMessage message = new MailMessage()
                        {
                            From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                            IsBodyHtml = true,
                            Subject = "Feliz Cumpleaños"
                        };
                        message.To.Add(MemberInfo.Email);
                        StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/HappyBirthDay.html")));

                        bodyNotificaction = bodyNotificaction.Replace("[USERNAME]", $"{MemberInfo.Names} {MemberInfo.Surnames}");
                        bodyNotificaction = bodyNotificaction.Replace("[HREF]", Utilities.UrlWebSite);
                        bodyNotificaction = bodyNotificaction.Replace("[YEAR]", DateTime.Now.ToString("yyyy"));
                        message.Body = bodyNotificaction.ToString();
                        var (ResponseEmailCode, ResponseEmailText) = message.SendEmail();
                    }
                }
            }
        }

        private string MapPath(string filePath)
        {
            string temp;
            filePath = filePath.Replace('/', '\\');
            var hostingRoot = System.Web.Hosting.HostingEnvironment.IsHosted

            ? System.Web.Hosting.HostingEnvironment.MapPath("~/")

            : Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

            temp = hostingRoot + filePath;
            return temp;
        }
    }
}