﻿using Quartz;
using Quartz.Impl;

namespace CR.CGC.GuiasScouts.BackEnd.Tasks
{
    public class ProgrammedTask
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail ScheduleMailsJob = JobBuilder.Create<ScheduledMails>().Build();

            IJobDetail PromiseDateJob = JobBuilder.Create<PromiseDateMails>().Build();

            ITrigger ScheduleMailsTrigger = TriggerBuilder.Create().WithDailyTimeIntervalSchedule(s => s.WithIntervalInHours(24).OnEveryDay().StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(8, 00))).Build();

            ITrigger PromiseDateTrigger = TriggerBuilder.Create().WithDailyTimeIntervalSchedule(s => s.WithIntervalInHours(24).OnEveryDay().StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(6, 00))).Build();

            scheduler.ScheduleJob(ScheduleMailsJob, ScheduleMailsTrigger);

            scheduler.ScheduleJob(PromiseDateJob, PromiseDateTrigger);
        }
    }
}