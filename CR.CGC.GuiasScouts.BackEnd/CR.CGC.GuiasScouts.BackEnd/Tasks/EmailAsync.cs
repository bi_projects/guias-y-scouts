﻿namespace CR.CGC.GuiasScouts.BackEnd.Tasks
{
    using DALBase;
    using System;
    using System.Net;
    using System.Net.Mail;
    using System.Runtime.Remoting.Messaging;

    public class EmailAsync
    {
        private delegate void AsyncMethodCaller(MailMessage message);

        public static void SendEmailAsync(MailMessage message)
        {
            AsyncMethodCaller caller = new AsyncMethodCaller(SendMailInSeperateThread);
            AsyncCallback callbackHandler = new AsyncCallback(AsyncCallback);
            caller.BeginInvoke(message, callbackHandler, null);
        }

        private static void SendMailInSeperateThread(MailMessage message)
        {
            try
            {
                SmtpClient client = new SmtpClient(Utilities.SMTPHost, Utilities.SMTPPort)
                {
                    Credentials = new NetworkCredential(Utilities.SMTPUser, Utilities.SMTPPassword),
                    EnableSsl = true
                };
                client.Timeout = 20000; // 20 second timeout
                client.Send(message);
                client.Dispose();
                message.Dispose();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
        }

        private static void AsyncCallback(IAsyncResult ar)
        {
            try
            {
                AsyncResult result = (AsyncResult)ar;
                AsyncMethodCaller caller = (AsyncMethodCaller)result.AsyncDelegate;
                caller.EndInvoke(ar);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
        }
    }
}