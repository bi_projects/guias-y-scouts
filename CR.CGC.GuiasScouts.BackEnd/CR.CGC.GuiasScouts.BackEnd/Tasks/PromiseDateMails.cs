﻿namespace CR.CGC.GuiasScouts.BackEnd.Tasks
{
    using CR.CGC.GuiasScouts.BackEnd.Models;
    using DALBase;
    using Entities;
    using Newtonsoft.Json;
    using Quartz;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.Script.Serialization;
    using static System.Net.HttpStatusCode;

    public class PromiseDateMails : IJob
    {
        /// <summary>
        /// Accion de la tarea programada para realizar el envio de correos por defecto del sistema: Cumpleaños proximos, Cambio en el cargo del miembro
        /// <para>La tarea programada se ejecuta todos los dias a las 8 AM</para>
        /// </summary>
        /// <param name="context">Contexto de la tarea programada</param>
        public void Execute(IJobExecutionContext context)
        {
            var (ResponseCode, ResponseText, DTSet) = DALBas.GetDataSet(new List<System.Data.SqlClient.SqlParameter>(), "Member", "GetDataGreetingMessageAPProcedure");
            if (ResponseCode == OK && DTSet.Tables.Count > 0)
            {
                DataTable table = DTSet.Tables[0];
                foreach (DataRow dtrow in table.Rows)
                {
                    var (ResponseCodeMember, ContainerText) = DALBas.GetObjectsFromModel("Member", new Dictionary<string, object>() { { "MemberId", dtrow["MemberId"] } });
                    Member member = new JavaScriptSerializer().Deserialize<ERContainer>(ContainerText).Members.First();
                    if (member != null)
                    {
                        List<MemberAlertSetting> settings = DALBas.GetMemberAlert(member.MemberId.Value);
                        MemberAlertSetting setting = settings.Find(f => f.AlertType.Name.Equals("Cumpleaños"));
                        if (setting != null && setting.Sms && !string.IsNullOrEmpty(Utilities.GetPhoneNumber(member)))
                            "Su correo ha sido actualizado exitosamente".SendSMS(Utilities.GetPhoneNumber(member));
                        if (setting != null && setting.Email)
                        {
                            MailMessage message = new MailMessage()
                            {
                                From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                                IsBodyHtml = true,
                                Subject = "Aniversario de promesa"
                            };
                            message.To.Add(dtrow["Email"].ToString());
                            StringBuilder bodyNotificaction = new StringBuilder(File.ReadAllText(MapPath("/Content/EmailTemplates/CustomAlert.html")));

                            bodyNotificaction = bodyNotificaction.Replace("[USER]", $"{dtrow["FullName"]}");
                            bodyNotificaction = bodyNotificaction.Replace("[MESSAGE]", $"{dtrow["GreetingMessage"]}");
                            bodyNotificaction = bodyNotificaction.Replace("[HREF]", Utilities.UrlWebSite);
                            bodyNotificaction = bodyNotificaction.Replace("[YEAR]", DateTime.Now.ToString("yyyy"));
                            message.Body = bodyNotificaction.ToString();
                            var (ResponseEmailCode, ResponseEmailText) = message.SendEmail();
                        }
                    }
                }
            }
        }

        private string MapPath(string filePath)
        {
            string temp;
            filePath = filePath.Replace('/', '\\');
            var hostingRoot = System.Web.Hosting.HostingEnvironment.IsHosted

            ? System.Web.Hosting.HostingEnvironment.MapPath("~/")

            : Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

            temp = hostingRoot + filePath;
            return temp;
        }
    }
}