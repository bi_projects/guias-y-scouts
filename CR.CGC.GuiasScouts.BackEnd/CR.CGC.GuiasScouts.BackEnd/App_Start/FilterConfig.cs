﻿using System.Web;
using System.Web.Mvc;

namespace CR.CGC.GuiasScouts.BackEnd
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
