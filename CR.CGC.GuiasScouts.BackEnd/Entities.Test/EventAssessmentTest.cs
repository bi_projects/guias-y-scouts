﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventAssessmentTest
    {
        [TestMethod]
        public void EventAssessmentEntity()
        {
            int? EventAssessmentId = 1;
            int MemberId = 1;
            int EventId = 1;
            decimal Assessment = Convert.ToDecimal(2.5);
            bool Enabled = true;

            var entidad = new EventAssessment()
            {
                EventAssessmentId = EventAssessmentId,
                MemberId = MemberId,
                EventId = EventId,
                Assessment = Assessment,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventAssessmentId, EventAssessmentId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.Assessment, Assessment);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
