﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class AlertTypeTest
    {
        [TestMethod]
        public void AlertTypeEntity()
        {
            int? AlertTypeId = 1;
            string Name = "TestText";
            string Description = "TestText";
            bool Other = true;
            bool Enabled = true;

            var entidad = new AlertType()
            {
                AlertTypeId = AlertTypeId,
                Name = Name,
                Description = Description,
                Other = Other,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.AlertTypeId, AlertTypeId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Other, Other);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
