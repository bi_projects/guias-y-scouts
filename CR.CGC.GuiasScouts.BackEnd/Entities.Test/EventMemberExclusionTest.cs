﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventMemberExclusionTest
    {
        [TestMethod]
        public void EventMemberExclusionEntity()
        {
            int? EventMemberExclusionId = 1;
            int EventId = 1;
            int MemberId = 1;
            bool Enabled = true;

            var entidad = new EventMemberExclusion()
            {
                EventMemberExclusionId = EventMemberExclusionId,
                EventId = EventId,
                MemberId = MemberId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventMemberExclusionId, EventMemberExclusionId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
