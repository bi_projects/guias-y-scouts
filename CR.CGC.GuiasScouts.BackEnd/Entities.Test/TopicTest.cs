﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class TopicTest
    {
        [TestMethod]
        public void TopicTestEntity()
        {
            int? TopicId = 1;
            int TypeId = 2;
            string Name = "TestText";
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new Topic()
            {
                TopicId = TopicId,
                TypeId = TypeId,
                Name = Name,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.TopicId, TopicId);
            Assert.AreEqual(entidad.TypeId, TypeId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
