﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class PositionTest
    {
        [TestMethod]
        public void PositionEntity()
        {
            int? PositionId = 3;
            string Name = "TestText";
            int Quantity = 2;
            int PositionTypeId = 4;
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new Position()
            {
                PositionId = PositionId,
                Name = Name,
                Quantity = Quantity,
                PositionTypeId = PositionTypeId,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.PositionId, PositionId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Quantity, Quantity);
            Assert.AreEqual(entidad.PositionTypeId, PositionTypeId);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
