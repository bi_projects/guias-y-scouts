﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventMemberInvitationTest
    {
        [TestMethod]
        public void EventMemberInvitationEntity()
        {
            int? EventMemberInvitationId = 1;
            int EventId = 1;
            int MemberId = 1;
            bool Enabled = true;

            var entidad = new EventMemberInvitation()
            {
                EventMemberInvitationId = EventMemberInvitationId,
                EventId = EventId,
                MemberId = MemberId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventMemberInvitationId, EventMemberInvitationId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
