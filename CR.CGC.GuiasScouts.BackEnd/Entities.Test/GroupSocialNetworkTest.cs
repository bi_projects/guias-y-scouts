﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class GroupSocialNetworkTest
    {
        [TestMethod]
        public void GroupSocialNetworkEntity()
        {
            int? GroupSocialNetworkId = 1;
            int GroupId = 2;
            int Type = 3;
            string Nickname = "TestText";
            bool Enabled = true;

            var entidad = new GroupSocialNetwork()
            {
                GroupSocialNetworkId = GroupSocialNetworkId,
                GroupId = GroupId,
                Type = Type,
                Nickname = Nickname,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.GroupSocialNetworkId, GroupSocialNetworkId);
            Assert.AreEqual(entidad.GroupId, GroupId);
            Assert.AreEqual(entidad.Type, Type);
            Assert.AreEqual(entidad.Nickname, Nickname);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
