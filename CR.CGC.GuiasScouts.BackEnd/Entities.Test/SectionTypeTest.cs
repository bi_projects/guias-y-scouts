﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class SectionTypeTest
    {
        [TestMethod]
        public void SectionTypeEntity()
        {
            int? SectionTypeId = 1;
            string Name = "TestText";
            int AgeRangeId = 2;
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new SectionType()
            {
                SectionTypeId = SectionTypeId,
                Name = Name,
                AgeRangeId = AgeRangeId,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.SectionTypeId, SectionTypeId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.AgeRangeId, AgeRangeId);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
