﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class LogbookDetailTest
    {
        [TestMethod]
        public void LogbookDetailEntity()
        {
            int? LogbookDetailId = 1;
            int LogbookId = 1;
            string ActionType = "TestText";
            string ActionSubType = "TestText";
            string Entity = "TestText";
            bool Enabled = true;

            var OldData = new ERContainer()
            {
                Model = "TestText",
                CurrentUser = 1,
                SearchText = "TestText",
                RowCount = 10,
                PageNumber = 10,
                PageSize = 20
            };
            var NewData = new ERContainer()
            {
                Model = "TestText",
                CurrentUser = 1,
                SearchText = "TestText",
                RowCount = 10,
                PageNumber = 10,
                PageSize = 20
            };
            var entidad = new LogbookDetail()
            {
                LogbookDetailId = LogbookDetailId,
                LogbookId = LogbookId,
                ActionType = ActionType,
                ActionSubType = ActionSubType,
                Entity = Entity,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.LogbookDetailId, LogbookDetailId);
            Assert.AreEqual(entidad.LogbookId, LogbookId);
            Assert.AreEqual(entidad.ActionType, ActionType);
            Assert.AreEqual(entidad.ActionSubType, ActionSubType);
            Assert.AreEqual(entidad.Entity, Entity);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
