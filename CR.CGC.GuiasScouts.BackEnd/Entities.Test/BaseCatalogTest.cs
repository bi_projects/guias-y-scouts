﻿using System;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class BaseCatalogTest
    {
        [TestMethod]
        public void BaseCatalogEntity()
        {
            int? BaseCatalogId = 1;
            string Name = "TestText";
            string Description = "TestText";
            int? ItemOf = null;
            bool Enabled = true;
            
            var entidad = new BaseCatalog()
            {
                BaseCatalogId = BaseCatalogId,
                Name = Name,
                ItemOf = ItemOf,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.BaseCatalogId, BaseCatalogId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.ItemOf, ItemOf);
            Assert.AreEqual(entidad.Enabled, Enabled);
            
        }
    }
}
