﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberLanguageTest
    {
        [TestMethod]
        public void MemberLanguageEntity()
        {
            int? MemberLanguageId = 1;
            int MemberId = 3;
            int LanguageId = 2;
            int LanguageReadingId = 4;
            int LanguageWritingId = 4;
            int LanguageConversationId = 4;
            bool Enabled = true;

            var entidad = new MemberLanguage()
            {
                MemberLanguageId = MemberLanguageId,
                MemberId = MemberId,
                LanguageId = LanguageId,
                LanguageReadingId = LanguageReadingId,
                LanguageWritingId = LanguageWritingId,
                LanguageConversationId = LanguageConversationId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberLanguageId, MemberLanguageId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.LanguageId, LanguageId);
            Assert.AreEqual(entidad.LanguageReadingId, LanguageReadingId);
            Assert.AreEqual(entidad.LanguageWritingId, LanguageWritingId);
            Assert.AreEqual(entidad.LanguageConversationId, LanguageConversationId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
