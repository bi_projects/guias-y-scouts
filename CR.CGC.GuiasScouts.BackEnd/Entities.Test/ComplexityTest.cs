﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class ComplexityTest
    {
        [TestMethod]
        public void ComplexityTestEntity()
        {
            int? ComplexityId = 1;
            string Name = "TestText";
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new Complexity()
            {
                ComplexityId = ComplexityId,
                Name = Name,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.ComplexityId, ComplexityId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
