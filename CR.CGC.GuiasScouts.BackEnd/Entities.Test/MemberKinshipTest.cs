﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberKinshipTest
    {
        [TestMethod]
        public void MemberKinshipEntity()
        {
            int? MemberKinshipId = 1;
            int MemberId = 3;
            int KinId = 4;
            int KinshipId = 2;
            bool Enabled = true;

            var entidad = new MemberKinship()
            {
                MemberKinshipId = MemberKinshipId,
                MemberId = MemberId,
                KinId = KinId,
                KinshipId = KinshipId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberKinshipId, MemberKinshipId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.KinId, KinId);
            Assert.AreEqual(entidad.KinshipId, KinshipId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
