﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class RoleCompetenceTest
    {
        [TestMethod]
        public void RoleCompetenceEntity()
        {
            int? RoleCompetenceId = 1;
            int RoleId = 2;
            int CompetenceId = 3;
            int LevelId = 2;
            bool Enabled = true;

            var entidad = new RoleCompetence()
            {
                RoleCompetenceId = RoleCompetenceId,
                RoleId = RoleId,
                CompetenceId = CompetenceId,
                LevelId = LevelId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.RoleCompetenceId, RoleCompetenceId);
            Assert.AreEqual(entidad.RoleId, RoleId);
            Assert.AreEqual(entidad.CompetenceId, CompetenceId);
            Assert.AreEqual(entidad.LevelId, LevelId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
