﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class PublicationAssessmentTest
    {
        [TestMethod]
        public void PublicationAssessmentTestEntity()
        {
            int? PublicationAssessmentId = 1;
            int MemberId = 6;
            int PublicationId = 10;
            decimal Assessment = Convert.ToDecimal(4.3);
            bool Enabled = true;

            var entidad = new PublicationAssessment()
            {
                PublicationAssessmentId = PublicationAssessmentId,
                MemberId = MemberId,
                PublicationId = PublicationId,
                Assessment = Assessment,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.PublicationAssessmentId, PublicationAssessmentId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.PublicationId, PublicationId);
            Assert.AreEqual(entidad.Assessment, Assessment);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
