﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberCompetenceEvaluationTest
    {
        [TestMethod]
        public void MemberCompetenceEvaluationEntity()
        {
            int? MemberCompetenceEvaluationId = 1;
            int MemberId = 3;
            int CompetenceId = 4;
            int LevelId = 5;
            DateTime EvaluationDate = DateTime.Now;
            bool Enabled = true;
            
            var entidad = new MemberCompetenceEvaluation()
            {
              MemberCompetenceEvaluationId = MemberCompetenceEvaluationId,
              MemberId = MemberId,
              CompetenceId = CompetenceId,
              LevelId = LevelId,
              EvaluationDate = EvaluationDate,
              Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberCompetenceEvaluationId, MemberCompetenceEvaluationId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.CompetenceId, CompetenceId);
            Assert.AreEqual(entidad.LevelId, LevelId);
            Assert.AreEqual(entidad.EvaluationDate, EvaluationDate);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
