﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberPhoneNumberTest
    {
        [TestMethod]
        public void MemberPhoneNumberEntity()
        {
            int? MemberPhoneNumberId = 1;
            int MemberId = 2;
            string PhoneNumber = "TestText";
            bool Enabled = true;

            var entidad = new  MemberPhoneNumber()
            {
                MemberPhoneNumberId = MemberPhoneNumberId,
                MemberId = MemberId,
                PhoneNumber = PhoneNumber,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberPhoneNumberId, MemberPhoneNumberId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.PhoneNumber, PhoneNumber);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
