﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberAlertTest
    {
        [TestMethod]
        public void MemberAlertEntity()
        {
            int? MemberAlertId = 1;
            int MemberId = 1;
            int AlertTypeId = 1;
            string Message = "TestText";
            string Url = "TestText";
            string SMSMessage = "TestText";
            DateTime CreationDate = DateTime.Now;
            bool Received = true;
            DateTime? DateReceived = DateTime.Now;
            bool Reading = true;
            DateTime? ReadingDate = DateTime.Now;
            bool SmsSent = true;
            DateTime? DateSendingSms = DateTime.Now;
            DateTime DueDate = DateTime.Now;
            bool Enabled = true;

            var entidad = new MemberAlert()
            {
                MemberAlertId = MemberAlertId,
                MemberId = MemberId,
                AlertTypeId = AlertTypeId,
                Message = Message,
                Url = Url,
                SMSMessage = SMSMessage,
                CreationDate = CreationDate,
                Received = Received,
                DateReceived = DateReceived,
                Reading = Reading,
                ReadingDate = ReadingDate,
                SmsSent = SmsSent,
                DateSendingSms = DateSendingSms,
                DueDate = DueDate,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberAlertId, MemberAlertId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.AlertTypeId, AlertTypeId);
            Assert.AreEqual(entidad.Message, Message);
            Assert.AreEqual(entidad.Url, Url);
            Assert.AreEqual(entidad.SMSMessage, SMSMessage);
            Assert.AreEqual(entidad.CreationDate, CreationDate);
            Assert.AreEqual(entidad.Received, Received);
            Assert.AreEqual(entidad.DateReceived, DateReceived);
            Assert.AreEqual(entidad.Reading, Reading);
            Assert.AreEqual(entidad.ReadingDate, ReadingDate);
            Assert.AreEqual(entidad.SmsSent, SmsSent);
            Assert.AreEqual(entidad.DateSendingSms, DateSendingSms);
            Assert.AreEqual(entidad.DueDate, DueDate);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
