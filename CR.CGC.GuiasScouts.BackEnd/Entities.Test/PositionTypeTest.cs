﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class PositionTypeTest
    {
        [TestMethod]
        public void PositionTypeEntity()
        {
            int? PositionTypeId = 1;
            string Name = "TestText";
            int CategoryId = 2;
            int GenderId = 3;
            int AgeRangeId = 2;
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new PositionType()
            {
                PositionTypeId = PositionTypeId,
                Name = Name,
                CategoryId = CategoryId,
                GenderId = GenderId,
                AgeRangeId = AgeRangeId,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.PositionTypeId, PositionTypeId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.CategoryId, CategoryId);
            Assert.AreEqual(entidad.GenderId, GenderId);
            Assert.AreEqual(entidad.AgeRangeId, AgeRangeId);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
