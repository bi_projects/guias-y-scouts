﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class ProvinceTest
    {
        [TestMethod]
        public void ProvinceEntity()
        {
            int? ProvinceId = 1;
            string Name = "TestText";
            string PostalCode = "TestText";
            bool Enabled = true;
            
            var entidad = new Province()
            {
                ProvinceId = ProvinceId,
                Name = Name,
                PostalCode = PostalCode,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.ProvinceId, ProvinceId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.PostalCode, PostalCode);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
