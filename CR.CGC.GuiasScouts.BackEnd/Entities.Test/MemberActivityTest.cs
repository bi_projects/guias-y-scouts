﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberActivityTest
    {
        [TestMethod]
        public void MemberActivityEntity()
        {
            int? MemberActivityId = 3;
            int MemberId = 2;
            int ActivityId = 15;
            DateTime ActivityDate = DateTime.Now;
            bool Enabled = true;

            var entidad = new MemberActivity()
            {
                MemberActivityId = MemberActivityId,
                MemberId = MemberId,
                ActivityId = ActivityId,
                ActivityDate = ActivityDate,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberActivityId, MemberActivityId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.ActivityId, ActivityId);
            Assert.AreEqual(entidad.ActivityDate, ActivityDate);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
