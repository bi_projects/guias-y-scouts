﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventSectionInvitationTest
    {
        [TestMethod]
        public void EventSectionInvitationEntity()
        {
            int? EventSectionInvitationId = 1;
            int EventId = 1;
            int SectionId = 1;
            bool Enabled = true;

            var entidad = new EventSectionInvitation()
            {
                EventSectionInvitationId = EventSectionInvitationId,
                EventId = EventId,
                SectionId = SectionId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventSectionInvitationId, EventSectionInvitationId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.SectionId, SectionId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
