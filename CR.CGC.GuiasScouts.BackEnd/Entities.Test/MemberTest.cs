﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberTest
    {
        [TestMethod]
        public void MemberEntity()
        {
            int? MemberId = 10;
            string Names = "TestText";
            string Surnames = "TestText";
            string Photo = "TestText";
            int IDType = 2;
            string IDNumber = "TestText";
            int NationalityId = 2;
            DateTime BirthDate = DateTime.Now.AddYears(-4);
            int GenderId = 1;
            DateTime StartDate = DateTime.Now;
            DateTime? PromiseDate = null;
            string Email = "TestText";
            int? UserId = null;
            bool IsGorS = false;
            bool IsTutor = false;
            DateTime? RegistrationDate = null;
            int MemberStatusId = 4;
            string Address = "TestText";
            int DistrictId = 3;
            string PostalCode = "TestText";
            int? ReligionId = 1;
            int? EducationLevelId = 2;
            int? SchoolId = 1;
            int? ProfessionId = 4;
            string Workplace = "TestText";
            bool Enabled = true;
            //---------------------------------------

            var entidad = new Member()
            {
                MemberId = MemberId,
                Names = Names,
                Surnames = Surnames,
                Photo = Photo,
                IDType = IDType,
                IDNumber = IDNumber,
                NationalityId = NationalityId,
                BirthDate = BirthDate,
                GenderId = GenderId,
                StartDate = StartDate,
                PromiseDate = PromiseDate,
                Email = Email,
                UserId = UserId,
                IsGorS = IsGorS,
                IsTutor = IsTutor,
                RegistrationDate = RegistrationDate,
                MemberStatusId = MemberStatusId,
                Address = Address,
                DistrictId = DistrictId,
                PostalCode = PostalCode,
                ReligionId = ReligionId,
                EducationLevelId = EducationLevelId,
                SchoolId = SchoolId,
                ProfessionId = ProfessionId,
                Workplace = Workplace,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.Names, Names);
            Assert.AreEqual(entidad.Surnames, Surnames);
            Assert.AreEqual(entidad.Photo, Photo);
            Assert.AreEqual(entidad.IDType, IDType);
            Assert.AreEqual(entidad.IDNumber, IDNumber);
            Assert.AreEqual(entidad.NationalityId, NationalityId);
            Assert.AreEqual(entidad.BirthDate, BirthDate);
            Assert.AreEqual(entidad.GenderId, GenderId);
            Assert.AreEqual(entidad.StartDate, StartDate);
            Assert.AreEqual(entidad.PromiseDate, PromiseDate);
            Assert.AreEqual(entidad.Email, Email);
            Assert.AreEqual(entidad.UserId, UserId);
            Assert.AreEqual(entidad.IsGorS, IsGorS);
            Assert.AreEqual(entidad.IsTutor, IsTutor);
            Assert.AreEqual(entidad.RegistrationDate, RegistrationDate);
            Assert.AreEqual(entidad.MemberStatusId, MemberStatusId);
            Assert.AreEqual(entidad.Address, Address);
            Assert.AreEqual(entidad.DistrictId, DistrictId);
            Assert.AreEqual(entidad.PostalCode, PostalCode);
            Assert.AreEqual(entidad.ReligionId, ReligionId);
            Assert.AreEqual(entidad.EducationLevelId, EducationLevelId);
            Assert.AreEqual(entidad.SchoolId, SchoolId);
            Assert.AreEqual(entidad.ProfessionId, ProfessionId);
            Assert.AreEqual(entidad.Workplace, Workplace);
            Assert.AreEqual(entidad.Enabled, Enabled);         
        }
    }
}
