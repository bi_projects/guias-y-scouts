﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberInterestThemeTest
    {
        [TestMethod]
        public void MemberInterestThemeEntity()
        {
            int? MemberInterestThemeId = 3;
            int MemberId = 2;
            int InterestThemeId = 3;
            bool Enabled = true;

            var entidad = new MemberInterestTheme()
            {
                MemberInterestThemeId = MemberInterestThemeId,
                MemberId = MemberId,
                InterestThemeId = InterestThemeId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberInterestThemeId, MemberInterestThemeId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.InterestThemeId, InterestThemeId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
