﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventGroupInvitationTest
    {
        [TestMethod]
        public void EventGroupInvitationEntity()
        {
            int? EventGroupInvitationId = 1;
            int EventId = 1;
            int GroupId = 1;
            bool Enabled = true;

            var entidad = new EventGroupInvitation()
            {
                EventGroupInvitationId = EventGroupInvitationId,
                EventId = EventId,
                GroupId = GroupId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventGroupInvitationId, EventGroupInvitationId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.GroupId, GroupId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
