﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventPositionExclusionTest
    {
        [TestMethod]
        public void EventPositionExclusionEntity()
        {
            int? EventPositionExclusionId = 1;
            int EventId = 1;
            int PositionId = 1;
            bool Enabled = true;

            var entidad = new EventPositionExclusion()
            {
                EventPositionExclusionId = EventPositionExclusionId,
                EventId = EventId,
                PositionId = PositionId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventPositionExclusionId, EventPositionExclusionId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.PositionId, PositionId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
