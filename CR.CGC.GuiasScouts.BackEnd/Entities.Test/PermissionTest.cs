﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class PermissionTest
    {
        [TestMethod]
        public void PermissionEntity()
        {
            int? PermissionId = 1;
            int RoleId = 2;
            string Controller = "TestText";
            string Action = "TestText";
            string Description = "TestText";
            bool ToSave = false;

            var entidad = new Permission()
            {
                PermissionId = PermissionId,
                RoleId = RoleId,
                Controller = Controller,
                Action = Action,
                Description = Description,
                ToSave = ToSave
            };

            Assert.AreEqual(entidad.PermissionId, PermissionId);
            Assert.AreEqual(entidad.RoleId, RoleId);
            Assert.AreEqual(entidad.Controller, Controller);
            Assert.AreEqual(entidad.Action, Action);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.ToSave, ToSave);
        }
    }
}
