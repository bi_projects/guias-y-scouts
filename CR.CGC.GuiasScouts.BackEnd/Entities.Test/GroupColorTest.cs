﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class GroupColorTest
    {
        [TestMethod]
        public void GroupColorEntity()
        {
            int? GroupColorId = 1;
            int GroupId = 2;
            string Color = "TestText";
            bool Enabled = true;

            var entidad = new GroupColor()
            {
                GroupColorId = GroupColorId,
                GroupId = GroupId,
                Color = Color,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.GroupColorId, GroupColorId);
            Assert.AreEqual(entidad.GroupId, GroupId);
            Assert.AreEqual(entidad.Color, Color);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
