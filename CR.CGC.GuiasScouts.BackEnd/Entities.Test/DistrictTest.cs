﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class DistrictTest
    {
        [TestMethod]
        public void DistrictEntity()
        {
            int? DistrictId = 1;
            int CantonId = 2;
            string Name = "TestText";
            string PostalCode = "TestText";
            bool Enabled = true;

            var entidad = new District()
            {
                DistrictId = DistrictId,
                CantonId = CantonId,
                Name = Name,
                PostalCode = PostalCode,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.DistrictId, DistrictId);
            Assert.AreEqual(entidad.CantonId, CantonId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.PostalCode, PostalCode);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
