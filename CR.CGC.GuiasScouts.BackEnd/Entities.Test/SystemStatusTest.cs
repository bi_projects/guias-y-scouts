﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class SystemStatusTest
    {
        [TestMethod]
        public void SystemStatusEntity()
        {
            int? SystemStatusId = 1;
            string Name = "TestText";
            string Description = "TestText";
            string Color = "TestText";
            bool Enabled = true;

            var entidad = new SystemStatus()
            {
                SystemStatusId = SystemStatusId,
                Name = Name,
                Description = Description,
                Color = Color,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.SystemStatusId, SystemStatusId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Color, Color);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
