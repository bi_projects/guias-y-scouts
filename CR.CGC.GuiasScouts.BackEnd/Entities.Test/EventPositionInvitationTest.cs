﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventPositionInvitationTest
    {
        [TestMethod]
        public void EventPositionInvitationEntity()
        {
            int? EventPositionInvitationId = 1;
            int EventId = 1;
            int PositionId = 1;
            bool Enabled = true;

            var entidad = new EventPositionInvitation()
            {
                EventPositionInvitationId = EventPositionInvitationId,
                EventId = EventId,
                PositionId = PositionId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventPositionInvitationId, EventPositionInvitationId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.PositionId, PositionId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
