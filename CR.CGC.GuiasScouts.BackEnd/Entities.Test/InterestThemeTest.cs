﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class InterestThemeTest
    {
        [TestMethod]
        public void InterestThemeEntity()
        {
            int? InterestThemeId = 3;
            string Name = "TestText";
            string FileLocation = "TestText";
            string BackgroundFileLocation = "TestText";
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new InterestTheme()
            {
                InterestThemeId = InterestThemeId,
                Name = Name,
                FileLocation = FileLocation,
                BackgroundFileLocation = BackgroundFileLocation,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.InterestThemeId, InterestThemeId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.FileLocation, FileLocation);
            Assert.AreEqual(entidad.BackgroundFileLocation, BackgroundFileLocation);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
