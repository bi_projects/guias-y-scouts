﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberGroupTest
    {
        [TestMethod]
        public void MemberGroupEntity()
        {
            int? MemberGroupId = 2;
            int MemberId = 5;
            int GroupId = 3;
            int SectionId = 2;
            int ProgressionId = 2;
            DateTime StartDate = DateTime.Now;
            DateTime? EndingDate = DateTime.Now;
            bool Enabled = true;

            var entidad = new MemberGroup()
            {
                MemberGroupId = MemberGroupId,
                MemberId = MemberId,
                GroupId = GroupId,
                SectionId = SectionId,
                ProgressionId = ProgressionId,
                StartDate = StartDate,
                EndingDate = EndingDate,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberGroupId, MemberGroupId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.GroupId, GroupId);
            Assert.AreEqual(entidad.SectionId, SectionId);
            Assert.AreEqual(entidad.ProgressionId, ProgressionId);
            Assert.AreEqual(entidad.StartDate, StartDate);
            Assert.AreEqual(entidad.EndingDate, EndingDate);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
