﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class CompetenceTest
    {
        [TestMethod]
        public void CompetenceEntity()
        {
            int? CompetenceId = 1;
            string Name = "TestText";
            int Type = 2;
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new Competence()
            {
                CompetenceId = CompetenceId,
                Name = Name,
                Type = Type,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.CompetenceId, CompetenceId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Type, Type);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
