﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class PriorityTest
    {
        [TestMethod]
        public void PriorityTestEntity()
        {
            int? PriorityId = 1;
            string Name = "TestText";
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new Priority()
            {
                PriorityId = PriorityId,
                Name = Name,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.PriorityId, PriorityId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
