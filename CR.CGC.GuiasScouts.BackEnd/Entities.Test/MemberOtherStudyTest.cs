﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberOtherStudyTest
    {
        [TestMethod]
        public void MemberOtherStudyEntity()
        {
            int? MemberOtherStudyId = 1;
            int MemberId = 2;
            string StudyName = "TestText";
            bool Enabled = true;

            var entidad = new MemberOtherStudy()
            {
                MemberOtherStudyId = MemberOtherStudyId,
                MemberId = MemberId,
                StudyName = StudyName,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberOtherStudyId, MemberOtherStudyId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.StudyName, StudyName);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
