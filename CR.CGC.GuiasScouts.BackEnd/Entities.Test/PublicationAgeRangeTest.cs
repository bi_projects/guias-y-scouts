﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class PublicationAgeRangeTest
    {
        [TestMethod]
        public void PublicationAgeRangeEntity()
        {
            int? PublicationAgeRangeId = 1;
            int PublicationId = 1;
            int AgeRangeId = 1;
            bool Enabled = true;

            var entidad = new PublicationAgeRange()
            {
                PublicationAgeRangeId = PublicationAgeRangeId,
                PublicationId = PublicationId,
                AgeRangeId = AgeRangeId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.PublicationAgeRangeId, PublicationAgeRangeId);
            Assert.AreEqual(entidad.PublicationId, PublicationId);
            Assert.AreEqual(entidad.AgeRangeId, AgeRangeId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
