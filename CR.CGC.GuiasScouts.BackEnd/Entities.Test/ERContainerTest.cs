﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class ERContainerTest
    {
        [TestMethod]
        public void ERContainerEntity()
        {
            string Model = "TestText";
            int CurrentUser = 1;
            string SearchText = "TestText";
            int RowCount = 10;
            int PageNumber = 10;
            int PageSize = 20;

            var entidad = new ERContainer()
            {
                Model = Model,
                CurrentUser = CurrentUser,
                SearchText = SearchText,
                RowCount = RowCount,
                PageNumber = PageNumber,
                PageSize = PageSize
            };

            Assert.AreEqual(entidad.Model, Model);
            Assert.AreEqual(entidad.CurrentUser, CurrentUser);
            Assert.AreEqual(entidad.SearchText, SearchText);
            Assert.AreEqual(entidad.RowCount, RowCount);
            Assert.AreEqual(entidad.PageNumber, PageNumber);
            Assert.AreEqual(entidad.PageSize, PageSize);
        }
    }
}
