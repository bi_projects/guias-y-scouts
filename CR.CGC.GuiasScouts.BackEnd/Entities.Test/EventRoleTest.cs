﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventRoleTest
    {
        [TestMethod]
        public void EventRoleEntity()
        {
            int? EventRoleId = 1;
            int EventId = 1;
            int RoleId = 1;
            bool Enabled = true;

            var entidad = new EventRole()
            {
                EventRoleId = EventRoleId,
                EventId = EventId,
                RoleId = RoleId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventRoleId, EventRoleId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.RoleId, RoleId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
