﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class RequestStatusTest
    {
        [TestMethod]
        public void RequestStatusEntity()
        {
            int? RequestStatusId = 1;
            string Name = "TestText";
            string Description = "TestText";
            string Color = "#F000000";
            bool Enabled = true;

            var entidad = new RequestStatus()
            {
                RequestStatusId = RequestStatusId,
                Name = Name,
                Description = Description,
                Color = Color,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.RequestStatusId, RequestStatusId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Color, Color);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
