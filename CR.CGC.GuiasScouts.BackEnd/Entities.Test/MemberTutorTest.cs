﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberTutorTest
    {
        [TestMethod]
        public void MemberTutorEntity()
        {
            int? MemberTutorId = 1;
            int MemberId = 2;
            int TutorId = 2;
            bool Enabled = false;

            var entidad = new MemberTutor()
            {
                MemberTutorId = MemberTutorId,
                MemberId = MemberId,
                TutorId = TutorId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberTutorId, MemberTutorId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.TutorId, TutorId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
