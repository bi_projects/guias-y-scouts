﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventAgeRangeTest
    {
        [TestMethod]
        public void EventAgeRangeEntity()
        {
            int? EventAgeRangeId = 1;
            int EventId = 1;
            int AgeRangeId = 1;
            bool Enabled = true;

            var entidad = new EventAgeRange()
            {
                EventAgeRangeId = EventAgeRangeId,
                EventId = EventId,
                AgeRangeId = AgeRangeId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventAgeRangeId, EventAgeRangeId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.AgeRangeId, AgeRangeId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
