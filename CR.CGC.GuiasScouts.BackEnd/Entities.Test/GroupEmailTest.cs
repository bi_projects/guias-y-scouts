﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class GroupEmailTest
    {
        [TestMethod]
        public void GroupEmailEntity()
        {
            int? GroupEmailId = 1;
            int GroupId = 2;
            string Email = "TestText";
            bool Enabled = true;

            var entidad = new GroupEmail()
            {
                GroupEmailId = GroupEmailId,
                GroupId = GroupId,
                Email = Email,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.GroupEmailId, GroupEmailId);
            Assert.AreEqual(entidad.GroupId, GroupId);
            Assert.AreEqual(entidad.Email, Email);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
