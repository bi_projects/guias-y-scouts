﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberAlertSettingTest
    {
        [TestMethod]
        public void MemberAlertSettingEntity()
        {
            int? MemberAlertSettingId = 1;
            int MemberId = 2;
            int AlertTypeId = 1;
            bool Sms = false;
            bool Email = true;
            bool Portal = true;
            bool Enabled = true;

            var entidad = new MemberAlertSetting()
            {
                MemberAlertSettingId = MemberAlertSettingId,
                MemberId = MemberId,
                AlertTypeId = AlertTypeId,
                Sms = Sms,
                Email = Email,
                Portal = Portal,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberAlertSettingId, MemberAlertSettingId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.AlertTypeId, AlertTypeId);
            Assert.AreEqual(entidad.Sms, Sms);
            Assert.AreEqual(entidad.Email, Email);
            Assert.AreEqual(entidad.Portal, Portal);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
