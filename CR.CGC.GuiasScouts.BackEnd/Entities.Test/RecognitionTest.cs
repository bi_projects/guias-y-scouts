﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class RecognitionTest
    {
        [TestMethod]
        public void RecognitionEntity()
        {
            int? RecognitionId = 1;
            string Name = "TestText";
            string Description = "TestText";
            string FileLocation = "TestText";
            int SystemStatusId = 2;
            bool Enabled = true;

            var entidad = new Recognition()
            {
                RecognitionId = RecognitionId,
                Name = Name,
                Description = Description,
                FileLocation = FileLocation,
                SystemStatusId = SystemStatusId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.RecognitionId, RecognitionId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.FileLocation, FileLocation);
            Assert.AreEqual(entidad.SystemStatusId, SystemStatusId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
