﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberNationalEventTest
    {
        [TestMethod]
        public void MemberNationalEventEntity()
        {
            int? MemberNationalEventId = 1;
            int MemberId = 2;
            int NationalEventId = 2;
            DateTime NationalEventDate = DateTime.Now;
            bool Enabled = true;

            var entidad = new MemberNationalEvent()
            {
                MemberNationalEventId = MemberNationalEventId,
                MemberId = MemberId,
                NationalEventId = NationalEventId,
                NationalEventDate = NationalEventDate,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberNationalEventId, MemberNationalEventId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.NationalEventId, NationalEventId);
            Assert.AreEqual(entidad.NationalEventDate, NationalEventDate);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
