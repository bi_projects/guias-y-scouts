﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventSectionExclusionTest
    {
        [TestMethod]
        public void EventSectionExclusionEntity()
        {
            int? EventSectionExclusionId = 1;
            int EventId = 1;
            int SectionId = 1;
            bool Enabled = true;

            var entidad = new EventSectionExclusion()
            {
                EventSectionExclusionId = EventSectionExclusionId,
                EventId = EventId,
                SectionId = SectionId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventSectionExclusionId, EventSectionExclusionId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.SectionId, SectionId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
