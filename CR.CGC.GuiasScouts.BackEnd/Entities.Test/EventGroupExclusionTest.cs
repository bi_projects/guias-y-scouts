﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventGroupExclusionTest
    {
        [TestMethod]
        public void EventGroupExclusionEntity()
        {
            int? EventGroupExclusionId = 1;
            int EventId = 1;
            int GroupId = 1;
            bool Enabled = true;

            var entidad = new EventGroupExclusion()
            {
                EventGroupExclusionId = EventGroupExclusionId,
                EventId = EventId,
                GroupId = GroupId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventGroupExclusionId, EventGroupExclusionId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.GroupId, GroupId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
