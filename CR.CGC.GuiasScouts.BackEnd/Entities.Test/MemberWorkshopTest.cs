﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberWorkshopTest
    {
        [TestMethod]
        public void MemberWorkshopEntity()
        {
            int? MemberWorkshopId = 1;
            int MemberId = 3;
            int WorkshopId = 4;
            DateTime WorkshopDate = DateTime.Now;
            bool Enabled = true;

            var entidad = new MemberWorkshop()
            {
                MemberWorkshopId = MemberWorkshopId,
                MemberId = MemberId,
                WorkshopId = WorkshopId,
                WorkshopDate = WorkshopDate,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberWorkshopId, MemberWorkshopId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.WorkshopId, WorkshopId);
            Assert.AreEqual(entidad.WorkshopDate, WorkshopDate);
            Assert.AreEqual(entidad.Enabled, Enabled);

        }
    }
}
