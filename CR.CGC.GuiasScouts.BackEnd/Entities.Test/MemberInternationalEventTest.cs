﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberInternationalEventTest
    {
        [TestMethod]
        public void MemberInternationalEventEntity()
        {
            int? MemberInternationalEventId = 2;
            int MemberId = 1;
            int InternationalEventId = 3;
            DateTime InternationalEventDate = DateTime.Now;
            bool Enabled = true;

            var entidad = new MemberInternationalEvent()
            {
                MemberInternationalEventId = MemberInternationalEventId,
                MemberId = MemberId,
                InternationalEventId = InternationalEventId,
                InternationalEventDate = InternationalEventDate,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberInternationalEventId, MemberInternationalEventId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.InternationalEventId, InternationalEventId);
            Assert.AreEqual(entidad.InternationalEventDate, InternationalEventDate);
            Assert.AreEqual(entidad.Enabled, Enabled);

        }
    }
}

