﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class RoleTest
    {
        [TestMethod]
        public void RoleEntity()
        {
            int? RoleId = 1;
            string Name = "TestText";
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new Role()
            {
                RoleId = RoleId,
                Name = Name,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.RoleId, RoleId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
            
        }
    }
}
