﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class RequestTest
    {
        [TestMethod]
        public void RequestEntity()
        {
            int? RequestId = 1;
            int TopicId = 1;
            string Name = "TestText";
            string Description = "TestText";
            DateTime CreationDate = DateTime.Now;
            DateTime? AttentionDate = DateTime.Now;
            int PriorityId = 2;
            int? ComplexityId = 1;
            int RequestingUser = 2;
            int? AssignedTo = 1;
            int RequestStatusId = 2;
            int Progress = 3;
            int Qualification = 2;
            string QualifyingComment = "TestText";
            bool Enabled = true;

            var entidad = new Request()
            {
                RequestId = RequestId,
                TopicId = TopicId,
                Name = Name,
                Description = Description,
                CreationDate = CreationDate,
                AttentionDate = AttentionDate,
                PriorityId = PriorityId,
                ComplexityId = ComplexityId,
                RequestingUser = RequestingUser,
                AssignedTo = AssignedTo,
                RequestStatusId = RequestStatusId,
                Progress = Progress,
                Qualification = Qualification,
                QualifyingComment = QualifyingComment,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.RequestId, RequestId);
            Assert.AreEqual(entidad.TopicId, TopicId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.CreationDate, CreationDate);
            Assert.AreEqual(entidad.AttentionDate, AttentionDate);
            Assert.AreEqual(entidad.PriorityId, PriorityId);
            Assert.AreEqual(entidad.ComplexityId, ComplexityId);
            Assert.AreEqual(entidad.RequestingUser, RequestingUser);
            Assert.AreEqual(entidad.AssignedTo, AssignedTo);
            Assert.AreEqual(entidad.RequestStatusId, RequestStatusId);
            Assert.AreEqual(entidad.Progress, Progress);
            Assert.AreEqual(entidad.Qualification, Qualification);
            Assert.AreEqual(entidad.QualifyingComment, QualifyingComment);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
