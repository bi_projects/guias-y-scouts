﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class GroupTest
    {
        [TestMethod]
        public void GroupEntity()
        {
            int? GroupId = 1;
            string Name = "TestText";
            int Number = 2;
            DateTime FoundationDate = DateTime.Now;
            string HeadkerchiefImage = "TestText";
            string HeadkerchiefMeaning = "TestText";
            string HeadkerchiefDescription = "TestText";
            int? ChiefMember = 2;
            bool HasABuilding = true;
            int DistrictId = 3;
            int LocalId = 2;
            string Address = "TestText";
            int SectorId = 4;
            decimal Latitude = Convert.ToDecimal(11.6);
            decimal Longitude = Convert.ToDecimal(10.8);
            int SystemStatusId = 1;
            bool Enabled = true;

            var entidad = new Group()
            {
                GroupId = GroupId,
                Name = Name,
                Number = Number,
                FoundationDate = FoundationDate,
                HeadkerchiefImage = HeadkerchiefImage,
                HeadkerchiefMeaning = HeadkerchiefMeaning,
                HeadkerchiefDescription = HeadkerchiefDescription,
                ChiefMember = ChiefMember,
                HasABuilding = HasABuilding,
                DistrictId = DistrictId,
                LocalId = LocalId,
                Address = Address,
                SectorId = SectorId,
                Latitude = Latitude,
                Longitude = Longitude,
                SystemStatusId = SystemStatusId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.GroupId, GroupId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Number, Number);
            Assert.AreEqual(entidad.FoundationDate, FoundationDate);
            Assert.AreEqual(entidad.HeadkerchiefImage, HeadkerchiefImage);
            Assert.AreEqual(entidad.HeadkerchiefMeaning, HeadkerchiefMeaning);
            Assert.AreEqual(entidad.HeadkerchiefDescription, HeadkerchiefDescription);
            Assert.AreEqual(entidad.ChiefMember, ChiefMember);
            Assert.AreEqual(entidad.HasABuilding, HasABuilding);
            Assert.AreEqual(entidad.DistrictId, DistrictId);
            Assert.AreEqual(entidad.LocalId, LocalId);
            Assert.AreEqual(entidad.Address, Address);
            Assert.AreEqual(entidad.SectorId, SectorId);
            Assert.AreEqual(entidad.Latitude, Latitude);
            Assert.AreEqual(entidad.Longitude, Longitude);
            Assert.AreEqual(entidad.SystemStatusId, SystemStatusId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
