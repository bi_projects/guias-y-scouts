﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberPositionTest
    {
        [TestMethod]
        public void MemberPositionEntity()
        {
            int? MemberPositionId = 2;
            int MemberId = 5;
            int PositionId = 3;
            bool Enabled = true;

            var entidad = new MemberPosition()
            {
                MemberPositionId = MemberPositionId,
                MemberId = MemberId,
                PositionId = PositionId,
                Enabled  = Enabled
            };

            Assert.AreEqual(entidad.MemberPositionId, MemberPositionId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.PositionId, PositionId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
