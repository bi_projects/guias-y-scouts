﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventStatusTest
    {
        [TestMethod]
        public void EventStatusEntity()
        {
            int? EventStatusId = 1;
            string Name = "TestText";
            string Description = "TestText";
            string Color = "TestText";
            bool Enabled = true;

            var entidad = new EventStatus()
            {
                EventStatusId = EventStatusId,
                Name = Name,
                Description = Description,
                Color = Color,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.EventStatusId, EventStatusId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Color, Color);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
