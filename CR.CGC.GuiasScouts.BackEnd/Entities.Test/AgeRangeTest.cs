﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Entities.Test
{
    [TestClass]
    public class AgeRangeTest
    {
        [TestMethod]
        public void AgeRangeEntity()
        {
            int? AgeRangeId = 1;
            string Name = "TestText";
            string Description = "TestText";
            int MinimumAge = 13;
            int MaximumAge = 23;
            bool Enabled = true;

            var entidad = new AgeRange()
            {
                AgeRangeId = AgeRangeId,
                Name = Name,
                Description = Description,
                MinimumAge = MinimumAge,
                MaximumAge = MaximumAge,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.AgeRangeId, AgeRangeId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.MinimumAge, MinimumAge);
            Assert.AreEqual(entidad.MaximumAge, MaximumAge);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
