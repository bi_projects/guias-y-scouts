﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class TemplateTest
    {
        [TestMethod]
        public void TemplateEntity()
        {
            int? TemplateId = 1;
            string Name = "TestText";
            string Description = "TestText";
            string Body = "TestText";
            bool Enabled = true;

            var entidad = new Template()
            {
                TemplateId = TemplateId,
                Name = Name,
                Description = Description,
                Body = Body,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.TemplateId, TemplateId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Body, Body);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
