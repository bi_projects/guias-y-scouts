﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberStatusTest
    {
        [TestMethod]
        public void MemberStatusEntity()
        {
            int? MemberStatusId = 1;
            string Name = "TestText";
            string Description = "TestText";
            string Color = "TestText";
            bool Enabled = true;

            var entidad = new MemberStatus()
            {
                MemberStatusId = MemberStatusId,
                Name = Name,
                Description = Description,
                Color = Color,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberStatusId, MemberStatusId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Color, Color);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
