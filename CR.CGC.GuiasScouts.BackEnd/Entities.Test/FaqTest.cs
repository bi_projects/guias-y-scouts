﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class FaqTest
    {
        [TestMethod]
        public void FaqEntity()
        {
            int? FaqId = 1;
            int Type = 2;
            string Question = "TestText";
            string Answer = "TestText";
            bool Enabled = true;

            var entidad = new Faq()
            {
                FaqId = FaqId,
                Type = Type,
                Question = Question,
                Answer = Answer,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.FaqId, FaqId);
            Assert.AreEqual(entidad.Type, Type);
            Assert.AreEqual(entidad.Question, Question);
            Assert.AreEqual(entidad.Answer, Answer);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
