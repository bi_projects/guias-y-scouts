﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class CompetenceLevelTest
    {
        [TestMethod]
        public void CompetenceLevelEntity()
        {
            int? LevelId = 1;
            string Name = "TestText";
            int Level = 3;
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new CompetenceLevel()
            {
                LevelId = LevelId,
                Name = Name,
                Level = Level,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.LevelId, LevelId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Level, Level);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
