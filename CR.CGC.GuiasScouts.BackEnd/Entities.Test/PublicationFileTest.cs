﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class PublicationFileTest
    {
        [TestMethod]
        public void PublicationFileEntity()
        {
            int? PublicationFileId = 1;
            int PublicationId = 1;
            string Name = "TestText";
            string Description = "TestText";
            string FileLocation = "TestText";
            bool Enabled = true;

            var entidad = new PublicationFile()
            {
                PublicationFileId = PublicationFileId,
                PublicationId = PublicationId,
                Name = Name,
                Description = Description,
                FileLocation = FileLocation,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.PublicationFileId, PublicationFileId);
            Assert.AreEqual(entidad.PublicationId, PublicationId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.FileLocation, FileLocation);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
