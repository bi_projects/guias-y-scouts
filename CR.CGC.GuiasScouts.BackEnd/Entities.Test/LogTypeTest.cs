﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class LogTypeTest
    {
        [TestMethod]
        public void LogTypeEntity()
        {
            int? LogTypeId = 1;
            string Name = "TestText";
            string Description = "TestText";
            bool Enabled = true;

            var entidad = new LogType()
            {
                LogTypeId = LogTypeId,
                Name = Name,
                Description = Description,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.LogTypeId, LogTypeId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
