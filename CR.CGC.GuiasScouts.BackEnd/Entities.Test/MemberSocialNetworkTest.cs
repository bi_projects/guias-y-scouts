﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberSocialNetworkTest
    {
        [TestMethod]
        public void MemberSocialNetworkEntity()
        {
            int? MemberSocialNetworkId = 1;
            int MemberId = 2;
            int TypeId = 3;
            string Nickname = "TestText";
            bool Enabled = true;

            var entidad = new MemberSocialNetwork()
            {
                MemberSocialNetworkId = MemberSocialNetworkId,
                MemberId = MemberId,
                TypeId = TypeId,
                Nickname = Nickname,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberSocialNetworkId, MemberSocialNetworkId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.TypeId, TypeId);
            Assert.AreEqual(entidad.Nickname, Nickname);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
