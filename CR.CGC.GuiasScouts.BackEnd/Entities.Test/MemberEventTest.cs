﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberEventTest
    {
        [TestMethod]
        public void MemberEventEntity()
        {
            int? MemberEventId = 1;
            int MemberId = 2;
            int EventId = 1;
            bool Enabled = true;

            var entidad = new MemberEvent()
            {
                MemberEventId = MemberEventId,
                MemberId = MemberId,
                EventId = EventId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberEventId, MemberEventId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
