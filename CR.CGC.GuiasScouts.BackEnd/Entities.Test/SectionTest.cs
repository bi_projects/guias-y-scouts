﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class SectionTest
    {
        [TestMethod]
        public void SectionEntity()
        {
            int? SectionId = 1;
            string Name = "TestText";
            int SectionTypeId = 2;
            string Description = "TestText";
            string FileLocation = "TestText";
            int SystemStatusId = 2;
            bool Enabled = true;

            var entidad = new Section()
            {
                SectionId = SectionId,
                Name = Name,
                SectionTypeId = SectionTypeId,
                Description = Description,
                FileLocation = FileLocation,
                SystemStatusId = SystemStatusId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.SectionId, SectionId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.SectionTypeId, SectionTypeId);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.FileLocation, FileLocation);
            Assert.AreEqual(entidad.SystemStatusId, SystemStatusId);
            Assert.AreEqual(entidad.Enabled, Enabled);

        }
    }
}
