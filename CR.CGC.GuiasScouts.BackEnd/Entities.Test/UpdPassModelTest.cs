﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class UpdPassModelTest
    {
        [TestMethod]
        public void UpdPassModelEntity()
        {
            string Email = "TestText";
            string UserName = "TestText";
            string CurrentPassword = "TestText";
            string NewPassword = "TestText";

            var entidad = new UpdPassModel()
            {
                Email = Email,
                UserName = UserName,
                CurrentPassword = CurrentPassword,
                NewPassword = NewPassword
            };

            Assert.AreEqual(entidad.Email, Email);
            Assert.AreEqual(entidad.UserName, UserName);
            Assert.AreEqual(entidad.CurrentPassword, CurrentPassword);
            Assert.AreEqual(entidad.NewPassword, NewPassword);
        }
    }
}
