﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class RequestFileTest
    {
        [TestMethod]
        public void RequestFileEntity()
        {
            int? RequestFileId = 1;
            int RequestId = 1;
            string Name = "TestText";
            string Description = "TestText";
            string FileLocation = "TestText";
            bool Enabled = true;

            var entidad = new RequestFile()
            {
                RequestFileId = RequestFileId,
                RequestId = RequestId,
                Name = Name,
                Description = Description,
                FileLocation = FileLocation,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.RequestFileId, RequestFileId);
            Assert.AreEqual(entidad.RequestId, RequestId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.FileLocation, FileLocation);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
