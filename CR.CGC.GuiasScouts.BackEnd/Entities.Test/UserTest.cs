﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class UserTest
    {
        [TestMethod]
        public void UserTestEntity()
        {
            int UserId = 1;
            int? RoleId = 2;
            string UserName = "TestText";
            string Email = "TestText";
            string Password = "TestText";
            DateTime? CurrentAccess = null;
            string CurrentIp = "TestText";
            string PasswordResetToken = "TestText";
            DateTime? DateLastAccess = null;
            string LastIp = "TestText";
            string Theme = "TestText";
            int? FontSize = 2;
            bool Active = false;
            bool Enabled = true;
            DateTime CreationDate = DateTime.Now;
            DateTime? ModifiedDate = null;
            bool IsFirstLogin = true;

            var entidad = new User()
            {
                UserId = UserId,
                RoleId = RoleId,
                UserName = UserName,
                Email = Email,
                Password = Password,
                CurrentAccess = CurrentAccess,
                CurrentIp = CurrentIp,
                PasswordResetToken = PasswordResetToken,
                DateLastAccess = DateLastAccess,
                LastIp = LastIp,
                Theme = Theme,
                FontSize = FontSize,
                Active = Active,
                Enabled = Enabled,
                CreationDate = CreationDate,
                ModifiedDate = ModifiedDate,
                IsFirstLogin = IsFirstLogin
            };

            Assert.AreEqual(entidad.UserId, UserId);
            Assert.AreEqual(entidad.RoleId, RoleId);
            Assert.AreEqual(entidad.UserName, UserName);
            Assert.AreEqual(entidad.Email, Email);
            Assert.AreEqual(entidad.Password, Password);
            Assert.AreEqual(entidad.CurrentAccess, CurrentAccess);
            Assert.AreEqual(entidad.CurrentIp, CurrentIp);
            Assert.AreEqual(entidad.PasswordResetToken, PasswordResetToken);
            Assert.AreEqual(entidad.DateLastAccess, DateLastAccess);
            Assert.AreEqual(entidad.LastIp, LastIp);
            Assert.AreEqual(entidad.Theme, Theme);
            Assert.AreEqual(entidad.FontSize, FontSize);
            Assert.AreEqual(entidad.Active, Active);
            Assert.AreEqual(entidad.Enabled, Enabled);
            Assert.AreEqual(entidad.CreationDate, CreationDate);
            Assert.AreEqual(entidad.ModifiedDate, ModifiedDate);
            Assert.AreEqual(entidad.IsFirstLogin, IsFirstLogin);
        }
    }
}
