﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberCourseTest
    {
        [TestMethod]
        public void MemberCourseEntity()
        {
            int? MemberCourseId = 5;
            int MemberId = 3;
            int CourseId = 2;
            DateTime CourseDate = DateTime.Now;
            bool Enabled = true;

            var entidad = new MemberCourse()
            {
                MemberCourseId = MemberCourseId,
                MemberId = MemberId,
                CourseId = CourseId,
                CourseDate = CourseDate,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberCourseId, MemberCourseId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.CourseId, CourseId);
            Assert.AreEqual(entidad.CourseDate, CourseDate);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
