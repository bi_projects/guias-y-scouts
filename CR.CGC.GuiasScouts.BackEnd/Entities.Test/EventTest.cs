﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EventTest
    {
        [TestMethod]
        public void EventEntity()
        {
            int? EventId = 1;
            string Name = "TestText";
            string EventImage = "TestText";
            string Description = "TestText";
            DateTime StartDateTime = DateTime.Now;
            DateTime EndDateTime = DateTime.Now;
            int QuotaLimit = 200;
            int ScopeId = 1;
            int ActivityTypeId = 1;
            decimal Cost = Convert.ToDecimal(2.5);
            bool ItIsFundable = true;
            DateTime InscriptionStart = DateTime.Now;
            DateTime InscriptionEnd = DateTime.Now;
            string Requirements = "TestText";
            string Place = "TestText";
            string Address = "TestText";
            decimal? Longitude = Convert.ToDecimal(13.5);
            decimal? Latitude = Convert.ToDecimal(11.5);
            int? TemplateId = 1;
            string TemplateCode = "TestText";
            string EmailBody = "TestText";
            int EventStatusId = 1;
            bool Enabled = true;

            var entidad = new Event()
            {
                EventId = EventId,
                Name = Name,
                EventImage = EventImage,
                Description = Description,
                StartDateTime = StartDateTime,
                EndDateTime = EndDateTime,
                QuotaLimit = QuotaLimit,
                ScopeId = ScopeId,
                ActivityTypeId = ActivityTypeId,
                Cost = Cost,
                ItIsFundable = ItIsFundable,
                InscriptionStart = InscriptionStart,
                InscriptionEnd = InscriptionEnd,
                Requirements = Requirements,
                Place = Place,
                Address = Address,
                Longitude = Longitude,
                Latitude = Latitude,
                TemplateId = TemplateId,
                TemplateCode = TemplateCode,
                EmailBody = EmailBody,
                EventStatusId = EventStatusId,
                Enabled = Enabled
            };

            var EventAgeRanges = new List<EventAgeRange>();
            var EventRoles = new List<EventRole>();

            Assert.AreEqual(entidad.EventId, EventId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.EventImage, EventImage);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.StartDateTime, StartDateTime);
            Assert.AreEqual(entidad.EndDateTime, EndDateTime);
            Assert.AreEqual(entidad.QuotaLimit, QuotaLimit);
            Assert.AreEqual(entidad.ScopeId, ScopeId);
            Assert.AreEqual(entidad.ActivityTypeId, ActivityTypeId);
            Assert.AreEqual(entidad.Cost, Cost);
            Assert.AreEqual(entidad.ItIsFundable, ItIsFundable);
            Assert.AreEqual(entidad.InscriptionStart, InscriptionStart);
            Assert.AreEqual(entidad.InscriptionEnd, InscriptionEnd);
            Assert.AreEqual(entidad.Requirements, Requirements);
            Assert.AreEqual(entidad.Place, Place);
            Assert.AreEqual(entidad.Address, Address);
            Assert.AreEqual(entidad.Longitude, Longitude);
            Assert.AreEqual(entidad.Latitude, Latitude);
            Assert.AreEqual(entidad.TemplateId, TemplateId);
            Assert.AreEqual(entidad.TemplateCode, TemplateCode);
            Assert.AreEqual(entidad.EmailBody, EmailBody);
            Assert.AreEqual(entidad.EventStatusId, EventStatusId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
