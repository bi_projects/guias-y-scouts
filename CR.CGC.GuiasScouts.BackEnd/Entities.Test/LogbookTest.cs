﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class LogbookTest
    {
        [TestMethod]
        public void LogbookEntity()
        {
            int? LogbookId = 1;
            int LogTypeId = 1;
            string Description = "TestText";
            int ExecutingUser = 1;
            DateTime ActionDate = DateTime.Now;
            bool Enabled = true;

            var entidad = new Logbook()
            {
                LogbookId = LogbookId,
                LogTypeId = LogTypeId,
                Description = Description,
                ExecutingUser = ExecutingUser,
                ActionDate = ActionDate,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.LogbookId, LogbookId);
            Assert.AreEqual(entidad.LogTypeId, LogTypeId);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.ExecutingUser, ExecutingUser);
            Assert.AreEqual(entidad.ActionDate, ActionDate);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
