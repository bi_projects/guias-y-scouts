﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class RequestCommentTest
    {
        [TestMethod]
        public void RequestCommentEntity()
        {
            int? RequestCommentId = 1;
            int RequestId = 1;
            string Comment = "TestText";
            int Commentator = 2;
            DateTime DateTimeComment = DateTime.Now;
            bool Enabled = true;

            var entidad = new RequestComment()
            {
                RequestCommentId = RequestCommentId,
                RequestId = RequestId,
                Comment = Comment,
                Commentator = Commentator,
                DateTimeComment = DateTimeComment,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.RequestCommentId, RequestCommentId);
            Assert.AreEqual(entidad.RequestId, RequestId);
            Assert.AreEqual(entidad.Comment, Comment);
            Assert.AreEqual(entidad.Commentator, Commentator);
            Assert.AreEqual(entidad.DateTimeComment, DateTimeComment);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
