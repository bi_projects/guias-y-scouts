﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class ProgressionTest
    {
        [TestMethod]
        public void ProgressionEntity()
        {
            int? ProgressionId = 1;
            string Name = "TestText";
            string FileLocation = "TestText";
            int SectionId = 2;
            int SystemStatusId = 3;
            bool Enabled = true;

            var entidad = new Progression()
            {
                ProgressionId = ProgressionId,
                Name = Name,
                FileLocation = FileLocation,
                SectionId = SectionId,
                SystemStatusId = SystemStatusId,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.ProgressionId, ProgressionId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.FileLocation, FileLocation);
            Assert.AreEqual(entidad.SectionId, SectionId);
            Assert.AreEqual(entidad.SystemStatusId, SystemStatusId);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
