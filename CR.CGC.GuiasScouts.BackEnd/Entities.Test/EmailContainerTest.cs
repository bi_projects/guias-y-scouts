﻿using System;
using System.Net.Mail;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class EmailContainerTest
    {
        [TestMethod]
        public void EmailContainerEntity()
        {
            string ListToSend = "TestText";

            var entidad = new EmailContainer()
            {
                ListToSend = ListToSend,
                mailMessage = new MailMessage()
                {
                    Subject = "TestText",
                    IsBodyHtml = true,
                    Body = "TestText",
                }
            };

            Assert.AreEqual(entidad.ListToSend, ListToSend);
        }
    }
}
