﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class PublicationTest
    {
        [TestMethod]
        public void PublicationEntity()
        {
            int? PublicationId = 1;
            string Title = "TestText";
            int InterestThemeId = 3;
            DateTime StartDate = DateTime.Now;
            DateTime EndDate = DateTime.Now;
            //int AgeRangeId = 3;
            string Description = "TestText";
            string FileLocation = "TestText";
            string Body = "TestText";
            string Keywords = "TestText";
            bool Enabled = true;

            var entidad = new Publication()
            {
                PublicationId = PublicationId,
                Title = Title,
                InterestThemeId = InterestThemeId,
                StartDate = StartDate,
                EndDate = EndDate,
                //AgeRangeId = AgeRangeId,
                Description = Description,
                FileLocation = FileLocation,
                Body = Body,
                Keywords = Keywords,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.PublicationId, PublicationId);
            Assert.AreEqual(entidad.Title, Title);
            Assert.AreEqual(entidad.InterestThemeId, InterestThemeId);
            Assert.AreEqual(entidad.StartDate, StartDate);
            Assert.AreEqual(entidad.EndDate, EndDate);
            //Assert.AreEqual(entidad.AgeRangeId, AgeRangeId);
            Assert.AreEqual(entidad.Description, Description);
            Assert.AreEqual(entidad.FileLocation, FileLocation);
            Assert.AreEqual(entidad.Body, Body);
            Assert.AreEqual(entidad.Keywords, Keywords);
            Assert.AreEqual(entidad.Enabled, Enabled);        
        }
    }
}
