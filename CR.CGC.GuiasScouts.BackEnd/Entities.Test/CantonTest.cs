﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class CantonTest
    {
        [TestMethod]
        public void CantonEntity()
        {
            int? CantonId = 1;
            int ProvinceId = 1;
            string Name = "TestText";
            string PostalCode = "TestText";
            bool Enabled = true;

            var entidad = new Canton()
            {
                CantonId = CantonId,
                ProvinceId = ProvinceId,
                Name = Name,
                PostalCode = PostalCode,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.CantonId, CantonId);
            Assert.AreEqual(entidad.ProvinceId, ProvinceId);
            Assert.AreEqual(entidad.Name, Name);
            Assert.AreEqual(entidad.PostalCode, PostalCode);
            Assert.AreEqual(entidad.Enabled, Enabled);

        }
    }
}
