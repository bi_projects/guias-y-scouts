﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Entities.Test
{
    [TestClass]
    public class MemberRecognitionTest
    {
        [TestMethod]
        public void MemberRecognitionEntity()
        {
            int? MemberRecognitionId = 2;
            int MemberId = 5;
            int RecognitionId = 3;
            DateTime DateAssignment = DateTime.Now;
            bool Enabled = true;

            var entidad = new MemberRecognition()
            {
                MemberRecognitionId = MemberRecognitionId,
                MemberId = MemberId,
                RecognitionId = RecognitionId,
                DateAssignment = DateAssignment,
                Enabled = Enabled
            };

            Assert.AreEqual(entidad.MemberRecognitionId, MemberRecognitionId);
            Assert.AreEqual(entidad.MemberId, MemberId);
            Assert.AreEqual(entidad.RecognitionId, RecognitionId);
            Assert.AreEqual(entidad.DateAssignment, DateAssignment);
            Assert.AreEqual(entidad.Enabled, Enabled);
        }
    }
}
