﻿namespace CR.CGC.GuiasScouts.BackEnd.Models
{
    using DALBase;
    using Entities;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Serialization;

    public static class DALBas
    {
        static SqlConnection conn;
        /// <summary>
        /// Variable que controla la coneccion a la base de datos
        /// </summary>
        public static SqlConnection Connection
        {
            get
            {
                if (conn == null)
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ScoutsConnection"].ConnectionString);
                return conn;
            }
            set
            {
                conn = value;
            }
        }

        /// <summary>
        /// Inserta uno o varios objetos en la base de datos evaluando las clases, subclases y listas de clases que contienen elementos con valores
        /// <para>Si la insercion se realiza con exito el valor para ResponseCode sera 200 en caso contrario sera un 500 y ResponseText tendra la cadena con el error de la operacion</para>
        /// </summary>
        /// <param name="EntityToSave">Datos en formato json/param>
        /// <param name="model">Modelo de la base de datos donde se guardaran los datos recibidos</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText, object container) InsertObject(string model, ERContainer container, bool pluralizeModel)
        {
            try
            {
                DataTable ResultingTable = new DataTable();
                Dictionary<string, string> procedures = Utilities.GetProceduresFromModel(model);
                if (!procedures.ContainsKey("InsertionProcedure"))
                    return (HttpStatusCode.BadRequest, "La operación guardar no se puede llevar a cabo con el item que intentas guardar", new { });
                string commandSchema = Utilities.GetSchemaFromModel(model);
                using (SqlCommand cmd = new SqlCommand($"{commandSchema}.{procedures["InsertionProcedure"]}", Connection) { CommandType = CommandType.StoredProcedure })
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters = Utilities.GetParametersFromContainer(container, parameters, model, pluralizeModel);
                    cmd.Parameters.AddRange(parameters.ToArray());
                    string xmlText = string.Empty;
                    XmlDocument xmlDoc;
                    Connection.Open();
                    container = new ERContainer();
                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            xmlText = reader.ReadOuterXml();
                            xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xmlText);
                            XmlSerializer serializer = new XmlSerializer(typeof(ERContainer));
                            using (TextReader treader = new StringReader("<?xml version=\"1.0\" encoding=\"utf- 8\"?>" + xmlDoc.FirstChild.OuterXml.Replace("False", "0").Replace("True", "1")))
                            {
                                container = (ERContainer)serializer.Deserialize(treader);
                            }
                        }
                        Connection.Close();
                        if (Convert.ToInt32(cmd.Parameters["@RETURN"].Value) != 0)
                            return (HttpStatusCode.InternalServerError, cmd.Parameters["@RETURN_MESSAGE"].Value.ToString(), new { });
                        return (HttpStatusCode.OK, "Operación realizada con éxito", container.GetType().GetProperty(new Pluralize.NET.Pluralizer().Pluralize(model)).GetValue(container));
                    }
                }
            }
            catch (Exception ex)
            {
                Connection.Close();
                return (HttpStatusCode.InternalServerError, ex.Message, new { });
            }
        }

        /// <summary>
        /// Metodo para obtener los objetos de un modelo de la base de datos
        /// <para>Si el valor ResponseCode retorna un status 200 entonces el valor de Result seran los objectos en caso contrario sera el mensaje de error del try catch</para>
        /// </summary>
        /// <param name="model">Modelo de la base de datos de donde se espera obtener varios objetos</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string Result) GetObjectsFromModel(string model, Dictionary<string, object> parameters)
        {
            DataSet dataSet = new DataSet();
            bool ContainsPaged = false;
            ERContainer container = new ERContainer();
            Dictionary<string, string> temp = Utilities.GetProceduresFromModel(model);
            if (!temp.ContainsKey("GetProcedure"))
                return (HttpStatusCode.NotFound, "No se permite aplicar la operación al elemento que deseas obtener, intenta nuevamente o comunícate con el administrador.");
            using (SqlCommand cmd = new SqlCommand($"{Utilities.GetSchemaFromModel(model)}.{temp["GetProcedure"]}", Connection) { CommandType = CommandType.StoredProcedure })
            {
                string jsonText = string.Empty;
                string xmlText = string.Empty;
                XmlDocument xmlDoc;
                foreach (KeyValuePair<string, object> parameter in parameters.Where(w => !w.Key.Equals("Model") && !w.Key.Equals("SearchText") && !w.Key.Equals("PageSize") && !w.Key.Equals("PageNumber")))
                {
                    cmd.Parameters.AddWithValue($"@{parameter.Key}", parameter.Value);
                }
                if (parameters.ContainsKey("SearchText") || parameters.ContainsKey("PageSize") || parameters.ContainsKey("PageNumber"))// paging parameters
                {
                    ContainsPaged = true;
                    parameters.Where(w => w.Key.Equals("SearchText") || w.Key.Equals("PageSize") || w.Key.Equals("PageNumber")).ToList().ForEach(parameter =>
                    {
                        cmd.Parameters.AddWithValue($"@{parameter.Key}", parameter.Value).Direction = ParameterDirection.InputOutput;
                    });
                    cmd.Parameters.Add(new SqlParameter("@RowCount", SqlDbType.Int) { Direction = ParameterDirection.InputOutput });
                }
                try
                {
                    Connection.Open();
                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            xmlText = reader.ReadOuterXml();
                            //xmlText = Regex.Replace(xmlText, @"\t|\n|\r", "");
                            xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xmlText);
                            XmlSerializer serializer = new XmlSerializer(typeof(ERContainer));
                            using (TextReader treader = new StringReader("<?xml version=\"1.0\" encoding=\"utf- 8\"?>" + xmlDoc.FirstChild.OuterXml.Replace("False", "0").Replace("True", "1")))
                            {
                                container = (ERContainer)serializer.Deserialize(treader);
                            }
                        }
                    }
                    Utilities.SetPagingProperties(ContainsPaged, container, cmd.Parameters);
                    Connection.Close();
                    return (HttpStatusCode.OK, JsonConvert.SerializeObject(container));
                }
                catch (Exception ex)
                {
                    Connection.Close();
                    return (HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }

        /// <summary>
        /// Metodo para retornar el resultado de un procedimiento distinto al Get comun que se encuentra en la propiedad GetProcedure de cada Entidad
        /// </summary>
        /// <param name="model">Modelo que se desea obtener. Ademas permite obtener las propiedades schema y procedimientos a los cuales puede ejecutar dicho modelo</param>
        /// <param name="procedure">Nombre del procedimiento especifico que se desea ejecutar</param>
        /// <param name="parameters">Parametros del procedimiento almacenado provenientes desde la url del get</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string Result, ERContainer ResultObject) GetCustomProcedure(string model, string procedure, Dictionary<string, object> parameters)
        {
            DataSet dataSet = new DataSet();
            ERContainer container = new ERContainer();
            Dictionary<string, string> temp = Utilities.GetProceduresFromModel(model);
            if (!temp.ContainsKey(procedure))
                return (HttpStatusCode.NotFound, "No se permite aplicar la operación al elemento que deseas obtener, intenta nuevamente o comunícate con el administrador.", new ERContainer());
            using (SqlCommand cmd = new SqlCommand($"{Utilities.GetSchemaFromModel(model)}.{temp[procedure]}", Connection) { CommandType = CommandType.StoredProcedure })
            {
                string jsonText = string.Empty;
                string xmlText = string.Empty;
                XmlDocument xmlDoc;
                foreach (KeyValuePair<string, object> parameter in parameters.Where(w => !w.Key.Equals("Model")))
                {
                    cmd.Parameters.AddWithValue($"@{parameter.Key}", parameter.Value);
                }
                try
                {
                    Connection.Open();
                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            xmlText = reader.ReadOuterXml();
                            xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xmlText);
                            XmlSerializer serializer = new XmlSerializer(typeof(ERContainer));
                            using (TextReader treader = new StringReader("<?xml version=\"1.0\" encoding=\"utf- 8\"?>" + xmlDoc.FirstChild.OuterXml.Replace("False", "0").Replace("True", "1")))
                            {
                                container = (ERContainer)serializer.Deserialize(treader);
                            }
                        }
                    }
                    Connection.Close();
                    return (HttpStatusCode.OK, JsonConvert.SerializeObject(container), container);
                }
                catch (Exception ex)
                {
                    Connection.Close();
                    return (HttpStatusCode.InternalServerError, ex.Message, new ERContainer());
                }
            }
        }

        /// <summary>
        /// Inserta uno o varios objetos en la base de datos evaluando las clases, subclases y listas de clases que contienen elementos con valores
        /// <para>Si la insercion se realiza con exito el valor para ResponseCode sera 200 en caso contrario sera un 500 y ResponseText tendra la cadena con el error de la operacion</para>
        /// </summary>
        /// <param name="EntityToSave">Datos en formato json/param>
        /// <param name="model">Modelo de la base de datos donde se guardaran los datos recibidos</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText, object ResultObject) InsertCustomProcedure(string model, string procedure, ERContainer container, bool pluralizeModel)
        {
            try
            {
                if (string.IsNullOrEmpty(procedure)) return (HttpStatusCode.BadRequest, "Debe especificarse el procedimiento a utilizar.", new { });
                string commandSchema = Utilities.GetSchemaFromModel(model);
                using (SqlCommand cmd = new SqlCommand($"{commandSchema}.{procedure}", Connection) { CommandType = CommandType.StoredProcedure })
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters = Utilities.GetParametersFromContainer(container, parameters, model, pluralizeModel);
                    cmd.Parameters.AddRange(parameters.ToArray());
                    string xmlText = string.Empty;
                    XmlDocument xmlDoc;
                    Connection.Open();
                    container = new ERContainer();
                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            xmlText = reader.ReadOuterXml();
                            xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xmlText);
                            XmlSerializer serializer = new XmlSerializer(typeof(ERContainer));
                            using (TextReader treader = new StringReader("<?xml version=\"1.0\" encoding=\"utf- 8\"?>" + xmlDoc.FirstChild.OuterXml.Replace("False", "0").Replace("True", "1")))
                            {
                                container = (ERContainer)serializer.Deserialize(treader);
                            }
                        }
                        Connection.Close();
                        if (Convert.ToInt32(cmd.Parameters["@RETURN"].Value) != 0)
                            return (HttpStatusCode.InternalServerError, cmd.Parameters["@RETURN_MESSAGE"].Value.ToString(), new { });
                        return (HttpStatusCode.OK, "Operación realizada con éxito", container.GetType().GetProperty(new Pluralize.NET.Pluralizer().Pluralize(model)).GetValue(container));
                    }
                }
            }
            catch (Exception ex)
            {
                Connection.Close();
                return (HttpStatusCode.InternalServerError, ex.Message, new { });
            }
        }

        /// <summary>
        /// Elimina  uno o varios objetos en la base de datos evaluando las clases, subclases y listas de clases que contienen elementos con valores
        /// <para>Si la operacion se realiza con exito el valor para ResponseCode sera 200 en caso contrario sera un 500 y ResponseText tendra la cadena con el error de la operacion</para>
        /// </summary>
        /// <param name="EntityToDelete">Datos en formato json/param>
        /// <param name="model">Modelo de la base de datos donde se realizara la eliminacion de los datos recibidos</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText, object ResultingMember) InsertMember(ERContainer container)
        {
            try
            {
                Dictionary<string, string> procedures = Utilities.GetProceduresFromModel("Member");
                if (!procedures.ContainsKey("InsertionProcedure"))
                    return (HttpStatusCode.BadRequest, "La operación guardar no se puede llevar a cabo con el item que intentas guardar", new { });
                string commandSchema = Utilities.GetSchemaFromModel("Member");
                using (SqlCommand cmd = new SqlCommand($"{commandSchema}.{procedures["InsertionProcedure"]}", Connection) { CommandType = CommandType.StoredProcedure })
                {
                    string jsonText = string.Empty;
                    string xmlText = string.Empty;
                    XmlDocument xmlDoc;
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters = Utilities.GetParametersFromContainer(container, parameters, "Member");
                    parameters = Utilities.GetParametersFromContainer(container, parameters, "User", false, false);
                    parameters.RemoveRange(parameters.Count - 2, 2);
                    parameters.Add(new SqlParameter() { ParameterName = "@NewUserName", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Output, Size = 100 });
                    parameters.Add(new SqlParameter() { ParameterName = "@NewRecordId", SqlDbType = SqlDbType.TinyInt, Direction = ParameterDirection.Output });
                    cmd.Parameters.AddRange(parameters.ToArray());
                    Connection.Open();
                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            xmlText = reader.ReadOuterXml();
                            xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xmlText);
                            XmlSerializer serializer = new XmlSerializer(typeof(ERContainer));
                            using (TextReader treader = new StringReader("<?xml version=\"1.0\" encoding=\"utf- 8\"?>" + xmlDoc.FirstChild.OuterXml.Replace("False", "0").Replace("True", "1")))
                            {
                                container = (ERContainer)serializer.Deserialize(treader);
                            }
                        }
                    }
                    Connection.Close();
                    if (Convert.ToInt32(cmd.Parameters["@RETURN"].Value) != 0)
                        return (HttpStatusCode.InternalServerError, cmd.Parameters["@RETURN_MESSAGE"].Value.ToString(), new { });
                    container.User.UserName = cmd.Parameters["@NewUserName"].Value.ToString();
                    return (HttpStatusCode.OK, "Operación realizada con éxito", container.Members.First());
                }
            }
            catch (Exception ex)
            {
                Connection.Close();
                return (HttpStatusCode.InternalServerError, ex.Message, new Member());
            }
        }

        /// <summary>
        /// Metodo para los inicios de sesion. En el se valida que sea un usuario valido ademas de identificar si es el primer inicio de sesion del cliente, de ser asi se solicita un cambio de contraseña
        /// </summary>
        /// <param name="container">Clase ERContainer donde se obtienen las propiedades del usuario</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText, ERContainer ResultingObject) LoginUser(this ERContainer container)
        {
            string jsonText = string.Empty;
            string xmlText = string.Empty;
            XmlDocument xmlDoc;
            try
            {
                Dictionary<string, string> temp = Utilities.GetProceduresFromModel("User");
                using (SqlCommand cmd = new SqlCommand($"{Utilities.GetSchemaFromModel("User")}.{temp["LoginProcedure"]}", Connection) { CommandType = CommandType.StoredProcedure })
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters = Utilities.GetParametersFromContainer(container, parameters, "User", false, false);
                    container = new ERContainer();
                    cmd.Parameters.AddRange(parameters.ToArray());
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@ItIsValid", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@ItIsTheFirstConnection", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Output });
                    Connection.Open();
                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            xmlText = reader.ReadOuterXml();
                            xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xmlText);
                            XmlSerializer serializer = new XmlSerializer(typeof(ERContainer));
                            using (TextReader treader = new StringReader("<?xml version=\"1.0\" encoding=\"utf- 8\"?>" + xmlDoc.FirstChild.OuterXml.Replace("False", "0").Replace("True", "1")))
                            {
                                container = (ERContainer)serializer.Deserialize(treader);
                            }
                        }
                    }
                    Connection.Close();
                    if (Convert.ToInt32(cmd.Parameters["@RETURN"].Value) != 0)//Si el retorno es diferente de cero entonces ocurrio algun error en la base de datos
                        return (HttpStatusCode.InternalServerError, cmd.Parameters["@RETURN_MESSAGE"].Value.ToString(), null);
                    if (Convert.ToBoolean(cmd.Parameters["@ItIsValid"].Value) == false)//Si el retorno es falso entonces la contraseña o el usuario son incorrectos
                        return (HttpStatusCode.InternalServerError, cmd.Parameters["@RETURN_MESSAGE"].Value.ToString(), null);
                    container.Members.First().User.IsFirstLogin = Convert.ToBoolean(cmd.Parameters["@ItIsTheFirstConnection"].Value);
                    return (HttpStatusCode.OK, "Operación realizada con éxito", container);
                }
            }
            catch (SqlException ex)
            {
                Connection.Close();
                if (ex.Number == 53)
                    return (HttpStatusCode.InternalServerError, "No se logró establecer conexión con el servidor, por favor intente nuevamente", null);
                if (ex.Number == -2)
                    return (HttpStatusCode.InternalServerError, "La comunicación con el servidor duró más de lo esperado, por favor intente nuevamente", null);
                return (HttpStatusCode.InternalServerError, ex.Message, null);
            }
        }

        /// <summary>
        /// Metodo para los cierres de sesion
        /// </summary>
        /// <param name="container">Clase ERContainer donde se obtienen las propiedades del usuario para cerrar la sesion</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText) LogOutUser(this ERContainer container)
        {
            try
            {
                Dictionary<string, string> temp = Utilities.GetProceduresFromModel("User");
                using (SqlCommand cmd = new SqlCommand($"{Utilities.GetSchemaFromModel("User")}.{temp["LogOutProcedure"]}", Connection) { CommandType = CommandType.StoredProcedure })
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters = Utilities.GetParametersFromContainer(container, parameters, "User", false, false);
                    cmd.Parameters.AddRange(parameters.ToArray());
                    Connection.Open();
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    if (Convert.ToInt32(cmd.Parameters["@RETURN"].Value) != 0)//Si el retorno es diferente de cero entonces ocurrio algun error en la base de datos
                        return (HttpStatusCode.InternalServerError, cmd.Parameters["@RETURN_MESSAGE"].Value.ToString());
                    return (HttpStatusCode.OK, "Operacion realizada con exito");
                }
            }
            catch (Exception ex)
            {
                Connection.Close();
                return (HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Metodo para los cierres de sesion
        /// </summary>
        /// <param name="container">Clase ERContainer donde se obtienen las propiedades del usuario para cerrar la sesion</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText) ValidatePasswordResetRequest(this ERContainer container)
        {
            try
            {
                Dictionary<string, string> temp = Utilities.GetProceduresFromModel("User");
                using (SqlCommand cmd = new SqlCommand($"{Utilities.GetSchemaFromModel("User")}.{temp["ValidatePasswordResetRequestProcedure"]}", Connection) { CommandType = CommandType.StoredProcedure })
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters = Utilities.GetParametersFromContainer(container, parameters, "User", false, false);
                    cmd.Parameters.AddRange(parameters.ToArray());
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@Source", SqlDbType = SqlDbType.NVarChar, Size = 600, Value = container.User.Source });
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@ItIsValid", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Output });

                    Connection.Open();
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    if (Convert.ToInt32(cmd.Parameters["@RETURN"].Value) != 0)//Si el retorno es diferente de cero entonces ocurrio algun error en la base de datos
                        return (HttpStatusCode.InternalServerError, cmd.Parameters["@RETURN_MESSAGE"].Value.ToString());
                    if (Convert.ToBoolean(cmd.Parameters["@ItIsValid"].Value) == false)//Si el retorno es falso entonces la contraseña o el usuario son incorrectos
                        return (HttpStatusCode.Unauthorized, "El correo para restablecer la contraseña no existe.");
                    return (HttpStatusCode.OK, "Operacion realizada con exito");
                }
            }
            catch (Exception ex)
            {
                Connection.Close();
                return (HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Metodo para los cierres de sesion
        /// </summary>
        /// <param name="container">Clase ERContainer donde se obtienen las propiedades del usuario para cerrar la sesion</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText, ERContainer container) ResetPassword(this ERContainer container, bool isAdministratorRequest)
        {
            try
            {
                Dictionary<string, string> temp = Utilities.GetProceduresFromModel("User");
                using (SqlCommand cmd = new SqlCommand($"{Utilities.GetSchemaFromModel("User")}.{temp["ResetPasswordProcedure"]}", Connection) { CommandType = CommandType.StoredProcedure })
                {
                    string jsonText = string.Empty;
                    string xmlText = string.Empty;
                    XmlDocument xmlDoc;
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters = Utilities.GetParametersFromContainer(container, parameters, "User", false, false);
                    container = new ERContainer();
                    cmd.Parameters.AddRange(parameters.ToArray());
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@ItIsValid", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@UserEmail", SqlDbType = SqlDbType.VarChar, Size = 255, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@InvokedByAdministrator", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.InputOutput, Value = isAdministratorRequest });
                    Connection.Open();
                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            xmlText = reader.ReadOuterXml();
                            xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xmlText);
                            XmlSerializer serializer = new XmlSerializer(typeof(ERContainer));
                            using (TextReader treader = new StringReader("<?xml version=\"1.0\" encoding=\"utf- 8\"?>" + xmlDoc.FirstChild.OuterXml.Replace("False", "0").Replace("True", "1")))
                            {
                                container = (ERContainer)serializer.Deserialize(treader);
                            }
                        }
                    }
                    Connection.Close();
                    if (Convert.ToInt32(cmd.Parameters["@RETURN"].Value) != 0)//Si el retorno es diferente de cero entonces ocurrio algun error en la base de datos
                        return (HttpStatusCode.InternalServerError, cmd.Parameters["@RETURN_MESSAGE"].Value.ToString(), container);
                    if (Convert.ToBoolean(cmd.Parameters["@ItIsValid"].Value) == false)//Si el retorno es falso entonces la contraseña o el usuario son incorrectos
                        return (HttpStatusCode.Unauthorized, "El token no es valido, por favor comunicate con el administrador del sistema.", container);
                    container.User.Email = cmd.Parameters["@UserEmail"].Value.ToString();
                    return (HttpStatusCode.OK, "Operacion realizada con exito", container);
                }
            }
            catch (Exception ex)
            {
                Connection.Close();
                return (HttpStatusCode.InternalServerError, ex.Message, container);
            }
        }

        /// <summary>
        /// Metodo para Activar a los ususarios
        /// </summary>
        /// <param name="container">Clase ERContainer donde se obtienen las propiedades del usuario para activarlo</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText, ERContainer container) ActivateUser(this ERContainer container)
        {
            try
            {
                Dictionary<string, string> temp = Utilities.GetProceduresFromModel("User");
                using (SqlCommand cmd = new SqlCommand($"{Utilities.GetSchemaFromModel("User")}.{temp["ActivateProcedure"]}", Connection) { CommandType = CommandType.StoredProcedure })
                {
                    string jsonText = string.Empty;
                    string xmlText = string.Empty;
                    XmlDocument xmlDoc;
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters = Utilities.GetParametersFromContainer(container, parameters, "User", false, false);
                    container = new ERContainer();
                    cmd.Parameters.AddRange(parameters.ToArray());
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@ItIsValid", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@UserEmail", SqlDbType = SqlDbType.VarChar, Size = 255, Direction = ParameterDirection.Output });
                    Connection.Open();
                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            xmlText = reader.ReadOuterXml();
                            xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xmlText);
                            XmlSerializer serializer = new XmlSerializer(typeof(ERContainer));
                            using (TextReader treader = new StringReader("<?xml version=\"1.0\" encoding=\"utf- 8\"?>" + xmlDoc.FirstChild.OuterXml.Replace("False", "0").Replace("True", "1")))
                            {
                                container = (ERContainer)serializer.Deserialize(treader);
                            }
                        }
                    }
                    Connection.Close();
                    if (Convert.ToInt32(cmd.Parameters["@RETURN"].Value) != 0)//Si el retorno es diferente de cero entonces ocurrio algun error en la base de datos
                        return (HttpStatusCode.InternalServerError, cmd.Parameters["@RETURN_MESSAGE"].Value.ToString(), container);
                    if (Convert.ToBoolean(cmd.Parameters["@ItIsValid"].Value) == false)//Si el retorno es falso entonces la contraseña o el usuario son incorrectos
                        return (HttpStatusCode.Unauthorized, "El token no es valido, por favor comunicate con el administrador del sistema.", container);
                    container.User.Email = cmd.Parameters["@UserEmail"].Value.ToString();
                    return (HttpStatusCode.OK, "Operacion realizada con exito", container);
                }
            }
            catch (Exception ex)
            {
                Connection.Close();
                return (HttpStatusCode.InternalServerError, ex.Message, container);
            }
        }

        /// <summary>
        /// Metodo para los cierres de sesion
        /// </summary>
        /// <param name="container">Clase ERContainer donde se obtienen las propiedades del usuario para cerrar la sesion</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText, object ResultingObject) ChangePassword(this ERContainer container, User NewInfo)
        {
            try
            {
                Dictionary<string, string> temp = Utilities.GetProceduresFromModel("User");
                using (SqlCommand cmd = new SqlCommand($"{Utilities.GetSchemaFromModel("User")}.{temp["ChangePasswordProcedure"]}", Connection) { CommandType = CommandType.StoredProcedure })
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    string xmlText = string.Empty;
                    XmlDocument xmlDoc;
                    parameters = Utilities.GetParametersFromContainer(container, parameters, "User", false, false);
                    container = new ERContainer();
                    cmd.Parameters.AddRange(parameters.ToArray());
                    cmd.Parameters.Add(new SqlParameter() { SqlDbType = SqlDbType.Structured, ParameterName = $"@NewDataUser", Value = NewInfo.ClassToDataTable() });
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@ItIsValid", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Output });
                    Connection.Open();
                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            xmlText = reader.ReadOuterXml();
                            xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xmlText);
                            XmlSerializer serializer = new XmlSerializer(typeof(ERContainer));
                            using (TextReader treader = new StringReader("<?xml version=\"1.0\" encoding=\"utf- 8\"?>" + xmlDoc.FirstChild.OuterXml.Replace("False", "0").Replace("True", "1")))
                            {
                                container = (ERContainer)serializer.Deserialize(treader);
                            }
                        }
                    }
                    Connection.Close();
                    if (Convert.ToInt32(cmd.Parameters["@RETURN"].Value) != 0)//Si el retorno es diferente de cero entonces ocurrio algun error en la base de datos
                        return (HttpStatusCode.InternalServerError, cmd.Parameters["@RETURN_MESSAGE"].Value.ToString(), new { });
                    if (Convert.ToBoolean(cmd.Parameters["@ItIsValid"].Value) == false)//Si el retorno es falso entonces la contraseña o el usuario son incorrectos
                        return (HttpStatusCode.Unauthorized, "La contraseña proporcionada no es valida.", new { });
                    return (HttpStatusCode.OK, "Operacion realizada con exito", container);
                }
            }
            catch (Exception ex)
            {
                Connection.Close();
                return (HttpStatusCode.InternalServerError, ex.Message, new { });
            }
        }

        public static (HttpStatusCode, string, DataSet) GetDataSet(List<SqlParameter> parameters, string model, string procedure)
        {
            using (DataSet DSReturn = new DataSet())
            {
                Dictionary<string, string> temp = Utilities.GetProceduresFromModel(model);
                if (!temp.ContainsKey(procedure))
                    return (HttpStatusCode.NotFound, "No se permite aplicar la operación al elemento que deseas obtener, intenta nuevamente o comunícate con el administrador.", DSReturn);
                try
                {
                    using (SqlCommand cmd = new SqlCommand($"{Utilities.GetSchemaFromModel(model)}.{temp[procedure]}"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = Connection;
                        cmd.Parameters.AddRange(parameters.ToArray());
                        Connection.Open();
                        DbDataReader reader = cmd.ExecuteReader();
                        do
                        {
                            DataTable table = new DataTable();
                            table.Load(reader);
                            DSReturn.Tables.Add(table);
                        } while (!reader.IsClosed && reader.NextResult());
                        Connection.Close();
                    }
                    return (HttpStatusCode.OK, string.Empty, DSReturn);
                }
                catch (Exception ex)
                {
                    Connection.Close();
                    return (HttpStatusCode.OK, ex.Message, DSReturn);
                }
            }
        }

        public static List<MemberAlertSetting> GetMemberAlert(int MemberId)
        {
            var (ResponseCode, settings) = GetObjectsFromModel("MemberAlertSetting", new Dictionary<string, object>() { { "MemberId", MemberId } });
            if (ResponseCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<ERContainer>(settings).MemberAlertSettings;
            return new List<MemberAlertSetting>();
        }
    }
}