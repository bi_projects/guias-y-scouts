﻿namespace DALBase
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Mail;
    using System.Reflection;
    using System.Security.Cryptography;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Script.Serialization;
    using Entities;
    using Newtonsoft.Json;
    using Pluralize.NET;

    public static class Utilities
    {
        /// <summary>
        /// Condicional para verificar que el tipo propertyInfo tenga las siguientes caracteristicas: que sea nullable, que no sea un tipo generico es decir una lista de clases y que su propertyType sea un tipo del sistema.
        /// adicionalmente el campo no tiene que estar dentro de las propiedades que se van a excluir por entidad como las propiedades de procedimientos, schema, etc.
        /// </summary>
        private static Func<PropertyInfo, bool> whereCondition = w => (Nullable.GetUnderlyingType(w.PropertyType) != null || !w.PropertyType.IsGenericType) && (Nullable.GetUnderlyingType(w.PropertyType) != null || SystemTypes.Contains(w.PropertyType)) && !ExcludedColumns.Select(s => s).Contains(w.Name) && !Attribute.IsDefined(w, typeof(IgnorePropertyAttribute));

        /// <summary>
        /// Lista de Campos que no deben ser agregados a las DataTables que se asignaran a los SqlParameter
        /// </summary>
        public static List<string> ExcludedColumns = new List<string>()
        {
            "Schema", "InsertionProcedure", "IsFirstLogin", "LoginProcedure", "LogOutProcedure", "ValidatePasswordResetRequestProcedure", "ResetPasswordProcedure", "Source", "ChangePasswordProcedure",
            "UpdateProcedure","GetProcedure","DeleteProcedure", "SendFile"
        };

        #region ----Variables para los envios de correos--------------
        /// <summary>
        /// Contiene la direccion de correo desde donde se enviaran los correos
        /// </summary>
        public static string FromMailAddress { get; set; } = ConfigurationManager.AppSettings["FromMailAddress"];
        /// <summary>
        /// Contiene el nombre que representa la direccion del correo electronico
        /// </summary>
        public static string NameMailAddress { get; set; } = ConfigurationManager.AppSettings["NameMailAddress"];
        /// <summary>
        /// Url del sitio, el valor se usa en correos y cualquier otra operacion que necesite saber cual es la direccion del sitio
        /// </summary>
        public static string UrlWebSite { get; set; } = ConfigurationManager.AppSettings["UrlWebSite"];
        /// <summary>
        /// Url en el sitio web desde donde los usuarios pueden obtener ayuda
        /// </summary>
        public static string UrlSuppport { get; set; } = ConfigurationManager.AppSettings["UrlSupport"];
        /// <summary>
        /// Correo al que los usuarios pueden hacer consultas
        /// </summary>
        public static string SupportMail { get; set; } = ConfigurationManager.AppSettings["SupportMail"];
        /// <summary>
        /// Correo o correos separados por coma a los cuales los clientes van a responder los mensajes que reciban
        /// </summary>
        public static string ReplyToList { get; set; } = ConfigurationManager.AppSettings["ReplyToList"];
        /// <summary>
        /// Host para la configuracion de envio de correos
        /// </summary>
        public static string SMTPHost { get; set; } = ConfigurationManager.AppSettings["SMTPHost"];
        /// <summary>
        /// Usuario para la configuracion de envio de correos
        /// </summary>
        public static string SMTPUser { get; set; } = ConfigurationManager.AppSettings["SMTPUser"];
        /// <summary>
        /// Contraseña para la configuracion de envio de correos
        /// </summary>
        public static string SMTPPassword { get; set; } = ConfigurationManager.AppSettings["SMTPPassword"];
        /// <summary>
        /// Puerto para la configuracion de envio de correos
        /// </summary>
        public static int SMTPPort { get; set; } = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
        #endregion

        #region SMS Variables
        /// <summary>
        /// Url del api para los envios de mensajes
        /// </summary>
        public static string SMSUrl { get; set; } = ConfigurationManager.AppSettings["SMSUrl"];

        public static string SMSKey { get; set; } = ConfigurationManager.AppSettings["SMSKey"];
        #endregion

        /// <summary>
        /// Lista de tipos del sistema para poder identificar las clases o listas de clases de los demas tipos como int, string, datetime, etc
        /// </summary>
        public static List<Type> SystemTypes { get; set; } = Assembly.GetExecutingAssembly().GetType().Module.Assembly.GetExportedTypes().ToList();

        /// <summary>
        /// Toma una lista de elementos de una clase para convertirlo en un DataTable y posteriormente poder ser agregado como SqlParameter
        /// </summary>
        /// <param name="ModelInfo">Propiedad que se desea convertir en DataTable</param>
        /// <param name="source">Clase que contiene la propiedad a convertir</param>
        /// <returns></returns>
        public static DataTable ListToDataTable(this object source)
        {
            DataTable table = new DataTable();
            IList ModelValues = source as IList;

            Type ModelType = ModelValues.GetType().GetGenericArguments()[0];
            ModelType.GetProperties().Where(whereCondition).ToList().ForEach(_ =>
            {
                table.Columns.Add(_.Name, _.PropertyType.IsGenericType ? _.PropertyType.GetGenericArguments()[0] : _.PropertyType);
            });

            foreach (var item in ModelValues)
            {
                table.Rows.Add(item.GetType().GetProperties().Where(whereCondition).Select(s => s.GetValue(item, null)).ToArray());
            }
            return table;
        }

        /// <summary>
        /// Toma una clase y la convierte en un DataTable de un solo registro y posteriormente poder ser agredado como un SqlParameter
        /// </summary>
        /// <param name="ModelInfo">Clase que se desea convertir en DataTable</param>
        /// <param name="source">Clase que contiene a la propiedad ModelInfo</param>
        /// <returns></returns>
        public static DataTable ClassToDataTable(this PropertyInfo ModelInfo, object source)
        {
            DataTable table = new DataTable();
            PropertyInfo[] properties = ModelInfo.GetValue(source)?.GetType().GetProperties() ?? Activator.CreateInstance(ModelInfo.PropertyType).GetType().GetProperties();
            foreach (PropertyInfo _ in properties)
            {
                if (whereCondition(_))
                    table.Columns.Add(_.Name, _.PropertyType.IsGenericType && _.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? _.PropertyType.GetGenericArguments()[0] : _.PropertyType);
            }
            table.Rows.Add(ModelInfo.GetValue(source).GetType().GetProperties().Where(whereCondition).ToList().Select(s => s.GetValue(ModelInfo.GetValue(source))).ToArray());
            return table;
        }

        public static void SetPagingProperties(bool containsPaged, ERContainer container, SqlParameterCollection parameters)
        {
            if (containsPaged)
            {
                container.PageSize = parameters.Contains("@PageSize") ? Convert.ToInt32(parameters["@PageSize"].Value) : 0;
                container.PageNumber = parameters.Contains("@PageNumber") ? Convert.ToInt32(parameters["@PageNumber"].Value) : 0;
                container.SearchText = parameters.Contains("@SearchText") ? Convert.ToString(parameters["@SearchText"].Value) : null;
                container.RowCount = parameters.Contains("@RowCount") ? Convert.ToInt32(parameters["@RowCount"].Value) : 0;
            }
        }

        public static ERContainer ConvertToERContainer(this string jsonText)
        {
            return JsonConvert.DeserializeObject<ERContainer>(jsonText);
        }

        /// <summary>
        /// Metodo para serializar una tabla
        /// </summary>
        /// <param name="dt">Tabla que se desea retornar</param>
        /// <returns></returns>
        public static string SerializeTable(DataTable dt)
        {
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return JsonConvert.SerializeObject(rows);
        }

        /// <summary>
        /// Toma una clase y la convierte en un DataTable de un solo registro y posteriormente poder ser agredado como un SqlParameter
        /// </summary>
        /// <param name="ModelInfo">Clase que se desea convertir en DataTable</param>
        /// <param name="source">Clase que contiene a la propiedad ModelInfo</param>
        /// <returns></returns>
        public static DataTable ClassToDataTable<T>(this T source) where T : class
        {
            DataTable table = new DataTable();
            foreach (PropertyInfo _ in source.GetType().GetProperties())
            {
                if (whereCondition(_))
                {
                    if (_.PropertyType.IsGenericType && _.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        table.Columns.Add(_.Name, _.PropertyType.GetGenericArguments()[0]);
                    else
                        table.Columns.Add(_.Name, _.PropertyType);
                }
            }
            table.Rows.Add(source.GetType().GetProperties().Where(whereCondition).ToList().Select(s => s.GetValue(source)).ToArray());
            return table;
        }

        /// <summary>
        /// Toma un DataTable y agrega mas filas al mismo
        /// <para>Si el objeto con los nuevos valores es una clase entonces las propiedades de dicha clase deben de coincidir con las columnas de la tabla</para>
        /// <para>Si el objeto con los nuevos valores es una lista generica entonces las propiedades de la clase generica de la lista deben de coincidir con las columnas de la tabla</para>
        /// </summary>
        /// <param name="table">Tabla a la que se desea agregar nuevos elementos</param>
        /// <param name="NewValues">Nuevos valores que se desean agregar a la tabla</param>
        /// <returns></returns>
        public static DataTable AddRange(this DataTable table, object NewValues)
        {
            DataTable TableWithNewValues = new DataTable();
            List<DataRow> rowsToAdd = new List<DataRow>();
            if (NewValues is IList ListNewValues)
            {
                TableWithNewValues = ListNewValues.ListToDataTable();
                foreach (DataRow newrow in TableWithNewValues.Rows)
                {
                    if (table.Rows.OfType<DataRow>().Where(w => w.ItemArray.SequenceEqual(newrow.ItemArray)).Count() == 0)
                        rowsToAdd.Add(newrow);
                }
                rowsToAdd.ForEach(_ => { table.Rows.Add(_.ItemArray); });
            }
            else
                table.Rows.Add(NewValues.GetType().GetProperties().Where(whereCondition).ToList().Select(s => s.GetValue(NewValues)).ToArray());
            return table;
        }

        /// <summary>
        /// Obtiene una lista de SqlParameter a partir de una clase contenedor de manera recursiva cuando reconozca que hay una lista generica o una subclase con valores
        /// </summary>
        /// <param name="container">Cualquier clase de la cual se puedan obtener subclases o listas genericas</param>
        /// <param name="parameters">Lista de parametros que se van a retornar luego de haber evaluado la clase padre</param>
        /// <param name="IncludeSubParameters">El metodo por defecto incluye las clases hijas dentro de los parametros, sin embargo cuando no se desee agregar clases dentro del contenedor, debe enviarse false</param>
        /// <returns></returns>  
        public static List<SqlParameter> GetParametersFromContainer(object container, List<SqlParameter> parameters, string model, bool IncludeSubParameters = true, bool pluralizeModel = true)
        {
            DataTable AsDataTable = new DataTable();
            string schema = string.Empty;
            if (pluralizeModel)
                model = new Pluralizer().Pluralize(model);
            foreach (PropertyInfo property in container.GetType().GetProperties().Where(w => model.Equals(w.Name) && !SystemTypes.Contains(w.PropertyType)))
            {
                string ParameterName = new Pluralizer().Singularize(property.Name);
                if (!parameters.Select(s => s.ParameterName).Contains($"@{ParameterName}"))
                {
                    if (!property.PropertyType.IsGenericType)
                    {
                        schema = property.GetValue(container).GetType().GetProperty("Schema").GetValue(property.GetValue(container)).ToString();
                        parameters.Add(new SqlParameter() { SqlDbType = SqlDbType.Structured, ParameterName = $"@{ParameterName}", Value = property.ClassToDataTable(container) });
                    }
                    else
                    {
                        if (property.GetValue(container) is IList GenericList && GenericList.Count > 0)
                        {
                            schema = GenericList[0].GetType().GetProperty("Schema").GetValue(GenericList[0]).ToString();
                            parameters.Add(new SqlParameter() { SqlDbType = SqlDbType.Structured, ParameterName = $"@{ParameterName}", Value = property.GetValue(container).ListToDataTable() });
                        }
                    }
                }
                else
                {
                    AsDataTable = parameters.Find(f => f.ParameterName == $"@{ParameterName}").Value as DataTable;
                    parameters.Find(f => f.ParameterName == $"@{ParameterName}").Value = AsDataTable.AddRange(property.GetValue(container));
                }
                if (IncludeSubParameters)
                    GetSubParameters(property.GetValue(container), parameters);
            }
            if (container.GetType().GetProperty("CurrentUser").GetValue(container) != null && parameters.Where(x => x.ParameterName.Equals("@CurrentUser")).ToList().Count == 0)
                parameters.Add(new SqlParameter() { ParameterName = "@CurrentUser", Value = container.GetType().GetProperty("CurrentUser").GetValue(container), SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
            parameters.Add(new SqlParameter() { ParameterName = "@RETURN", SqlDbType = SqlDbType.TinyInt, Direction = ParameterDirection.Output });
            parameters.Add(new SqlParameter() { ParameterName = "@RETURN_MESSAGE", SqlDbType = SqlDbType.NVarChar, Direction = ParameterDirection.Output, Size = 500 });
            return parameters;
        }
        /// <summary>
        /// Este metodo permite obtener las entidades que se encuentran dentro de una clase, de esta manera se obtiene un subnivel de la clase que se desea afectar de alguna manera
        /// </summary>
        /// <param name="container"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static void GetSubParameters(object container, List<SqlParameter> parameters)
        {
            bool isList = container is IList;
            bool propIsList = false;
            DataTable AsDataTable = new DataTable();
            if (isList)
            {
                foreach (var item in container as IList)
                {
                    GetSubParameters(item, parameters);
                }
            }
            else
            {
                foreach (PropertyInfo prop in container.GetType().GetProperties().Where(w => !SystemTypes.Contains(w.PropertyType) && Nullable.GetUnderlyingType(w.PropertyType) == null && !Attribute.IsDefined(w, typeof(IgnorePropertyAttribute))))
                {
                    propIsList = prop.GetValue(container) is IList;
                    string ParameterName = new Pluralizer().Singularize(propIsList ? prop.PropertyType.GetGenericArguments()[0].Name : prop.Name);
                    if (!parameters.Select(s => s.ParameterName).Contains($"@{ParameterName}"))
                    {
                        parameters.Add(new SqlParameter() { SqlDbType = SqlDbType.Structured, ParameterName = $"@{ParameterName}", Value = propIsList ? prop.GetValue(container).ListToDataTable() : prop.ClassToDataTable(container) });
                    }
                    else
                    {
                        AsDataTable = parameters.Find(f => f.ParameterName == $"@{ParameterName}").Value as DataTable;
                        parameters.Find(f => f.ParameterName == $"@{ParameterName}").Value = AsDataTable.AddRange(prop.GetValue(container));
                    }
                }
            }
        }

        /// <summary>
        /// Metodo de extension para los tipos MailMessage, para realizar el envio de dicho tipo. Si el ResponseCode retorna 200 el mensaje logro enviarse de manera satisfactoria de lo contrario se puedes obtener el error a partir de ResponseText
        /// </summary>
        /// <param name="message">Mensaje previamente estructurado para enviarlo</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText) SendEmail(this MailMessage message)
        {
            try
            {
                using (SmtpClient client = new SmtpClient(SMTPHost, SMTPPort))
                {
                    client.Credentials = new NetworkCredential(SMTPUser, SMTPPassword);
                    client.EnableSsl = true;
                    foreach (string EmailReplyToList in ReplyToList.Split(','))
                    {
                        message.ReplyToList.Add(EmailReplyToList);
                    }
                    client.Send(message);
                    return (HttpStatusCode.OK, "Correo enviado con exito");
                }
            }
            catch (Exception ex)
            {
                return (HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Metodo de extension para los tipos MailMessage, para realizar el envio de dicho tipo. Si el ResponseCode retorna 200 el mensaje logro enviarse de manera satisfactoria de lo contrario se puedes obtener el error a partir de ResponseText
        /// </summary>
        /// <param name="message">Mensaje previamente estructurado para enviarlo</param>
        /// <returns></returns>
        public static (HttpStatusCode ResponseCode, string ResponseText) SendEmailAsync(this MailMessage message)
        {
            try
            {
                SmtpClient client = new SmtpClient(SMTPHost, SMTPPort);
                client.Credentials = new NetworkCredential(SMTPUser, SMTPPassword);
                client.EnableSsl = true;
                foreach (string EmailReplyToList in ReplyToList.Split(','))
                {
                    message.ReplyToList.Add(EmailReplyToList);
                }
                client.SendCompleted += new SendCompletedEventHandler(Email_OnCompleted);
                client.SendAsync(message, "");
                client.Dispose();
                return (HttpStatusCode.OK, "Correo enviado con exito");
            }
            catch (Exception ex)
            {
                return (HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private static void Email_OnCompleted(object sender, AsyncCompletedEventArgs e)
        {
            var mail = (MailMessage)e.UserState;
            var error = string.Empty;
            if (e.Error != null)
            {
                error = string.Format("Error sending email to {0} with subject [{1}]: {2}", mail.To, mail.Subject, e.Error);
            }
            else if (e.Cancelled)
            {
                error = string.Format("Cancelled email to {0} with subject [{1}].", mail.To, mail.Subject);
            }
            else
            {
                error = string.Format("Sent email to {0} with subject [{1}].", mail.To, mail.Subject);
            }
        }

        public static async Task<HttpResponseMessage> SendSMSAsync(this string message, string phone)
        {
            using (HttpClient client = new HttpClient())
            {
                return await client.GetAsync($"{SMSUrl}/{SMSKey}/t={phone}&m={message}").ConfigureAwait(false);
            }
        }

        public static async void SendSMS(this string message, string phone)
        {
            using (HttpClient client = new HttpClient())
            {
                await client.GetAsync($"{SMSUrl}/{SMSKey}/t={phone}&m={message}");
            }
        }

        public static async void SendSMS(this string message, List<string> phones)
        {
            foreach (string phone in phones)
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage responseMessage = await client.GetAsync($"{SMSUrl}/{SMSKey}/t={phone}&m={message}");
                }
            }
        }

        /// <summary>
        /// Generador de tokens en formato hexadecimal con un limite de 64 caracteres para las solicitudes de restablecimiento de contraseña
        /// </summary>
        /// <returns></returns>
        public static string GetToken()
        {
            byte[] RandomString = Encoding.ASCII.GetBytes(Guid.NewGuid().ToString());
            SHA256Managed HashString = new SHA256Managed();
            string HashValue = string.Join("", HashString.ComputeHash(RandomString).Select(s => string.Format("{0:x2}", s).ToString()));
            return HashValue.Substring(0, (HashValue.Length > 64 ? 64 : HashValue.Length));
        }
        /// <summary>
        /// Obtiene una lista de tipo KeyValuePair y la convierte al tipo Dictionary
        /// </summary>
        /// <param name="ValuePair"></param>
        /// <returns></returns>
        public static Dictionary<string, object> ToDictionary(IEnumerable<KeyValuePair<string, string>> ValuePair)
        {
            Dictionary<string, object> temp = new Dictionary<string, object>();
            foreach (KeyValuePair<string, string> item in ValuePair)
            {
                temp.Add(item.Key, item.Value);
            }
            return temp;
        }

        public static IEnumerable<KeyValuePair<string, string>> TryCast(object obj)
        {
            List<KeyValuePair<string, string>> temp = new List<KeyValuePair<string, string>>();
            if (obj is IList listValues)
            {
                foreach (KeyValuePair<string, string> item in listValues)
                {
                    temp.Add(item);
                }
            }
            return temp;
        }

        /// <summary>
        /// Obtiene los los procedimientos de una entidad ya sea una clase o una lista de clases
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetProceduresFromModel(string Model)
        {
            Dictionary<string, string> procedures = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Model))
            {
                if (!Model.Equals("User"))
                    Model = new Pluralizer().Pluralize(Model);
                PropertyInfo PropertyContainer = new ERContainer().GetType().GetProperty(Model);
                if (PropertyContainer.PropertyType.IsGenericType)
                {
                    Type ModelType = PropertyContainer.PropertyType.GetGenericArguments()[0];
                    object container = Activator.CreateInstance(ModelType);
                    foreach (PropertyInfo property in container.GetType().GetProperties().Where(w => w.Name.Contains("Procedure")))
                    {
                        procedures.Add(property.Name, property.GetValue(container).ToString());
                    }
                }
                else
                {
                    object instance = Activator.CreateInstance(PropertyContainer.PropertyType);
                    foreach (PropertyInfo property in instance.GetType().GetProperties().Where(w => w.Name.Contains("Procedure")))
                    {
                        string procedure = property.GetValue(instance).ToString();
                        procedures.Add(property.Name, procedure);
                    }
                }

            }
            return procedures;
        }

        /// <summary>
        /// Obtiene el schema de una entidad ya sea una clase o una lista de una clase
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static string GetSchemaFromModel(string Model)
        {
            if (!Model.Equals("User"))
                Model = new Pluralizer().Pluralize(Model);
            PropertyInfo PropertyContainer = new ERContainer().GetType().GetProperty(Model);
            Type ModelType = PropertyContainer.PropertyType.IsGenericType ? PropertyContainer.PropertyType.GetGenericArguments()[0] : PropertyContainer.PropertyType;
            object container = Activator.CreateInstance(ModelType);
            return container.GetType().GetProperty("Schema").GetValue(container).ToString();
        }

        public static StringBuilder SetDefaultValuesToEmail(StringBuilder bodyNotificaction, User user, Member member)
        {
            //Seteo de variables en el cuerpo del correo
            bodyNotificaction = bodyNotificaction.Replace("[USER]", member.Names + (string.IsNullOrEmpty(member.Surnames) ? string.Empty : " " + member.Surnames));
            bodyNotificaction = bodyNotificaction.Replace("[USERNAME]", user.UserName);
            bodyNotificaction = bodyNotificaction.Replace("[YEAR]", DateTime.Now.ToString("yyyy"));
            bodyNotificaction = bodyNotificaction.Replace("[URLSUPPORT]", UrlSuppport);
            bodyNotificaction = bodyNotificaction.Replace("[URLWEBSITE]", UrlWebSite);
            bodyNotificaction = bodyNotificaction.Replace("[SUPPORTMAIL]", SupportMail);
            return bodyNotificaction;
        }

        public static string GetPhoneNumber(Member MemberInfo)
        {
            List<string> AllNumbers = new List<string>();
            string phoneNumber = string.Empty;
            AllNumbers = MemberInfo.MemberPhoneNumbers.Where(w => w.PhoneNumber.Count() > 0).Select(s => s.PhoneNumber).ToList();
            AllNumbers = AllNumbers.Select(s => Regex.Replace(s, "[^0-9]+", "", RegexOptions.Compiled).Remove(0, 3)).ToList();
            if (AllNumbers.Count > 0)
                phoneNumber = AllNumbers.Where(w => w.StartsWith("8"))?.FirstOrDefault() ?? string.Empty;
            return phoneNumber;
        }
    }
}