﻿namespace DALBase
{
    using System;
    using System.Linq;
    using System.Security.Cryptography;

    public class RandomPassword
    {
        // Tamaño minimo y maximo de la contraseña
        private static int DEFAULT_MIN_PASSWORD_LENGTH = 8;
        private static int DEFAULT_MAX_PASSWORD_LENGTH = 12;

        // Caracteres soportados para la contraseña
        private static string PASSWORD_CHARS_LCASE = "abcdefghijklmnopqrstuvwxyz";
        private static string PASSWORD_CHARS_UCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static string PASSWORD_CHARS_NUMERIC = "0123456789";
        private static string PASSWORD_CHARS_SPECIAL = "*$-+?_&=!%{}/@";
        private static string ResultingPassword = string.Empty;

        /// <summary>
        /// Metodo para generar una contraseña aleatoria entre 8 a 12 caracteres. La contraseña incluye Mayusculas, Minusculas, Digitos y Caracteres especiales
        /// </summary>
        /// <returns></returns>
        public static string GetPassword()
        {
            do
            {
                ResultingPassword = Generate();
            } while (!(ResultingPassword.Any(c => char.IsDigit(c)) && ResultingPassword.Any(c => char.IsUpper(c)) && ResultingPassword.Any(c => char.IsLower(c)) && ResultingPassword.Any(c => char.IsLetter(c))));
            return ResultingPassword;
        }

        private static string Generate()
        {
            // Matriz local que contenga caracteres de contraseña admitidos
            // agrupados por tipos
            char[][] CharGroups = new char[][] { PASSWORD_CHARS_LCASE.ToCharArray(), PASSWORD_CHARS_UCASE.ToCharArray(), PASSWORD_CHARS_NUMERIC.ToCharArray(), PASSWORD_CHARS_SPECIAL.ToCharArray() };

            // Use esta matriz para rastrear el número de caracteres no utilizados en cada
            // grupo de personajes
            int[] CharsLeftInGroup = new int[CharGroups.Length];

            // Inicialmente, no se utilizan todos los caracteres de cada grupo.
            for (int i = 0; i < CharsLeftInGroup.Length; i++)
                CharsLeftInGroup[i] = CharGroups[i].Length;

            // Utilice este conjunto para seguir (iterar) grupos de caracteres no utilizados.
            int[] LeftGroupsOrder = new int[CharGroups.Length];

            // Inicialmente, no se usan todos los grupos de caracteres.
            for (int i = 0; i < LeftGroupsOrder.Length; i++)
                LeftGroupsOrder[i] = i;

            // Usa una matriz de 4 bytes para completarla con bytes aleatorios y luego conviértela
            // a un valor entero.
            byte[] RandomBytes = new byte[4];

            // Genera 4 bytes aleatorios
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(RandomBytes);

            // Convertir los 4 bytes en un valor int32
            int seed = BitConverter.ToInt32(RandomBytes, 0);

            // Ahora, esta es una aleatorización real.
            Random random = new Random(seed);

            // Esta matriz contendrá caracteres de contraseña.
            char[] password = null;
            password = new char[random.Next(DEFAULT_MIN_PASSWORD_LENGTH, DEFAULT_MAX_PASSWORD_LENGTH + 1)];

            // Indice del siguiente carácter que se agregará a la contraseña.
            int NextCharIndex;

            // Indice del siguiente grupo de caracteres a procesar.
            int NextGroupIndex;

            // Indice que se usara para rastrear grupos de caracteres no procesados.
            int NextLeftGroupsOrderIndex;
            // Indice del último carácter no procesado en un grupo.
            int LastCharIndex;
            // Indice del último grupo no procesado.
            int LastLeftGroupsOrderIdx = LeftGroupsOrder.Length - 1;

            for (int i = 0; i < password.Length; i++)
            {
                if (LastLeftGroupsOrderIdx == 0)
                    NextLeftGroupsOrderIndex = 0;
                else
                    NextLeftGroupsOrderIndex = random.Next(0, LastLeftGroupsOrderIdx);

                NextGroupIndex = LeftGroupsOrder[NextLeftGroupsOrderIndex];
                LastCharIndex = CharsLeftInGroup[NextGroupIndex] - 1;

                if (LastCharIndex == 0)
                    NextCharIndex = 0;
                else
                    NextCharIndex = random.Next(0, LastCharIndex + 1);

                password[i] = CharGroups[NextGroupIndex][NextCharIndex];

                if (LastCharIndex == 0)
                    CharsLeftInGroup[NextGroupIndex] = CharGroups[NextGroupIndex].Length;
                else
                {
                    if (LastCharIndex != NextCharIndex)
                    {
                        char temp = CharGroups[NextGroupIndex][LastCharIndex];
                        CharGroups[NextGroupIndex][LastCharIndex] = CharGroups[NextGroupIndex][NextCharIndex];
                        CharGroups[NextGroupIndex][NextCharIndex] = temp;
                    }
                    CharsLeftInGroup[NextGroupIndex]--;
                }

                if (LastLeftGroupsOrderIdx == 0)
                    LastLeftGroupsOrderIdx = LeftGroupsOrder.Length - 1;
                else
                {
                    if (LastLeftGroupsOrderIdx != NextLeftGroupsOrderIndex)
                    {
                        int temp = LeftGroupsOrder[LastLeftGroupsOrderIdx];
                        LeftGroupsOrder[LastLeftGroupsOrderIdx] = LeftGroupsOrder[NextLeftGroupsOrderIndex];
                        LeftGroupsOrder[NextLeftGroupsOrderIndex] = temp;
                    }
                    LastLeftGroupsOrderIdx--;
                }
            }

            return new string(password);
        }
    }
}
