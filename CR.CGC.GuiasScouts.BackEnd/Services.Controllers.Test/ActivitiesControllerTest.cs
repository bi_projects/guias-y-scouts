﻿using System;
using System.Net.Mail;
using CR.CGC.GuiasScouts.BackEnd.Controllers;
using DALBase;
using Entities;
using static System.Net.HttpStatusCode;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Services.Controllers.Test
{
    [TestClass]
    public class ActivitiesControllerTest
    {
        ActivitiesController Actividades;
        ERContainer Correo;
        List<Event> Events;

        public ActivitiesControllerTest()
        {
            Actividades = new ActivitiesController();

            Correo = new ERContainer()
            {
                Events = new List<Event>()
                {
                new Event()
                {
                    EventId = 1
                }
                },
         
            };
        }

        [TestMethod]
        public void SendMailsTest()
        {
            var response = Actividades.SendEmails(Correo);
            Assert.IsTrue(response.StatusCode == OK);
        }
    }
}
