﻿using CR.CGC.GuiasScouts.BackEnd.Controllers;
using CR.CGC.GuiasScouts.BackEnd.Models;
using DALBase;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using static System.Net.HttpStatusCode;

namespace Services.Controllers.Test
{
    [TestClass]
    public class SecurityControllerTest
    {
        SecurityController SecurityEntity;
        HttpClient Client;
        UpdPassModel entidad;
        User usuario;
        User usuarioSesion;
        User confirmFP;

        public SecurityControllerTest()
        {
            SecurityEntity = new SecurityController();
            Client = new HttpClient();

            entidad = new UpdPassModel
            {
                UserName = "gys_admin",
                CurrentPassword = "123456789",
                Email = "gerald.morales@bi.com.ni",
                NewPassword = "123456789."
            };

            usuario = new User
            {
                UserId = 10,
                UserName = "gys_admin",
                Password = "123456789",
                PasswordResetToken = "",
                Active = true,
                Enabled = true
            };


            confirmFP = new User
            {
                UserId = 2,
                Active = true,
                Enabled = true,
                Email = "erica.argueta@bi.com.ni",
                UserName = "eargueta",
                Password = "Argueta11*",
                Source = "ConfirmForgotPasswordTest"
            };

            usuarioSesion = new User
            {
                UserId = 2,
                Active = true,
                Enabled = true,
                Email = "erica.argueta@bi.com.ni",
                UserName = "eargueta",
                Password = "Argueta11*"
            };

        }
        [TestMethod]
        public void UpdatePasswordTest()
        {
            var response = SecurityEntity.UpdatePassword(entidad);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CreateFirstUserTest()
        {
            var response = SecurityEntity.CreateFirstUser(usuario);
            Assert.IsTrue(response.StatusCode == OK);
        }

        [TestMethod]
        public void LoginInTest()
        {
            var response = SecurityEntity.LogIn(usuarioSesion);
            Assert.IsTrue(response.StatusCode == OK);
        }

        [TestMethod]
        public void ConfirmForgotPasswordTest()
        {
            confirmFP.PasswordResetToken = GetToken();
            var (responseCode, ResponseText) = new ERContainer() { User = confirmFP }.ValidatePasswordResetRequest();
            if (responseCode == OK)
            {
                var response = SecurityEntity.ConfirmForgotPassword(confirmFP);
                Assert.IsNotNull(response);
            }
            else
                Assert.IsTrue(false);
        }

        /// <summary>
        /// Generador de tokens en formato hexadecimal con un limite de 64 caracteres para las solicitudes de restablecimiento de contraseña
        /// </summary>
        /// <returns></returns>
        public static string GetToken()
        {
            byte[] RandomString = Encoding.ASCII.GetBytes(Guid.NewGuid().ToString());
            SHA256Managed HashString = new SHA256Managed();
            string HashValue = string.Join("", HashString.ComputeHash(RandomString).Select(s => string.Format("{0:x2}", s).ToString()));
            return HashValue.Substring(0, (HashValue.Length > 64 ? 64 : HashValue.Length));
        }
    }
}
