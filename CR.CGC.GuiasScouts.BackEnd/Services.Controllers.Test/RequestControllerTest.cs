﻿using System;
using CR.CGC.GuiasScouts.BackEnd.Controllers;
using Entities;
using static System.Net.HttpStatusCode;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DALBase;
using System.Net.Mail;

namespace Services.Controllers.Test
{
    [TestClass]
    public class RequestControllerTest
    {
        RequestsController Solicitudes;
        EmailContainer Correo;

        public RequestControllerTest()
        {
            Solicitudes = new RequestsController();

            Correo = new EmailContainer()
            {
                mailMessage = new System.Net.Mail.MailMessage()
                {
                    From = new MailAddress(Utilities.FromMailAddress, Utilities.NameMailAddress),
                    IsBodyHtml = false,
                    Subject = "TestText",
                    Body = "TestText"
                },
                ListToSend = "erica.argueta@bi.com.ni, armando.bustos@bi.com.ni"
            };
        }

        [TestMethod]
        public void SendMailTest()
        {
            var response = Solicitudes.SendMail(Correo);
            Assert.IsTrue(response.StatusCode == OK);
        }


    }
}
