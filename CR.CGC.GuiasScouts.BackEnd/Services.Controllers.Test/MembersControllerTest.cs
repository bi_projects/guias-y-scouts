﻿using CR.CGC.GuiasScouts.BackEnd.Controllers;
using CR.CGC.GuiasScouts.BackEnd.Models;
using DALBase;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using static System.Net.HttpStatusCode;

namespace Services.Controllers.Test
{
    [TestClass]
    public class MembersControllerTest
    {
        private MembersController MemberEntity;
        private static HttpClient Client;
        private ERContainer Entity;

        public MembersControllerTest()
        { 
            MemberEntity = new MembersController();
            Client = new HttpClient();

            Entity = new ERContainer
            {
                Model = "Member",
                Members = new List<Member>() {
                    new Member()
                    {
                        Names = "TestText",
                        Surnames = "TestText",
                        Photo = "TestText",
                        IDType = 2,
                        IDNumber = "TestText",
                        NationalityId = 4,
                        BirthDate = DateTime.Now.AddYears(-4),
                        GenderId = 2,
                        StartDate = DateTime.Now,
                        PromiseDate = DateTime.Now,
                        Email = "TestText",
                        IsGorS = true,
                        IsTutor = false,
                        MemberStatusId = 1,
                        Address = "TestText",
                        DistrictId = 5,
                        PostalCode = "TestText",
                        Workplace = "TestText",
                        Enabled = false
                    }
                }
            };

        }

        [TestMethod]
        public void CreateAccountTest()
        {
            var response = MemberEntity.Insert(Entity);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetListForMembersTest()
        {
       
            KeyValuePair<string, string> obj = new KeyValuePair<string, string>("Model", "User");
            var response = MemberEntity.GetListForMembers(obj);

            Assert.IsTrue(response.StatusCode == OK);
        }
    }
}
