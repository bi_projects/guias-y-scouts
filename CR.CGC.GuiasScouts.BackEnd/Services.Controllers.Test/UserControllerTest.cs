﻿using System;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CR.CGC.GuiasScouts.BackEnd.Controllers;
using CR.CGC.GuiasScouts.BackEnd.Models;
using DALBase;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using static System.Net.HttpStatusCode;

namespace Services.Controllers.Test
{
    [TestClass]
    public class UserControllerTest
    {
        User confirmFP;
        User UserInactive;
        UserController UserEntity;
        User NewUser;

        public UserControllerTest()
        {
            UserEntity = new UserController();
            confirmFP = new User
            {
                UserId = 2,
                Active = true,
                Enabled = true,
                Email = "erica.argueta@bi.com.ni",
                UserName = "earguetab",
                Password = "Argueta11*",
                Source = "ForcePasswordChangeTest"
        };

            UserInactive = new User
            {
                UserId = 2,
                Active = false,
                Enabled = false,
                Email = "erica.argueta@bi.com.ni",
                UserName = "earguetab",
                Password = "Argueta11*",
                Source = "Activation"
            };

        }
        
        [TestMethod]
        public void ForcePasswordChangeTest()
        {

            confirmFP.PasswordResetToken = GetToken();
            ERContainer container = new ERContainer() { User = confirmFP };
            var (ResponseCodeReset, ResponseTextReset) = container.ValidatePasswordResetRequest();
            if (ResponseCodeReset == OK)
            {
                var (ResponseCode, ResponseText, ResultingObject) = container.ResetPassword(true);
                if (ResponseCode == OK)
                {
                    var response = UserEntity.ForcePasswordChange(confirmFP);
                    Assert.IsNotNull(response);
                }
                else
                    Assert.IsTrue(false);
            }
            else
                Assert.IsTrue(false);
        }

        [TestMethod]
        public void ActivationTest()
        {
            ERContainer container = new ERContainer() { User = UserInactive };
            UserInactive.PasswordResetToken = string.Empty;
            UserInactive.Password = RandomPassword.GetPassword();
            container.Users.Add(UserInactive);
            container.User = UserInactive;

            var (ResponseCode, ResponseText, ResultingObject) = DALBas.ActivateUser(container);

            if (ResponseCode == OK)
            {
                var response = UserEntity.Activation(container);
                Assert.IsTrue(response.StatusCode == OK);   
            }
            else
                Assert.IsTrue(false);

        }

        /// <summary>
        /// Generador de tokens en formato hexadecimal con un limite de 64 caracteres para las solicitudes de restablecimiento de contraseña
        /// </summary>
        /// <returns></returns>
        public static string GetToken()
        {
            byte[] RandomString = Encoding.ASCII.GetBytes(Guid.NewGuid().ToString());
            SHA256Managed HashString = new SHA256Managed();
            string HashValue = string.Join("", HashString.ComputeHash(RandomString).Select(s => string.Format("{0:x2}", s).ToString()));
            return HashValue.Substring(0, (HashValue.Length > 64 ? 64 : HashValue.Length));
        }

    }
}
