﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using CR.CGC.GuiasScouts.BackEnd.Controllers;
using static System.Net.HttpStatusCode;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Services.Controllers.Test
{
    [TestClass]
    public class CRUDControllerTest
    {
        private CRUDController CRUDEntity;
        private ERContainer Entity;
        private static HttpClient Client;

        public CRUDControllerTest()
        {
            CRUDEntity = new CRUDController();
            Client = new HttpClient();

            Entity = new ERContainer
            {
                Model = "BaseCatalog",
                BaseCatalogs = new List<BaseCatalog>() {
                    new BaseCatalog()
                    {
                        BaseCatalogId = 0,
                        Name = "TestText"+DateTime.Now.ToString("yy-MM-dd, HH:mm:ss"),
                        Description = "TestText",
                        Enabled = true,
                        ItemOf = 4
                    }
                }
            };
        }

        [TestMethod]
        public void InsertTest()
        {
            var response = CRUDEntity.Insert(Entity);

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetObjectsTest()
        {
            KeyValuePair<string, string> obj = new KeyValuePair<string, string>("Model", "User");
            var response = CRUDEntity.GetObjects(obj);

            Assert.IsTrue(response.StatusCode == OK);
        }
    }
}
