#Registry, activity management and CRM system for Guides and Scouts Costa Rica

#Development Actors:
1 Project Manager

2 .Net Developers

1 Web Developer / Web Designer


#Used technology
Net Framework 4.7

C #

MSSQL Server 2016


#Style
Normal mode

Night mode

Daltonic mode

Font Accessibility Management


#Modules
Alerts: Notifications through SMS, Emails and alerts popups in the system, consistent with the self-management of alerts of each enrolled user.

Registry: Users enrollment, progressions and group assignments

Activities: Event management with template customization (Mailchimp alike)

Publications: News and notes managed by authorized actors for sending personalized templates (Mailchimp alike)

CRM: Control of tickets with personalized access levels

History: History of actions per user with personalized template


#Development process
Conceptualization

Arts design

HTML - CSS Layouts Modeling

Implementation

Deployment

#Design phases:
Template design - take a look at (https://bitbucket.org/bi_projects/guias-y-scouts/downloads/PROTOTYPES.zip)

Mobile First layout

CSS animations and transitions
