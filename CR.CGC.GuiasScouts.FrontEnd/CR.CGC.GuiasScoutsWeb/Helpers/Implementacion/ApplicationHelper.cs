﻿/*==========================================================================
Archivo:            IApplicationHelper
Descripción:        Helper de la Aplicación                      
Autor:              Armando Bustos                           
Fecha de creación:  04-01-2018                                              
Versión:            1.0                                                       
Derechos Reservados BI S.A.                                         
==========================================================================*/
using CR.CGC.GuiasScoutsWeb.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using static System.Net.HttpStatusCode;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Entities;
using Pluralize.NET;

namespace CR.CGC.GuiasScoutsWeb.Implementacion
{
    public class ApplicationHelper : IApplicationHelper
    {
        private string urlApi = "";
        private JavaScriptSerializer serializer = new JavaScriptSerializer();
        private HttpClient client = new HttpClient();
        private const int PageSize = 10;

        public ApplicationHelper(string url) { this.urlApi = url; }


        public async Task<object> CreateObject<T>(string apiUrl, object obj)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync(urlApi + apiUrl, obj);
                if (response.StatusCode == OK)
                {
                    return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    return response.Content.ReadAsStringAsync().Result.ToString().TrimStart('"').TrimEnd('"');
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo para realizar una peticion POST al api
        /// </summary>
        /// <param name="Resource">Ruta del recurso a la cual se desea realizar la peticion</param>
        /// <param name="obj">Objeto que sera enviado en la peticion</param>
        /// <returns></returns>
        public async Task<(System.Net.HttpStatusCode ResponseCode, string ResponseText)> CreatePost(string Resource, object obj)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync(urlApi + Resource, obj);
                if (response.StatusCode == OK)
                    return (OK, "");
                string ResultText = await response.Content.ReadAsStringAsync();
                return (response.StatusCode, ResultText.TrimStart('"').TrimEnd('"'));
            }
            catch (Exception ex)
            {
                return (InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Metodo para realizar una peticion POST al api y obtener el objeto creado
        /// </summary>
        /// <param name="Resource">Ruta del recurso a la cual se desea realizar la peticion</param>
        /// <param name="obj">Objeto que sera enviado en la peticion</param>
        /// <returns></returns>
        public async Task<(System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj)> CreatePostGetElement<T>(string Resource, object obj)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync(urlApi + Resource, obj);
                string ResultText = await response.Content.ReadAsStringAsync();
                ResultText = ResultText.TrimStart('"').TrimEnd('"').Replace("\\\\/", "\\/").Replace("\\\"", "\"");
                if (response.StatusCode == OK)
                    return (OK, string.Empty, JsonConvert.DeserializeObject<T>(ResultText));
                return (response.StatusCode, ResultText.TrimStart('"').TrimEnd('"'), Activator.CreateInstance<T>());
            }
            catch (Exception ex)
            {
                return (InternalServerError, ex.Message, Activator.CreateInstance<T>());
            }
        }

        /// <summary>
        /// Obtiene un elemento o lista de elementos siempre y cuando el elemento a convertir sea una clase o lista de clases
        /// <para>Es necesario que en cada peticion que se realiza haciendo uso de este metodo siempre exista un parametro dentro del diccionario que contenga la Key=Model con el modelo que se desea obtener</para>
        /// </summary>
        /// <typeparam name="T">Clase o lista de clase</typeparam>
        /// <param name="ApiUrl">Ruta del recurso a la cual se desea hacer la peticion para obtener los elementos</param>
        /// <param name="attr">Atributos que seran parte de las condiciones a tomar en cuenta para encontrar los elementos buscados</param>
        /// <returns></returns>
        public object CreateGetAsync<T>(string Resource, Dictionary<string, object> attr)
        {
            try
            {
                string parameters = attr.Count > 0 ? string.Join("&", attr.ToList().Select(s => string.Concat(s.Key, "=", s.Value))) : null;
                ERContainer container;
                Task<HttpResponseMessage> TaskGet = client.GetAsync(urlApi + Resource + (parameters != null ? "?" + parameters : ""));
                TaskGet.Wait();
                HttpResponseMessage response = TaskGet.Result;
                if (response.StatusCode == OK)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    result = result.Replace("\\\\/", "\\/").Replace("\\\"", "\"").TrimStart('"').TrimEnd('"');
                    container = JsonConvert.DeserializeObject<ERContainer>(result);
                    if (parameters == null && typeof(T).Equals(container.GetType())) return container;
                    else return container?.GetType().GetProperties().ToList().Find(f => f.Name.Equals(attr["Model"].ToString() + "s")).GetValue(container) ?? Activator.CreateInstance<T>();
                }
                return response.Content.ReadAsStringAsync().Result.ToString().TrimStart('"').TrimEnd('"');
            }
            catch (Exception ex)
            {
                return (InternalServerError, ex.Message, Activator.CreateInstance<T>());
            }
        }

        /// <summary>
        /// Obtiene un elemento o lista de elementos siempre y cuando el elemento a convertir sea una clase o lista de clases
        /// <para>El metodo retorna una tupla donde si el ResponseCode es igual a 200 el 3er valor del retorno contiene el elemento en caso contrario el segundo valor tiene el mensaje de error</para>
        /// <para>Es necesario que en cada peticion que se realiza haciendo uso de este metodo siempre exista un parametro dentro del diccionario que contenga la Key=Model con el modelo que se desea obtener</para>
        /// </summary>
        /// <typeparam name="T">Clase o lista de clase</typeparam>
        /// <param name="ApiUrl">Ruta del recurso a la cual se desea hacer la peticion para obtener los elementos</param>
        /// <param name="attr">Atributos que seran parte de las condiciones a tomar en cuenta para encontrar los elementos buscados</param>
        /// <returns></returns>
        public (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, string SearchText, int? RowCount, int? NumberPage) GetEntitiesAsync<T>(string Resource, Dictionary<string, object> attr)
        {
            try
            {
                string parameters = string.Join("&", attr.ToList().Select(s => string.Concat(s.Key, "=", s.Value)));
                ERContainer container;
                Task<HttpResponseMessage> TaskGet = client.GetAsync(urlApi + Resource + "?" + parameters);
                TaskGet.Wait();
                HttpResponseMessage response = TaskGet.Result;
                if (response.StatusCode == OK)
                {
                    string result_test = response.Content.ReadAsStringAsync().Result;
                    string result = response.Content.ReadAsStringAsync().Result.Replace("\\\\/", "\\/").Replace("\\\"", "\"").TrimStart('"').TrimEnd('"');
                    //container = new JavaScriptSerializer().Deserialize<ERContainer>(result);
                    container = JsonConvert.DeserializeObject<ERContainer>(result);
                    if (container is null)
                        return (InternalServerError, "Ocurrió un error inesperado al obtener la información solicitada, por favor intenta mas tarde. Si el problema persiste comunicate con el administrador del sistema", Activator.CreateInstance<T>(), null, null, null);
                    object ElementsOrElement = container.GetType().GetProperties().ToList().Find(f => f.Name.Equals(new Pluralize.NET.Pluralizer().Pluralize(attr["Model"].ToString()))).GetValue(container);
                    if (!(typeof(T).Name == "List`1"))
                        return (response.StatusCode, string.Empty, (ElementsOrElement as IList<T>).First(), null, null, null);
                    return (response.StatusCode, string.Empty, (T)ElementsOrElement, container.SearchText, container.RowCount, container.PageNumber);
                }
                return (response.StatusCode, response.Content.ReadAsStringAsync().Result.ToString().TrimStart('"').TrimEnd('"'), Activator.CreateInstance<T>(), null, null, null);
            }
            catch (Exception ex)
            {
                return (InternalServerError, ex.Message, Activator.CreateInstance<T>(), null, null, null);
            }
        }


        #region Outsourced

        public (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj) GetEntitiesList<T>(string controller, string model)
        {
            try
            {
                Dictionary<string, object> attr = new Dictionary<string, object>() { { "Model", model } };
                var response = GetEntitiesAsync<T>(controller + "/Model/GetObjects", attr);
                return (response.ResponseCode, response.ResponseText, response.obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, string SearchText, int RowCount, int NumberPage) GetEntitiesListPag<T>(string controller, string model, string SearchText, int PageNumber)
        {
            try
            {
                Dictionary<string, object> attr = new Dictionary<string, object>() { { "Model", model }, { "SearchText", (SearchText ?? "") }, { "PageNumber", PageNumber }, { "PageSize", PageSize } };
                var response = GetEntitiesAsync<T>(controller + "/Model/GetObjects", attr);
                return (response.ResponseCode, response.ResponseText, response.obj, response.SearchText ?? "", response.RowCount ?? 0, response.NumberPage ?? 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, int RowCount, int NumberPage) GetEntitiesListPag<T>(string controller, string model, int PageNumber)
        {
            try
            {
                Dictionary<string, object> attr = new Dictionary<string, object>() { { "Model", model }, { "PageNumber", PageNumber }, { "PageSize", PageSize } };
                var response = GetEntitiesAsync<T>(controller + "/Model/GetObjects", attr);
                return (response.ResponseCode, response.ResponseText, response.obj, response.RowCount ?? 0, response.NumberPage ?? 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj) GetEntity<T>(string controller, string model, string field, int id)
        {
            try
            {
                Dictionary<string, object> attr = new Dictionary<string, object>() { { "Model", model }, { field, id } };
                var response = GetEntitiesAsync<T>(controller + "/Model/GetObjects", attr);
                return (response.ResponseCode, response.ResponseText, response.obj);
                ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj) GetEntitiesByParams<T>(string controller, string model, (string, string)[] parameters)
        {
            try
            {
                Dictionary<string, object> attr = new Dictionary<string, object>() { { "Model", model } };
                foreach (var item in parameters) { attr.Add(item.Item1, item.Item2); }

                var response = GetEntitiesAsync<T>(controller + "/Model/GetObjects", attr);
                return (response.ResponseCode, response.ResponseText, response.obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, string SearchText, int RowCount, int NumberPage) GetEntitiesByParamsPag<T>(string controller, string model, string SearchText, int PageNumber, (string, string)[] parameters)
        {
            try
            {
                Dictionary<string, object> attr = new Dictionary<string, object>() { { "Model", model }, { "SearchText", (SearchText ?? "") }, { "PageNumber", PageNumber }, { "PageSize", PageSize } };
                foreach (var item in parameters) { attr.Add(item.Item1, item.Item2); }

                var response = GetEntitiesAsync<T>(controller + "/Model/GetObjects", attr);
                return (response.ResponseCode, response.ResponseText, response.obj, response.SearchText ?? "", response.RowCount ?? 0, response.NumberPage ?? 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, int RowCount, int NumberPage) GetEntitiesByParamsPag<T>(string controller, string model, int PageNumber, (string, string)[] parameters)
        {
            try
            {
                Dictionary<string, object> attr = new Dictionary<string, object>() { { "Model", model }, { "PageNumber", PageNumber }, { "PageSize", PageSize } };
                foreach (var item in parameters) { attr.Add(item.Item1, item.Item2); }

                var response = GetEntitiesAsync<T>(controller + "/Model/GetObjects", attr);
                return (response.ResponseCode, response.ResponseText, response.obj, response.RowCount ?? 0, response.NumberPage ?? 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, int RowCount, int NumberPage) GetCustomEntitiesByParamsPag<T>(string Resource, string model, int PageNumber, (string, string)[] parameters)
        {
            try
            {
                Dictionary<string, object> attr = new Dictionary<string, object>() { { "Model", model }, { "PageNumber", PageNumber }, { "PageSize", PageSize } };
                foreach (var item in parameters) { attr.Add(item.Item1, item.Item2); }

                var response = GetEntitiesAsync<T>(Resource, attr);
                return (response.ResponseCode, response.ResponseText, response.obj, response.RowCount ?? 0, response.NumberPage ?? 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}