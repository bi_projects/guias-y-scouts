﻿/*==========================================================================
Archivo:            IApplicationHelper
Descripción:        Interfaz de Helper de la Aplicación                      
Autor:              Armando Bustos                           
Fecha de creación:  04-01-2018                                              
Versión:            1.0                                                       
Derechos Reservados BI S.A.                                         
==========================================================================*/
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CR.CGC.GuiasScoutsWeb.Interfaces
{
    public interface IApplicationHelper
    {
        Task<object> CreateObject<T>(string apiUrl, object obj);

        Task<(System.Net.HttpStatusCode ResponseCode, string ResponseText)> CreatePost(string apiUrl, object obj);

        Task<(System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj)> CreatePostGetElement<T>(string apiUrl, object obj);

        object CreateGetAsync<T>(string ApiUrl, Dictionary<string, object> attr);

        (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, string SearchText, int? RowCount, int? NumberPage) GetEntitiesAsync<T>(string ApiUrl, Dictionary<string, object> attr);

        #region Outsourced

        (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj) GetEntitiesList<T>(string controller, string model);
        (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, string SearchText, int RowCount, int NumberPage) GetEntitiesListPag<T>(string controller, string model, string SearchText, int NumberPage);
        (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, int RowCount, int NumberPage) GetEntitiesListPag<T>(string controller, string model, int PageNumber);

        (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj) GetEntity<T>(string controller, string model, string field, int id);

        (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj) GetEntitiesByParams<T>(string controller, string model, (string, string)[] parameters);
        (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, string SearchText, int RowCount, int NumberPage) GetEntitiesByParamsPag<T>(string controller, string model, string SearchText, int NumberPage, (string, string)[] parameters);
        (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, int RowCount, int NumberPage) GetEntitiesByParamsPag<T>(string controller, string model, int PageNumber, (string, string)[] parameters);

        (System.Net.HttpStatusCode ResponseCode, string ResponseText, T obj, int RowCount, int NumberPage) GetCustomEntitiesByParamsPag<T>(string Resource, string model, int PageNumber, (string, string)[] parameters);

        #endregion

    }
}