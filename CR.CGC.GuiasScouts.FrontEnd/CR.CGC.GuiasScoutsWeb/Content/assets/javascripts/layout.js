
var slider = new mdc.slider.MDCSlider(document.querySelector('.mdc-slider'));
slider.listen('MDCSlider:change', function () {
    document.querySelector('html').style.fontSize = slider.value + 'px';
    userConfiguration.saveConf();
});

var menuChild = document.getElementsByClassName('menu-child');
Array.prototype.forEach.call(menuChild, function (item) {
    item.addEventListener('click', function () {
        item.parentElement.click();
    });
});

var menuButtonEl = null;
var bodyClassList = null;
var menuElList = null;

var appMenus = {
    loadMenus: function () {
        menuButtonEl = document.getElementsByClassName('menu-button');
        bodyClassList = [];
        menuElList = [];

        Array.prototype.forEach.call(menuButtonEl, function (item) {
            var menuEl = document.querySelector('#' + item.dataset.menuel);
            var menu = new mdc.menu.MDCMenu(menuEl);
            menu.setAnchorCorner(mdc.menu.MDCMenuFoundation.Corner.BOTTOM_START);
            menuElList.push([item, menuEl, menu]);

            Array.prototype.forEach.call(menu.items, function (node) {
                if (node.dataset.bodyclass != null && node.dataset.bodyclass != undefined) {
                    bodyClassList.push(node.dataset.bodyclass);
                }
            });
            menu.quickOpen = false;

            item.addEventListener('click', function () {
                Array.prototype.forEach.call(menuElList, function (el) {
                    if (item == el[0]) {
                        item.className = item.className.replace(" active", "");

                        if (el[2].open) el[2].hide();
                        else el[2].show();

                        item.className += " active";
                    }
                });
            });

            menuEl.addEventListener('MDCMenu:selected', function (evt) {
                var detail = evt.detail;
                item.className = item.className.replace(" active", "");
                if (detail.item.dataset.bodyclass != undefined && detail.item.dataset.bodyclass != null) {
                    Array.prototype.forEach.call(bodyClassList, function (className) {
                        document.querySelector('body').className = document.querySelector('body').className.replace(" " + className, "");
                    });
                    document.querySelector('body').className += " " + evt.detail.item.dataset.bodyclass;
                    document.querySelector('body').dataset.theme = evt.detail.item.dataset.bodyclass;
                }
                Array.prototype.forEach.call(menuElList, function (el) {
                    el[2].root_.className += " mdc-menu--animating-open";
                });
            });

            menuEl.addEventListener('MDCMenu:cancel', function (evt) {
                item.className = item.className.replace(" active", "");
                Array.prototype.forEach.call(menuElList, function (el) {
                    el[2].root_.className += " mdc-menu--animating-open";
                });

                if (item.className.indexOf('social_network_menu') !== -1) {
                    var socialParent = $(item).parents('[data-social_menu]')[0];
                    socialParent.className = socialParent.className.replace('social_menu_new_popup', '');
                }
            });
        });
    },

    init: function () {
        this.loadMenus();
    }
}

appMenus.init();

/// preloader
$(document).ready(function () {
    pageFunctions.hideElement($('#preloader'));
});