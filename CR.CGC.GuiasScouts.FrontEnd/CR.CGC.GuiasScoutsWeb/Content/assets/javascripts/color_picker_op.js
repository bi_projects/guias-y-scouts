var picker = new CP(document.querySelector('input')),
    code = document.createElement('input');

// prevent showing native color picker panel
picker.target.onclick = function(e) {
    e.preventDefault();
};

code.className = 'color-code';
code.pattern = '^#[A-Fa-f0-9]{6}$';
code.type = 'text';

picker.on("enter", function() {
    code.value = '#' + CP._HSV2HEX(this.get());
});

picker.on("change", function(color) {
    picker.target.value = '#' + color;
    code.value = '#' + color;
});

picker.picker.firstChild.appendChild(code);

function update() {
    if (this.value.length) {
        picker.set(this.value);
        picker.trigger("change", [this.value.slice(1)]);
    }
}

code.oncut = update;
code.onpaste = update;
code.onkeyup = update;
code.oninput = update;