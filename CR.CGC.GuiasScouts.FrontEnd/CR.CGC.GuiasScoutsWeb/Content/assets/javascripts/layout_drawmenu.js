var menuTabBar, bodyMenuTabBar;
var layoutDrawer = {
    // menu drawer
    menuListener: function () {
        let drawer = new mdc.drawer.MDCTemporaryDrawer(document.querySelector('.mdc-drawer--temporary'));
        document.querySelector('.menu').addEventListener('click', function () { drawer.open = true; });
    },
    activateWebMenu: function (el) {
        el.className += " active";
    },
    activeWebMenuClassRenamer: function () {
        active_web_menu = document.querySelector(".block.pad_1.active");
        if (active_web_menu != null) {
            active_web_menu.className = active_web_menu.className.replace(" active", "");
        }
    },
    appMenuActions: function () {
        var tabEl = document.querySelector('.main-menu');
        if (tabEl != null) {
            menuTabBar = new mdc.tabs.MDCTabBar(tabEl);
            menuTabBar.listen('MDCTabBar:change', function () {
                var menuTabs = menuTabBar.activeTab.root_;
                web_menu = document.getElementsByClassName("block pad_1");
                layoutDrawer.activeWebMenuClassRenamer();
                Array.prototype.forEach.call(web_menu, function (el) {
                    if (menuTabBar.activeTab.root_.dataset.rel == el.dataset.rel) {
                        layoutDrawer.activateWebMenu(el);
                    }
                })
                layoutDrawer.menuTabSelectorState(menuTabs);
            });
        }


        web_menu = document.getElementsByClassName("block pad_1");
        Array.prototype.forEach.call(web_menu, function (el) {
            el.addEventListener('click', function () {
                layoutDrawer.activeWebMenuClassRenamer();
                layoutDrawer.activateWebMenu(el);
                layoutDrawer.menuTabSelectorState(el);

                menuTabs = tabEl.getElementsByClassName("mdc-tab");
                currentTab = tabEl.querySelector(".menu-item.mdc-tab--active");
                if (currentTab != null) {
                    currentTab.className = currentTab.className.replace(" mdc-tab--active", "");
                }
                Array.prototype.forEach.call(menuTabs, function (dom) {
                    if (dom.dataset.rel == el.dataset.rel) {
                        tabIndex = Array.prototype.slice.call(menuTabs).indexOf(dom);
                        menuTabBar.activeTabIndex = tabIndex;
                    }
                });
            });
        });
    },
    bodyMenuActions: function () {
        var tabEl = document.querySelector('.body-menu');
        if (tabEl != null) {
            bodyMenuTabBar = new mdc.tabs.MDCTabBar(tabEl);
            bodyMenuTabBar.listen('MDCTabBar:change', function () {
                var menuTabs = bodyMenuTabBar.activeTab.root_;
                layoutDrawer.bodyMenuTabSelectorState(menuTabs);
            });
        }
    },
    tabScroller: function () {
        // tabscroller
        var scrollerEl = document.getElementsByClassName('mdc-tab-bar-scroller');
        Array.prototype.forEach.call(scrollerEl, function (el) {
            var tabBarScroller = new mdc.tabs.MDCTabBarScroller(el);

            tabBarScroller.layout();
            tabBarScroller.tabBar.layout();
        });
    },
    menuTabSelectorState: function (menuTabs) {
        mob_menu = document.querySelector(".container_submenu_mobile.selected");
        web_menu = document.querySelector(".level_second_menu_content.selected");
        if (mob_menu != null) {
            mob_menu.className = mob_menu.className.replace(" selected", "");
        }
        if (web_menu != null) {
            web_menu.className = web_menu.className.replace(" selected", "");
        }
        menuTabsArray = document.getElementsByClassName(menuTabs.dataset.rel);
        Array.prototype.forEach.call(menuTabsArray, function (el) {
            el.className += " selected";
        })
    },
    bodyMenuTabSelectorState: function (menuTabs) {
        content = document.querySelector(".display_select.active");
        if (content != null) {
            content.className = content.className.replace(" active", "");
        }
        bodyMenuTabsArray = document.getElementsByClassName(menuTabs.dataset.rel);
        Array.prototype.forEach.call(bodyMenuTabsArray, function (el) {
            el.className += " active";
        })
    },

    menuWebShowMoreText: function () {
        var elems = document.getElementsByClassName('disable_icon');
        var _elems = document.getElementsByClassName('description_menu_web');
        Array.prototype.forEach.call(elems, function (el) {
            el.addEventListener('click', function () {
                Array.prototype.forEach.call(_elems, function (_el) {
                    if (_el) {
                        if (_el.className.indexOf("unhidden") != -1) {
                            _el.className = _el.className.replace('unhidden', 'hidden');
                        } else {
                            _el.className = _el.className.replace('hidden', 'unhidden');
                        }
                    }
                    if (_el) {
                        if (_el.className.indexOf("unselected") != -1) {
                            _el.className = _el.className.replace('unselected', 'selected');
                        } else {
                            _el.className = _el.className.replace('selected', 'unselected');
                        }
                    }
                });
            });
        });
    },
}

var app = {
    init: function () {
        layoutDrawer.menuListener();
        layoutDrawer.tabScroller();
        layoutDrawer.appMenuActions();
        layoutDrawer.bodyMenuActions();
        layoutDrawer.menuWebShowMoreText();
    }
}

app.init();