﻿namespace CR.CGC.GuiasScoutsWeb.Models
{
    using global::Entities;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class FaqListViewModel
    {
        public int FaqModuleId { get; set; }

        [MaxLength(100)]
        public string ModuleName { get; set; }

        public int FaqsCount { get; set; }
    }

    public class SendFaqViewModel
    {
        public int FaqModuleId { get; set; }
        public int? FaqId { get; set; }
    }

    public class FaqDetailViewModel
    {
        public List<MenuModel> MenuModules { get; set; }
        public List<BaseCatalog> Modules { get; set; }
    }
}