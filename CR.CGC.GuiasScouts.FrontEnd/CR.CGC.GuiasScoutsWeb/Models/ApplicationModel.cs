﻿using CR.CGC.GuiasScoutsWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CR.CGC.GuiasScoutsWeb.Models
{
    public class ApplicationModel
    {
        /// <summary>
        /// Variable de auditoria para el Usuario de creacion del registro
        /// </summary>
        [DataMember()]
        public int UsuarioCrea { get { return CustomSession.CurrentSession.CurrentUser.Member.User.UserId; } }
        /// <summary>
        /// Variable de auditoria para la fecha de creacion del registro
        /// </summary>
        [DataMember()]
        public DateTime FechaCrea { get { return DateTime.Now; } }
        /// <summary>
        /// Variable de auditoria para el Usuario de modificacion del registro
        /// </summary>
        [DataMember()]
        public int UsuarioMod { get { return CustomSession.CurrentSession.CurrentUser.Member.User.UserId; } }
        /// <summary>
        /// Variable de auditoria para la fecha de modificacion del registro
        /// </summary>
        [DataMember()]
        public DateTime FechaMod { get { return DateTime.Now; } }
        /// <summary>
        /// Variable de auditoria para el tipo de transaccion realizada
        /// </summary>
        [DataMember()]
        public int Trans { get; }
        /// <summary>
        /// Variable de auditoria para la descripcion de la transaccion realizada
        /// </summary>
        [DataMember()]
        public string Desc { get; }
    }
}