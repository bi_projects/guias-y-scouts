﻿using CR.CGC.GuiasScoutsWeb.Common;
using Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CR.CGC.GuiasScoutsWeb.Models
{
    //public class UserModel
    //{
    //    public string UserName { get; set; }
    //    public string Password { get; set; }

    //    public PersonalInfo PersonalInfo { get; set; }
    //}

    public class UserRole
    {
        public int UserRoleId { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public bool Enabled { get; set; }
        public List<PermisoModel> Permisos { get; set; }
    }

    public class RolModel
    {
        public int IdRol { get; set; }
        public string NombreRol { get; set; }

        public List<PermisoModel> Permisos { get; set; }
        public List<PermissionModel> VisualPermissions { get; set; }
    }


    public class PermisoModel
    {
        public string NombrePermiso { get; set; }
        public string NombreController { get; set; }
    }


}