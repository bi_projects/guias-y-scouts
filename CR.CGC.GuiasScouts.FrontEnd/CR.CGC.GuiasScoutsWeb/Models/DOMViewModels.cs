﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CR.CGC.GuiasScoutsWeb.Models
{

    public class SelectViewModel
    {
        public string Text { get; set; }
        public string Id { get; set; }
        public bool Disabled { get; set; }
        public int SelectedId { get; set; }
        public string TextInBlank { get; set; }

        public bool iconAlert { get; set; }
        public bool InModal { get; set; }

        public List<SelectOptionViewModel> Options { get; set; } = new List<SelectOptionViewModel>();
    }

    public class SelectOptionViewModel
    {
        public string Text { get; set; }
        public int Id { get; set; }
        public bool Disabled { get; set; }
        public string Color { get; set; }
    }

    public class ResponseQuery
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class TableToolsViewModel
    {
        public string DeleteFunction { get; set; }
        public string DeleteTooltip { get; set; }
        public string UrlAdd { get; set; }
        public string AddTooltip { get; set; }

        public string SearchText { get; set; }
        public string UrlGet { get; set; }
        public string GridContainerId { get; set; }
    }

    public class PaginationToolsViewModel
    {
        public int RowCount { get; set; }
        public int NumberPage { get; set; }
        public int ActualRows { get; set; }

        public string SearchText { get; set; }
    }

    public class SocialNetworkViewModel
    {
        public int Index { get; set; }
        public int TypeId { get; set; }
    }

    public class SelectLocationViewModel
    {
        public SelectList SelectList { get; set; }
        public string Model { get; set; }
        public string ElemId { get; set; }
        public bool InModal { get; set; }
        public bool Disabled { get; set; }
    }

    public class ItemBinnacleViewModel
    {
        public string ItemType { get; set; }
        public string DisplayName { get; set; }
        public string OldData { get; set; }
        public string NewData { get; set; }
        public string ActionType { get; set; }

        public bool IsImageItem { get; set; } = false;
        public bool IsExternal { get; set; } = false;
        public List<ItemBinnacleViewModel> Details { get; set; } = new List<ItemBinnacleViewModel>();
    }
}