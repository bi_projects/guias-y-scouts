﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.CGC.GuiasScoutsWeb.Models
{
    public class PermissionViewModel
    {
        public Role Role { get; set; }
        public List<PermissionModel> PermissionsView { get; set; }
        public List<Permission> Permissions { get; set; }
    }
}