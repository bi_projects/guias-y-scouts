﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.CGC.GuiasScoutsWeb.Models
{
    public class RoleCompetenceViewModel
    {
        public Role Role { get; set; }
        public List<Competence> Competencies { get; set; }
        public List<CompetenceLevel> Levels { get; set; }
    }
}