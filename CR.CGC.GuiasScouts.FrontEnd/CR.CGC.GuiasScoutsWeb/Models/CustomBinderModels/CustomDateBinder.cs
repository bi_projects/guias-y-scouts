﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace CR.CGC.GuiasScoutsWeb.Models.CustomBinderModels
{
    public class CustomDateBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext == null) throw new ArgumentNullException("controllerContext", "controllerContext is null.");
            if (bindingContext == null) throw new ArgumentNullException("bindingContext", "bindingContext is null.");
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value == null) {
                return value;
                //throw new ArgumentNullException(bindingContext.ModelName);
            } 

            CultureInfo cultureInf = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            cultureInf.DateTimeFormat.ShortDatePattern = "dd-MM-yyyy";
            cultureInf.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyy HH:mm";
            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, value);

            try
            {
                var date = value.ConvertTo(typeof(DateTime), cultureInf);
                return date;
            }
            catch (Exception ex)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex);
                return null;
            }
        }
    }

    public class CustomDecimalBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext == null) throw new ArgumentNullException("controllerContext", "controllerContext is null.");
            if (bindingContext == null) throw new ArgumentNullException("bindingContext", "bindingContext is null.");
            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valueProviderResult == null) throw new ArgumentNullException(bindingContext.ModelName);

            //CultureInfo cultureInf = (CultureInfo)CultureInfo.CurrentUICulture.Clone();
            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProviderResult);

            try
            {
                var value = Convert.ToDecimal(valueProviderResult.AttemptedValue, CultureInfo.InvariantCulture);
                //var value = decimal.Parse(valueProviderResult.AttemptedValue == string.Empty ? null : valueProviderResult.AttemptedValue, NumberStyles.AllowCurrencySymbol | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, cultureInf);
                //var value = valueProviderResult.ConvertTo(typeof(Decimal), cultureInf);
                return value;
            }
            catch (Exception ex)
            {
                //bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex);
                if (valueProviderResult.AttemptedValue.Equals("N.aN") || valueProviderResult.AttemptedValue.Equals("NaN") || valueProviderResult.AttemptedValue.Equals("Infini.ty") || valueProviderResult.AttemptedValue.Equals("Infinity") || string.IsNullOrEmpty(valueProviderResult.AttemptedValue)) return null;
            }

            return 0m;
        }
    }

}