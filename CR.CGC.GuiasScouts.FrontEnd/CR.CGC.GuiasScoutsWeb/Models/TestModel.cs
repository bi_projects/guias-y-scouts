﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.CGC.GuiasScoutsWeb.Models
{
    public class TestModel : ApplicationModel
    {
        public int Id { get; set; }
        public string NombreDummy { get; set; }
        public string DescripcionDummy { get; set; }
    }

    public class MasterTestModel : ApplicationModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int IdForeign { get; set; }

        public List<DetailTestModel> Childs { get; set; }

        public MasterTestModel()
        {
            Childs = new List<DetailTestModel>();
        }
    }

    public class DetailTestModel : ApplicationModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int IdForeign { get; set; }
    }
}