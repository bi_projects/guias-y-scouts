﻿using Entities;
using System.Collections.Generic;

namespace CR.CGC.GuiasScoutsWeb.Models
{
    public class PermissionRequestViewModel
    {
        public int RoleId { get; set; }
        public List<Permission> Permissions { get; set; }
    }
}