﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.CGC.GuiasScoutsWeb.Models
{
    public class UtilitiesModel
    {
    }

    public class MenuModel
    {
        public string NameMenu { get; set; }
        public string UrlMenu { get; set; }
        public string IconMenu { get; set; }
        public string CodeMenu { get; set; }

        public string ImageMenu { get; set; }
        public string DescriptionMenu { get; set; }

        public List<MenuModel> SubMenus { get; set; }

        public MenuModel()
        {
            this.SubMenus = new List<MenuModel>();
        }
    }

    public class PermissionModel
    {
        public string EntityName { get; set; }
        public List<PermissionItemModel> CommonMethods { get; set; }
        public List<PermissionItemModel> SpecialMethods { get; set; }

        public PermissionModel()
        {
            this.CommonMethods = new List<PermissionItemModel> ();
            this.SpecialMethods = new List<PermissionItemModel> ();
        }
    }

    public class PermissionItemModel
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}