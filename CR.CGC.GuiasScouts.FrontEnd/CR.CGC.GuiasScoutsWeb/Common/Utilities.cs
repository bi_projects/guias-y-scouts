﻿using CR.CGC.GuiasScoutsWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Entities;

namespace CR.CGC.GuiasScoutsWeb.Common
{
    public static class Utilities
    {
        public static object Request { get; private set; }

        /// <summary>
        /// Renderiza una vista y la retorna como string
        /// </summary>
        /// <param name="controllerContext">Contexto del controlador</param>
        /// <param name="model">Modelo de la vista</param>
        /// <param name="partial">Ruta relativa de la vista partial a renderizar</param>
        /// <returns></returns>
        public static string RenderRazorViewToString(ControllerContext controllerContext, object model, string partial)
        {
            try
            {
                controllerContext.Controller.ViewData.ModelState.Clear();
                controllerContext.Controller.ViewData.Model = model;

                using (var sw = new StringWriter())
                {
                    var ViewResult = ViewEngines.Engines.FindPartialView(controllerContext, partial);
                    var ViewContext = new ViewContext(controllerContext, ViewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, sw);
                    ViewResult.View.Render(ViewContext, sw);
                    ViewResult.ViewEngine.ReleaseView(controllerContext, ViewResult.View);
                    return sw.GetStringBuilder().ToString();
                }
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Elimina archivos almacenados en la carpeta de documentos de la aplicación 
        /// </summary>
        /// <param name="FileUrl">Ruta relativa del archivo</param>
        /// <returns></returns>
        public static bool DeleteFile(string FileUrl)
        {
            try
            {
                string fullpath = HttpContext.Current.Server.MapPath(FileUrl);
                if (File.Exists(fullpath))
                    File.Delete(fullpath);
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Guarda un nuevo archivo en la carpeta de documentos de la aplicación 
        /// </summary>
        /// <param name="file">Archivo a guardar</param>
        /// <param name="folderName">Nombre de la sub-carpeta del archivo</param>
        /// <param name="subFolderName">Nombre de la sub sub-carpeta del archivo</param>
        /// <returns></returns>
        public static Entities.File SaveFile(HttpPostedFileWrapper file, string folderName, string subFolderName)
        {
            Entities.File model = new Entities.File();

            string fileName = Path.GetFileNameWithoutExtension(file.FileName).Replace("ñ", "n").Replace("Ñ", "N").Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u").Replace("'", "").Replace("~", "").Replace(" ", "");
            if (fileName.Length > Constants.FileNameLength) fileName = fileName.Substring(0, Constants.FileNameLength);
            string extension = Path.GetExtension(file.FileName);
            string nowVal = DateTime.Now.ToString("dd-MM-yyyy--HH-mm-ss");

            string currentDir = HttpContext.Current.Server.MapPath("~/Documents");
            string fileUrl = currentDir + "\\" + folderName + "\\" + subFolderName;
            bool exists = Directory.Exists(fileUrl);

            if (!exists) Directory.CreateDirectory(currentDir + @"\" + folderName + @"\" + subFolderName);

            var route = new StringBuilder();
            route.Append(fileUrl);
            route.Append("\\" + fileName);
            route.Append("_" + nowVal);
            route.Append(extension);
            file.SaveAs(route.ToString());

            model.FileName = fileName + extension;
            model.FileUrl = "~/Documents/" + folderName + "/" + subFolderName + "/" + fileName + "_" + nowVal + extension;

            return model;
        }

        /// <summary>
        /// Recupera la lista para la creación del menú principal
        /// </summary>
        public static List<MenuModel> GetMenu()
        {
            Assembly asm = Assembly.GetExecutingAssembly();

            var controllerlist = asm.GetTypes()
                .AsEnumerable()
                .Where(type => typeof(Controller).IsAssignableFrom(type) && type.GetCustomAttribute(typeof(IsMenu)) != null)
                .Select(x => new
                {
                    ControllerName = x.Name.Replace("Controller", ""),
                    MenuName = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIsMenu() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIsMenu() : x.Name),
                    IconName = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIcon() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIcon() : ""),
                    ImageName = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetImage() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetImage() : ""),
                    Description = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetDescription() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetDescription() : ""),
                    Code = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetCode() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetCode() : ""),
                    Order = ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetOrder()
                })
                .OrderBy(x => x.Order)
                .ToList();

            var methodlist = asm.GetTypes()
                .AsEnumerable()
                .Where(type => typeof(Controller).IsAssignableFrom(type) && type.GetCustomAttribute(typeof(IsMenu)) != null)
                .SelectMany(type => type.GetMethods())
                .Where(method => method.IsPublic && method.ReturnType.Name == "ActionResult" && method.GetCustomAttribute(typeof(IsMenu)) != null)
                .Select(x => new
                {
                    ControllerName = x.DeclaringType.Name.Replace("Controller", ""),
                    MethodName = x.Name,
                    SubMenuName = ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIsMenu(),
                    Parent = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetHasParent() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetHasParent() : ((IsMenu)x.DeclaringType.GetCustomAttribute(typeof(IsMenu))).GetIsMenu()),
                    IconName = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIcon() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIcon() : ""),
                    ImageName = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetImage() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetImage() : ""),
                    Description = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetDescription() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetDescription() : ""),
                    Code = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetCode() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetCode() : ""),
                    Order = ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetOrder()
                })
                .OrderBy(x => x.Order)
                .ToList();

            List<MenuModel> menu = new List<MenuModel>();

            foreach (var item in controllerlist)
            {
                menu.Add(CreateMenu(item.MenuName, item.ControllerName, item.IconName, item.ImageName, item.Description, item.Code, methodlist.Where(x => x.ControllerName == item.ControllerName).ToList(), "SingleMenu"));
            }

            return menu;
        }

        /// <summary>
        /// Crea cada uno de los items 
        /// </summary>
        /// <param name="Controller">Nombre del controlador que contiene la lista de acciones para el menú</param>
        /// <param name="Action">Nombre de la acción padre de los items del menú</param>
        /// <returns></returns>
        private static MenuModel CreateMenu(string parentName, string controllerName, string iconName, string imageName, string description, string code, IEnumerable<object> menu, string methodName)
        {
            MenuModel newMenu = new MenuModel();
            newMenu.NameMenu = parentName;
            newMenu.UrlMenu = new UrlHelper(HttpContext.Current.Request.RequestContext).Action(methodName, controllerName);
            newMenu.IconMenu = iconName;

            newMenu.ImageMenu = imageName;
            newMenu.DescriptionMenu = description;
            newMenu.CodeMenu = code;

            foreach (var item in menu)
            {
                if (parentName == item.GetType().GetProperty("Parent").GetValue(item).ToString())
                {
                    newMenu.SubMenus.Add(CreateMenu(
                        item.GetType().GetProperty("SubMenuName").GetValue(item).ToString(),
                        item.GetType().GetProperty("ControllerName").GetValue(item).ToString(),
                        item.GetType().GetProperty("IconName").GetValue(item).ToString(),
                        item.GetType().GetProperty("ImageName").GetValue(item).ToString(),
                        item.GetType().GetProperty("Description").GetValue(item).ToString(),
                        item.GetType().GetProperty("Code").GetValue(item).ToString(),
                        menu,
                        item.GetType().GetProperty("MethodName").GetValue(item).ToString()
                        ));
                }
            }

            return newMenu;
        }

        /// <summary>
        /// Recupera la lista para la creación del menú de tercer nivel
        /// </summary>
        /// <param name="Controller">Nombre del controlador que contiene la lista de acciones para el menú</param>
        /// <param name="Action">Nombre de la acción padre de los items del menú</param>
        /// <returns></returns>
        public static List<MenuModel> GetMenuTab(string Controller, string Action)
        {
            List<MenuModel> mainMenu = GetMenu();
            List<MenuModel> tabMenu = mainMenu.Where(m => m.CodeMenu == Controller).FirstOrDefault().SubMenus.Where(s => s.CodeMenu == Action).FirstOrDefault().SubMenus;

            return tabMenu;
        }

    }

}