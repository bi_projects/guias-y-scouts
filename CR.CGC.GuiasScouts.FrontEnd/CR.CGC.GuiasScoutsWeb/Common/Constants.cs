﻿using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using static System.Net.HttpStatusCode;

namespace CR.CGC.GuiasScoutsWeb.Common
{
    /// <summary>
    /// Contantes de la aplicación
    /// </summary>
    public class Constants
    {
        public const string saName = "gys_admin";
        public const int FileNameLength = 90;
        private static string _BaseUrl = System.Configuration.ConfigurationManager.AppSettings["WebApi"].ToString();
        private static IApplicationHelper _helper = new ApplicationHelper(_BaseUrl);

        public static (CatalogConstants, SocialNetworkConstants, ERContainer) SetConstants()
        {
            CatalogConstants _catalogConstants = new CatalogConstants();
            SocialNetworkConstants _socialNetworkConstants = new SocialNetworkConstants();
            ERContainer _listContainer = new ERContainer();

            var (ResponseCode, ResponseText, catalogs) = _helper.GetEntitiesList<List<BaseCatalog>>("Config", "BaseCatalog");
            if (ResponseCode == OK)
            {
                catalogs.Where(x => x.ItemOf == null).ToList().ForEach(x =>
                {
                    if (x.Name == CatalogConstants.ModuleName) _catalogConstants.ModuleId = (int)x.BaseCatalogId;

                    if (x.Name == CatalogConstants.LocalName) _catalogConstants.LocalId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.SectorName) _catalogConstants.SectorId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.NationalityName) _catalogConstants.NationalityId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.GenderName) _catalogConstants.GenderId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.IdentificationTypeName) _catalogConstants.IdentificationTypeId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.PositionTypeCategoryName) _catalogConstants.PositionTypeCategoryId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.ReligionName) _catalogConstants.ReligionId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.EducationLevelName) _catalogConstants.EducationLevelId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.SchoolName) _catalogConstants.SchoolId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.KinshipName) _catalogConstants.KinshipId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.LanguageLevelName) _catalogConstants.LanguageLevelId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.LanguageName) _catalogConstants.LanguageId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.NationalEventName) _catalogConstants.NationalEventId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.InternationalEventName) _catalogConstants.InternationalEventId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.CourseName) _catalogConstants.CourseId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.WorkshopName) _catalogConstants.WorkshopId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.ActivityName) _catalogConstants.ActivityId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.ProfessionName) _catalogConstants.ProfessionId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.SocialNetworkName) _catalogConstants.SocialNetworkId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.ActivityTypeName) _catalogConstants.ActivityTypeId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.ActivityStatusName) _catalogConstants.ActivityStatusId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.RequestTypeName) _catalogConstants.RequestTypeId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.RequestStatusName) _catalogConstants.RequestStatusId = (int)x.BaseCatalogId;
                    if (x.Name == CatalogConstants.ActivityScopeName) _catalogConstants.ActivityScopeId = (int)x.BaseCatalogId;
                });

                catalogs.Where(x => x.ItemOf == _catalogConstants.SocialNetworkId).ToList().ForEach(x =>
                {
                    if (x.Name == SocialNetworkConstants.FacebookName) _socialNetworkConstants.FacebookId = (int)x.BaseCatalogId;
                    if (x.Name == SocialNetworkConstants.TwitterName) _socialNetworkConstants.TwitterId = (int)x.BaseCatalogId;
                    if (x.Name == SocialNetworkConstants.InstagramName) _socialNetworkConstants.InstagramId = (int)x.BaseCatalogId;
                    if (x.Name == SocialNetworkConstants.GoogleName) _socialNetworkConstants.GoogleId = (int)x.BaseCatalogId;
                    if (x.Name == SocialNetworkConstants.YoutubeName) _socialNetworkConstants.YoutubeId = (int)x.BaseCatalogId;
                    if (x.Name == SocialNetworkConstants.LinkedInName) _socialNetworkConstants.LinkedInId = (int)x.BaseCatalogId;
                });

                if (catalogs.Where(x => x.ItemOf == _catalogConstants.IdentificationTypeId && x.Name == CatalogConstants.IdentificationTypeBlankName).FirstOrDefault() != null)
                    _catalogConstants.IdentificationTypeBlankId = (int)catalogs.Where(x => x.ItemOf == _catalogConstants.IdentificationTypeId && x.Name == CatalogConstants.IdentificationTypeBlankName).FirstOrDefault().BaseCatalogId;

                if (catalogs.Where(x => x.ItemOf == _catalogConstants.IdentificationTypeId && x.Name == CatalogConstants.IdentificationTypeNationalName).FirstOrDefault() != null)
                    _catalogConstants.IdentificationTypeNationalId = (int)catalogs.Where(x => x.ItemOf == _catalogConstants.IdentificationTypeId && x.Name == CatalogConstants.IdentificationTypeNationalName).FirstOrDefault().BaseCatalogId;

                if (catalogs.Where(x => x.ItemOf == _catalogConstants.IdentificationTypeId && x.Name == CatalogConstants.IdentificationTypeInternationalName).FirstOrDefault() != null)
                    _catalogConstants.IdentificationTypeInternationalId = (int)catalogs.Where(x => x.ItemOf == _catalogConstants.IdentificationTypeId && x.Name == CatalogConstants.IdentificationTypeInternationalName).FirstOrDefault().BaseCatalogId;

                if (catalogs.Where(x => x.ItemOf == _catalogConstants.PositionTypeCategoryId && x.Name == CatalogConstants.PositionTypeYoungName).FirstOrDefault() != null)
                    _catalogConstants.PositionTypeYoungId = (int)catalogs.Where(x => x.ItemOf == _catalogConstants.PositionTypeCategoryId && x.Name == CatalogConstants.PositionTypeYoungName).FirstOrDefault().BaseCatalogId;

                if (catalogs.Where(x => x.ItemOf == _catalogConstants.PositionTypeCategoryId && x.Name == CatalogConstants.PositionTypeOldName).FirstOrDefault() != null)
                    _catalogConstants.PositionTypeOldId = (int)catalogs.Where(x => x.ItemOf == _catalogConstants.PositionTypeCategoryId && x.Name == CatalogConstants.PositionTypeOldName).FirstOrDefault().BaseCatalogId;
            }

            try
            {
                _listContainer = (ERContainer)_helper.CreateGetAsync<ERContainer>("Members/List/ForMembers", new Dictionary<string, object>());
            }
            catch (Exception ex)
            {
                _listContainer = new ERContainer();
            }


            return (_catalogConstants, _socialNetworkConstants, _listContainer);
        }
    }

    /// <summary>
    /// Constantes de ID's de la tabla BaseCatalog
    /// </summary>
    public class CatalogConstants
    {
        public const string ModuleName = "Módulo del sistema";
        public int ModuleId { get; set; }

        public const string LocalName = "Local";
        public int LocalId { get; set; }
        public const string SectorName = "Sector";
        public int SectorId { get; set; }
        public const string NationalityName = "Nacionalidad";
        public int NationalityId { get; set; }
        public const string GenderName = "Género";
        public int GenderId { get; set; }
        public const string IdentificationTypeName = "Tipo de identificación";
        public int IdentificationTypeId { get; set; }
        public const string PositionTypeCategoryName = "Tipo de cargo";
        public int PositionTypeCategoryId { get; set; }
        public const string ReligionName = "Religión";
        public int ReligionId { get; set; }
        public const string EducationLevelName = "Nivel académico";
        public int EducationLevelId { get; set; }
        public const string SchoolName = "Centro Educativo";
        public int SchoolId { get; set; }
        public const string KinshipName = "Parentesco";
        public int KinshipId { get; set; }
        public const string LanguageLevelName = "Nivel de idioma";
        public int LanguageLevelId { get; set; }
        public const string LanguageName = "Idioma";
        public int LanguageId { get; set; }
        public const string NationalEventName = "Eventos Nacionales";
        public int NationalEventId { get; set; }
        public const string InternationalEventName = "Eventos Internacionales";
        public int InternationalEventId { get; set; }
        public const string CourseName = "Curso";
        public int CourseId { get; set; }
        public const string WorkshopName = "Taller";
        public int WorkshopId { get; set; }
        public const string ActivityName = "Actividad";
        public int ActivityId { get; set; }
        public const string ProfessionName = "Profesión";
        public int ProfessionId { get; set; }
        public const string SocialNetworkName = "Red social";
        public int SocialNetworkId { get; set; }
        public const string ActivityTypeName = "Tipo de actividad";
        public int ActivityTypeId { get; set; }
        public const string ActivityStatusName = "Estado de actividad";
        public int ActivityStatusId { get; set; }
        public const string RequestTypeName = "Tipo de solicitud";
        public int RequestTypeId { get; set; }
        public const string RequestStatusName = "Estado de solicitud";
        public int RequestStatusId { get; set; }
        public const string ActivityScopeName = "Alcance de actividad";
        public int ActivityScopeId { get; set; }

        public const string PositionTypeYoungName = "Juvenil";
        public int PositionTypeYoungId { get; set; }
        public const string PositionTypeOldName = "Adulto";
        public int PositionTypeOldId { get; set; }
        public const string IdentificationTypeBlankName = "Sin identificación";
        public int IdentificationTypeBlankId { get; set; }
        public const string IdentificationTypeNationalName = "Cédula";
        public int IdentificationTypeNationalId { get; set; }
        public const string IdentificationTypeInternationalName = "Cédula de Residencia";
        public int IdentificationTypeInternationalId { get; set; }
    }

    /// <summary>
    /// Contantes de ID's de la lista de Redes Sociales
    /// </summary>
    public class SocialNetworkConstants
    {
        public const string FacebookName = "Facebook";
        public int FacebookId { get; set; }
        public const string TwitterName = "Twitter";
        public int TwitterId { get; set; }
        public const string InstagramName = "Instagram";
        public int InstagramId { get; set; }
        public const string GoogleName = "Google+";
        public int GoogleId { get; set; }
        public const string YoutubeName = "Youtube";
        public int YoutubeId { get; set; }
        public const string LinkedInName = "LinkedIn";
        public int LinkedInId { get; set; }
    }

    /// <summary>
    /// Contantes de Tipos de Permiso (Comunes o especiales), para el atributo [IsPermission]
    /// </summary>
    public class PermissionTypes
    {
        public const string List = "ListRecords";
        public const string New = "NewRecord";
        public const string View = "ViewRecord";
        public const string Delete = "DeleteRecord";
        public const string Other = "OtherAction";
    }

    /// <summary>
    /// Contantes de Nombres de Platillas para correo
    /// </summary>
    public class EmailTemplateNames
    {
        public const string Default = "Template_Default";
        public const string T1_Col = "Template_1_Col";
        public const string T1_2_Col = "Template_1_2_Col";
        public const string T1_1_2_Col = "Template_1_1_2_Col";
        public const string T1_3_Col = "Template_1_3_Col";
        public const string Article = "Template_Article";
        public const string History = "Template_History";
        public const string Workshop = "Template_Workshop";
        public const string Scouting = "Template_Scouting";
    }

    /// <summary>
    /// Contantes de Nombres de los Iconos para la configuracion de alertas
    /// </summary>
    public class AlertIconConstants
    {
        public const string MenuCustomizeName = "Otros";

        public static string GetAlertIcon(string moduleName)
        {
            switch (moduleName)
            {
                case "Publicaciones":
                    return "icon_publicaciones";
                case "Actividades":
                    return "icon_actividades";
                case "Solicitudes":
                    return "icon_solicitudes";
                case "Otros":
                    return "icon_chispero";

                default:
                    return string.Empty;
            }
        }

        public static string GetAlertColor(string moduleName)
        {
            switch (moduleName)
            {
                case "Publicaciones":
                    return "bck_publicaciones";
                case "Actividades":
                    return "bck_actividades";
                case "Solicitudes":
                    return "bck_solicitudes";
                case "Otros":
                    return "bck_miembro";

                default:
                    return string.Empty;
            }
        }

    }

    /// <summary>
    /// Contantes de Nombres para las acciones de bitacora
    /// </summary>
    public class BinnacleConstants
    {
        public const string Insert = "INSERT";
        public const string Update = "UPDATE";
        public const string Delete = "DELETE";

        public const string Subscription = "SUBSCRIPTION";
        public const string Unsubscribe = "UNSUBSCRIBE";
        public const string Assesment = "ASSESSMENT";
        public const string Attendance = "ATTENDANCE";
        public const string Unattendance = "UNATTENDANCE";

        public const string PublicationTypeName = "Publicaciones";
        public const string ActivityTypeName = "Actividades";
        public const string RequestTypeName = "Solicitudes";
        public const string MemberTypeName = "Gestión de miembro";
        public const string UserTypeName = "Gestión de usuario";

        public static string GetLogTypeIcon(string LogType, bool bck = false)
        {
            if (bck)
            {
                switch (LogType)
                {
                    case PublicationTypeName:
                        return "bck_publicaciones";
                    case ActivityTypeName:
                        return "bck_actividades";
                    case RequestTypeName:
                        return "bck_solicitudes";
                    case MemberTypeName:
                        return "bck_miembro";
                    case UserTypeName:
                        return "bck_miembro";

                    default:
                        return string.Empty;
                }
            }
            else
            {
                switch (LogType)
                {
                    case PublicationTypeName:
                        return "icon_publicaciones";
                    case ActivityTypeName:
                        return "icon_actividades";
                    case RequestTypeName:
                        return "icon_solicitudes";
                    case MemberTypeName:
                        return "icon_miembro";
                    case UserTypeName:
                        return "icon_lock";

                    default:
                        return string.Empty;
                }
            }
        }

        public static string GetActionTypeIcon(string ActionType)
        {
            switch (ActionType)
            {
                case Insert:
                    return "icon_add_btn";
                case Update:
                    return "icon_edit";
                case Delete:
                    return "icon_trash";

                case Subscription:
                    return "icon_check_1 ";
                case Unsubscribe:
                    return "icon_close_1";
                case Assesment:
                    return "icon_star_half";
                case Attendance:
                    return "icon_like";
                case Unattendance:
                    return "icon_dislike";

                default:
                    return string.Empty;
            }
        }

        public static string GetBinnacleEntityName(string Entity)
        {
            switch (Entity)
            {
                case "Publication":
                    return PublicationTypeName;
                case "Activity":
                    return ActivityTypeName;
                case "Request":
                    return RequestTypeName;
                case "Member":
                    return MemberTypeName;
                case "User":
                    return UserTypeName;

                default:
                    return string.Empty;
            }
        }

    }

    /// <summary>
    /// Contantes de tipos de items para la bitácora
    /// </summary>
    public class ItemBinnacleConstants
    {
        public const string ImageType = "ImageProperty";
        public const string FileType = "FileProperty";
        public const string DefaultType = "DefaultProperty";
        public const string ExternalType = "ExternalProperty";
    }
}