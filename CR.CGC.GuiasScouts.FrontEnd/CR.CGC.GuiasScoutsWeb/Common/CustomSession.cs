﻿using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.CGC.GuiasScoutsWeb.Common
{
    public class CustomSession
    {
        private static string _SessionName = "__MY_SESSION__"; //Guid.NewGuid().ToString();

        private CustomSession() { }

        /// <summary>
        /// Crea una sesión personalizada para cada usuario autenticado
        /// </summary>
        public static CustomSession CurrentSession
        {
            get
            {
                CustomSession session = (CustomSession)HttpContext.Current.Session[_SessionName];
                if (session == null)
                {
                    session = new CustomSession();
                    HttpContext.Current.Session[_SessionName] = session;
                }
                return session;
            }
        }

        /// <summary>
        /// Variable que controla el usuario autenticado y al que se puede acceder para gestionar sus atributos
        /// </summary>
        public LoginViewModel CurrentUser { get; set; }


        public CatalogConstants CatalogConstants { get; set; }
        public SocialNetworkConstants SocialNetworkConstants { get; set; }
        public ERContainer ListContainer { get; set; } = new ERContainer();
    }
}