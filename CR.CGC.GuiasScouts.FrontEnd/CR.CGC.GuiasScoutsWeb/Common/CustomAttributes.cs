﻿using System;

namespace CR.CGC.GuiasScoutsWeb.Common
{
    public class CustomAttributes
    {
    }

    /// <summary>
    /// Atributo para definir acciones especificas como parte del menu dinamico de la aplicación
    /// </summary>
    public class IsMenu : Attribute
    {
        private int order = 0;
        private string isMenu = null;
        private string hasParent = null;
        private string iconName = null;

        private string imageName = null;
        private string description = null;
        private string code = null;

        public IsMenu(int order, string isMenu = null, string iconName = null, string hasParent = null, string imageName = null, string description = null, string code = null)
        {
            this.order = order;
            this.isMenu = isMenu;
            this.hasParent = hasParent;
            this.iconName = iconName;

            this.imageName = imageName;
            this.description = description;
            this.code = code;
        }

        public string GetIsMenu() { return this.isMenu; }

        public string GetHasParent() { return this.hasParent; }
        public string GetIcon() { return this.iconName; }

        public int GetOrder() { return this.order; }

        public string GetImage() { return this.imageName; }
        public string GetDescription() { return this.description; }
        public string GetCode() { return this.code; }
    }

    /// <summary>
    /// Atributo para definir las acciones de la aplicación a las que se pueden acceder a través de un permiso
    /// </summary>
    public class IsPermission : Attribute
    {
        private string Entity = null;
        private string Type = null;
        private string Name = null;
        private string Description = null;

        public IsPermission(string entity, string type, string name = null, string description = null) {
            this.Entity = entity;
            this.Type = type;
            this.Name = name;
            this.Description = description;
        }

        public string GetEntity() { return this.Entity; }
        public string GetPermType() { return this.Type; }
        public string GetName() { return this.Name; }
        public string GetDescription() { return this.Description; }
    }
}