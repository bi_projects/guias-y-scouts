﻿using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using static System.Net.HttpStatusCode;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    [IsMenu(9, "Configuración", "icon_configuracion", null, "caption_security.jpg", "Esta es la descripción para Configuraciones. Solamente es un texto de prueba, recordar cambiarlo.", "Configuration")]
    public class ConfigurationController : ApplicationController
    {
        private static string _BaseUrl = System.Configuration.ConfigurationManager.AppSettings["WebApi"].ToString();

        private IApplicationHelper _helper = new ApplicationHelper(_BaseUrl);

        #region Security

        [IsMenu(1, "Seguridad", null, null, null, null, "Security")]
        [Route("Configuration/Security/Index")]
        public ActionResult SecurityMenu()
        {
            return RedirectToAction("Roles");
        }

        #region Role

        [IsMenu(1, "Roles", null, "Seguridad", null, null, "RolesList")]
        [Route("Configuration/Security/RolesList")]
        [IsPermission("Rol", PermissionTypes.List, null, "Role List")]
        public ActionResult Roles()
        {
            return View("Security/Roles/Index");
        }

        public ActionResult GetGridRoles(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, Roles, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Role>>("Config", "Role", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };

            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, Roles, "~/Views/Configuration/Security/Roles/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Security/Roles/_GridPartial", Roles);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Security/Roles/_GridPartial", new List<Role>());
            }
        }

        [Route("Configuration/Security/NewRole")]
        [IsPermission("Rol", PermissionTypes.New, null, "RoleCreation")]
        public ActionResult NewRole(int? RoleId)
        {
            if (RoleId == null)
                return View("Security/Roles/Create", new Role());
            else if (RoleId == 0)
                return View("Security/Roles/Create", new Role());
            else
            {
                var (ResponseCode, ResponseText, role) = _helper.GetEntity<Role>("Config", "Role", "RoleId", (int)RoleId);

                if (ResponseCode == OK)
                    return View("Security/Roles/Create", role);
                else
                    return View("Security/Roles/Create", new Role());
            }
        }

        public ActionResult GetRoleCompetencies(Role role)
        {
            var (ResponseCode, ResponseText, competencies) = _helper.GetEntitiesList<List<Competence>>("Config", "Competence");
            if (ResponseCode == OK) competencies = competencies.Where(x => x.Enabled).ToList();
            else competencies = new List<Competence>();

            List<CompetenceLevel> levels;
            (ResponseCode, ResponseText, levels) = _helper.GetEntitiesList<List<CompetenceLevel>>("Config", "CompetenceLevel");
            if (ResponseCode == OK) levels = levels.Where(x => x.Enabled).ToList();
            else levels = new List<CompetenceLevel>();

            return PartialView("Security/Roles/_CreateCompetencePartial", new RoleCompetenceViewModel() { Role = role, Competencies = competencies, Levels = levels });
        }

        [Route("Configuration/Security/PermissionList")]
        [IsPermission("Rol", PermissionTypes.Other, "Asociación de permisos", "RoleAssociation")]
        public ActionResult PermissionList(int RoleId = 0)
        {
            if (RoleId <= 0)
                return Roles();
            else
            {
                var (ResponseCode, ResponseText, role) = _helper.GetEntity<Role>("Config", "Role", "RoleId", RoleId);

                if (ResponseCode == OK)
                {
                    List<Permission> permissions;
                    (ResponseCode, ResponseText, permissions) = _helper.GetEntity<List<Permission>>("Config", "Permission", "RoleId", RoleId);

                    return View("Security/Roles/Permission", new PermissionViewModel() { Role = role, PermissionsView = GetListPermissions(), Permissions = permissions });
                }
                else
                    return Roles();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveRole(Role role)
        {
            if (role.RoleId != null && role.RoleId != 0) role.RoleCompetencies.ForEach(x => x.RoleId = (int)role.RoleId);
            if (role.RoleCompetencies != null && role.RoleCompetencies.Count != 0)
                role.RoleCompetencies = role.RoleCompetencies.Where(x => x.Enabled).ToList();


            role.Enabled = true;
            ERContainer container = new ERContainer() { Roles = new List<Role>() { role }, Model = "Role", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Rol", PermissionTypes.Delete, null, "RoleElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteRole(int? RoleId, List<int> Roles, string Search, int Number = 1)
        {
            if (RoleId == null && Roles == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (RoleId != null)
            {
                var (ResponseCode, ResponseText, role) = _helper.GetEntity<Role>("Config", "Role", "RoleId", (int)RoleId);
                if (ResponseCode == OK)
                {
                    role.Enabled = !role.Enabled;
                    ERContainer container = new ERContainer() { Roles = new List<Role>() { role }, Model = "Role", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridRoles(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Role>>("Config", "Role");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Roles.Contains((int)x.RoleId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Roles = list, Model = "Role", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridRoles(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridRoles(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SavePermission([Bind(Prefix = "Role")]PermissionRequestViewModel permissions)
        {
            if (permissions.RoleId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 500, ResponseText = "Parámetros no válidos." });
            }

            permissions.Permissions = permissions.Permissions.Where(x => x.ToSave).ToList();
            permissions.Permissions.ForEach(x => x.RoleId = permissions.RoleId);

            ERContainer container = new ERContainer() { Permissions = permissions.Permissions, Model = "Permission" }; //Add CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
            {
                if (permissions.RoleId == CustomSession.CurrentSession.CurrentUser.Member.User.RoleId)
                {
                    var (MemberResponseCode, MemberResponseText, Member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", (int)CustomSession.CurrentSession.CurrentUser.Member.MemberId);
                    if (MemberResponseCode == OK) CustomSession.CurrentSession.CurrentUser.Member = Member;
                }
                return Json(new { ResponseCode, ResponseText });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        #endregion

        #region User

        [IsMenu(1, "Usuarios", null, "Seguridad", null, null, "UsersList")]
        [Route("Configuration/Security/UsersList")]
        [IsPermission("Usuario", PermissionTypes.List, null, "User List")]
        public ActionResult Users()
        {
            return View("Security/Users/Index");
        }

        public ActionResult GetGridUsers(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, Roles, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Member>>("Members", "Member", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };

            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, Roles, "~/Views/Configuration/Security/Users/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Security/Users/_GridPartial", Roles);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Security/Users/_GridPartial", new List<Member>());
            }
        }

        [Route("Configuration/Security/EditRole")]
        [IsPermission("Usuario", PermissionTypes.Other, "Asignación de Rol", "Edit Role")]
        public ActionResult EditRole(int MemberId = 0)
        {
            if (MemberId <= 0) return Users();

            var (ListResponseCode, ListResponseText, List) = _helper.GetEntitiesList<List<Role>>("Config", "Role");
            if (ListResponseCode == OK) ViewBag.RoleList = new SelectList(List.Where(x => x.Enabled).ToList(), "RoleId", "Name");
            else ViewBag.RoleList = new SelectList(new List<Role>(), "RoleId", "Name");

            var (ResponseCode, ResponseText, member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", MemberId);
            if (ResponseCode == OK) return View("Security/Users/EditRole", member);
            else return Users();
        }

        [Route("Configuration/Security/EditPassword")]
        [IsPermission("Usuario", PermissionTypes.Other, "Editar Contraseñas", "Edit Password")]
        public ActionResult EditPassword(int MemberId = 0)
        {
            if (MemberId <= 0) return Users();

            var (ResponseCode, ResponseText, member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", MemberId);
            if (ResponseCode == OK) return View("Security/Users/EditPassword", member);
            else return Users();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveUserRole([Bind(Prefix = "User")]User user)
        {
            if (user.UserId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 500, ResponseText = "Parámetros no válidos." });
            }

            var (ResponseCode, ResponseText, Senduser) = _helper.GetEntity<User>("Config", "User", "UserId", user.UserId);
            if (ResponseCode == OK)
            {
                if (Senduser.RoleId == null)
                {
                    Senduser.RoleId = user.RoleId;
                    (ResponseCode, ResponseText) = await _helper.CreatePost("User/Account/Activation", new ERContainer() { Users = new List<User>() { Senduser }, CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId });
                }
                else
                {
                    Senduser.RoleId = user.RoleId;
                    Senduser.PasswordResetToken = null;
                    ERContainer container = new ERContainer() { Users = new List<User>() { Senduser }, Model = "User", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                }

                if (ResponseCode == OK)
                    return Json(new { ResponseCode, ResponseText });
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText });
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveUserPass([Bind(Prefix = "User")]User user)
        {
            if (user.UserId <= 0 || user.Password != user.PasswordResetToken)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 500, ResponseText = "Parámetros no válidos." });
            }

            var (ResponseCode, ResponseText, Senduser) = _helper.GetEntity<User>("Config", "User", "UserId", user.UserId);
            if (ResponseCode == OK)
            {
                user.Email = Senduser.Email;
                user.Source = string.Empty;
                (ResponseCode, ResponseText) = await _helper.CreatePost("User/Account/ForcePasswordChange", user);

                if (ResponseCode == OK)
                    return Json(new { ResponseCode, ResponseText });
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText });
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [HttpPost]
        public async Task<ActionResult> SaveUserConf(User user)
        {
            var (ResponseCode, ResponseText, Senduser) = _helper.GetEntity<User>("Config", "User", "UserId", CustomSession.CurrentSession.CurrentUser.Member.User.UserId);
            if (ResponseCode == OK)
            {
                Senduser.Theme = user.Theme;
                Senduser.FontSize = user.FontSize;
                ERContainer container = new ERContainer() { Users = new List<User>() { Senduser }, Model = "User", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                if (ResponseCode == OK)
                {
                    var (ResponseCodeMember, ResponseTextMember, Member) = _helper.GetEntity<Member>("Config", "Member", "MemberId", (int)CustomSession.CurrentSession.CurrentUser.Member.MemberId);
                    if (ResponseCodeMember == OK) CustomSession.CurrentSession.CurrentUser.Member = Member;
                    return Json(new { ResponseCode, ResponseText });
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText });
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Usuario", PermissionTypes.Delete, null, "UserElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteUser(int? UserId, List<int> Users, string Search, int Number = 1)
        {
            if (UserId == null && Users == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (UserId != null)
            {
                var (ResponseCode, ResponseText, user) = _helper.GetEntity<User>("Config", "User", "UserId", (int)UserId);
                if (ResponseCode == OK)
                {
                    user.Enabled = !user.Enabled;
                    user.Active = user.Enabled;
                    ERContainer container = new ERContainer() { Users = new List<User>() { user }, Model = "User", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridUsers(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<User>>("Config", "User");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Users.Contains((int)x.UserId)).ToList();
                    foreach (var item in list)
                    {
                        item.Enabled = !item.Enabled;
                        item.Active = item.Enabled;
                    }

                    ERContainer container = new ERContainer() { Users = list, Model = "User", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridUsers(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridUsers(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #endregion

        #region Alerts

        [IsMenu(1, "Alertas", null, null, null, null, "Alerts")]
        [Route("Configuration/Alerts/Index")]
        public ActionResult AlertMenu()
        {
            return RedirectToAction("Alerts");
        }

        #region Alerts Actions

        [IsMenu(1, "Lista de Alertas", null, "Alertas", null, null, "AlertList")]
        [Route("Configuration/Alerts/AlertList")]
        [IsPermission("Alertas", PermissionTypes.List, null, "AlertList")]
        public ActionResult Alerts()
        {
            var (ResponseCode, ResponseText, Types) = _helper.GetEntitiesList<List<AlertType>>("Config", "AlertType");
            if (ResponseCode == OK)
            {
                ViewBag.ListTypes = Types.Where(x => x.Enabled).ToList();

                List<MemberAlertSetting> MemberList;
                (ResponseCode, ResponseText, MemberList) = _helper.GetEntitiesByParams<List<MemberAlertSetting>>("Members", "MemberAlertSetting", new[] { ("MemberId", CustomSession.CurrentSession.CurrentUser.Member.MemberId.ToString()) });
                if (ResponseCode == OK)
                {
                    return View("Alerts/Index", MemberList.Where(x => x.Enabled).ToList());
                }
                else return RedirectToAction("ProfileView");
            }
            else return RedirectToAction("ProfileView");
        }

        [IsPermission("Alertas", PermissionTypes.New, null, "AlertCreation")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveAlert([Bind(Prefix = "Alert")]List<MemberAlertSetting> settings)
        {
            if (settings.Count == 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 500, ResponseText = "Parámetros no válidos." });
            }

            List<int> customAlerts = settings.Where(x => x.Enabled).Select(x => x.AlertTypeId).ToList();

            var (ResponseCode, ResponseText, Types) = _helper.GetEntitiesList<List<AlertType>>("Config", "AlertType");
            if (ResponseCode == OK)
            {
                List<MemberAlertSetting> MemberList;
                (ResponseCode, ResponseText, MemberList) = _helper.GetEntitiesByParams<List<MemberAlertSetting>>("Members", "MemberAlertSetting", new[] { ("MemberId", CustomSession.CurrentSession.CurrentUser.Member.MemberId.ToString()) });
                if (ResponseCode == OK)
                {
                    int? CustomTypeId = Types.Where(x => x.Name == AlertIconConstants.MenuCustomizeName).FirstOrDefault().AlertTypeId;
                    List<MemberAlertSetting> toSend = new List<MemberAlertSetting>();
                    foreach (var item in settings.Where(x => !x.Enabled && !Types.Where(y => y.Other).Select(y => (int)y.AlertTypeId).ToList().Contains(x.AlertTypeId)).ToList())
                    {
                        toSend.Add(new MemberAlertSetting()
                        {
                            MemberAlertSettingId = 0,
                            MemberId = (int)CustomSession.CurrentSession.CurrentUser.Member.MemberId,
                            AlertTypeId = item.AlertTypeId,
                            Sms = item.Sms,
                            Email = item.Email,
                            Portal = true,
                            Enabled = true
                        });

                        if (CustomTypeId != null && CustomTypeId == item.AlertTypeId)
                        {
                            foreach (var custom in Types.Where(x => x.Other).ToList())
                            {
                                toSend.Add(new MemberAlertSetting()
                                {
                                    MemberAlertSettingId = 0,
                                    MemberId = (int)CustomSession.CurrentSession.CurrentUser.Member.MemberId,
                                    AlertTypeId = (int)custom.AlertTypeId,
                                    Sms = item.Sms,
                                    Email = item.Email,
                                    Portal = true,
                                    Enabled = customAlerts.Contains((int)custom.AlertTypeId)
                                });
                            }
                        }
                    }

                    toSend = toSend.Where(x => !Types.Where(y => y.Other).Select(y => (int)y.AlertTypeId).ToList().Contains(x.AlertTypeId) || (Types.Where(y => y.Other).Select(y => (int)y.AlertTypeId).ToList().Contains(x.AlertTypeId) && x.Enabled)).ToList();
                    if (MemberList.Count > 0)
                    {
                        foreach (var item in MemberList.Where(x => Types.Where(y => y.Other).Select(y => (int)y.AlertTypeId).ToList().Contains(x.AlertTypeId)).ToList())
                        {
                            if (!toSend.Select(x => x.AlertTypeId).ToList().Contains(item.AlertTypeId))
                            {
                                item.Enabled = false;
                                toSend.Add(item);
                            }
                        }
                    }

                    ERContainer container = new ERContainer() { MemberAlertSettings = toSend, Model = "MemberAlertSetting", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                    {
                        return Json(new { ResponseCode, ResponseText });
                    }
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Ocurrió un error modificando las configuraciones.";
                    return Json(new { ResponseCode = 500, ResponseText = "Ocurrió un error modificando las configuraciones." });
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Ocurrió un error modificando las configuraciones.";
                return Json(new { ResponseCode = 500, ResponseText = "Ocurrió un error modificando las configuraciones." });
            }
        }

        #endregion

        #region Notifications

        [Route("Configuration/Alerts/NotificationList")]
        [IsPermission("Notificaciones", PermissionTypes.List, null, "NotificationList")]
        public ActionResult Notifications()
        {
            return View("Alerts/Notifications/Index");
        }

        public ActionResult GetGridNotifications(int Number = 1)
        {
            var (ResponseCode, ResponseText, Notifications, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<MemberAlert>>("Config", "MemberAlert", Number, new[] { ("MemberId", CustomSession.CurrentSession.CurrentUser.Member.MemberId.ToString()) });

            ViewBag.Pagination = new PaginationToolsViewModel() { RowCount = RowCount, NumberPage = NumberPage };

            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, Notifications, "~/Views/Configuration/Alerts/Notifications/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Alerts/Notifications/_GridPartial", Notifications);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return RedirectToAction("ProfileView");
            }

        }

        public async Task<ActionResult> GetNotifications()
        {
            var (ResponseCode, ResponseText, Notifications) = _helper.GetEntitiesByParams<List<MemberAlert>>("Config", "MemberAlert", new[] { ("MemberId", CustomSession.CurrentSession.CurrentUser.Member.MemberId.ToString()) });
            if (ResponseCode == OK)
            {
                ViewBag.CounterAlert = Notifications.Where(x => !x.Reading).OrderByDescending(x => x.CreationDate).ToList().Count;
                if (ViewBag.CounterAlert > 99) ViewBag.CounterAlert = 99;

                Notifications = Notifications.Where(x => !x.Reading).Take(5).ToList();
                Notifications.ForEach(x =>
                {
                    x.Received = true;
                    if (x.DateReceived == null) x.DateReceived = DateTime.Now;
                });

                ERContainer container = new ERContainer() { MemberAlerts = Notifications, Model = "MemberAlert", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);
                if (ResponseCode == OK)
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, Notifications, "~/Views/Shared/_NotificationPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText });
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Ocurrió un error al obtener las notificaciones.";
                return Json(new { ResponseCode = 500, ResponseText = "Ocurrió un error al obtener las notificaciones." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> SaveNotification(int MemberAlertId)
        {
            if (MemberAlertId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            var (ResponseCode, ResponseText, Notification) = _helper.GetEntity<MemberAlert>("Config", "MemberAlert", "MemberAlertId", MemberAlertId);
            if (ResponseCode == OK)
            {
                if (!Notification.Reading)
                {
                    Notification.Reading = true;
                    if (Notification.ReadingDate == null) Notification.ReadingDate = DateTime.Now;

                    ERContainer container = new ERContainer() { MemberAlerts = new List<MemberAlert>() { Notification }, Model = "MemberAlert", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

                    if (ResponseCode == OK)
                    {
                        Response.StatusDescription = "Guardado exitosamente.";
                        return Json(new { ResponseCode, ResponseText = "Guardado exitosamente." });
                    }
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }

                Response.StatusDescription = "Guardado exitosamente.";
                return Json(new { ResponseCode, ResponseText = "Guardado exitosamente." });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        #endregion

        #endregion

        #region Profile

        [IsMenu(3, "Perfil", null, null, null, null, "Profile")]
        [Route("Configuration/Profile/Index")]
        public ActionResult ProfileMenu()
        {
            return RedirectToAction("ProfileView");
        }

        #region Profile Actions

        [IsMenu(1, "Detalles", null, "Perfil", null, null, "ProfileView")]
        [Route("Configuration/Profile/ProfileView")]
        [IsPermission("Perfil", PermissionTypes.View, null, "ProfileView")]
        public ActionResult ProfileView()
        {
            return View("Profiles/Details", CustomSession.CurrentSession.CurrentUser.Member);
        }

        [IsMenu(2, "Editar", null, "Perfil", null, null, "ProfileEdit")]
        [Route("Configuration/Profile/ProfileEdit")]
        [IsPermission("Perfil", PermissionTypes.New, null, "ProfileEdit")]
        public ActionResult ProfileEdit()
        {
            return View("Profiles/Create", new ProfileViewModel() { Member = CustomSession.CurrentSession.CurrentUser.Member });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveProfile(ProfileViewModel profile)
        {
            if (profile.ActualPassword != null && profile.ActualPassword != string.Empty)
            {
                var (ResponseCodeUpd, ResponseTextUpd, memberUpd) = _helper.GetEntity<Member>("Members", "Member", "MemberId", (int)CustomSession.CurrentSession.CurrentUser.Member.MemberId);
                if (ResponseCodeUpd == OK)
                {
                    if (profile.Member.SendFile != null)
                    {
                        Entities.File toSave = Utilities.SaveFile(profile.Member.SendFile, "Member", "Records");
                        memberUpd.Photo = toSave.FileUrl;
                    }

                    memberUpd.SendFile = null;
                    memberUpd.Email = profile.Member.Email;
                    memberUpd.User.Email = profile.Member.Email;
                    memberUpd.User.Password = profile.ActualPassword;
                    memberUpd.Enabled = true;
                    ERContainer container = new ERContainer()
                    {
                        Members = new List<Member>() { memberUpd },
                        CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId,
                        Model = "Member"
                    };
                    var (ResponseCode, ResponseText, obj) = await _helper.CreatePostGetElement<Member>("User/Account/EmailUpdate", container);
                    if (ResponseCode == OK)
                    {
                        CustomSession.CurrentSession.CurrentUser.Member = obj;
                        return Json(new { ResponseCode, ResponseText });
                    }
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseTextUpd;
                    return Json(new { ResponseCode = ResponseCodeUpd, ResponseText = ResponseTextUpd });
                }

            }
            return Json(new { ResponseCode = NotAcceptable, ResponseText = "La contraseña actual es requerida" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveProfilePassword(ProfileViewModel profile)
        {
            if (profile.ActualPassword != null && profile.ActualPassword != string.Empty)
            {
                if (profile.NewPassword != profile.ConfirmPassword)
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Las nuevas contraseñas no coinciden.";
                    return Json(new { ResponseCode = 400, ResponseText = "Las nuevas contraseñas no coinciden." });
                }
                else
                {
                    var container = await _helper.CreateObject<ERContainer>("Security/Account/UpdatePassword", new UpdPassModel()
                    {
                        Email = CustomSession.CurrentSession.CurrentUser.Member.User.Email,
                        UserName = CustomSession.CurrentSession.CurrentUser.Member.User.UserName,
                        CurrentPassword = profile.ActualPassword,
                        CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId,
                        NewPassword = profile.NewPassword
                    });

                    if (container.GetType().Equals(typeof(ERContainer)))
                    {
                        User responseModel = ((ERContainer)container).Users.FirstOrDefault();
                    }
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = (string)container;
                        return Json(new { ResponseCode = 400, ResponseText = (string)container });
                    }
                }
            }
            return Json(new { ResponseCode = NotAcceptable, ResponseText = "La contraseña actual es requerida" });
        }

        #endregion

        #endregion

        #region Publications

        [IsMenu(4, "Publicaciones", null, null, null, null, "Publications")]
        [Route("Configuration/Publications/Index")]
        public ActionResult PublicationMenu()
        {
            return RedirectToAction("NewSubscription");
        }

        #region Subscriptions View

        [IsMenu(1, "Suscripciones", null, "Publicaciones", null, null, "NewSubscription")]
        [Route("Configuration/Publications/SubscriptionsList")]
        [IsPermission("Suscripción", PermissionTypes.View, null, "SubscriptionsDetail")]
        public ActionResult NewSubscription(string SearchText = null)
        {
            ViewBag.SearchText = SearchText;
            return View("Subscriptions/Create");
        }

        public ActionResult GetGridSubscriptions(string SearchText = null, string TabId = null)
        {
            System.Net.HttpStatusCode ResponseCode;
            string ResponseText;
            List<InterestTheme> Themes;

            if (SearchText == null)
                (ResponseCode, ResponseText, Themes) = _helper.GetEntitiesList<List<InterestTheme>>("Publications", "InterestTheme");
            else
            {
                (ResponseCode, ResponseText, Themes) = _helper.GetEntitiesByParams<List<InterestTheme>>("Publications", "InterestTheme", new[] { ("SearchText", (SearchText ?? "")) });
                ViewBag.SearchText = SearchText;
            }

            if (ResponseCode == OK)
            {
                List<MemberInterestTheme> member;
                (ResponseCode, ResponseText, member) = _helper.GetEntitiesList<List<MemberInterestTheme>>("Members", "MemberInterestTheme");
                if (ResponseCode == OK)
                    ViewBag.ListIndividual = member.Where(x => x.MemberId == CustomSession.CurrentSession.CurrentUser.Member.MemberId && x.Enabled).Select(x => x.InterestThemeId).ToList();
                else
                    ViewBag.ListIndividual = new List<int>();

                ViewBag.TabId = TabId;
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, Themes, "~/Views/Configuration/Subscriptions/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Subscriptions/_GridPartial", Themes);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ViewBag.ListIndividual = new List<int>();
                    return PartialView("Subscriptions/_GridPartial", new List<InterestTheme>());
                }
            }

        }

        [IsPermission("Suscripción", PermissionTypes.New, null, "NewSubscription")]
        [HttpPost]
        public async Task<ActionResult> SaveMemberInterestTheme(int InterestThemeId)
        {
            if (InterestThemeId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            MemberInterestTheme Subscription = new MemberInterestTheme()
            {
                MemberInterestThemeId = 0,
                MemberId = (int)CustomSession.CurrentSession.CurrentUser.Member.MemberId,
                InterestThemeId = InterestThemeId,
                Enabled = true
            };

            ERContainer container = new ERContainer() { MemberInterestThemes = new List<MemberInterestTheme>() { Subscription }, Model = "MemberInterestTheme", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

            if (ResponseCode == OK)
            {
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText = "Guardado exitosamente." });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Suscripción", PermissionTypes.Delete, null, "SubscriptionDelete")]
        [HttpDelete]
        public async Task<ActionResult> DeleteMemberInterestTheme(int InterestThemeId)
        {
            if (InterestThemeId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            var (ResponseCode, ResponseText, themes) = _helper.GetEntitiesList<List<MemberInterestTheme>>("Members", "MemberInterestTheme");
            if (ResponseCode == OK)
            {
                MemberInterestTheme subscription = themes.Where(x => x.InterestThemeId == InterestThemeId && x.MemberId == CustomSession.CurrentSession.CurrentUser.Member.MemberId).FirstOrDefault();
                if (subscription != null)
                {
                    subscription.Enabled = false;
                    ERContainer container = new ERContainer() { MemberInterestThemes = new List<MemberInterestTheme>() { subscription }, Model = "MemberInterestTheme", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);
                    if (ResponseCode == OK)
                    {
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText = "Guardado exitosamente." });
                    }
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Ha ocurrido un error buscando el registro.";
                return Json(new { ResponseCode, ResponseText = "Ha ocurrido un error buscando el registro." });
            }

        }

        #endregion

        #endregion

        #region FAQS

        [IsMenu(5, "Preguntas Frecuentes", null, null, null, null, "FAQ")]
        [Route("Configuration/FAQ/Index")]
        public ActionResult FAQMenu()
        {
            return RedirectToAction("FAQList");
        }

        #region FAQ View

        [IsMenu(1, "Preguntas Frecuentes", null, "FAQs", null, null, "FAQList")]
        [Route("Configuration/FAQ/FAQList")]
        [IsPermission("FAQ", PermissionTypes.List, null, "FAQList")]
        public ActionResult FAQList()
        {
            return View("FAQ/Index");
        }

        public ActionResult GetGridFAQs(string Search, int Number = 1)
        {
            List<FaqListViewModel> list = new List<FaqListViewModel>();
            List<Faq> faqList;
            var (ResponseCode, ResponseText, modules, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ModuleId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };

            if (ResponseCode == OK)
            {

                (ResponseCode, ResponseText, faqList) = _helper.GetEntitiesList<List<Faq>>("Config", "Faq");
                foreach (var item in modules)
                {
                    list.Add(new FaqListViewModel()
                    {
                        FaqModuleId = (int)item.BaseCatalogId,
                        ModuleName = item.Name,
                        FaqsCount = faqList.Where(x => x.Type == (int)item.BaseCatalogId && x.Enabled).ToList().Count
                    });
                }

                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, list, "~/Views/Configuration/FAQ/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("FAQ/_GridPartial", list);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("FAQ/_GridPartial", new List<FaqListViewModel>());
            }
        }

        [Route("Configuration/FAQ/NewFAQ")]
        [IsPermission("FAQ", PermissionTypes.New, null, "FAQCreation")]
        public ActionResult NewFAQ(int? FaqId, int FaqModuleId = 0)
        {
            if (FaqModuleId <= 0)
                return FAQList();
            else
                return View("FAQ/Create", new SendFaqViewModel() { FaqModuleId = FaqModuleId, FaqId = FaqId });
        }

        public ActionResult GetFAQ(SendFaqViewModel model)
        {
            if (model.FaqModuleId <= 0) return FAQList();
            var (ListResponseCode, ListResponseText, List) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ModuleId.ToString()) });
            if (ListResponseCode == OK)
                ViewBag.ModulesList = new SelectList(List, "BaseCatalogId", "Name");
            else
                ViewBag.ModulesList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            if (model.FaqId == null)
            {
                return PartialView("FAQ/_ToAddPartial", new Faq() { Type = model.FaqModuleId });
            }
            else if (model.FaqId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new Faq() { Type = model.FaqModuleId }, "~/Views/Configuration/FAQ/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, faq) = _helper.GetEntity<Faq>("Config", "Faq", "FaqId", (int)model.FaqId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, faq, "~/Views/Configuration/FAQ/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("FAQ/_ToAddPartial", faq);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("FAQ/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetFAQS(int FaqModuleId = 0)
        {
            if (FaqModuleId <= 0) PartialView("FAQ/_AggregatesPartial", new List<Faq>());

            var (ResponseCode, ResponseText, faqs) = _helper.GetEntitiesByParams<List<Faq>>("Config", "Faq", new[] { ("Type", $"{FaqModuleId}") });

            if (ResponseCode == OK) return PartialView("FAQ/_AggregatesPartial", faqs.Where(x => x.Type == FaqModuleId && x.Enabled).ToList());
            else return PartialView("FAQ/_AggregatesPartial", new List<Faq>());
        }

        public ActionResult GetFAQS_AJAX(int FaqModuleId = 0)
        {
            if (FaqModuleId <= 0)
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Parámetros no válidos.";
                    return Json(new { ResponseCode = 500, ResponseText = "Parámetros no válidos." }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("FAQ/_AggregatesPartial", new List<Faq>());
            }

            var (ResponseCode, ResponseText, faqs) = _helper.GetEntitiesByParams<List<Faq>>("Config", "Faq", new[] { ("Type", $"{FaqModuleId}") });

            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, faqs.Where(x => x.Type == FaqModuleId && x.Enabled).ToList(), "~/Views/Configuration/FAQ/_AggregatesPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("FAQ/_AggregatesPartial", faqs.Where(x => x.Type == FaqModuleId && x.Enabled).ToList());
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("FAQ/_AggregatesPartial", new List<Faq>());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveFAQ(Faq faq)
        {
            if (!ModelState.IsValid) return View(faq);

            faq.Enabled = true;
            ERContainer container = new ERContainer() { Faqs = new List<Faq>() { faq }, Model = "Faq", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("FAQ/_CreateBodyPartial", new SendFaqViewModel() { FaqModuleId = faq.Type });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("FAQ", PermissionTypes.Delete, null, "FAQElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteFAQ(int FaqId)
        {
            if (FaqId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            var (ResponseCode, ResponseText, faq) = _helper.GetEntity<Faq>("Config", "Faq", "FaqId", (int)FaqId);
            if (ResponseCode == OK)
            {
                faq.Enabled = !faq.Enabled;
                ERContainer container = new ERContainer() { Faqs = new List<Faq>() { faq }, Model = "Faq", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                if (ResponseCode == OK)
                    return GetFAQS_AJAX(faq.Type);
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText });
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "El registro no fue encontrado.";
                return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
            }
        }

        #endregion

        #endregion

        #region Catalogs

        [IsMenu(6, "Lista de Catálogos", null, null, null, null, "Catalogs")]
        [Route("Configuration/Catalogs/Index")]
        public ActionResult CatalogMenu()
        {
            return RedirectToAction("Competencies");
        }

        #region Competencias opcion 0

        [IsMenu(1, "Competencias", null, "Lista de Catálogos", null, null, "CompetenciesList")]
        [Route("Configuration/Catalogs/CompetenciesList")]
        [IsPermission("Competencia", PermissionTypes.List, null, "CompetenceList")]
        public ActionResult Competencies()
        {
            return View("Catalogs/Competencies/Index");
        }

        [Route("Configuration/Catalogs/NewCompetence")]
        [IsPermission("Competencia", PermissionTypes.New, null, "CompetenceCreation")]
        public ActionResult NewCompetence(int? CompetenceId)
        {
            return View("Catalogs/Competencies/Create", CompetenceId);
        }

        public ActionResult GetCompetence(int? CompetenceId)
        {
            GetTypesOfCompetence();

            if (CompetenceId == null)
            {
                return PartialView("Catalogs/Competencies/_SectionToAdd", new Competence());
            }
            else if (CompetenceId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new Competence(), "~/Views/Configuration/Catalogs/Competencies/_SectionToAdd.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, competence) = _helper.GetEntity<Competence>("Config", "Competence", "CompetenceId", (int)CompetenceId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, competence, "~/Views/Configuration/Catalogs/Competencies/_SectionToAdd.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Competencies/_SectionToAdd", competence);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Competencies/_SectionToAdd", new Competence());
                }
            }
        }

        public ActionResult GetGridCompetences(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, Competences, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Competence>>("Config", "Competence", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };

            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, Competences, "~/Views/Configuration/Catalogs/Competencies/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Competencies/_GridPartial", Competences);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Competencies/_GridPartial", new List<Competence>());
            }
        }

        public ActionResult GetCompetences()
        {
            var (ResponseCode, ResponseText, Competences) = _helper.GetEntitiesList<List<Competence>>("Config", "Competence");

            if (ResponseCode == OK) return PartialView("Catalogs/Competencies/_AggregatesSection", Competences.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/Competencies/_AggregatesSection", new List<Competence>());
        }

        public void GetTypesOfCompetence()
        {
            var (ResponseCode, ResponseText, TypesOfCompetence) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", "7") });
            if (ResponseCode == OK)
                ViewBag.TypesOfCompetence = new SelectList(TypesOfCompetence, "BaseCatalogId", "Name");
            else
                ViewBag.TypesOfCompetence = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveCompetence(Competence competence)
        {
            if (!ModelState.IsValid) return View(competence);

            competence.Enabled = true;
            ERContainer container = new ERContainer() { Competences = new List<Competence>() { competence }, Model = "Competence", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/Competencies/_CreateBody");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Competencia", PermissionTypes.Delete, null, "CompetenceElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteCompetence(int? CompetenceId, List<int> Competences, string Search, int Number = 1)
        {
            if (CompetenceId == null && Competences == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (CompetenceId != null)
            {
                var (ResponseCode, ResponseText, competence) = _helper.GetEntity<Competence>("Config", "Competence", "CompetenceId", (int)CompetenceId);
                if (ResponseCode == OK)
                {
                    competence.Enabled = !competence.Enabled;
                    ERContainer container = new ERContainer() { Competences = new List<Competence>() { competence }, Model = "Competence", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridCompetences(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Competence>>("Config", "Competence");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Competences.Contains((int)x.CompetenceId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Competences = list, Model = "Competence", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridCompetences(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridCompetences(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Niveles de competencias opcion 1

        [IsMenu(2, "Niveles de competencia", null, "Lista de Catálogos", null, null, "CompetenciesLevelsList")]
        [Route("Configuration/Catalogs/CompetenciesLevelsList")]
        [IsPermission("Nivel de Competencia", PermissionTypes.List, null, "CompetenceLevelList")]
        public ActionResult CompetenciesLevels()
        {
            return View("Catalogs/CompetenciesLevels/Index");
        }

        [Route("Configuration/Catalogs/NewCompetenceLevel")]
        [IsPermission("Nivel de Competencia", PermissionTypes.New, null, "CompetenceLevelCreation")]
        public ActionResult NewCompetenceLevel(int? LevelId)
        {
            return View("Catalogs/CompetenciesLevels/Create", LevelId);
        }

        public ActionResult GetCompetenceLevel(int? LevelId)
        {
            if (LevelId == null)
            {
                return PartialView("Catalogs/CompetenciesLevels/_ToAddPartial", new CompetenceLevel());
            }
            else if (LevelId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new CompetenceLevel(), "~/Views/Configuration/Catalogs/CompetenciesLevels/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, competenceLevel) = _helper.GetEntity<CompetenceLevel>("Config", "CompetenceLevel", "LevelId", (int)LevelId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, competenceLevel, "~/Views/Configuration/Catalogs/CompetenciesLevels/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/CompetenciesLevels/_ToAddPartial", competenceLevel);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/CompetenciesLevels/_ToAddPartial", new CompetenceLevel());
                }
            }
        }

        public ActionResult GetGridCompetenciesLevels(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, CompetenciesLevels, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<CompetenceLevel>>("Config", "CompetenceLevel", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };

            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, CompetenciesLevels, "~/Views/Configuration/Catalogs/CompetenciesLevels/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/CompetenciesLevels/_GridPartial", CompetenciesLevels);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/CompetenciesLevels/_GridPartial", new List<CompetenceLevel>());
            }
        }

        public ActionResult GetCompetenciesLevels()
        {
            var (ResponseCode, ResponseText, levels) = _helper.GetEntitiesList<List<CompetenceLevel>>("Config", "CompetenceLevel");

            if (ResponseCode == OK) return PartialView("Catalogs/CompetenciesLevels/_AggregatesPartial", levels.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/CompetenciesLevels/_AggregatesPartial", new List<CompetenceLevel>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveCompetenceLevel(CompetenceLevel competenceLevel)
        {
            if (!ModelState.IsValid) return View(competenceLevel);

            competenceLevel.Enabled = true;
            ERContainer container = new ERContainer() { CompetenceLevels = new List<CompetenceLevel>() { competenceLevel }, Model = "CompetenceLevel", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/CompetenciesLevels/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Nivel de Competencia", PermissionTypes.Delete, null, "CompetenceLevelElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteCompetenceLevel(int? LevelId, List<int> CompetenciesLevels, string Search, int Number = 1)
        {
            if (LevelId == null && CompetenciesLevels == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (LevelId != null)
            {
                var (ResponseCode, ResponseText, competenceLevel) = _helper.GetEntity<CompetenceLevel>("Config", "CompetenceLevel", "LevelId", (int)LevelId);
                if (ResponseCode == OK)
                {
                    competenceLevel.Enabled = !competenceLevel.Enabled;
                    ERContainer container = new ERContainer() { CompetenceLevels = new List<CompetenceLevel>() { competenceLevel }, Model = "CompetenceLevel", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridCompetenciesLevels(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<CompetenceLevel>>("Config", "CompetenceLevel");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => CompetenciesLevels.Contains((int)x.LevelId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { CompetenceLevels = list, Model = "CompetenceLevel", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridCompetenciesLevels(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridCompetenciesLevels(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Estado de miembros opcion 2

        [IsMenu(3, "Estado de miembro", null, "Lista de Catálogos", null, null, "MemberStatusList")]
        [Route("Configuration/Catalogs/MemberStatusList")]
        [IsPermission("Estado de Miembro", PermissionTypes.List, null, "MemberStatusList")]
        public ActionResult MemberStatus()
        {
            return View("Catalogs/MemberStatus/Index");
        }

        [Route("Configuration/Catalogs/NewMemberStatus")]
        [IsPermission("Estado de Miembro", PermissionTypes.New, null, "MemberStatusCreation")]
        public ActionResult NewMemberStatus(int? MemberStatusId)
        {
            return View("Catalogs/MemberStatus/Create", MemberStatusId);
        }

        public ActionResult GetMemberStatus(int? MemberStatusId)
        {
            if (MemberStatusId == null)
            {
                return PartialView("Catalogs/MemberStatus/_ToAddPartial", new MemberStatus());
            }
            else if (MemberStatusId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new MemberStatus(), "~/Views/Configuration/Catalogs/MemberStatus/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, memberStatus) = _helper.GetEntity<MemberStatus>("Config", "MemberStatus", "MemberStatusId", (int)MemberStatusId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, memberStatus, "~/Views/Configuration/Catalogs/MemberStatus/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/MemberStatus/_ToAddPartial", memberStatus);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/MemberStatus/_ToAddPartial", new MemberStatus());
                }
            }
        }

        public ActionResult GetGridMemberStatuses(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, MemberStatuses, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<MemberStatus>>("Config", "MemberStatus", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };

            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, MemberStatuses, "~/Views/Configuration/Catalogs/MemberStatus/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/MemberStatus/_GridPartial", MemberStatuses);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/MemberStatus/_GridPartial", new List<MemberStatus>());
            }
        }

        public ActionResult GetMemberStatuses()
        {
            var (ResponseCode, ResponseText, MemberStatuses) = _helper.GetEntitiesList<List<MemberStatus>>("Config", "MemberStatus");

            if (ResponseCode == OK) return PartialView("Catalogs/MemberStatus/_AggregatesPartial", MemberStatuses.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/MemberStatus/_AggregatesPartial", new List<MemberStatus>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMemberStatus(MemberStatus memberStatus)
        {
            if (!ModelState.IsValid) return View(memberStatus);

            memberStatus.Enabled = true;
            ERContainer container = new ERContainer() { MemberStatuses = new List<MemberStatus>() { memberStatus }, Model = "MemberStatus", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/MemberStatus/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Estado de Miembro", PermissionTypes.Delete, null, "MemberStatusElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteMemberStatus(int? MemberStatusId, List<int> MemberStatuses, string Search, int Number = 1)
        {
            if (MemberStatusId == null && MemberStatuses == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (MemberStatusId != null)
            {
                var (ResponseCode, ResponseText, memberStatus) = _helper.GetEntity<MemberStatus>("Config", "MemberStatus", "MemberStatusId", (int)MemberStatusId);
                if (ResponseCode == OK)
                {
                    memberStatus.Enabled = !memberStatus.Enabled;
                    ERContainer container = new ERContainer() { MemberStatuses = new List<MemberStatus>() { memberStatus }, Model = "MemberStatus", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridMemberStatuses(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<MemberStatus>>("Config", "MemberStatus");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => MemberStatuses.Contains((int)x.MemberStatusId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { MemberStatuses = list, Model = "MemberStatus", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridMemberStatuses(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridMemberStatuses(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Niveles de idioma opcion 4

        [IsMenu(5, "Niveles de idioma", null, "Lista de Catálogos", null, null, "LanguageLevelsList")]
        [Route("Configuration/Catalogs/LanguageLevelsList")]
        [IsPermission("Nivel de Idioma", PermissionTypes.List, null, "LanguageLevelsList")]
        public ActionResult LanguageLevels()
        {
            return View("Catalogs/LanguageLevels/Index");
        }

        public ActionResult GetGridLanguageLevels(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, languages, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.LanguageLevelId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, languages, "~/Views/Configuration/Catalogs/LanguageLevels/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/LanguageLevels/_GridPartial", languages);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/LanguageLevels/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewLanguageLevel")]
        [IsPermission("Nivel de Idioma", PermissionTypes.New, null, "LanguageLevelsCreation")]
        public ActionResult NewLanguageLevel(int? LevelId)
        {
            return View("Catalogs/LanguageLevels/Create", LevelId);
        }

        public ActionResult GetLanguageLevel(int? LevelId)
        {
            if (LevelId == null)
            {
                return PartialView("Catalogs/LanguageLevels/_ToAddPartial", new BaseCatalog());
            }
            else if (LevelId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/LanguageLevels/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, languageLevel) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)LevelId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, languageLevel, "~/Views/Configuration/Catalogs/LanguageLevels/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/LanguageLevels/_ToAddPartial", languageLevel);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/LanguageLevels/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetLanguageLevels()
        {
            var (ResponseCode, ResponseText, levels) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.LanguageLevelId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/LanguageLevels/_AggregatesPartial", levels.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/CompetenciesLevels/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveLanguageLevel(BaseCatalog languageLevel)
        {
            if (!ModelState.IsValid) return View(languageLevel);

            languageLevel.Enabled = true;
            languageLevel.ItemOf = CustomSession.CurrentSession.CatalogConstants.LanguageLevelId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { languageLevel }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/LanguageLevels/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Nivel de Idioma", PermissionTypes.Delete, null, "LanguageLevelsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteLanguageLevel(int? LevelId, List<int> LanguageLevels, string Search, int Number = 1)
        {
            if (LevelId == null && LanguageLevels == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (LevelId != null)
            {
                var (ResponseCode, ResponseText, languageLevels) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)LevelId);
                if (ResponseCode == OK)
                {
                    languageLevels.Enabled = !languageLevels.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { languageLevels }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridLanguageLevels(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.LanguageLevelId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => LanguageLevels.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridLanguageLevels(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridLanguageLevels(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Rangos de edades opcion 5

        [IsMenu(6, "Rangos de edades", null, "Lista de Catálogos", null, null, "AgeRangesList")]
        [Route("Configuration/Catalogs/AgeRangesList")]
        [IsPermission("Rango de Edad", PermissionTypes.List, null, "AgeRangesList")]
        public ActionResult AgeRanges()
        {
            return View("Catalogs/AgeRanges/Index");
        }

        [Route("Configuration/Catalogs/NewAgeRange")]
        [IsPermission("Rango de Edad", PermissionTypes.New, null, "AgeRangesCreation")]
        public ActionResult NewAgeRange(int? AgeRangeId)
        {
            return View("Catalogs/AgeRanges/Create", AgeRangeId);
        }

        public ActionResult GetAgeRange(int? AgeRangeId)
        {
            if (AgeRangeId == null)
            {
                return PartialView("Catalogs/AgeRanges/_ToAddPartial", new AgeRange());
            }
            else if (AgeRangeId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new AgeRange(), "~/Views/Configuration/Catalogs/AgeRanges/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, ageRange) = _helper.GetEntity<AgeRange>("Config", "AgeRange", "AgeRangeId", (int)AgeRangeId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, ageRange, "~/Views/Configuration/Catalogs/AgeRanges/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/AgeRanges/_ToAddPartial", ageRange);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/AgeRanges/_ToAddPartial", new AgeRange());
                }
            }
        }

        public ActionResult GetGridAgeRanges(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, AgeRanges, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<AgeRange>>("Config", "AgeRange", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };

            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, AgeRanges, "~/Views/Configuration/Catalogs/AgeRanges/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/AgeRanges/_GridPartial", AgeRanges);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/AgeRanges/_GridPartial", new List<AgeRange>());
            }
        }

        public ActionResult GetAgeRanges()
        {
            var (ResponseCode, ResponseText, ranges) = _helper.GetEntitiesList<List<AgeRange>>("Config", "AgeRange");

            if (ResponseCode == OK) return PartialView("Catalogs/AgeRanges/_AggregatesPartial", ranges.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/AgeRanges/_AggregatesPartial", new List<AgeRange>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveAgeRange(AgeRange ageRange)
        {
            if (!ModelState.IsValid) return View(ageRange);

            ageRange.Enabled = true;
            ERContainer container = new ERContainer() { AgeRanges = new List<AgeRange>() { ageRange }, Model = "AgeRange", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/AgeRanges/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Rango de Edad", PermissionTypes.Delete, null, "AgeRangesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteAgeRange(int? AgeRangeId, List<int> AgeRanges, string Search, int Number = 1)
        {
            if (AgeRangeId == null && AgeRanges == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (AgeRangeId != null)
            {
                var (ResponseCode, ResponseText, ageRange) = _helper.GetEntity<AgeRange>("Config", "AgeRange", "AgeRangeId", (int)AgeRangeId);
                if (ResponseCode == OK)
                {
                    ageRange.Enabled = !ageRange.Enabled;
                    ERContainer container = new ERContainer() { AgeRanges = new List<AgeRange>() { ageRange }, Model = "AgeRange", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridAgeRanges(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<AgeRange>>("Config", "AgeRange");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => AgeRanges.Contains((int)x.AgeRangeId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { AgeRanges = list, Model = "AgeRange", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridAgeRanges(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridAgeRanges(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Tipos de actividad opcion 6

        [IsMenu(7, "Tipos de actividad", null, "Lista de Catálogos", null, null, "ActivityTypesList")]
        [Route("Configuration/Catalogs/ActivityTypesList")]
        [IsPermission("Tipo de Actividad", PermissionTypes.List, null, "ActivityTypesList")]
        public ActionResult ActivityTypes()
        {
            return View("Catalogs/ActivityTypes/Index");
        }

        public ActionResult GetGridActivityTypes(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, activities, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityTypeId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, activities, "~/Views/Configuration/Catalogs/ActivityTypes/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/ActivityTypes/_GridPartial", activities);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/ActivityTypes/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewActivityType")]
        [IsPermission("Tipo de Actividad", PermissionTypes.New, null, "ActivityTypesCreation")]
        public ActionResult NewActivityType(int? ActivityTypeId)
        {
            return View("Catalogs/ActivityTypes/Create", ActivityTypeId);
        }

        public ActionResult GetActivityType(int? ActivityTypeId)
        {
            if (ActivityTypeId == null)
            {
                return PartialView("Catalogs/ActivityTypes/_ToAddPartial", new BaseCatalog());
            }
            else if (ActivityTypeId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/ActivityTypes/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, activityTypes) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)ActivityTypeId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, activityTypes, "~/Views/Configuration/Catalogs/ActivityTypes/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/ActivityTypes/_ToAddPartial", activityTypes);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/ActivityTypes/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetActivityTypes()
        {
            var (ResponseCode, ResponseText, activities) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityTypeId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/ActivityTypes/_AggregatesPartial", activities.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/ActivityTypes/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveActivityType(BaseCatalog activityType)
        {
            if (!ModelState.IsValid) return View(activityType);

            activityType.Enabled = true;
            activityType.ItemOf = CustomSession.CurrentSession.CatalogConstants.ActivityTypeId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { activityType }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/ActivityTypes/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Tipo de Actividad", PermissionTypes.Delete, null, "ActivityTypesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteActivityTypes(int? ActivityTypeId, List<int> ActivityTypes, string Search, int Number = 1)
        {
            if (ActivityTypeId == null && ActivityTypes == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (ActivityTypeId != null)
            {
                var (ResponseCode, ResponseText, activityTypes) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)ActivityTypeId);
                if (ResponseCode == OK)
                {
                    activityTypes.Enabled = !activityTypes.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { activityTypes }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridActivityTypes(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityTypeId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => ActivityTypes.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridActivityTypes(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridActivityTypes(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Estado de actividad opcion 7

        [IsMenu(8, "Estado de actividad", null, "Lista de Catálogos", null, null, "ActivityStatusesList")]
        [Route("Configuration/Catalogs/ActivityStatusesList")]
        [IsPermission("Estado de Actividad", PermissionTypes.List, null, "ActivityStatusesList")]
        public ActionResult ActivityStatuses()
        {
            return View("Catalogs/ActivityStatuses/Index");
        }

        public ActionResult GetGridActivityStatuses(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, statuses, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityStatusId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, statuses, "~/Views/Configuration/Catalogs/ActivityStatuses/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/ActivityStatuses/_GridPartial", statuses);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/ActivityStatuses/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewActivityStatus")]
        [IsPermission("Estado de Actividad", PermissionTypes.New, null, "ActivityStatusesCreation")]
        public ActionResult NewActivityStatus(int? BaseCatalogId)
        {
            return View("Catalogs/ActivityStatuses/Create", BaseCatalogId);
        }

        public ActionResult GetActivityStatus(int? BaseCatalogId)
        {
            if (BaseCatalogId == null)
            {
                return PartialView("Catalogs/ActivityStatuses/_ToAddPartial", new BaseCatalog());
            }
            else if (BaseCatalogId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/ActivityStatuses/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, activityStatus) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)BaseCatalogId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, activityStatus, "~/Views/Configuration/Catalogs/ActivityStatuses/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/ActivityStatuses/_ToAddPartial", activityStatus);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/ActivityStatuses/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetActivityStatuses()
        {
            var (ResponseCode, ResponseText, statuses) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityStatusId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/ActivityStatuses/_AggregatesPartial", statuses.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/ActivityStatuses/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveActivityStatus(BaseCatalog activityStatus)
        {
            if (!ModelState.IsValid) return View(activityStatus);

            activityStatus.Enabled = true;
            activityStatus.ItemOf = CustomSession.CurrentSession.CatalogConstants.ActivityStatusId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { activityStatus }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/ActivityStatuses/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Estado de Actividad", PermissionTypes.Delete, null, "ActivityStatusesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteActivityStatus(int? BaseCatalogId, List<int> ActivityStatuses, string Search, int Number = 1)
        {
            if (BaseCatalogId == null && ActivityStatuses == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (BaseCatalogId != null)
            {
                var (ResponseCode, ResponseText, activityStatus) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)BaseCatalogId);
                if (ResponseCode == OK)
                {
                    activityStatus.Enabled = !activityStatus.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { activityStatus }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridActivityStatuses(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityStatusId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => ActivityStatuses.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridActivityStatuses(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridActivityStatuses(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Tipos de solicitudes opcion 8

        [IsMenu(9, "Tipos de solicitud ", null, "Lista de Catálogos", null, null, "RequestTypesList")]
        [Route("Configuration/Catalogs/RequestTypesList")]
        [IsPermission("Tipo de Solicitud", PermissionTypes.List, null, "RequestTypesList")]
        public ActionResult RequestTypes()
        {
            return View("Catalogs/RequestTypes/Index");
        }

        public ActionResult GetGridRequestTypes(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, types, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.RequestTypeId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, types, "~/Views/Configuration/Catalogs/RequestTypes/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/RequestTypes/_GridPartial", types);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/RequestTypes/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewRequestType")]
        [IsPermission("Tipo de Solicitud", PermissionTypes.New, null, "RequestTypesCreation")]
        public ActionResult NewRequestType(int? BaseCatalogId)
        {
            return View("Catalogs/RequestTypes/Create", BaseCatalogId);
        }

        public ActionResult GetRequestType(int? BaseCatalogId)
        {
            if (BaseCatalogId == null)
            {
                return PartialView("Catalogs/RequestTypes/_ToAddPartial", new BaseCatalog());
            }
            else if (BaseCatalogId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/RequestTypes/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, requestType) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)BaseCatalogId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, requestType, "~/Views/Configuration/Catalogs/RequestTypes/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/RequestTypes/_ToAddPartial", requestType);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/RequestTypes/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetRequestTypes()
        {
            var (ResponseCode, ResponseText, types) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.RequestTypeId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/RequestTypes/_AggregatesPartial", types.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/RequestTypes/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveRequestType(BaseCatalog requestType)
        {
            if (!ModelState.IsValid) return View(requestType);

            requestType.Enabled = true;
            requestType.ItemOf = CustomSession.CurrentSession.CatalogConstants.RequestTypeId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { requestType }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/RequestTypes/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Tipo de Solicitud", PermissionTypes.Delete, null, "RequestTypesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteRequestType(int? BaseCatalogId, List<int> RequestTypes, string Search, int Number = 1)
        {
            if (BaseCatalogId == null && RequestTypes == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (BaseCatalogId != null)
            {
                var (ResponseCode, ResponseText, requestType) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)BaseCatalogId);
                if (ResponseCode == OK)
                {
                    requestType.Enabled = !requestType.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { requestType }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridRequestTypes(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.RequestTypeId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => RequestTypes.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridRequestTypes(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridRequestTypes(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Estado de solicitudes opcion 9

        [IsMenu(10, "Estados de solicitud", null, "Lista de Catálogos", null, null, "RequestStatusesList")]
        [Route("Configuration/Catalogs/RequestStatusesList")]
        [IsPermission("Estado de Solicitud", PermissionTypes.List, null, "RequestStatusesList")]
        public ActionResult RequestStatuses()
        {
            return View("Catalogs/RequestStatuses/Index");
        }

        public ActionResult GetGridRequestStatuses(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, requestStatuses, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<RequestStatus>>("Config", "RequestStatus", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, requestStatuses, "~/Views/Configuration/Catalogs/RequestStatuses/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/RequestStatuses/_GridPartial", requestStatuses);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/RequestStatuses/_GridPartial", new List<RequestStatus>());
            }
        }

        [Route("Configuration/Catalogs/NewRequestStatus")]
        [IsPermission("Estado de Solicitud", PermissionTypes.New, null, "RequestStatusesCreation")]
        public ActionResult NewRequestStatus(int? RequestStatusId)
        {
            return View("Catalogs/RequestStatuses/Create", RequestStatusId);
        }

        public ActionResult GetRequestStatus(int? RequestStatusId)
        {
            if (RequestStatusId == null)
            {
                return PartialView("Catalogs/RequestStatuses/_ToAddPartial", new RequestStatus());
            }
            else if (RequestStatusId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new RequestStatus(), "~/Views/Configuration/Catalogs/RequestStatuses/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, requestStatuses) = _helper.GetEntity<RequestStatus>("Config", "RequestStatus", "RequestStatusId", (int)RequestStatusId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, requestStatuses, "~/Views/Configuration/Catalogs/RequestStatuses/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/RequestStatuses/_ToAddPartial", requestStatuses);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/RequestStatuses/_ToAddPartial", new RequestStatus());
                }
            }
        }

        public ActionResult GetRequestStatuses()
        {
            var (ResponseCode, ResponseText, requestStatuses) = _helper.GetEntitiesList<List<RequestStatus>>("Config", "RequestStatus");

            if (ResponseCode == OK) return PartialView("Catalogs/RequestStatuses/_AggregatesPartial", requestStatuses.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/RequestStatuses/_AggregatesPartial", new List<RequestStatus>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveRequestStatus(RequestStatus requestStatus)
        {
            if (!ModelState.IsValid) return View(requestStatus);

            requestStatus.Enabled = true;
            ERContainer container = new ERContainer() { RequestStatuses = new List<RequestStatus>() { requestStatus }, Model = "RequestStatus", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/RequestStatuses/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Estado de Solicitud", PermissionTypes.Delete, null, "RequestStatusesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteRequestStatuses(int? RequestStatusId, List<int> RequestStatuses, string Search, int Number = 1)
        {
            if (RequestStatusId == null && RequestStatuses == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (RequestStatusId != null)
            {
                var (ResponseCode, ResponseText, requestStatus) = _helper.GetEntity<RequestStatus>("Config", "RequestStatus", "RequestStatusId", (int)RequestStatusId);
                if (ResponseCode == OK)
                {
                    requestStatus.Enabled = !requestStatus.Enabled;
                    ERContainer container = new ERContainer() { RequestStatuses = new List<RequestStatus>() { requestStatus }, Model = "RequestStatus", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridRequestStatuses(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<RequestStatus>>("Config", "RequestStatus");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => RequestStatuses.Contains((int)x.RequestStatusId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { RequestStatuses = list, Model = "RequestStatus", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridRequestStatuses(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridRequestStatuses(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Sector opcion 10

        [IsMenu(11, "Sector", null, "Lista de Catálogos", null, null, "SectorsList")]
        [Route("Configuration/Catalogs/SectorsList")]
        [IsPermission("Sector", PermissionTypes.List, null, "SectorsList")]
        public ActionResult Sectors()
        {
            return View("Catalogs/Sectors/Index");
        }

        public ActionResult GetGridSectors(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, sector, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.SectorId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, sector, "~/Views/Configuration/Catalogs/Sectors/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Sectors/_GridPartial", sector);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Sectors/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewSector")]
        [IsPermission("Sector", PermissionTypes.New, null, "SectorsCreation")]
        public ActionResult NewSector(int? BaseCatalogId)
        {
            return View("Catalogs/Sectors/Create", BaseCatalogId);
        }

        public ActionResult GetSector(int? BaseCatalogId)
        {
            if (BaseCatalogId == null)
            {
                return PartialView("Catalogs/Sectors/_ToAddPartial", new BaseCatalog());
            }
            else if (BaseCatalogId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/Sectors/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, requestType) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)BaseCatalogId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, requestType, "~/Views/Configuration/Catalogs/Sectors/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Sectors/_ToAddPartial", requestType);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Sectors/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetSectors()
        {
            var (ResponseCode, ResponseText, types) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.SectorId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/Sectors/_AggregatesPartial", types.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/Sectors/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveSector(BaseCatalog sector)
        {
            if (!ModelState.IsValid) return View(sector);

            sector.Enabled = true;
            sector.ItemOf = CustomSession.CurrentSession.CatalogConstants.SectorId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { sector }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/Sectors/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Sector", PermissionTypes.Delete, null, "SectorsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteSector(int? BaseCatalogId, List<int> Sectors, string Search, int Number = 1)
        {
            if (BaseCatalogId == null && Sectors == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (BaseCatalogId != null)
            {
                var (ResponseCode, ResponseText, sector) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)BaseCatalogId);
                if (ResponseCode == OK)
                {
                    sector.Enabled = !sector.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { sector }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridSectors(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.SectorId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Sectors.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridSectors(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridSectors(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Tipos de Seccion opcion 11

        [IsMenu(12, "Tipo de sección", null, "Lista de Catálogos", null, null, "SectionTypesList")]
        [Route("Configuration/Catalogs/SectionTypesList")]
        [IsPermission("Tipo de Sección", PermissionTypes.List, null, "SectionTypesList")]
        public ActionResult SectionTypes()
        {
            return View("Catalogs/SectionTypes/Index");
        }

        public ActionResult GetGridSectionTypes(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, sectionTypes, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<SectionType>>("Config", "SectionType", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, sectionTypes, "~/Views/Configuration/Catalogs/SectionTypes/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/SectionTypes/_GridPartial", sectionTypes);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/SectionTypes/_GridPartial", new List<SectionType>());
            }
        }

        [Route("Configuration/Catalogs/NewSectionType")]
        [IsPermission("Tipo de Sección", PermissionTypes.New, null, "SectionTypesCreation")]
        public ActionResult NewSectionType(int? SectionTypeId)
        {
            return View("Catalogs/SectionTypes/Create", SectionTypeId);
        }

        public ActionResult GetSectionType(int? SectionTypeId)
        {
            var (ListResponseCode, ListResponseText, ListAgeRanges) = _helper.GetEntitiesList<List<AgeRange>>("Config", "AgeRange");
            if (ListResponseCode == OK)
            {
                foreach (var item in ListAgeRanges) item.Name = item.Name + " / " + item.MinimumAge.ToString() + "-" + item.MaximumAge.ToString();
                ViewBag.AgeRangesList = new SelectList(ListAgeRanges.Where(x => x.Enabled).ToList(), "AgeRangeId", "Name");
            }
            else
                ViewBag.AgeRangesList = new SelectList(new List<AgeRange>(), "AgeRangeId", "Name");

            if (SectionTypeId == null)
            {
                return PartialView("Catalogs/SectionTypes/_ToAddPartial", new SectionType());
            }
            else if (SectionTypeId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new SectionType(), "~/Views/Configuration/Catalogs/SectionTypes/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, sectionType) = _helper.GetEntity<SectionType>("Config", "SectionType", "SectionTypeId", (int)SectionTypeId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, sectionType, "~/Views/Configuration/Catalogs/SectionTypes/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/SectionTypes/_ToAddPartial", sectionType);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/SectionTypes/_ToAddPartial", new SectionType());
                }
            }
        }

        public ActionResult GetSectionTypes()
        {
            var (ResponseCode, ResponseText, types) = _helper.GetEntitiesList<List<SectionType>>("Config", "SectionType");

            if (ResponseCode == OK) return PartialView("Catalogs/SectionTypes/_AggregatesPartial", types.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/SectionTypes/_AggregatesPartial", new List<SectionType>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveSectionType(SectionType sectionType)
        {
            if (!ModelState.IsValid) return View(sectionType);

            sectionType.Enabled = true;
            ERContainer container = new ERContainer() { SectionTypes = new List<SectionType>() { sectionType }, Model = "SectionType", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/SectionTypes/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Tipo de Sección", PermissionTypes.Delete, null, "SectionTypesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteSectionType(int? SectionTypeId, List<int> SectionTypes, string Search, int Number = 1)
        {
            if (SectionTypeId == null && SectionTypes == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (SectionTypeId != null)
            {
                var (ResponseCode, ResponseText, sectionType) = _helper.GetEntity<SectionType>("Config", "SectionType", "SectionTypeId", (int)SectionTypeId);
                if (ResponseCode == OK)
                {
                    sectionType.Enabled = !sectionType.Enabled;
                    ERContainer container = new ERContainer() { SectionTypes = new List<SectionType>() { sectionType }, Model = "SectionType", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridSectionTypes(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<SectionType>>("Config", "SectionType");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => SectionTypes.Contains((int)x.SectionTypeId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { SectionTypes = list, Model = "SectionType", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridSectionTypes(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridSectionTypes(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Tipos de cargo opcion 12

        [IsMenu(13, "Tipo de cargo", null, "Lista de Catálogos", null, null, "PositionTypesList")]
        [Route("Configuration/Catalogs/PositionTypesList")]
        [IsPermission("Tipo de Cargo", PermissionTypes.List, null, "PositionTypesList")]
        public ActionResult PositionTypes()
        {
            return View("Catalogs/PositionTypes/Index");
        }

        public ActionResult GetGridPositionTypes(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, positionType, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<PositionType>>("Config", "PositionType", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, positionType, "~/Views/Configuration/Catalogs/PositionTypes/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/PositionTypes/_GridPartial", positionType);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/PositionTypes/_GridPartial", new List<PositionType>());
            }
        }

        [Route("Configuration/Catalogs/NewPositionType")]
        [IsPermission("Tipo de Cargo", PermissionTypes.New, null, "PositionTypesCreation")]
        public ActionResult NewPositionType(int? PositionTypeId)
        {
            return View("Catalogs/PositionTypes/Create", PositionTypeId);
        }

        public ActionResult GetPositionType(int? PositionTypeId)
        {
            //DDLists
            var (ListResponseCode, ListResponseText, List) = _helper.GetEntitiesList<List<AgeRange>>("Config", "AgeRange");
            if (ListResponseCode == OK)
            {
                foreach (var item in List) item.Name = item.Name + " / " + item.MinimumAge.ToString() + "-" + item.MaximumAge.ToString();
                ViewBag.AgeRangesList = new SelectList(List.Where(x => x.Enabled).ToList(), "AgeRangeId", "Name");
            }
            else
                ViewBag.AgeRangesList = new SelectList(new List<AgeRange>(), "AgeRangeId", "Name");

            List<BaseCatalog> ListCat;
            (ListResponseCode, ListResponseText, ListCat) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.PositionTypeCategoryId.ToString()) });
            if (ListResponseCode == OK)
                ViewBag.CategoriesList = new SelectList(ListCat.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else
                ViewBag.CategoriesList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            (ListResponseCode, ListResponseText, ListCat) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.GenderId.ToString()) });
            if (ListResponseCode == OK)
                ViewBag.GendersList = new SelectList(ListCat.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else
                ViewBag.GendersList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");


            //Get
            if (PositionTypeId == null)
            {
                return PartialView("Catalogs/PositionTypes/_ToAddPartial", new PositionType());
            }
            else if (PositionTypeId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new PositionType(), "~/Views/Configuration/Catalogs/PositionTypes/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, positionType) = _helper.GetEntity<PositionType>("Config", "PositionType", "PositionTypeId", (int)PositionTypeId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, positionType, "~/Views/Configuration/Catalogs/PositionTypes/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/PositionTypes/_ToAddPartial", positionType);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/PositionTypes/_ToAddPartial", new PositionType());
                }
            }
        }

        public ActionResult GetPositionTypes()
        {
            var (ResponseCode, ResponseText, types) = _helper.GetEntitiesList<List<PositionType>>("Config", "PositionType");

            if (ResponseCode == OK) return PartialView("Catalogs/PositionTypes/_AggregatesPartial", types.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/PositionTypes/_AggregatesPartial", new List<PositionType>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SavePositionType(PositionType positionType)
        {
            if (!ModelState.IsValid) return View(positionType);

            positionType.Enabled = true;
            ERContainer container = new ERContainer() { PositionTypes = new List<PositionType>() { positionType }, Model = "PositionType", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/PositionTypes/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Tipo de Cargo", PermissionTypes.Delete, null, "PositionTypesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeletePositionType(int? PositionTypeId, List<int> PositionTypes, string Search, int Number = 1)
        {
            if (PositionTypeId == null && PositionTypes == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (PositionTypeId != null)
            {
                var (ResponseCode, ResponseText, positionType) = _helper.GetEntity<PositionType>("Config", "PositionType", "PositionTypeId", (int)PositionTypeId);
                if (ResponseCode == OK)
                {
                    positionType.Enabled = !positionType.Enabled;
                    ERContainer container = new ERContainer() { PositionTypes = new List<PositionType>() { positionType }, Model = "PositionType", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridPositionTypes(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<PositionType>>("Config", "PositionType");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => PositionTypes.Contains((int)x.PositionTypeId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { PositionTypes = list, Model = "PositionType", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridPositionTypes(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridPositionTypes(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Genero opcion 13

        [IsMenu(14, "Género", null, "Lista de Catálogos", null, null, "GendersList")]
        [Route("Configuration/Catalogs/GendersList")]
        [IsPermission("Género", PermissionTypes.List, null, "GendersList")]
        public ActionResult Genders()
        {
            return View("Catalogs/Genders/Index");
        }

        public ActionResult GetGridGenders(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, genders, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.GenderId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, genders, "~/Views/Configuration/Catalogs/Genders/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Genders/_GridPartial", genders);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Genders/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewGender")]
        [IsPermission("Género", PermissionTypes.New, null, "GendersCreation")]
        public ActionResult NewGender(int? GenderId)
        {
            return View("Catalogs/Genders/Create", GenderId);
        }

        public ActionResult GetGender(int? GenderId)
        {
            if (GenderId == null)
            {
                return PartialView("Catalogs/Genders/_ToAddPartial", new BaseCatalog());
            }
            else if (GenderId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/Genders/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, Genders) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)GenderId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, Genders, "~/Views/Configuration/Catalogs/Genders/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Genders/_ToAddPartial", Genders);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Genders/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetGenders()
        {
            var (ResponseCode, ResponseText, Genders) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.GenderId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/Genders/_AggregatesPartial", Genders.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/Genders/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveGender(BaseCatalog gender)
        {
            if (!ModelState.IsValid) return View(gender);

            gender.Enabled = true;
            gender.ItemOf = CustomSession.CurrentSession.CatalogConstants.GenderId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { gender }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/Genders/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Género", PermissionTypes.Delete, null, "GendersElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteGenders(int? GenderId, List<int> Genders, string Search, int Number = 1)
        {
            if (GenderId == null && Genders == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (GenderId != null)
            {
                var (ResponseCode, ResponseText, genders) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)GenderId);
                if (ResponseCode == OK)
                {
                    genders.Enabled = !genders.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { genders }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridGenders(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.GenderId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Genders.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridGenders(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridGenders(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Courses option 14

        [IsMenu(15, "Curso", null, "Lista de Catálogos", null, null, "CoursesList")]
        [Route("Configuration/Catalogs/CoursesList")]
        [IsPermission("Curso", PermissionTypes.List, null, "CoursesList")]
        public ActionResult Courses()
        {
            return View("Catalogs/Courses/Index");
        }

        public ActionResult GetGridCourses(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, courses, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.CourseId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, courses, "~/Views/Configuration/Catalogs/Courses/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Courses/_GridPartial", courses);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Courses/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewCourse")]
        [IsPermission("Curso", PermissionTypes.New, null, "CoursesCreation")]
        public ActionResult NewCourse(int? CourseId)
        {
            return View("Catalogs/Courses/Create", CourseId);
        }

        public ActionResult GetCourse(int? CourseId)
        {
            if (CourseId == null)
            {
                return PartialView("Catalogs/Courses/_ToAddPartial", new BaseCatalog());
            }
            else if (CourseId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/Courses/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, Courses) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)CourseId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, Courses, "~/Views/Configuration/Catalogs/Courses/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Courses/_ToAddPartial", Courses);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Courses/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetCourses()
        {
            var (ResponseCode, ResponseText, Courses) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.CourseId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/Courses/_AggregatesPartial", Courses.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/Courses/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveCourse(BaseCatalog course)
        {
            if (!ModelState.IsValid) return View(course);

            course.Enabled = true;
            course.ItemOf = CustomSession.CurrentSession.CatalogConstants.CourseId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { course }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/Courses/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Curso", PermissionTypes.Delete, null, "CoursesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteCourses(int? CourseId, List<int> Courses, string Search, int Number = 1)
        {
            if (CourseId == null && Courses == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (CourseId != null)
            {
                var (ResponseCode, ResponseText, courses) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)CourseId);
                if (ResponseCode == OK)
                {
                    courses.Enabled = !courses.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { courses }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridCourses(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.CourseId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Courses.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridCourses(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridCourses(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Workshops option 15

        [IsMenu(16, "Taller", null, "Lista de Catálogos", null, null, "WorkshopsList")]
        [Route("Configuration/Catalogs/WorkshopsList")]
        [IsPermission("Taller", PermissionTypes.List, null, "WorkshopsList")]
        public ActionResult Workshops()
        {
            return View("Catalogs/Workshops/Index");
        }

        public ActionResult GetGridWorkshops(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, workshops, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.WorkshopId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, workshops, "~/Views/Configuration/Catalogs/Workshops/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Workshops/_GridPartial", workshops);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Workshops/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewWorkshop")]
        [IsPermission("Taller", PermissionTypes.New, null, "WorkshopsCreation")]
        public ActionResult NewWorkshop(int? WorkshopId)
        {
            return View("Catalogs/Workshops/Create", WorkshopId);
        }

        public ActionResult GetWorkshop(int? WorkshopId)
        {
            if (WorkshopId == null)
            {
                return PartialView("Catalogs/Workshops/_ToAddPartial", new BaseCatalog());
            }
            else if (WorkshopId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/Workshops/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, Workshops) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)WorkshopId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, Workshops, "~/Views/Configuration/Catalogs/Workshops/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Workshops/_ToAddPartial", Workshops);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Workshops/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetWorkshops()
        {
            var (ResponseCode, ResponseText, Workshops) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.WorkshopId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/Workshops/_AggregatesPartial", Workshops.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/Workshops/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveWorkshop(BaseCatalog workshop)
        {
            if (!ModelState.IsValid) return View(workshop);

            workshop.Enabled = true;
            workshop.ItemOf = CustomSession.CurrentSession.CatalogConstants.WorkshopId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { workshop }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/Workshops/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Taller", PermissionTypes.Delete, null, "WorkshopsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteWorkshops(int? WorkshopId, List<int> Workshops, string Search, int Number = 1)
        {
            if (WorkshopId == null && Workshops == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (WorkshopId != null)
            {
                var (ResponseCode, ResponseText, workshops) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)WorkshopId);
                if (ResponseCode == OK)
                {
                    workshops.Enabled = !workshops.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { workshops }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridWorkshops(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.WorkshopId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Workshops.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridWorkshops(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridWorkshops(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Activity option 16

        [IsMenu(17, "Actividad", null, "Lista de Catálogos", null, null, "ActivitiesList")]
        [Route("Configuration/Catalogs/ActivitiesList")]
        [IsPermission("Actividad", PermissionTypes.List, null, "ActivitiesList")]
        public ActionResult Activities()
        {
            return View("Catalogs/Activities/Index");
        }

        public ActionResult GetGridActivities(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, activities, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, activities, "~/Views/Configuration/Catalogs/Activities/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Activities/_GridPartial", activities);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Activities/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewActivity")]
        [IsPermission("Actividad", PermissionTypes.New, null, "ActivitiesCreation")]
        public ActionResult NewActivity(int? ActivityId)
        {
            return View("Catalogs/Activities/Create", ActivityId);
        }

        public ActionResult GetActivity(int? ActivityId)
        {
            if (ActivityId == null)
            {
                return PartialView("Catalogs/Activities/_ToAddPartial", new BaseCatalog());
            }
            else if (ActivityId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/Activities/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, Activities) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)ActivityId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, Activities, "~/Views/Configuration/Catalogs/Activities/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Activities/_ToAddPartial", Activities);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Activities/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetActivities()
        {
            var (ResponseCode, ResponseText, Activities) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/Activities/_AggregatesPartial", Activities.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/Activities/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveActivity(BaseCatalog activity)
        {
            if (!ModelState.IsValid) return View(activity);

            activity.Enabled = true;
            activity.ItemOf = CustomSession.CurrentSession.CatalogConstants.ActivityId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { activity }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/Activities/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Actividad", PermissionTypes.Delete, null, "ActivitiesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteActivities(int? ActivityId, List<int> Activities, string Search, int Number = 1)
        {
            if (ActivityId == null && Activities == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (ActivityId != null)
            {
                var (ResponseCode, ResponseText, activities) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)ActivityId);
                if (ResponseCode == OK)
                {
                    activities.Enabled = !activities.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { activities }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridActivities(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Activities.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridActivities(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridActivities(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region NationalEvent option 17

        [IsMenu(18, "Evento Nacional", null, "Lista de Catálogos", null, null, "NationalEventsList")]
        [Route("Configuration/Catalogs/NationalEventsList")]
        [IsPermission("Evento Nacional", PermissionTypes.List, null, "NationalEventsList")]
        public ActionResult NationalEvents()
        {
            return View("Catalogs/NationalEvents/Index");
        }

        public ActionResult GetGridNationalEvents(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, nationalevents, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.NationalEventId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, nationalevents, "~/Views/Configuration/Catalogs/NationalEvents/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/NationalEvents/_GridPartial", nationalevents);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/NationalEvents/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewNationalEvent")]
        [IsPermission("Evento Nacional", PermissionTypes.New, null, "NationalEventsCreation")]
        public ActionResult NewNationalEvent(int? NationalEventId)
        {
            return View("Catalogs/NationalEvents/Create", NationalEventId);
        }

        public ActionResult GetNationalEvent(int? NationalEventId)
        {
            if (NationalEventId == null)
            {
                return PartialView("Catalogs/NationalEvents/_ToAddPartial", new BaseCatalog());
            }
            else if (NationalEventId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/NationalEvents/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, NationalEvents) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)NationalEventId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, NationalEvents, "~/Views/Configuration/Catalogs/NationalEvents/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/NationalEvents/_ToAddPartial", NationalEvents);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/NationalEvents/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetNationalEvents()
        {
            var (ResponseCode, ResponseText, NationalEvents) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.NationalEventId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/NationalEvents/_AggregatesPartial", NationalEvents.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/NationalEvents/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveNationalEvent(BaseCatalog nationalevent)
        {
            if (!ModelState.IsValid) return View(nationalevent);

            nationalevent.Enabled = true;
            nationalevent.ItemOf = CustomSession.CurrentSession.CatalogConstants.NationalEventId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { nationalevent }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/NationalEvents/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Evento Nacional", PermissionTypes.Delete, null, "NationalEventsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteNationalEvents(int? NationalEventId, List<int> NationalEvents, string Search, int Number = 1)
        {
            if (NationalEventId == null && NationalEvents == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (NationalEventId != null)
            {
                var (ResponseCode, ResponseText, nationalevents) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)NationalEventId);
                if (ResponseCode == OK)
                {
                    nationalevents.Enabled = !nationalevents.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { nationalevents }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridNationalEvents(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.NationalEventId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => NationalEvents.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridNationalEvents(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridNationalEvents(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region InternationalEvent option 18

        [IsMenu(19, "Evento Internacional", null, "Lista de Catálogos", null, null, "InternationalEventsList")]
        [Route("Configuration/Catalogs/InternationalEventsList")]
        [IsPermission("Evento Internacional", PermissionTypes.List, null, "InternationalEventsList")]
        public ActionResult InternationalEvents()
        {
            return View("Catalogs/InternationalEvents/Index");
        }

        public ActionResult GetGridInternationalEvents(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, internationalevents, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", Search, Number, new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.InternationalEventId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, internationalevents, "~/Views/Configuration/Catalogs/InternationalEvents/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/InternationalEvents/_GridPartial", internationalevents);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/InternationalEvents/_GridPartial", new List<BaseCatalog>());
            }
        }

        [Route("Configuration/Catalogs/NewInternationalEvent")]
        [IsPermission("Evento Internacional", PermissionTypes.New, null, "InternationalEventsCreation")]
        public ActionResult NewInternationalEvent(int? InternationalEventId)
        {
            return View("Catalogs/InternationalEvents/Create", InternationalEventId);
        }

        public ActionResult GetInternationalEvent(int? InternationalEventId)
        {
            if (InternationalEventId == null)
            {
                return PartialView("Catalogs/InternationalEvents/_ToAddPartial", new BaseCatalog());
            }
            else if (InternationalEventId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new BaseCatalog(), "~/Views/Configuration/Catalogs/InternationalEvents/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, InternationalEvents) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)InternationalEventId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, InternationalEvents, "~/Views/Configuration/Catalogs/InternationalEvents/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/InternationalEvents/_ToAddPartial", InternationalEvents);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/InternationalEvents/_ToAddPartial", new BaseCatalog());
                }
            }
        }

        public ActionResult GetInternationalEvents()
        {
            var (ResponseCode, ResponseText, InternationalEvents) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.InternationalEventId.ToString()) });

            if (ResponseCode == OK) return PartialView("Catalogs/InternationalEvents/_AggregatesPartial", InternationalEvents.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/InternationalEvents/_AggregatesPartial", new List<BaseCatalog>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveInternationalEvent(BaseCatalog internationalevent)
        {
            if (!ModelState.IsValid) return View(internationalevent);

            internationalevent.Enabled = true;
            internationalevent.ItemOf = CustomSession.CurrentSession.CatalogConstants.InternationalEventId;
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { internationalevent }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/InternationalEvents/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Evento Internacional", PermissionTypes.Delete, null, "InternationalEventsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteInternationalEvents(int? InternationalEventId, List<int> InternationalEvents, string Search, int Number = 1)
        {
            if (InternationalEventId == null && InternationalEvents == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (InternationalEventId != null)
            {
                var (ResponseCode, ResponseText, internationalevents) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", (int)InternationalEventId);
                if (ResponseCode == OK)
                {
                    internationalevents.Enabled = !internationalevents.Enabled;
                    ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { internationalevents }, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridInternationalEvents(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.InternationalEventId.ToString()) });
                if (ResponseCode == OK)
                {
                    list = list.Where(x => InternationalEvents.Contains((int)x.BaseCatalogId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { BaseCatalogs = list, Model = "BaseCatalog", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridInternationalEvents(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridInternationalEvents(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #endregion

    }
}