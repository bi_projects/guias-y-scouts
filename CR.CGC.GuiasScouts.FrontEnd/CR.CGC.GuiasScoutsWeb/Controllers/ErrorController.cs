﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    public class ErrorController : ApplicationController
    {
        // GET: Error
        public ActionResult Index(int error = 0)
        {
            ViewBag.Title = "Ocurrio un error inesperado";
            ViewBag.Description = "Esto es muy vergonzoso, esperemos que no vuelva a pasar ..";
            return View("ErrorPage");
        }

        public ActionResult NotFound()
        {
            ViewBag.Title = "Página no encontrada";
            ViewBag.Description = "La URL que está intentando ingresar no existe";
            return View();
        }

        public ActionResult BadRequest()
        {
            ViewBag.Title = "Solicitud incorrecta";
            ViewBag.Description = "Solicitud incorrecta";
            return View();
        }

        public ActionResult Forbidden()
        {
            ViewBag.Title = "Página denegada";
            ViewBag.Description = "Página denegada";
            return View();
        }

        public ActionResult ServerError()
        {
            ViewBag.Title = "Error Interno";
            ViewBag.Description = "Esto es muy vergonzoso, esperemos que no vuelva a pasar ..";
            return View("ErrorPage");
        }
    }
}