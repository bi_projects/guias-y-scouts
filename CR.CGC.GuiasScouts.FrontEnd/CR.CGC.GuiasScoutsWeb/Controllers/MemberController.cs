﻿using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using static System.Net.HttpStatusCode;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    [IsMenu(2, "Miembros", "icon_miembro", null, "caption_member.jpg", "Esta es la descripción para Miembros. Solamente es un texto de prueba, recordar cambiarlo.", "Member")]
    public class MemberController : ApplicationController
    {
        private static string _BaseUrl = System.Configuration.ConfigurationManager.AppSettings["WebApi"].ToString();
        private IApplicationHelper _helper = new ApplicationHelper(_BaseUrl);

        #region Records

        [IsMenu(1, "Registros", null, null, null, null, "Records")]
        [Route("Member/Records/Index")]
        public ActionResult Records()
        {
            return RedirectToAction("Members");
        }

        #region Member

        [Route("Member/Records/MembersList")]
        [IsPermission("Miembro", PermissionTypes.List, null, "MembersList")]
        public ActionResult Members()
        {
            return View("Records/Members/Index");
        }

        public ActionResult GetGridMembers(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, members, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<Member>>("Members", "Member", Search, Number, new[] { ("CurrentUser", CustomSession.CurrentSession.CurrentUser.Member.User.UserId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, members, "~/Views/Member/Records/Members/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Records/Members/_GridPartial", members);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Records/Members/_GridPartial", new List<Member>());
            }
        }

        [Route("Member/Records/NewMember")]
        [IsPermission("Miembro", PermissionTypes.New, null, "MembersCreation")]
        public ActionResult NewMember(int? MemberId)
        {
            try
            {
                LoadSelectListMember(MemberId);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Members");
            }

            //MODEL
            if (MemberId == null)
            {
                return View("Records/Members/Create", new Member());
            }
            else if (MemberId == 0)
            {
                return View("Records/Members/Create", new Member());
            }
            else
            {
                var (ResponseCode, ResponseText, member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", (int)MemberId);

                if (ResponseCode == OK)
                {
                    member.MemberPositions = member.MemberPositions.Where(x => x.Enabled).ToList();
                    member.MemberKinships = member.MemberKinships.Where(x => x.Enabled).ToList();
                    member.MemberSocialNetworks = member.MemberSocialNetworks.Where(x => x.Enabled).ToList();
                    member.MemberPhoneNumbers = member.MemberPhoneNumbers.Where(x => x.Enabled).ToList();
                    member.MemberLanguages = member.MemberLanguages.Where(x => x.Enabled).ToList();
                    member.MemberCourses = member.MemberCourses.Where(x => x.Enabled).ToList();
                    member.MemberWorkshops = member.MemberWorkshops.Where(x => x.Enabled).ToList();
                    member.MemberActivities = member.MemberActivities.Where(x => x.Enabled).ToList();
                    member.MemberNationalEvents = member.MemberNationalEvents.Where(x => x.Enabled).ToList();
                    member.MemberInternationalEvents = member.MemberInternationalEvents.Where(x => x.Enabled).ToList();
                    member.MemberOtherStudies = member.MemberOtherStudies.Where(x => x.Enabled).ToList();
                    member.MemberTutors = member.MemberTutors.Where(x => x.Enabled).ToList();

                    return View("Records/Members/Create", member);
                }
                else return View("Records/Members/Create", new Member());
            }
        }

        public ERContainer LoadSelectListMemberTutor()
        {
            try
            {
                ERContainer container = (ERContainer)_helper.CreateGetAsync<ERContainer>("Members/List/ForMembers", new Dictionary<string, object>());

                ViewBag.ListNationality = new SelectList(container.Nationalities.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                ViewBag.ListGender = new SelectList(container.Genders.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                ViewBag.ListTypes = new SelectList(container.IdentificationTypes.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                ViewBag.ProvinceList = new SelectList(container.Provinces.Where(x => x.Enabled).ToList(), "ProvinceId", "Name");
                ViewBag.CantonList = new SelectList(new List<Canton>(), "CantonId", "Name");
                ViewBag.DistrictList = new SelectList(new List<District>(), "DistrictId", "Name");

                var (MemberResponseCode, MemberResponseText, ListMember) = _helper.GetEntitiesList<List<Member>>("Members", "Member");
                if (MemberResponseCode == OK)
                {
                    ListMember.ForEach(x => x.Names = x.Names + " " + x.Surnames);
                    ListMember = ListMember.Where(x => x.Enabled).ToList();
                    ViewBag.ListMember = new SelectList(ListMember.Where(x => x.Enabled).ToList(), "MemberId", "Names");
                }
                else ViewBag.ListMember = new SelectList(new List<Member>(), "MemberId", "Names");

                return container;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LoadSelectListMember(int? MemberId)
        {
            try
            {
                ERContainer container = LoadSelectListMemberTutor();

                container.MemberStatuses.ForEach(x => x.Name = x.Name + x.Color);
                ViewBag.StatusList = new SelectList(container.MemberStatuses.Where(x => x.Enabled).ToList(), "MemberStatusId", "Name");

                container.Positions.ForEach(x => { x.Name = x.Name + " - " + x.PositionType.Name; });
                ViewBag.PositionList_Young = new SelectList(container.Positions.Where(x => x.PositionType.BaseCatalog.BaseCatalogId == CustomSession.CurrentSession.CatalogConstants.PositionTypeYoungId && x.Enabled).ToList(), "PositionId", "Name");
                ViewBag.PositionList_Old = new SelectList(container.Positions.Where(x => x.PositionType.BaseCatalog.BaseCatalogId == CustomSession.CurrentSession.CatalogConstants.PositionTypeOldId && x.Enabled).ToList(), "PositionId", "Name");

                ViewBag.ListReligion = new SelectList(container.Religions.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                ViewBag.ListEducationLevel = new SelectList(container.EducationLevels.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                ViewBag.ListSchool = new SelectList(container.Schools.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                ViewBag.ListProfession = new SelectList(container.Professions.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");

                if (MemberId != null && MemberId > 0)
                {
                    ViewBag.ListKinship = new SelectList(container.Kinships.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                    ViewBag.ListLanguage = new SelectList(container.Languages.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                    ViewBag.ListLanguageLevel = new SelectList(container.LanguageLevels.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                    ViewBag.ListCourse = new SelectList(container.Courses.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                    ViewBag.ListWorkshop = new SelectList(container.Workshops.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                    ViewBag.ListActivity = new SelectList(container.Activities.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                    ViewBag.ListNationalEvent = new SelectList(container.NationalEvents.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                    ViewBag.ListInternationalEvent = new SelectList(container.InternationalEvents.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetMemberPositionPartial(int index, string model)
        {
            var (PositionResponseCode, PositionResponseText, ListPosition) = _helper.GetEntitiesList<List<Position>>("Members", "Position");
            if (PositionResponseCode == OK)
            {
                ListPosition.ForEach(x => { x.Name = x.Name + " - " + x.PositionType.Name; });
                ViewBag.PositionList_Young = new SelectList(ListPosition.Where(x => x.PositionType.BaseCatalog.BaseCatalogId == CustomSession.CurrentSession.CatalogConstants.PositionTypeYoungId && x.Enabled).ToList(), "PositionId", "Name");
                ViewBag.PositionList_Old = new SelectList(ListPosition.Where(x => x.PositionType.BaseCatalog.BaseCatalogId == CustomSession.CurrentSession.CatalogConstants.PositionTypeOldId && x.Enabled).ToList(), "PositionId", "Name");
            }
            else
            {
                ViewBag.PositionList_Young = new SelectList(new List<Position>(), "PositionId", "Name");
                ViewBag.PositionList_Old = new SelectList(new List<Position>(), "PositionId", "Name");
            }

            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Records/Members/_MemberPositionPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberKinshipPartial(int index, string model)
        {
            var (KinshipResponseCode, KinshipResponseText, ListKinship) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.KinshipId.ToString()) });
            if (KinshipResponseCode == OK) ViewBag.ListKinship = new SelectList(ListKinship.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.ListKinship = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            var (MemberResponseCode, MemberResponseText, ListMember) = _helper.GetEntitiesList<List<Member>>("Members", "Member");
            if (MemberResponseCode == OK)
            {
                ListMember.ForEach(x => x.Names = x.Names + " " + x.Surnames);
                ListMember = ListMember.Where(x => x.Enabled).ToList();
                ViewBag.ListMember = new SelectList(ListMember.Where(x => x.Enabled).ToList(), "MemberId", "Names");
            }
            else ViewBag.ListMember = new SelectList(new List<Member>(), "MemberId", "Names");

            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Records/Members/_MemberKinshipPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberSocialNetworkPartial(int index, int type, string model)
        {
            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, new SocialNetworkViewModel() { Index = index, TypeId = type }, "~/Views/Member/Records/Members/_MemberSocialNetworkPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberPhoneNumberPartial(int index, string model)
        {
            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Records/Members/_MemberPhoneNumberPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberLanguagePartial(int index, string model)
        {
            var (LanguageResponseCode, LanguageResponseText, ListLanguage) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.LanguageId.ToString()) });
            if (LanguageResponseCode == OK) ViewBag.ListLanguage = new SelectList(ListLanguage.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.ListLanguage = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            var (LanguageLevelResponseCode, LanguageLevelResponseText, ListLanguageLevel) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.LanguageLevelId.ToString()) });
            if (LanguageLevelResponseCode == OK) ViewBag.ListLanguageLevel = new SelectList(ListLanguageLevel.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.ListLanguageLevel = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Records/Members/_MemberLanguagePartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberNationalEventPartial(int index, string model)
        {
            var (ResponseCode, ResponseText, List) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.NationalEventId.ToString()) });
            if (ResponseCode == OK) ViewBag.ListNationalEvent = new SelectList(List.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.ListNationalEvent = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Records/Members/_MemberNationalEventPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberInternationalEventPartial(int index, string model)
        {
            var (ResponseCode, ResponseText, List) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.InternationalEventId.ToString()) });
            if (ResponseCode == OK) ViewBag.ListInternationalEvent = new SelectList(List.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.ListInternationalEvent = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Records/Members/_MemberInternationalEventPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberCoursePartial(int index, string model)
        {
            var (ResponseCode, ResponseText, List) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.CourseId.ToString()) });
            if (ResponseCode == OK) ViewBag.ListCourse = new SelectList(List.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.ListCourse = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Records/Members/_MemberCoursePartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberWorkshopPartial(int index, string model)
        {
            var (ResponseCode, ResponseText, List) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.WorkshopId.ToString()) });
            if (ResponseCode == OK) ViewBag.ListWorkshop = new SelectList(List.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.ListWorkshop = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Records/Members/_MemberWorkshopPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberActivityPartial(int index, string model)
        {
            var (ResponseCode, ResponseText, List) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityId.ToString()) });
            if (ResponseCode == OK) ViewBag.ListActivity = new SelectList(List.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.ListActivity = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Records/Members/_MemberActivityPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberOtherStudyPartial(int index, string model)
        {
            ViewBag.ModelName = model;

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Records/Members/_MemberOtherStudyPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberTutorPartial(int MemberId)
        {
            var (ResponseCode, ResponseText, member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", MemberId);

            if (ResponseCode == OK)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, member, "~/Views/Member/Records/Members/_MemberTutorPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        public ActionResult GetMemberTutorDialogPartial(int MemberId = 0, bool Active = false)
        {
            LoadSelectListMemberTutor();
            ViewBag.ActiveTutor = Active;

            if (MemberId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new Member(), "~/Views/Member/Records/Members/_MemberTutorDialogPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", MemberId);

                if (ResponseCode == OK)
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, member, "~/Views/Member/Records/Members/_MemberTutorDialogPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText });
                }
            }
        }

        [Route("Member/Records/NewMemberRecognition")]
        [IsPermission("Miembro", PermissionTypes.Other, "Asignar Reconocimientos", "NewMemberRecognition")]
        public ActionResult NewMemberRecognition(int MemberId, int? MemberRecognitionId)
        {
            if (MemberId <= 0) Members();
            var (ResponseCode, ResponseText, Member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", MemberId);
            if (ResponseCode != OK) Members();

            var (RecognitionResponseCode, RecognitionResponseText, RecognitionList) = _helper.GetEntitiesList<List<Recognition>>("Members", "Recognition");
            if (RecognitionResponseCode == OK) ViewBag.ListRecognition = new SelectList(RecognitionList.Where(x => x.Enabled).ToList(), "RecognitionId", "Name");
            else ViewBag.ListRecognition = new SelectList(new List<Recognition>(), "RecognitionId", "Name");

            ViewBag.MemberRecognitionId = MemberRecognitionId ?? 0;
            return View("Records/Members/AddMemberRecognition", Member);
        }

        [Route("Member/Records/NewMemberGroup")]
        [IsPermission("Miembro", PermissionTypes.Other, "Asignar Grupo / Sección / Etapa", "NewMemberGroup")]
        public ActionResult NewMemberGroup(int MemberId, int? MemberGroupId)
        {
            if (MemberId <= 0) Members();
            var (ResponseCode, ResponseText, Member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", MemberId);
            if (ResponseCode != OK) Members();

            var (GroupResponseCode, GroupResponseText, GroupList) = _helper.GetEntitiesList<List<Group>>("Members", "Group");
            if (GroupResponseCode == OK) ViewBag.ListGroup = new SelectList(GroupList.Where(x => x.Enabled).ToList(), "GroupId", "Name");
            else ViewBag.ListGroup = new SelectList(new List<Group>(), "GroupId", "Name");

            var (SectionResponseCode, SectionResponseText, SectionList) = _helper.GetEntitiesList<List<Section>>("Members", "Section");
            if (SectionResponseCode == OK) ViewBag.ListSection = new SelectList(SectionList.Where(x => x.Enabled).ToList(), "SectionId", "Name");
            else ViewBag.ListSection = new SelectList(new List<Section>(), "SectionId", "Name");

            ViewBag.ListProgression = new SelectList(new List<Progression>(), "ProgressionId", "Name");

            ViewBag.MemberGroupId = MemberGroupId ?? 0;
            return View("Records/Members/AddMemberGroup", Member);
        }

        public ActionResult GetSelectProgressionPartial(int SectionId, bool Disabled = false)
        {
            var (ListResponseCode, ListResponseText, ListProgression) = _helper.GetEntitiesList<List<Progression>>("Members", "Progression");
            ViewBag.selectDisabled = Disabled;
            var response = Utilities.RenderRazorViewToString(ControllerContext, new SelectList(ListResponseCode == OK ? ListProgression.Where(x => x.SectionId == SectionId && x.Enabled).ToList() : new List<Progression>(), "ProgressionId", "Name"), "~/Views/Member/Records/Members/_SelectProgressionPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [Route("Member/Records/NewMemberCompetenceEvaluation")]
        [IsPermission("Miembro", PermissionTypes.Other, "Evaluación de Competencias", "NewMemberCompetenceEvaluation")]
        public ActionResult NewMemberCompetenceEvaluation(int MemberId, DateTime? EvaluationDate)
        {
            if (MemberId <= 0) Members();
            var (ResponseCode, ResponseText, Member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", MemberId);
            if (ResponseCode != OK) Members();

            var (LevelResponseCode, LevelResponseText, LevelList) = _helper.GetEntitiesList<List<CompetenceLevel>>("Config", "CompetenceLevel");
            if (LevelResponseCode == OK) ViewBag.ListLevel = new SelectList(LevelList.Where(x => x.Enabled).ToList(), "LevelId", "Level");
            else ViewBag.ListLevel = new SelectList(new List<CompetenceLevel>(), "LevelId", "Level");

            var (RoleResponseCode, RoleResponseText, RoleList) = _helper.GetEntity<Role>("Config", "Role", "RoleId", (int)Member.User.RoleId);
            if (RoleResponseCode == OK) ViewBag.ListCompetence = RoleList.RoleCompetencies;
            else ViewBag.ListCompetence = new List<RoleCompetence>();

            ViewBag.EvaluationDate = EvaluationDate ?? DateTime.Now;
            return View("Records/Members/AddMemberCompetenceEvaluation", Member);
        }

        [Route("Member/Records/GetMemberDetails")]
        [IsPermission("Miembro", PermissionTypes.View, null, "MembersDetails")]
        public ActionResult GetMemberDetails(int MemberId)
        {
            if (MemberId <= 0) return Members();

            var (ResponseCode, ResponseText, member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", (int)MemberId);

            var (RoleResponseCode, RoleResponseText, RoleList) = _helper.GetEntity<Role>("Config", "Role", "RoleId", (int)member.User.RoleId);
            if (RoleResponseCode == OK) ViewBag.ListCompetence = RoleList.RoleCompetencies;
            else ViewBag.ListCompetence = new List<RoleCompetence>();

            if (ResponseCode == OK)
            {
                member.MemberPositions = member.MemberPositions.Where(x => x.Enabled).ToList();
                member.MemberKinships = member.MemberKinships.Where(x => x.Enabled).ToList();
                member.MemberSocialNetworks = member.MemberSocialNetworks.Where(x => x.Enabled).ToList();
                member.MemberPhoneNumbers = member.MemberPhoneNumbers.Where(x => x.Enabled).ToList();
                member.MemberLanguages = member.MemberLanguages.Where(x => x.Enabled).ToList();
                member.MemberCourses = member.MemberCourses.Where(x => x.Enabled).ToList();
                member.MemberWorkshops = member.MemberWorkshops.Where(x => x.Enabled).ToList();
                member.MemberActivities = member.MemberActivities.Where(x => x.Enabled).ToList();
                member.MemberNationalEvents = member.MemberNationalEvents.Where(x => x.Enabled).ToList();
                member.MemberInternationalEvents = member.MemberInternationalEvents.Where(x => x.Enabled).ToList();
                member.MemberOtherStudies = member.MemberOtherStudies.Where(x => x.Enabled).ToList();
                member.MemberTutors = member.MemberTutors.Where(x => x.Enabled).ToList();

                member.MemberRecognitions = member.MemberRecognitions.Where(x => x.Enabled).ToList();
                member.MemberGroups = member.MemberGroups.Where(x => x.Enabled).ToList();
                member.MemberCompetenceEvaluations = member.MemberCompetenceEvaluations.Where(x => x.Enabled).ToList();

                return View("Records/Members/Details", member);
            }
            else return Members();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMember_Young([Bind(Prefix = "Young")]Member member)
        {
            if (!ModelState.IsValid) return View(member);
            return await SaveMember(member);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMember_Old([Bind(Prefix = "Old")]Member member)
        {
            if (!ModelState.IsValid) return View(member);
            return await SaveMember(member);
        }

        [HttpPost]
        public async Task<ActionResult> SaveMember(Member member)
        {
            if (member.MemberId == null || member.MemberId == 0)
            {
                member.MemberPositions.ForEach(x => x.Enabled = true);
                member.MemberKinships.ForEach(x => x.Enabled = true);
                member.MemberSocialNetworks.ForEach(x => x.Enabled = true);
                member.MemberPhoneNumbers.ForEach(x => x.Enabled = true);
                member.MemberLanguages.ForEach(x => x.Enabled = true);
                member.MemberCourses.ForEach(x => x.Enabled = true);
                member.MemberWorkshops.ForEach(x => x.Enabled = true);
                member.MemberActivities.ForEach(x => x.Enabled = true);
                member.MemberNationalEvents.ForEach(x => x.Enabled = true);
                member.MemberInternationalEvents.ForEach(x => x.Enabled = true);
                member.MemberOtherStudies.ForEach(x => x.Enabled = true);
                member.MemberTutors.ForEach(x => x.Enabled = true);
            }
            else
            {
                member.MemberPositions.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberKinships.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberSocialNetworks.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberPhoneNumbers.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberLanguages.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberCourses.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberWorkshops.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberActivities.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberNationalEvents.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberInternationalEvents.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberOtherStudies.ForEach(x => x.MemberId = (int)member.MemberId);
                member.MemberTutors.ForEach(x => x.MemberId = (int)member.MemberId);

                var (ResponseCodeUpd, ResponseTextUpd, memberUpd) = _helper.GetEntity<Member>("Members", "Member", "MemberId", (int)member.MemberId);

                if (ResponseCodeUpd == OK)
                {
                    memberUpd.MemberPositions = memberUpd.MemberPositions.Where(x => x.Enabled).ToList();
                    memberUpd.MemberKinships = memberUpd.MemberKinships.Where(x => x.Enabled).ToList();
                    memberUpd.MemberSocialNetworks = memberUpd.MemberSocialNetworks.Where(x => x.Enabled).ToList();
                    memberUpd.MemberPhoneNumbers = memberUpd.MemberPhoneNumbers.Where(x => x.Enabled).ToList();
                    memberUpd.MemberLanguages = memberUpd.MemberLanguages.Where(x => x.Enabled).ToList();
                    memberUpd.MemberCourses = memberUpd.MemberCourses.Where(x => x.Enabled).ToList();
                    memberUpd.MemberWorkshops = memberUpd.MemberWorkshops.Where(x => x.Enabled).ToList();
                    memberUpd.MemberActivities = memberUpd.MemberActivities.Where(x => x.Enabled).ToList();
                    memberUpd.MemberNationalEvents = memberUpd.MemberNationalEvents.Where(x => x.Enabled).ToList();
                    memberUpd.MemberInternationalEvents = memberUpd.MemberInternationalEvents.Where(x => x.Enabled).ToList();
                    memberUpd.MemberOtherStudies = memberUpd.MemberOtherStudies.Where(x => x.Enabled).ToList();
                    memberUpd.MemberTutors = memberUpd.MemberTutors.Where(x => x.Enabled).ToList();

                    member.MemberPositions = EditChildList<MemberPosition>(member.MemberPositions, memberUpd.MemberPositions, "MemberPositionId", new[] { "PositionId" });
                    member.MemberKinships = EditChildList<MemberKinship>(member.MemberKinships, memberUpd.MemberKinships, "MemberKinshipId", new[] { "KinId", "KinshipId" });
                    member.MemberSocialNetworks = EditChildList<MemberSocialNetwork>(member.MemberSocialNetworks, memberUpd.MemberSocialNetworks, "MemberSocialNetworkId", new[] { "TypeId", "Nickname" });
                    member.MemberPhoneNumbers = EditChildList<MemberPhoneNumber>(member.MemberPhoneNumbers, memberUpd.MemberPhoneNumbers, "MemberPhoneNumberId", new[] { "PhoneNumber" });
                    member.MemberLanguages = EditChildList<MemberLanguage>(member.MemberLanguages, memberUpd.MemberLanguages, "MemberLanguageId", new[] { "LanguageId", "LanguageReadingId", "LanguageWritingId", "LanguageConversationId" });
                    member.MemberCourses = EditChildList<MemberCourse>(member.MemberCourses, memberUpd.MemberCourses, "MemberCourseId", new[] { "CourseId", "CourseDate" });
                    member.MemberWorkshops = EditChildList<MemberWorkshop>(member.MemberWorkshops, memberUpd.MemberWorkshops, "MemberWorkshopId", new[] { "WorkshopId", "WorkshopDate" });
                    member.MemberActivities = EditChildList<MemberActivity>(member.MemberActivities, memberUpd.MemberActivities, "MemberActivityId", new[] { "ActivityId", "ActivityDate" });
                    member.MemberNationalEvents = EditChildList<MemberNationalEvent>(member.MemberNationalEvents, memberUpd.MemberNationalEvents, "MemberNationalEventId", new[] { "NationalEventId", "NationalEventDate" });
                    member.MemberInternationalEvents = EditChildList<MemberInternationalEvent>(member.MemberInternationalEvents, memberUpd.MemberInternationalEvents, "MemberInternationalEventId", new[] { "InternationalEventId", "InternationalEventDate" });
                    member.MemberOtherStudies = EditChildList<MemberOtherStudy>(member.MemberOtherStudies, memberUpd.MemberOtherStudies, "MemberOtherStudyId", new[] { "StudyName" });
                    member.MemberTutors = EditChildList<MemberTutor>(member.MemberTutors, memberUpd.MemberTutors, "MemberTutorId", new[] { "TutorId" });
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseTextUpd;
                    return Json(new { ResponseCode = ResponseCodeUpd, ResponseText = ResponseTextUpd });
                }
            }

            if (member.SendFile != null)
            {
                Entities.File toSave = Utilities.SaveFile(member.SendFile, "Member", "Records");
                member.Photo = toSave.FileUrl;
            }

            member.SendFile = null;
            member.Enabled = true;
            ERContainer container = new ERContainer()
            {
                Members = new List<Member>() { member },
                CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId,
                Model = "Member"
            };
            var (ResponseCode, ResponseText, obj) = await _helper.CreatePostGetElement<Member>("Members/Record/New", container);
            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMember_Tutor([Bind(Prefix = "Tutor")]Member member)
        {
            if (!ModelState.IsValid) return View(member);

            if (member.MemberId == null || member.MemberId == 0)
            {
                member.MemberPositions.ForEach(x => x.Enabled = true);
                member.MemberKinships.ForEach(x => x.Enabled = true);
                member.MemberSocialNetworks.ForEach(x => x.Enabled = true);
                member.MemberPhoneNumbers.ForEach(x => x.Enabled = true);
                member.MemberLanguages.ForEach(x => x.Enabled = true);
                member.MemberCourses.ForEach(x => x.Enabled = true);
                member.MemberWorkshops.ForEach(x => x.Enabled = true);
                member.MemberActivities.ForEach(x => x.Enabled = true);
                member.MemberNationalEvents.ForEach(x => x.Enabled = true);
                member.MemberInternationalEvents.ForEach(x => x.Enabled = true);
                member.MemberOtherStudies.ForEach(x => x.Enabled = true);
            }

            var (SystemStatusResponseCode, SystemStatusResponseText, ListStatus) = _helper.GetEntitiesList<List<MemberStatus>>("Config", "MemberStatus");
            if (SystemStatusResponseCode == OK)
                member.MemberStatusId = (int)ListStatus.Where(x => x.Enabled).First().MemberStatusId;
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = SystemStatusResponseText;
                return Json(new { ResponseCode = SystemStatusResponseCode, ResponseText = SystemStatusResponseText });
            }

            if (member.SendFile != null)
            {
                Entities.File toSave = Utilities.SaveFile(member.SendFile, "Member", "Records");
                member.Photo = toSave.FileUrl;
            }

            member.SendFile = null;
            member.IsTutor = true;
            member.Enabled = true;
            ERContainer container = new ERContainer()
            {
                Members = new List<Member>() { member },
                CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId,
                Model = "Member"
            };
            var (ResponseCode, ResponseText, obj) = await _helper.CreatePostGetElement<Member>("Members/Record/New", container);
            if (ResponseCode == OK)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, obj, "~/Views/Member/Records/Members/_MemberTutorPartial.cshtml");
                return Json(new { ResponseCode, ResponseText = response });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Miembro", PermissionTypes.Delete, null, "MembersElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteMember(int? MemberId, List<int> Members, string Search, int Number = 1)
        {
            if (MemberId == null && Members == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (MemberId != null)
            {
                var (ResponseCode, ResponseText, member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", (int)MemberId);
                if (ResponseCode == OK)
                {
                    member.Enabled = !member.Enabled;
                    ERContainer container = new ERContainer() { Members = new List<Member>() { member }, Model = "Member", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Record/New", container);

                    if (ResponseCode == OK)
                        return GetGridMembers(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Member>>("Members", "Member");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Members.Contains((int)x.MemberId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Members = list, Model = "Member", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridMembers(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Record/New", container);
                    if (ResponseCode == OK)
                        return GetGridMembers(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMemberRecognition([Bind(Prefix = "MemberRecognition")]MemberRecognition memberRecognition)
        {
            if (!ModelState.IsValid) return View(memberRecognition);

            memberRecognition.Enabled = true;
            ERContainer container = new ERContainer()
            {
                MemberRecognitions = new List<MemberRecognition>() { memberRecognition },
                CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId,
                Model = "MemberRecognition"
            };
            var (ResponseCode, ResponseText, obj) = await _helper.CreatePostGetElement<List<MemberRecognition>>("Members/Model/NewObject", container);
            if (ResponseCode == OK)
            {
                return Json(new { ResponseCode, ResponseText });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Miembro", PermissionTypes.Other, "Eliminar Reconocimientos", "MemberRecognitionElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteMemberRecognition(int MemberId, int MemberRecognitionId)
        {
            if (MemberRecognitionId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }
            else
            {
                var (ResponseCode, ResponseText, memberRecognition) = _helper.GetEntitiesByParams<MemberRecognition>("Members", "MemberRecognition", new[] { ("MemberRecognitionId", MemberRecognitionId.ToString()), ("MemberId", MemberId.ToString()) });
                if (ResponseCode == OK)
                {
                    memberRecognition.Enabled = !memberRecognition.Enabled;
                    ERContainer container = new ERContainer() { MemberRecognitions = new List<MemberRecognition>() { memberRecognition }, CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId, Model = "MemberRecognition" };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return Json(new { ResponseCode, ResponseText });
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMemberGroup([Bind(Prefix = "MemberGroup")]MemberGroup memberGroup)
        {
            if (!ModelState.IsValid) return View(memberGroup);

            var (MemberResponseCode, MemberResponseText, Member) = _helper.GetEntity<Member>("Members", "Member", "MemberId", memberGroup.MemberId);
            if (MemberResponseCode == OK)
            {
                if (Member.MemberGroups.Where(x => x.EndingDate == null && x.Enabled).ToList().Count > 0 && memberGroup.MemberGroupId == 0)
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Existen Etapas sin completar.";
                    return Json(new { ResponseCode = 500, ResponseText = "Existen Etapas sin completar." });
                }
                else
                {
                    foreach (var item in Member.MemberGroups.Where(x => x.Enabled && x.MemberGroupId != memberGroup.MemberGroupId).ToList())
                    {
                        if (memberGroup.EndingDate == null)
                        {
                            if (!(DateTime.Compare(item.StartDate, memberGroup.StartDate) != 0 || DateTime.Compare((DateTime)item.EndingDate, memberGroup.StartDate) != 0))
                            {
                                Response.StatusCode = 400;
                                Response.StatusDescription = "Existen errores con las fechas de anteriores etapas, secciones y grupos.";
                                return Json(new { ResponseCode = 500, ResponseText = "Existen errores con las fechas de anteriores etapas, secciones y grupos." });
                            }
                        }
                        else
                        {
                            if (!((DateTime.Compare(item.StartDate, memberGroup.StartDate) > 0 && DateTime.Compare(item.StartDate, (DateTime)memberGroup.EndingDate) > 0) || (DateTime.Compare((DateTime)item.EndingDate, memberGroup.StartDate) < 0 && DateTime.Compare((DateTime)item.EndingDate, (DateTime)memberGroup.EndingDate) < 0)))
                            {
                                Response.StatusCode = 400;
                                Response.StatusDescription = "Existen errores con las fechas de anteriores etapas, secciones y grupos.";
                                return Json(new { ResponseCode = 500, ResponseText = "Existen errores con las fechas de anteriores etapas, secciones y grupos." });
                            }
                        }
                    }
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = MemberResponseText;
                return Json(new { ResponseCode = MemberResponseCode, ResponseText = MemberResponseText });
            }

            memberGroup.Enabled = true;
            ERContainer container = new ERContainer()
            {
                MemberGroups = new List<MemberGroup>() { memberGroup },
                CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId,
                Model = "MemberGroup"
            };
            var (ResponseCode, ResponseText, obj) = await _helper.CreatePostGetElement<List<MemberGroup>>("Members/Model/NewObject", container);
            if (ResponseCode == OK)
            {
                return Json(new { ResponseCode, ResponseText });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Miembro", PermissionTypes.Other, "Eliminar Grupos Asociados", "MemberRecognitionElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteMemberGroup(int MemberId, int MemberGroupId)
        {
            if (MemberGroupId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }
            else
            {
                var (ResponseCode, ResponseText, memberGroup) = _helper.GetEntitiesByParams<MemberGroup>("Members", "MemberGroup", new[] { ("MemberGroupId", MemberGroupId.ToString()), ("MemberId", MemberId.ToString()) });
                if (ResponseCode == OK)
                {
                    memberGroup.Enabled = !memberGroup.Enabled;
                    ERContainer container = new ERContainer() { MemberGroups = new List<MemberGroup>() { memberGroup }, CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId, Model = "MemberGroup" };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return Json(new { ResponseCode, ResponseText });
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMemberCompetenceEvaluation(List<MemberCompetenceEvaluation> MemberCompetenceEvaluations)
        {
            if (!ModelState.IsValid) return View(MemberCompetenceEvaluations);

            MemberCompetenceEvaluations.ForEach(x => x.Enabled = true);
            ERContainer container = new ERContainer()
            {
                MemberCompetenceEvaluations = MemberCompetenceEvaluations,
                CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId,
                Model = "MemberCompetenceEvaluation"
            };
            var (ResponseCode, ResponseText, obj) = await _helper.CreatePostGetElement<List<MemberCompetenceEvaluation>>("Members/Model/NewObject", container);
            if (ResponseCode == OK)
            {
                return Json(new { ResponseCode, ResponseText });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Miembro", PermissionTypes.Other, "Eliminar Evaluaciones", "MemberCompetenceEvaluationElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteMemberCompetenceEvaluation(int MemberId, DateTime EvaluationDate)
        {
            if (EvaluationDate == DateTime.MinValue)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }
            else
            {
                var (ResponseCode, ResponseText, memberCompetenceEvaluations) = _helper.GetEntitiesByParams<List<MemberCompetenceEvaluation>>("Members", "MemberCompetenceEvaluation", new[] { ("EvaluationDate", EvaluationDate.ToString("yyyy-MM-dd")), ("MemberId", MemberId.ToString()) });
                if (ResponseCode == OK)
                {
                    memberCompetenceEvaluations.ForEach(x => x.Enabled = false);
                    ERContainer container = new ERContainer() { MemberCompetenceEvaluations = memberCompetenceEvaluations, CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId, Model = "MemberCompetenceEvaluation" };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return Json(new { ResponseCode, ResponseText });
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
        }

        #endregion

        #endregion

        #region Catalogs

        [IsMenu(2, "Catálogos", null, null, null, null, "Catalogs")]
        [Route("Member/Catalogs/Index")]
        public ActionResult CatalogsIndex()
        {
            return RedirectToAction("Groups");
        }

        #region Group option 1

        [IsMenu(1, "Grupos", null, "Catálogos", null, null, "GroupsList")]
        [Route("Member/Catalogs/GroupsList")]
        [IsPermission("Grupo", PermissionTypes.List, null, "GroupsList")]
        public ActionResult Groups()
        {
            return View("Catalogs/Groups/Index");
        }

        public ActionResult GetGridGroups(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, groups, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Group>>("Members", "Group", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, groups, "~/Views/Member/Catalogs/Groups/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Groups/_GridPartial", groups);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Groups/_GridPartial", new List<Group>());
            }
        }

        [Route("Member/Catalogs/NewGroup")]
        [IsPermission("Grupo", PermissionTypes.New, null, "GroupsCreation")]
        public ActionResult NewGroup(int? GroupId)
        {
            var (ListResponseCode, ListResponseText, ListStatus) = _helper.GetEntitiesList<List<SystemStatus>>("Config", "SystemStatus");
            if (ListResponseCode == OK)
            {
                foreach (var item in ListStatus) item.Name = item.Name + item.Color;
                ViewBag.StatusList = new SelectList(ListStatus.Where(x => x.Enabled).ToList(), "SystemStatusId", "Name");
            }
            else ViewBag.StatusList = new SelectList(new List<SystemStatus>(), "SystemStatusId", "Name");

            List<BaseCatalog> ListCatalog;
            (ListResponseCode, ListResponseText, ListCatalog) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.LocalId.ToString()) });
            if (ListResponseCode == OK) ViewBag.LocalList = new SelectList(ListCatalog.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.LocalList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            (ListResponseCode, ListResponseText, ListCatalog) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.SectorId.ToString()) });
            if (ListResponseCode == OK) ViewBag.SectorList = new SelectList(ListCatalog.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.SectorList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            List<Member> ListMember = new List<Member>();
            if (GroupId != null && GroupId != 0)
            {
                List<MemberGroup> result = new List<MemberGroup>();
                (ListResponseCode, ListResponseText, result) = _helper.GetEntitiesList<List<MemberGroup>>("Members", "MemberGroup");
                if (ListResponseCode == OK)
                {
                    List<int> associated = result.Where(x => x.GroupId == GroupId && x.Enabled).GroupBy(x => x.MemberId).Select(x => x.Key).ToList();
                    if (associated.Count > 0)
                    {
                        (ListResponseCode, ListResponseText, ListMember) = _helper.GetEntitiesList<List<Member>>("Members", "Member");
                        if (ListResponseCode == OK) ViewBag.MemberList = new SelectList(ListMember.Where(x => x.Enabled && associated.Contains((int)x.MemberId)).ToList(), "MemberId", "Names");
                        else ViewBag.MemberList = new SelectList(new List<Member>(), "MemberId", "Names");
                    }
                    else ViewBag.MemberList = new SelectList(new List<Member>(), "MemberId", "Names");
                }
                else ViewBag.MemberList = new SelectList(new List<Member>(), "MemberId", "Names");
            }
            else ViewBag.MemberList = new SelectList(new List<Member>(), "MemberId", "Names");

            List<Province> ListProvince;
            (ListResponseCode, ListResponseText, ListProvince) = _helper.GetEntitiesList<List<Province>>("Config", "Province");
            if (ListResponseCode == OK) ViewBag.ProvinceList = new SelectList(ListProvince.Where(x => x.Enabled).ToList(), "ProvinceId", "Name");
            else ViewBag.ProvinceList = new SelectList(new List<Province>(), "ProvinceId", "Name");

            ViewBag.CantonList = new SelectList(new List<Canton>(), "CantonId", "Name");
            ViewBag.DistrictList = new SelectList(new List<District>(), "DistrictId", "Name");

            //MODEL
            if (GroupId == null)
            {
                return View("Catalogs/Groups/Create", new Group());
            }
            else if (GroupId == 0)
            {
                return View("Catalogs/Groups/Create", new Group());
            }
            else
            {
                var (ResponseCode, ResponseText, group) = _helper.GetEntity<Group>("Members", "Group", "GroupId", (int)GroupId);

                if (ResponseCode == OK)
                {
                    group.GroupColors = group.GroupColors.Where(x => x.Enabled).ToList();
                    group.GroupEmails = group.GroupEmails.Where(x => x.Enabled).ToList();
                    group.GroupSocialNetworks = group.GroupSocialNetworks.Where(x => x.Enabled).ToList();
                    return View("Catalogs/Groups/Create", group);
                }
                else return View("Catalogs/Groups/Create", new Group());
            }
        }

        public ActionResult GetGroupMailPartial(int index)
        {
            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Catalogs/Groups/_GroupMailPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGroupColorPartial(int index)
        {
            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Member/Catalogs/Groups/_GroupColorPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGroupSocialNetworkPartial(int index, int type)
        {
            var response = Utilities.RenderRazorViewToString(ControllerContext, new SocialNetworkViewModel() { Index = index, TypeId = type }, "~/Views/Member/Catalogs/Groups/_GroupSocialNetworkPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveGroup(Group group)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Datos no válidos:" + GetErrorMessage(ModelState);
                return Json(new { ResponseCode = 500, ResponseText = "Datos no válidos:" + Environment.NewLine + GetErrorMessage(ModelState) });
            }

            if (group.GroupId == null || group.GroupId == 0)
            {
                group.GroupColors.ForEach(x => x.Enabled = true);
                group.GroupEmails.ForEach(x => x.Enabled = true);
                group.GroupSocialNetworks.ForEach(x => x.Enabled = true);
            }
            else
            {
                group.GroupColors.ForEach(x => x.GroupId = (int)group.GroupId);
                group.GroupEmails.ForEach(x => x.GroupId = (int)group.GroupId);
                group.GroupSocialNetworks.ForEach(x => x.GroupId = (int)group.GroupId);

                var (ResponseCodeUpd, ResponseTextUpd, groupUpd) = _helper.GetEntity<Group>("Members", "Group", "GroupId", (int)group.GroupId);

                if (ResponseCodeUpd == OK)
                {
                    groupUpd.GroupColors = groupUpd.GroupColors.Where(x => x.Enabled).ToList();
                    groupUpd.GroupEmails = groupUpd.GroupEmails.Where(x => x.Enabled).ToList();
                    groupUpd.GroupSocialNetworks = groupUpd.GroupSocialNetworks.Where(x => x.Enabled).ToList();

                    group.GroupColors = EditChildList<GroupColor>(group.GroupColors, groupUpd.GroupColors, "GroupColorId", new[] { "Color" });
                    group.GroupEmails = EditChildList<GroupEmail>(group.GroupEmails, groupUpd.GroupEmails, "GroupEmailId", new[] { "Email" });
                    group.GroupSocialNetworks = EditChildList<GroupSocialNetwork>(group.GroupSocialNetworks, groupUpd.GroupSocialNetworks, "GroupSocialNetworkId", new[] { "Type", "Nickname" });
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseTextUpd;
                    return Json(new { ResponseCode = ResponseCodeUpd, ResponseText = ResponseTextUpd });
                }
            }

            if (group.SendFile != null)
            {
                Entities.File toSave = Utilities.SaveFile(group.SendFile, "Member", "Catalogs");
                group.HeadkerchiefImage = toSave.FileUrl;
            }

            group.SendFile = null;
            group.Enabled = true;
            ERContainer container = new ERContainer() { Groups = new List<Group>() { group }, Model = "Group", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText, obj) = await _helper.CreatePostGetElement<List<Group>>("Members/Model/NewObject", container);

            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Grupo", PermissionTypes.Delete, null, "GroupsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteGroup(int? GroupId, List<int> Groups, string Search, int Number = 1)
        {
            if (GroupId == null && Groups == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (GroupId != null)
            {
                var (ResponseCode, ResponseText, group) = _helper.GetEntity<Group>("Members", "Group", "GroupId", (int)GroupId);
                if (ResponseCode == OK)
                {
                    group.Enabled = !group.Enabled;
                    ERContainer container = new ERContainer() { Groups = new List<Group>() { group }, Model = "Group", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridGroups(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Group>>("Members", "Group");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Groups.Contains((int)x.GroupId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Groups = list, Model = "Group", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridGroups(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridGroups(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Section option 2

        [IsMenu(2, "Secciones", null, "Catálogos", null, null, "SectionsList")]
        [Route("Member/Catalogs/SectionsList")]
        [IsPermission("Sección", PermissionTypes.List, null, "SectionsList")]
        public ActionResult Sections()
        {
            return View("Catalogs/Sections/Index");
        }

        public ActionResult GetGridSections(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, sections, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Section>>("Members", "Section", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, sections, "~/Views/Member/Catalogs/Sections/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Sections/_GridPartial", sections);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Sections/_GridPartial", new List<Section>());
            }
        }

        [Route("Member/Catalogs/NewSection")]
        [IsPermission("Sección", PermissionTypes.New, null, "SectionsCreation")]
        public ActionResult NewSection(int? SectionId)
        {
            var (ListResponseCode, ListResponseText, ListSection) = _helper.GetEntitiesList<List<SectionType>>("Config", "SectionType");
            if (ListResponseCode == OK)
                ViewBag.TypeList = new SelectList(ListSection.Where(x => x.Enabled).ToList(), "SectionTypeId", "Name");
            else
                ViewBag.TypeList = new SelectList(new List<SectionType>(), "SectionTypeId", "Name");
            List<SystemStatus> ListStatus;
            (ListResponseCode, ListResponseText, ListStatus) = _helper.GetEntitiesList<List<SystemStatus>>("Config", "SystemStatus");
            if (ListResponseCode == OK)
            {
                foreach (var item in ListStatus) item.Name = item.Name + item.Color;
                ViewBag.StatusList = new SelectList(ListStatus.Where(x => x.Enabled).ToList(), "SystemStatusId", "Name");
            }
            else
                ViewBag.StatusList = new SelectList(new List<SystemStatus>(), "SystemStatusId", "Name");


            //MODEL
            if (SectionId == null)
            {
                return View("Catalogs/Sections/Create", new Section());
            }
            else if (SectionId == 0)
            {
                return View("Catalogs/Sections/Create", new Section());
            }
            else
            {
                var (ResponseCode, ResponseText, section) = _helper.GetEntity<Section>("Members", "Section", "SectionId", (int)SectionId);

                if (ResponseCode == OK)
                    return View("Catalogs/Sections/Create", section);
                else
                    return View("Catalogs/Sections/Create", new Section());
            }
        }

        public ActionResult GetSectionTypePartial(int? SectionTypeId)
        {
            if (SectionTypeId == null)
            {
                return PartialView("Catalogs/Sections/_SectionTypePartial", new SectionType());
            }
            else if (SectionTypeId == 0)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, new SectionType(), "~/Views/Member/Catalogs/Sections/_SectionTypePartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Sections/_SectionTypePartial", new SectionType());
            }
            else
            {
                var (ResponseCode, ResponseText, sectionType) = _helper.GetEntity<SectionType>("Config", "SectionType", "SectionTypeId", (int)SectionTypeId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, sectionType, "~/Views/Member/Catalogs/Sections/_SectionTypePartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Sections/_SectionTypePartial", sectionType);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Sections/_SectionTypePartial", new SectionType());
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveSection(Section section)
        {
            if (!ModelState.IsValid) return View(section);

            if (section.SendFile != null)
            {
                Entities.File toSave = Utilities.SaveFile(section.SendFile, "Member", "Catalogs");
                section.FileLocation = toSave.FileUrl;
            }

            section.SendFile = null;
            section.Enabled = true;
            ERContainer container = new ERContainer() { Sections = new List<Section>() { section }, Model = "Section", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText, sections) = await _helper.CreatePostGetElement<List<Section>>("Members/Model/NewObject", container);

            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Sección", PermissionTypes.Delete, null, "SectionsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteSection(int? SectionId, List<int> Sections, string Search, int Number = 1)
        {
            if (SectionId == null && Sections == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (SectionId != null)
            {
                var (ResponseCode, ResponseText, section) = _helper.GetEntity<Section>("Members", "Section", "SectionId", (int)SectionId);
                if (ResponseCode == OK)
                {
                    section.Enabled = !section.Enabled;
                    ERContainer container = new ERContainer() { Sections = new List<Section>() { section }, Model = "Section", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridSections(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Section>>("Members", "Section");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Sections.Contains((int)x.SectionId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Sections = list, Model = "Section", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridSections(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridSections(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Progression option 3

        [IsMenu(3, "Progresión y etapas", null, "Catálogos", null, null, "ProgressionsList")]
        [Route("Member/Catalogs/ProgressionsList")]
        [IsPermission("Progresión", PermissionTypes.List, null, "ProgressionsList")]
        public ActionResult Progressions()
        {
            return View("Catalogs/Progressions/Index");
        }

        public ActionResult GetGridProgressions(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, progressions, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Progression>>("Members", "Progression", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, progressions, "~/Views/Member/Catalogs/Progressions/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Progressions/_GridPartial", progressions);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Progressions/_GridPartial", new List<Progression>());
            }
        }

        [Route("Member/Catalogs/NewProgression")]
        [IsPermission("Progresión", PermissionTypes.New, null, "ProgressionsCreation")]
        public ActionResult NewProgression(int? ProgressionId)
        {
            var (ListResponseCode, ListResponseText, ListStatus) = _helper.GetEntitiesList<List<SystemStatus>>("Config", "SystemStatus");
            if (ListResponseCode == OK)
            {
                foreach (var item in ListStatus) item.Name = item.Name + item.Color;
                ViewBag.StatusList = new SelectList(ListStatus, "SystemStatusId", "Name");
            }
            else ViewBag.StatusList = new SelectList(new List<SystemStatus>(), "SystemStatusId", "Name");
            List<Section> ListSection;
            (ListResponseCode, ListResponseText, ListSection) = _helper.GetEntitiesList<List<Section>>("Config", "Section");
            if (ListResponseCode == OK) ViewBag.SectionList = new SelectList(ListSection.Where(x => x.Enabled).ToList(), "SectionId", "Name");
            else ViewBag.SectionList = new SelectList(new List<Section>(), "SectionId", "Name");


            //MODEL
            if (ProgressionId == null)
            {
                return View("Catalogs/Progressions/Create", new Progression());
            }
            else if (ProgressionId == 0)
            {
                return View("Catalogs/Progressions/Create", new Progression());
            }
            else
            {
                var (ResponseCode, ResponseText, progression) = _helper.GetEntity<Progression>("Members", "Progression", "ProgressionId", (int)ProgressionId);

                if (ResponseCode == OK)
                    return View("Catalogs/Progressions/Create", progression);
                else
                    return View("Catalogs/Progressions/Create", new Progression());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveProgression(Progression progression)
        {
            if (!ModelState.IsValid) return View(progression);

            if (progression.SendFile != null)
            {
                Entities.File toSave = Utilities.SaveFile(progression.SendFile, "Member", "Catalogs");
                progression.FileLocation = toSave.FileUrl;
            }

            progression.SendFile = null;
            progression.Enabled = true;
            ERContainer container = new ERContainer() { Progressions = new List<Progression>() { progression }, Model = "Progression", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Progresión", PermissionTypes.Delete, null, "ProgressionsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteProgression(int? ProgressionId, List<int> Progressions, string Search, int Number = 1)
        {
            if (ProgressionId == null && Progressions == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (ProgressionId != null)
            {
                var (ResponseCode, ResponseText, progression) = _helper.GetEntity<Progression>("Members", "Progression", "ProgressionId", (int)ProgressionId);
                if (ResponseCode == OK)
                {
                    progression.Enabled = !progression.Enabled;
                    ERContainer container = new ERContainer() { Progressions = new List<Progression>() { progression }, Model = "Progression", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridProgressions(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Progression>>("Members", "Progression");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Progressions.Contains((int)x.ProgressionId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Progressions = list, Model = "Progression", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridProgressions(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridProgressions(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Recognition option 4

        [IsMenu(4, "Reconocimientos", null, "Catálogos", null, null, "RecognitionsList")]
        [Route("Member/Catalogs/RecognitionsList")]
        [IsPermission("Reconocimiento", PermissionTypes.List, null, "RecognitionsList")]
        public ActionResult Recognitions()
        {
            return View("Catalogs/Recognitions/Index");
        }

        public ActionResult GetGridRecognitions(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, recognitions, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Recognition>>("Members", "Recognition", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, recognitions, "~/Views/Member/Catalogs/Recognitions/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Recognitions/_GridPartial", recognitions);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Recognitions/_GridPartial", new List<Recognition>());
            }
        }

        [Route("Member/Catalogs/NewRecognition")]
        [IsPermission("Reconocimiento", PermissionTypes.New, null, "RecognitionsCreation")]
        public ActionResult NewRecognition(int? RecognitionId)
        {
            var (ListResponseCode, ListResponseText, ListStatus) = _helper.GetEntitiesList<List<SystemStatus>>("Config", "SystemStatus");
            if (ListResponseCode == OK)
            {
                foreach (var item in ListStatus) item.Name = item.Name + item.Color;
                ViewBag.StatusList = new SelectList(ListStatus, "SystemStatusId", "Name");
            }
            else ViewBag.StatusList = new SelectList(new List<SystemStatus>(), "SystemStatusId", "Name");

            //MODEL
            if (RecognitionId == null)
            {
                return View("Catalogs/Recognitions/Create", new Recognition());
            }
            else if (RecognitionId == 0)
            {
                return View("Catalogs/Recognitions/Create", new Recognition());
            }
            else
            {
                var (ResponseCode, ResponseText, recognition) = _helper.GetEntity<Recognition>("Members", "Recognition", "RecognitionId", (int)RecognitionId);

                if (ResponseCode == OK)
                    return View("Catalogs/Recognitions/Create", recognition);
                else
                    return View("Catalogs/Recognitions/Create", new Recognition());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveRecognition(Recognition recognition)
        {
            if (!ModelState.IsValid) return View(recognition);

            if (recognition.SendFile != null)
            {
                Entities.File toSave = Utilities.SaveFile(recognition.SendFile, "Member", "Catalogs");
                recognition.FileLocation = toSave.FileUrl;
            }

            recognition.SendFile = null;
            recognition.Enabled = true;
            ERContainer container = new ERContainer() { Recognitions = new List<Recognition>() { recognition }, Model = "Recognition", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Reconocimiento", PermissionTypes.Delete, null, "RecognitionsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteRecognition(int? RecognitionId, List<int> Recognitions, string Search, int Number = 1)
        {
            if (RecognitionId == null && Recognitions == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (RecognitionId != null)
            {
                var (ResponseCode, ResponseText, recognition) = _helper.GetEntity<Recognition>("Members", "Recognition", "RecognitionId", (int)RecognitionId);
                if (ResponseCode == OK)
                {
                    recognition.Enabled = !recognition.Enabled;
                    ERContainer container = new ERContainer() { Recognitions = new List<Recognition>() { recognition }, Model = "Recognition", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridRecognitions(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Recognition>>("Members", "Recognition");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Recognitions.Contains((int)x.RecognitionId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Recognitions = list, Model = "Recognition", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridRecognitions(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridRecognitions(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Position option 5

        [IsMenu(5, "Cargos", null, "Catálogos", null, null, "PositionsList")]
        [Route("Member/Catalogs/PositionsList")]
        [IsPermission("Cargo", PermissionTypes.List, null, "PositionsList")]
        public ActionResult Positions()
        {
            return View("Catalogs/Positions/Index");
        }

        public ActionResult GetGridPositions(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, positions, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Position>>("Members", "Position", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, positions, "~/Views/Member/Catalogs/Positions/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Positions/_GridPartial", positions);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Positions/_GridPartial", new List<Position>());
            }
        }

        [Route("Member/Catalogs/NewPosition")]
        [IsPermission("Cargo", PermissionTypes.New, null, "PositionsCreation")]
        public ActionResult NewPosition(int? PositionId)
        {
            var (ListResponseCode, ListResponseText, ListType) = _helper.GetEntitiesList<List<PositionType>>("Config", "PositionType");
            if (ListResponseCode == OK)
            {
                ViewBag.TypeList_Young = new SelectList(ListType.Where(x => x.BaseCatalog.BaseCatalogId == CustomSession.CurrentSession.CatalogConstants.PositionTypeYoungId && x.Enabled).ToList(), "PositionTypeId", "Name");
                ViewBag.TypeList_Old = new SelectList(ListType.Where(x => x.BaseCatalog.BaseCatalogId == CustomSession.CurrentSession.CatalogConstants.PositionTypeOldId && x.Enabled).ToList(), "PositionTypeId", "Name");
            }
            else
            {
                ViewBag.TypeList_Young = new SelectList(new List<PositionType>(), "PositionTypeId", "Name");
                ViewBag.TypeList_Old = new SelectList(new List<PositionType>(), "PositionTypeId", "Name");
            }

            //MODEL
            if (PositionId == null)
                return View("Catalogs/Positions/Create", new Position());
            else if (PositionId == 0)
                return View("Catalogs/Positions/Create", new Position());
            else
            {
                var (ResponseCode, ResponseText, position) = _helper.GetEntity<Position>("Members", "Position", "PositionId", (int)PositionId);

                if (ResponseCode == OK)
                    return View("Catalogs/Positions/Create", position);
                else
                    return View("Catalogs/Positions/Create", new Position());
            }
        }

        public ActionResult GetPositionTypePartial(int? PositionTypeId, int Type)
        {
            ViewBag.Type = Type;
            var (ResponseCode, ResponseText, ListGender) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", "2") });
            if (ResponseCode == OK) ViewBag.GenderList = new SelectList(ListGender.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.GenderList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            if (PositionTypeId == null)
            {
                return PartialView("Catalogs/Positions/_PositionTypePartial", new PositionType());
            }
            else if (PositionTypeId == 0)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, new PositionType(), "~/Views/Member/Catalogs/Positions/_PositionTypePartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Positions/_PositionTypePartial", new PositionType());
            }
            else
            {
                PositionType positionType;
                (ResponseCode, ResponseText, positionType) = _helper.GetEntity<PositionType>("Config", "PositionType", "PositionTypeId", (int)PositionTypeId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, positionType, "~/Views/Member/Catalogs/Positions/_PositionTypePartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Positions/_PositionTypePartial", positionType);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400; Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Positions/_PositionTypePartial", new PositionType());
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SavePosition_Young([Bind(Prefix = "Young")]Position position)
        {
            if (!ModelState.IsValid) return View(position);
            return await SavePosition(position);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SavePosition_Old([Bind(Prefix = "Old")]Position position)
        {
            if (!ModelState.IsValid) return View(position);
            return await SavePosition(position);
        }

        private async Task<JsonResult> SavePosition(Position position)
        {
            position.Enabled = true;
            ERContainer container = new ERContainer() { Positions = new List<Position>() { position }, Model = "Position", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Cargo", PermissionTypes.Delete, null, "PositionsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeletePosition(int? PositionId, List<int> Positions, string Search, int Number = 1)
        {
            if (PositionId == null && Positions == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (PositionId != null)
            {
                var (ResponseCode, ResponseText, position) = _helper.GetEntity<Position>("Members", "Position", "PositionId", (int)PositionId);
                if (ResponseCode == OK)
                {
                    position.Enabled = !position.Enabled;
                    ERContainer container = new ERContainer() { Positions = new List<Position>() { position }, Model = "Position", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridPositions(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Position>>("Members", "Position");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Positions.Contains((int)x.PositionId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Positions = list, Model = "Position", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridPositions(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridPositions(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #endregion

        public ActionResult GetSelectCantonPartial(int ProvinceId, string Model = null, string ElemId = null, bool InModal = false, bool Disabled = false)
        {
            try
            {
                var (ListResponseCode, ListResponseText, ListCanton) = _helper.GetEntitiesByParams<List<Canton>>("Config", "Canton", new[] { ("ProvinceId", ProvinceId.ToString()) });
                var response = Utilities.RenderRazorViewToString(ControllerContext, new SelectLocationViewModel()
                {
                    SelectList = (ListResponseCode == OK ? new SelectList(ListCanton, "CantonId", "Name") : new SelectList(new List<Canton>(), "CantonId", "Name")),
                    Model = Model,
                    ElemId = (ElemId ?? "CantonId"),
                    InModal = InModal,
                    Disabled = Disabled
                }, "~/Views/Shared/_SelectCantonPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ex.Message;
                return Json(new { ResponseCode = 400, ResponseText = ex.Message });
            }
        }

        public ActionResult GetSelectDistrictPartial(int CantonId, string Model = null, string ElemId = null, bool InModal = false, bool Disabled = false)
        {
            try
            {
                var (ListResponseCode, ListResponseText, ListDistrict) = _helper.GetEntitiesByParams<List<District>>("Config", "District", new[] { ("CantonId", CantonId.ToString()) });
                ListDistrict.ForEach(x => { x.Name = x.Name + "+" + (x.PostalCode ?? "00000"); });
                var response = Utilities.RenderRazorViewToString(ControllerContext, new SelectLocationViewModel()
                {
                    SelectList = (ListResponseCode == OK ? new SelectList(ListDistrict, "DistrictId", "Name") : new SelectList(new List<District>(), "DistrictId", "Name")),
                    Model = Model,
                    ElemId = (ElemId ?? "DistrictId"),
                    InModal = InModal,
                    Disabled = Disabled
                }, "~/Views/Shared/_SelectDistrictPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ex.Message;
                return Json(new { ResponseCode = 400, ResponseText = ex.Message });
            }
        }

        public ActionResult GetSearchMembers(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, members, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Member>>("Members", "Member", Search, Number);
            if (ResponseCode == OK)
            {
                return Json(new { response = members }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}