﻿using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using static System.Net.HttpStatusCode;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    [IsMenu(4, "Solicitudes", "icon_solicitudes", null, "caption_request.jpg", "Esta es la descripción para Solicitudes. Solamente es un texto de prueba, recordar cambiarlo.", "CRM")]
    public class CRMController : ApplicationController
    {
        private static string _BaseUrl = System.Configuration.ConfigurationManager.AppSettings["WebApi"].ToString();
        private IApplicationHelper _helper = new ApplicationHelper(_BaseUrl);

        #region Records

        [IsMenu(1, "Registros", null, null, null, null, "Records")]
        [Route("CRM/Records/Index")]
        public ActionResult RecordsIndex()
        {
            return RedirectToAction("Requests");
        }

        #region Requests

        [Route("CRM/Records/RequestsList")]
        [IsPermission("Solicitud", PermissionTypes.List, null, "RequestsList")]
        public ActionResult Requests()
        {
            return View("Records/Requests/Index");
        }

        public ActionResult GetGridRequests(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, requests, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<Request>>("Requests", "Request", Search, Number, new[] { ("CurrentUser", CustomSession.CurrentSession.CurrentUser.Member.User.UserId.ToString()) });
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                requests = requests.Where(x => x.RequestingUser == CustomSession.CurrentSession.CurrentUser.Member.User.UserId).ToList();
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, requests, "~/Views/CRM/Records/Requests/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Records/Requests/_GridPartial", requests);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Records/Requests/_GridPartial", new List<Request>());
            }
        }

        [Route("CRM/Records/NewRequest")]
        [IsPermission("Solicitud", PermissionTypes.New, null, "RequestsCreation")]
        public ActionResult NewRequest(int? RequestId)
        {
            var (ListResponseCode, ListResponseText, ListPriority) = _helper.GetEntitiesList<List<Priority>>("Requests", "Priority");
            if (ListResponseCode == OK) ViewBag.PrioritiesList = new SelectList(ListPriority.Where(x => x.Enabled).ToList(), "PriorityId", "Name");
            else ViewBag.PrioritiesList = new SelectList(new List<Priority>(), "PriorityId", "Name");

            List<BaseCatalog> ListRequestType;
            (ListResponseCode, ListResponseText, ListRequestType) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.RequestTypeId.ToString()) });
            if (ListResponseCode == OK) ViewBag.RequestTypesList = new SelectList(ListRequestType.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.RequestTypesList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            if (RequestId == null || RequestId == 0)
            {
                return View("Records/Requests/Create", new Request());
            }
            else
            {
                var (ResponseCode, ResponseText, topic) = _helper.GetEntity<Request>("Requests", "Request", "RequestId", (int)RequestId);
                if (ResponseCode == OK)
                {
                    topic.RequestFiles = topic.RequestFiles.Where(x => x.Enabled).ToList();
                    topic.Description = topic.Description.Replace("\\n", Environment.NewLine);
                    return View("Records/Requests/Create", topic);
                }
                else
                    return View("Records/Requests/Create", new Request());
            }

        }

        public ActionResult GetRequestTopicPartial(int TopicTypeId)
        {
            var (ListResponseCode, ListResponseText, List) = _helper.GetEntitiesList<List<Topic>>("Requests", "Topic");

            var response = Utilities.RenderRazorViewToString(ControllerContext, new SelectList(List.Where(x => x.Enabled && x.TypeId == TopicTypeId).ToList(), "TopicId", "Name"), "~/Views/CRM/Records/Requests/_RequestTopicPartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [Route("CRM/Records/RequestDetails")]
        [IsPermission("Solicitud", PermissionTypes.View, null, "ViewRequests")]
        public ActionResult RequestDetails(int RequestId = 0)
        {
            if (RequestId <= 0) return RedirectToAction("Requests");
            else
            {
                var (ResponseCode, ResponseText, request) = _helper.GetEntity<Request>("Requests", "Request", "RequestId", RequestId);
                if (ResponseCode == OK)
                {
                    request.RequestFiles = request.RequestFiles.Where(x => x.Enabled).ToList();
                    return View("Records/Requests/Details", request);
                }
                else return RedirectToAction("Requests");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveRequest(Request request)
        {
            //if (!ModelState.IsValid) return View(request);
            request.SendFiles = request.SendFiles.Where(x => x != null).ToList();

            if (request.RequestId == null || request.RequestId <= 0)
            {
                request.CreationDate = DateTime.Now.Date;
                request.RequestingUser = (int)CustomSession.CurrentSession.CurrentUser.Member.User.UserId;
                request.RequestStatusId = 1;
                request.Enabled = true;

                if (request.SendFiles.Count > 0)
                {
                    foreach (var item in request.SendFiles)
                    {
                        Entities.File toSave = Utilities.SaveFile(item, "CRM", "Records");

                        request.RequestFiles.Add(new RequestFile()
                        {
                            RequestFileId = 0,
                            RequestId = 0,
                            Name = toSave.FileName,
                            FileLocation = toSave.FileUrl,
                            Enabled = true,
                        });
                    }
                }
                request.SendFiles = null;
            }
            else
            {
                var (ResponseCodeGet, ResponseTextGet, actualReq) = _helper.GetEntity<Request>("Requests", "Request", "RequestId", (int)request.RequestId);
                List<int> exist = request.RequestFiles.Select(x => (int)x.RequestFileId).ToList();

                actualReq.RequestFiles.ForEach(x => { if (!exist.Contains((int)x.RequestFileId)) x.Enabled = false; });

                actualReq.Name = request.Name;
                actualReq.Description = request.Description;
                actualReq.PriorityId = request.PriorityId;
                actualReq.TopicId = request.TopicId;

                if (request.SendFiles.Count > 0)
                {
                    foreach (var item in request.SendFiles)
                    {
                        Entities.File toSave = Utilities.SaveFile(item, "CRM", "Records");

                        actualReq.RequestFiles.Add(new RequestFile()
                        {
                            RequestFileId = 0,
                            RequestId = (int)request.RequestId,
                            Name = toSave.FileName,
                            FileLocation = toSave.FileUrl,
                            Enabled = true,
                        });
                    }
                }
                actualReq.SendFiles = null;
                request = actualReq;
            }

            ERContainer container = new ERContainer() { Requests = new List<Request>() { request }, Model = "Request", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);

            if (ResponseCode == OK)
            {
                Response.StatusCode = 200;
                Response.StatusDescription = "Guardado Exitosamente.";
                return Json(new { ResponseCode, ResponseText = "Guardado Exitosamente." });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Solicitud", PermissionTypes.Delete, null, "RequestsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteRequests(int? RequestId, List<int> Requests, string Search, int Number = 1)
        {
            if (RequestId == null && Requests == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (RequestId != null)
            {
                var (ResponseCode, ResponseText, request) = _helper.GetEntity<Request>("Requests", "Request", "RequestId", (int)RequestId);
                if (ResponseCode == OK)
                {
                    request.Enabled = !request.Enabled;
                    request.Description = request.Description.Replace("\\n", Environment.NewLine);
                    ERContainer container = new ERContainer() { Requests = new List<Request>() { request }, Model = "Request", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridRequests(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Request>>("Requests", "Request");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Requests.Contains((int)x.RequestId)).ToList();
                    list.ForEach(x =>
                    {
                        x.Enabled = !x.Enabled;
                        x.Description = x.Description.Replace("\\n", Environment.NewLine);
                    });

                    ERContainer container = new ERContainer() { Requests = list, Model = "Request", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridRequests(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridRequests(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #endregion

        #region Management


        [IsMenu(2, "Gestión", null, null, null, null, "Management")]
        [Route("CRM/Management/Index")]
        public ActionResult ManagementIndex()
        {
            return RedirectToAction("Assigned");
        }

        #region AssignedTo

        [Route("CRM/Management/Assigned")]
        [IsPermission("Solicitud", PermissionTypes.Other, "Listar solicitudes asignadas al usuario", "RequestsList")]
        public ActionResult Assigned()
        {
            var (ResponseCode, ResponseText, requests) = _helper.GetEntitiesList<List<Request>>("Requests", "Request");
            if (ResponseCode == OK)
            {
                requests = requests.Where(x => x.Enabled && x.AssignedTo != null && x.AssignedTo == CustomSession.CurrentSession.CurrentUser.Member.User.UserId).ToList();
                return View("Records/Requests/Assigned", requests);
            }
            else
            {
                return View("Records/Requests/Assigned", new List<Request>());
            }
        }

        [Route("CRM/Management/ManagementList")]
        [IsPermission("Solicitud", PermissionTypes.Other, "Lista de solicitudes asignadas / no asignadas", "RequestsList")]
        public ActionResult Management(bool Managed = false)
        {
            ViewBag.Managed = Managed;
            var (ResponseCode, ResponseText, requests) = _helper.GetEntitiesList<List<Request>>("Requests", "Request");
            if (ResponseCode == OK)
            {
                return View("Records/Requests/Management", requests.Where(x => x.Enabled).ToList());
            }
            else
            {
                return View("Records/Requests/Management", new List<Request>());
            }
        }

        [Route("CRM/Management/EditAssigned")]
        [IsPermission("Solicitud", PermissionTypes.Other, "Gestionar solicitudes asignadas al usuario", "EditAssigned")]
        public ActionResult EditAssigned(int RequestId = 0)
        {
            if (RequestId <= 0) return RedirectToAction("Assigned");
            else
            {
                var (ListResponseCode, ListResponseText, ListStatus) = _helper.GetEntitiesList<List<RequestStatus>>("Requests", "RequestStatus");
                if (ListResponseCode == OK)
                {
                    ListStatus.ForEach(x => { x.Name = x.Name + x.Color; });
                    ViewBag.StatusList = new SelectList(ListStatus.Where(x => x.Enabled).ToList(), "RequestStatusId", "Name");
                }
                else ViewBag.StatusList = new SelectList(new List<Priority>(), "RequestStatusId", "Name");

                List<Priority> ListPriority;
                (ListResponseCode, ListResponseText, ListPriority) = _helper.GetEntitiesList<List<Priority>>("Requests", "Priority");
                if (ListResponseCode == OK) ViewBag.PrioritiesList = new SelectList(ListPriority.Where(x => x.Enabled).ToList(), "PriorityId", "Name");
                else ViewBag.PrioritiesList = new SelectList(new List<Priority>(), "PriorityId", "Name");

                List<BaseCatalog> ListRequestType;
                (ListResponseCode, ListResponseText, ListRequestType) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.RequestTypeId.ToString()) });
                if (ListResponseCode == OK) ViewBag.RequestTypesList = new SelectList(ListRequestType.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
                else ViewBag.RequestTypesList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

                List<Complexity> ListComplexity;
                (ListResponseCode, ListResponseText, ListComplexity) = _helper.GetEntitiesList<List<Complexity>>("Requests", "Complexity");
                if (ListResponseCode == OK) ViewBag.ComplexitiesList = new SelectList(ListComplexity.Where(x => x.Enabled).ToList(), "ComplexityId", "Name");
                else ViewBag.ComplexitiesList = new SelectList(new List<Priority>(), "ComplexityId", "Name");


                var (ResponseCode, ResponseText, request) = _helper.GetEntity<Request>("Requests", "Request", "RequestId", RequestId);
                if (ResponseCode == OK)
                {
                    request.RequestFiles = request.RequestFiles.Where(x => x.Enabled).ToList();
                    return View("Records/Requests/EditAssigned", request);
                }
                else return RedirectToAction("Assigned");
            }
        }

        [Route("CRM/Management/AssignRequest")]
        [IsPermission("Solicitud", PermissionTypes.Other, "Asignar / Reasignar solicitudes", "AssignRequest")]
        public ActionResult AssignRequest(int RequestId = 0)
        {
            if (RequestId <= 0) return RedirectToAction("Management");
            else
            {
                var (ResponseCode, ResponseText, membersList) = _helper.GetEntitiesList<List<Member>>("Members", "Member");
                if (ResponseCode == OK)
                {
                    membersList.ForEach(x =>
                    {
                        x.MemberId = x.User.UserId;
                        x.Names = x.Names + " " + x.Surnames;
                    });
                    ViewBag.ListMember = new SelectList(membersList.Where(x => x.Enabled).ToList(), "MemberId", "Names");
                }
                else ViewBag.ListMember = new SelectList(new List<Member>(), "MemberId", "Names");

                Request request;
                (ResponseCode, ResponseText, request) = _helper.GetEntity<Request>("Requests", "Request", "RequestId", RequestId);
                if (ResponseCode == OK)
                {
                    request.RequestFiles = request.RequestFiles.Where(x => x.Enabled).ToList();
                    return View("Records/Requests/AssignRequest", request);
                }
                else return RedirectToAction("Management");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveAssignRequest(Request request)
        {
            if (request.RequestId == null || request.RequestId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Datos no válidos.";
                return Json(new { ResponseCode = 500, ResponseText = "Datos no válidos." });
            }
            else
            {
                var (ResponseCodeGet, ResponseTextGet, actualReq) = _helper.GetEntity<Request>("Requests", "Request", "RequestId", (int)request.RequestId);
                actualReq.Description = actualReq.Description.Replace("\\n", Environment.NewLine);
                actualReq.AssignedTo = request.AssignedTo;

                ERContainer container = new ERContainer() { Requests = new List<Request>() { actualReq }, Model = "Request", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                var (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);

                if (ResponseCode == OK)
                {
                    Response.StatusCode = 200;
                    Response.StatusDescription = "Guardado Exitosamente.";
                    return Json(new { ResponseCode, ResponseText = "Guardado Exitosamente." });
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText });
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveAssigned(Request request)
        {
            if (request.RequestId == null || request.RequestId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Datos no válidos.";
                return Json(new { ResponseCode = 500, ResponseText = "Datos no válidos." });
            }
            else
            {
                var (ResponseCodeGet, ResponseTextGet, actualReq) = _helper.GetEntity<Request>("Requests", "Request", "RequestId", (int)request.RequestId);
                actualReq.Description = actualReq.Description.Replace("\\n", Environment.NewLine);
                actualReq.RequestStatusId = request.RequestStatusId;
                actualReq.PriorityId = request.PriorityId;
                actualReq.TopicId = request.TopicId;
                actualReq.ComplexityId = request.ComplexityId;

                if (actualReq.AttentionDate == null) actualReq.AttentionDate = DateTime.Now.Date;

                ERContainer container = new ERContainer() { Requests = new List<Request>() { actualReq }, Model = "Request", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                var (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);

                if (ResponseCode == OK)
                {
                    Response.StatusCode = 200;
                    Response.StatusDescription = "Guardado Exitosamente.";
                    return Json(new { ResponseCode, ResponseText = "Guardado Exitosamente." });
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText });
                }
            }
        }

        [HttpPost]
        public async Task<ActionResult> SaveRequestComment(RequestComment comment)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Datos no válidos.";
                return Json(new { ResponseCode = 500, ResponseText = "Datos no válidos." });
            }

            comment.DateTimeComment = DateTime.Now;
            comment.Enabled = true;

            ERContainer container = new ERContainer() { RequestComments = new List<RequestComment>() { comment }, Model = "RequestComment", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText, comments) = await _helper.CreatePostGetElement<List<RequestComment>>("Requests/Model/NewObject", container);
            if (ResponseCode == OK)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, comments, "~/Views/CRM/Records/Requests/_RequestCommentPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        #endregion

        #endregion

        #region Catalogs

        [IsMenu(3, "Catálogos", null, null, null, null, "Catalogs")]
        [Route("CRM/Catalogs/Index")]
        public ActionResult CatalogsIndex()
        {
            return RedirectToAction("Priorities");
        }

        #region Priorities

        [IsMenu(1, "Prioridad", null, "Catálogos", null, null, "PrioritiesList")]
        [Route("CRM/Catalogs/PrioritiesList")]
        [IsPermission("Prioridad", PermissionTypes.List, null, "PrioritiesList")]
        public ActionResult Priorities()
        {
            return View("Catalogs/Priorities/Index");
        }

        public ActionResult GetGridPriorities(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, priorities, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Priority>>("Requests", "Priority", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, priorities, "~/Views/CRM/Catalogs/Priorities/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Priorities/_GridPartial", priorities);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Priorities/_GridPartial", new List<Priority>());
            }
        }

        [Route("CRM/Catalogs/NewPriority")]
        [IsPermission("Prioridad", PermissionTypes.New, null, "PrioritiesCreation")]
        public ActionResult NewPriority(int? PriorityId)
        {
            return View("Catalogs/Priorities/Create", PriorityId);
        }

        public ActionResult GetPriority(int? PriorityId)
        {
            if (PriorityId == null)
            {
                return PartialView("Catalogs/Priorities/_ToAddPartial", new Priority());
            }
            else if (PriorityId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new Priority(), "~/Views/CRM/Catalogs/Priorities/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, Priorities) = _helper.GetEntity<Priority>("Requests", "Priority", "PriorityId", (int)PriorityId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, Priorities, "~/Views/CRM/Catalogs/Priorities/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Priorities/_ToAddPartial", Priorities);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Priorities/_ToAddPartial", new Priority());
                }
            }
        }

        public ActionResult GetPriorities()
        {
            var (ResponseCode, ResponseText, Priorities) = _helper.GetEntitiesList<List<Priority>>("Requests", "Priority");

            if (ResponseCode == OK) return PartialView("Catalogs/Priorities/_AggregatesPartial", Priorities.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/Priorities/_AggregatesPartial", new List<Priority>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SavePriority(Priority priority)
        {
            if (!ModelState.IsValid) return View(priority);

            priority.Enabled = true;
            ERContainer container = new ERContainer() { Priorities = new List<Priority>() { priority }, Model = "Priority", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/Priorities/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Prioridad", PermissionTypes.Delete, null, "PrioritiesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeletePriorities(int? PriorityId, List<int> Priorities, string Search, int Number = 1)
        {
            if (PriorityId == null && Priorities == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (PriorityId != null)
            {
                var (ResponseCode, ResponseText, priority) = _helper.GetEntity<Priority>("Requests", "Priority", "PriorityId", (int)PriorityId);
                if (ResponseCode == OK)
                {
                    priority.Enabled = !priority.Enabled;
                    ERContainer container = new ERContainer() { Priorities = new List<Priority>() { priority }, Model = "Priority", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridPriorities(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Priority>>("Requests", "Priority");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Priorities.Contains((int)x.PriorityId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Priorities = list, Model = "Priority", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridPriorities(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridPriorities(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Complexities

        [IsMenu(2, "Complejidad", null, "Catálogos", null, null, "ComplexitiesList")]
        [Route("CRM/Catalogs/ComplexitiesList")]
        [IsPermission("Complejidad", PermissionTypes.List, null, "ComplexitiesList")]
        public ActionResult Complexities()
        {
            return View("Catalogs/Complexities/Index");
        }

        public ActionResult GetGridComplexities(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, complexities, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Complexity>>("Requests", "Complexity", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, complexities, "~/Views/CRM/Catalogs/Complexities/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Complexities/_GridPartial", complexities);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Complexities/_GridPartial", new List<Complexity>());
            }
        }

        [Route("CRM/Catalogs/NewComplexity")]
        [IsPermission("Complejidad", PermissionTypes.New, null, "ComplexitiesCreation")]
        public ActionResult NewComplexity(int? ComplexityId)
        {
            return View("Catalogs/Complexities/Create", ComplexityId);
        }

        public ActionResult GetComplexity(int? ComplexityId)
        {
            if (ComplexityId == null)
            {
                return PartialView("Catalogs/Complexities/_ToAddPartial", new Complexity());
            }
            else if (ComplexityId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new Complexity(), "~/Views/CRM/Catalogs/Complexities/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, Complexities) = _helper.GetEntity<Complexity>("Requests", "Complexity", "ComplexityId", (int)ComplexityId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, Complexities, "~/Views/CRM/Catalogs/Complexities/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Complexities/_ToAddPartial", Complexities);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Complexities/_ToAddPartial", new Complexity());
                }
            }
        }

        public ActionResult GetComplexities()
        {
            var (ResponseCode, ResponseText, Complexities) = _helper.GetEntitiesList<List<Complexity>>("Requests", "Complexity");

            if (ResponseCode == OK) return PartialView("Catalogs/Complexities/_AggregatesPartial", Complexities.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/Complexities/_AggregatesPartial", new List<Complexity>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveComplexity(Complexity complexity)
        {
            if (!ModelState.IsValid) return View(complexity);

            complexity.Enabled = true;
            ERContainer container = new ERContainer() { Complexities = new List<Complexity>() { complexity }, Model = "Complexity", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/Complexities/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Complejidad", PermissionTypes.Delete, null, "ComplexitiesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteComplexities(int? ComplexityId, List<int> Complexities, string Search, int Number = 1)
        {
            if (ComplexityId == null && Complexities == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (ComplexityId != null)
            {
                var (ResponseCode, ResponseText, complexity) = _helper.GetEntity<Complexity>("Requests", "Complexity", "ComplexityId", (int)ComplexityId);
                if (ResponseCode == OK)
                {
                    complexity.Enabled = !complexity.Enabled;
                    ERContainer container = new ERContainer() { Complexities = new List<Complexity>() { complexity }, Model = "Complexity", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridComplexities(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Complexity>>("Requests", "Complexity");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Complexities.Contains((int)x.ComplexityId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Complexities = list, Model = "Complexity", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridComplexities(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridComplexities(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #region Topics

        [IsMenu(2, "Temática", null, "Catálogos", null, null, "TopicsList")]
        [Route("CRM/Catalogs/TopicsList")]
        [IsPermission("Temática", PermissionTypes.List, null, "TopicsList")]
        public ActionResult Topics()
        {
            return View("Catalogs/Topics/Index");
        }

        public ActionResult GetGridTopics(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, topics, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Topic>>("Requests", "Topic", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, topics, "~/Views/CRM/Catalogs/Topics/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Topics/_GridPartial", topics);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Topics/_GridPartial", new List<Topic>());
            }
        }

        [Route("CRM/Catalogs/NewTopic")]
        [IsPermission("Temática", PermissionTypes.New, null, "TopicsCreation")]
        public ActionResult NewTopic(int? TopicId)
        {
            return View("Catalogs/Topics/Create", TopicId);
        }

        public ActionResult GetTopic(int? TopicId)
        {
            var (ListResponseCode, ListResponseText, ListRequestType) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.RequestTypeId.ToString()) });
            if (ListResponseCode == OK) ViewBag.RequestTypesList = new SelectList(ListRequestType.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.RequestTypesList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            if (TopicId == null)
            {
                return PartialView("Catalogs/Topics/_ToAddPartial", new Topic());
            }
            else if (TopicId == 0)
            {
                var response = Utilities.RenderRazorViewToString(ControllerContext, new Topic(), "~/Views/CRM/Catalogs/Topics/_ToAddPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, Topics) = _helper.GetEntity<Topic>("Requests", "Topic", "TopicId", (int)TopicId);

                if (ResponseCode == OK)
                {
                    if (Request.IsAjaxRequest())
                    {
                        var response = Utilities.RenderRazorViewToString(ControllerContext, Topics, "~/Views/CRM/Catalogs/Topics/_ToAddPartial.cshtml");
                        return Json(new { response }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Topics/_ToAddPartial", Topics);
                }
                else
                {
                    if (Request.IsAjaxRequest())
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                    }
                    else return PartialView("Catalogs/Topics/_ToAddPartial", new Topic());
                }
            }
        }

        public ActionResult GetTopics()
        {
            var (ResponseCode, ResponseText, Topics) = _helper.GetEntitiesList<List<Topic>>("Requests", "Topic");

            if (ResponseCode == OK) return PartialView("Catalogs/Topics/_AggregatesPartial", Topics.Where(x => x.Enabled).ToList());
            else return PartialView("Catalogs/Topics/_AggregatesPartial", new List<Topic>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveTopic(Topic topic)
        {
            if (!ModelState.IsValid) return View(topic);

            topic.Enabled = true;
            ERContainer container = new ERContainer() { Topics = new List<Topic>() { topic }, Model = "Topic", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);

            if (ResponseCode == OK)
                return PartialView("Catalogs/Topics/_CreateBodyPartial");
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Temática", PermissionTypes.Delete, null, "TopicsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteTopics(int? TopicId, List<int> Topics, string Search, int Number = 1)
        {
            if (TopicId == null && Topics == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (TopicId != null)
            {
                var (ResponseCode, ResponseText, topic) = _helper.GetEntity<Topic>("Requests", "Topic", "TopicId", (int)TopicId);
                if (ResponseCode == OK)
                {
                    topic.Enabled = !topic.Enabled;
                    ERContainer container = new ERContainer() { Topics = new List<Topic>() { topic }, Model = "Topic", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridTopics(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Topic>>("Requests", "Topic");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Topics.Contains((int)x.TopicId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Topics = list, Model = "Topic", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridTopics(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridTopics(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #endregion

    }
}