﻿using CR.CGC.GuiasScoutsWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    //[IsMenu(2, "Menu Primario", "home")]
    public class HomeController : ApplicationController
    {
        //[IsMenu(2, "Menu Primario: Sub Menu 1", "remove")]
        public ActionResult Index()
        {
            return RedirectToAction("SubscriptionsIndex", "Publication");
        }

        //[IsMenu(1, "Menu Primario: Sub Menu 2", "pencil", null, "Administrative.png", "Esta es una descripción!")]
        //[Permission()]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        //[Permission()]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}