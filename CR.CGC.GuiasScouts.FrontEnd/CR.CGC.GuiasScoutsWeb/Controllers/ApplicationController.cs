﻿using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    [Authorize]
    public class ApplicationController : Controller
    {
        protected LoginViewModel _currentUser = null;
        protected CatalogConstants _catalogConstants;
        protected SocialNetworkConstants _socialNetworkConstants;

        protected readonly string[] _listDefaultMethods = { "Index", "Show", "Create", "Edit", "Delete" };
        protected readonly string _propertiesFile = "~/.properties";
        private const string _saName = Common.Constants.saName;

        public ApplicationController()
        {
            this._currentUser = CustomSession.CurrentSession.CurrentUser;
            this._catalogConstants = CustomSession.CurrentSession.CatalogConstants;
            this._socialNetworkConstants = CustomSession.CurrentSession.SocialNetworkConstants;
        }

        public ActionResult GetMenu()
        {
            return PartialView("~/Views/Shared/_MenuNavigationPartial.cshtml", Utilities.GetMenu());
        }

        public ActionResult GetMenuTab(string Controller, string Action)
        {
            return PartialView("~/Views/Shared/_MenuTabPartial.cshtml", Utilities.GetMenuTab(Controller, Action));
        }

        [HttpPost]
        public ActionResult CreateSelect(List<SelectViewModel> selects)
        {
            List<ResponseQuery> response = new List<ResponseQuery>();

            try
            {
                foreach (var item in selects)
                {
                    response.Add(new ResponseQuery() { Key = item.Id, Value = Utilities.RenderRazorViewToString(ControllerContext, item, "~/Views/Shared/_SelectMaterialPartial.cshtml") });
                }
                return Json(new { response });
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ex.Message;
                return Json(new { ResponseCode = 500, ex.Message });
            }
        }

        protected string GetErrorMessage(ModelStateDictionary state)
        {
            List<string> ViewErrors = new List<string>();

            foreach (ModelState modelValue in state.Values)
            {
                foreach (ModelError modelError in modelValue.Errors)
                {
                    ViewErrors.Add(modelError.ErrorMessage);
                }
            }
            return string.Join(", ", ViewErrors);
        }

        #region Documents

        [HttpGet]
        public FileResult DownloadFile(string FileUrl)
        {
            string fullPath = Request.MapPath(FileUrl);
            bool exists = System.IO.File.Exists(fullPath);
            if (exists)
            {
                try
                {
                    return File(FileUrl, "application/force-download", Path.GetFileName(FileUrl));
                }
                catch (Exception)
                {
                    return FileNotFound(FileUrl);
                }
            }
            else
            {
                return FileNotFound(FileUrl);
            }
        }

        [HttpGet]
        public ActionResult ViewFile(string FileUrl)
        {
            string fullPath = Request.MapPath(FileUrl);
            if (FileUrl.Contains(".png") || FileUrl.Contains(".PNG"))
            {
                if (System.IO.File.Exists(fullPath)) return File(FileUrl, "image/png", Path.GetFileName(FileUrl));
                else return File(FileUrl, "image/png", Path.GetFileName("~/Documents/Default/default_image_2.png"));
            }
            else if (FileUrl.Contains(".jpg") || FileUrl.Contains(".JPG"))
            {
                if (System.IO.File.Exists(fullPath)) return File(FileUrl, "image/jpeg", Path.GetFileName(FileUrl));
                else return File(FileUrl, "image/jpeg", Path.GetFileName("~/Documents/Default/default_image_2.png"));
            }
            else if (FileUrl.Contains(".pdf"))
            {
                var path = Server.MapPath(FileUrl);
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                return File(fs, "application/pdf");
            }
            else
            {
                return Json("Vista no disposible.");
            }
        }

        public FileResult FileNotFound(string fileUrl)
        {
            string AppDirectory = HttpRuntime.AppDomainAppPath;
            string DirectoryName = Path.GetDirectoryName(fileUrl);
            string FileName = "FileNotFound.txt";

            FileInfo info = new FileInfo(string.Format("{0}{1}\\{2}", AppDirectory, DirectoryName.Substring(1), FileName));
            fileUrl = string.Format("{0}\\{1}", DirectoryName, FileName);
            if (!info.Exists)
            {
                using (StreamWriter writer = info.CreateText())
                {
                    writer.WriteLine("No se ha encontrado el archivo solicitado.");
                    writer.Close();
                }
            }

            return File(info.FullName, "application/force-download", FileName);
        }

        [HttpGet]
        public ActionResult EmailTemplateContent(string TemplateCode)
        {
            if (TemplateCode == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            string fullPath = Request.MapPath("~/Content/EmailTemplates/" + TemplateCode + ".html");
            if (System.IO.File.Exists(fullPath))
            {
                try
                {
                    string response = System.IO.File.ReadAllText(fullPath);
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ex.Message;
                    return Json(new { ResponseCode = 400, ResponseText = ex.Message });
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }
        }

        #endregion

        public ActionResult SingleMenu()
        {
            return null;
        }

        public ActionResult GetPartial(string partial, object model)
        {
            return PartialView(partial, model);
        }


        protected List<PermissionModel> GetListPermissions()
        {
            List<PermissionModel> response = new List<PermissionModel>();
            Assembly asm = Assembly.GetExecutingAssembly();

            var methodList = asm.GetTypes()
                .AsEnumerable()
                .Where(type => typeof(Controller).IsAssignableFrom(type))
                .SelectMany(type => type.GetMethods())
                .Where(method => method.IsPublic && (method.ReturnType.Name == "ActionResult" || method.ReturnType.Name == "Task`1") && method.GetCustomAttribute(typeof(IsPermission)) != null)
                .Select(x => new
                {
                    EntityName = ((IsPermission)x.GetCustomAttribute(typeof(IsPermission))).GetEntity(),
                    Type = ((IsPermission)x.GetCustomAttribute(typeof(IsPermission))).GetPermType(),
                    Name = ((IsPermission)x.GetCustomAttribute(typeof(IsPermission))).GetName(),
                    Description = ((IsPermission)x.GetCustomAttribute(typeof(IsPermission))).GetDescription(),
                    Controller = x.DeclaringType.Name.Replace("Controller", ""),
                    Action = x.Name,
                })
                .OrderBy(x => x.EntityName)
                .ToList();

            foreach (var item in methodList.GroupBy(x => x.EntityName).Select(x => x.Key).ToList())
            {
                response.Add(new PermissionModel()
                {
                    EntityName = item,
                    CommonMethods = methodList.Where(x => x.EntityName == item && x.Type != PermissionTypes.Other).Select(x => new PermissionItemModel() { Name = x.Type, Description = x.Description, Controller = x.Controller, Action = x.Action }).ToList(),
                    SpecialMethods = methodList.Where(x => x.EntityName == item && x.Type == PermissionTypes.Other).GroupBy(x => x.Name).Select(k => k.FirstOrDefault()).Select(x => new PermissionItemModel() { Name = x.Name, Description = x.Description, Controller = x.Controller, Action = x.Action }).ToList(),
                });
            }

            return response;
        }

        protected List<T> EditChildList<T>(List<T> viewList, List<T> actualList, string objectId, string[] parameters)
        {
            List<int> list = new List<int>();

            viewList.ForEach(x => { if ((int?)GetValueByProperty<T>(x, objectId) != null && (int?)GetValueByProperty<T>(x, objectId) != 0) list.Add((int)GetValueByProperty<T>(x, objectId)); });

            actualList.ForEach(x =>
            {
                if (list.Contains((int)GetValueByProperty<T>(x, objectId)))
                {
                    foreach (var param in parameters)
                    {
                        SetValueByProperty<T>(x, param, GetValueByProperty<T>(viewList.Where(_ => (int?)GetValueByProperty<T>(_, objectId) == (int?)GetValueByProperty<T>(x, objectId)).First(), param));
                    }
                }
                else
                    SetValueByProperty<T>(x, "Enabled", false);
            });

            viewList.ForEach(x => { if ((int?)GetValueByProperty<T>(x, objectId) == null || (int?)GetValueByProperty<T>(x, objectId) == 0) actualList.Add(x); });

            return actualList;
        }

        protected List<T> EditDuplicateList<T>(List<T> viewList, string stringId, string[] parameters)
        {
            List<T> listResp = new List<T>();

            foreach (var item in viewList)
            {
                bool flag = true;
                foreach (var inList in listResp)
                {
                    if (IsEqualParamsObject<T>(item, inList, parameters))
                    {
                        if (((int?)GetValueByProperty<T>(item, stringId) == null || (int?)GetValueByProperty<T>(item, stringId) == 0) && ((int?)GetValueByProperty<T>(inList, stringId) == null || (int?)GetValueByProperty<T>(inList, stringId) == 0))
                        {
                            SetValueByProperty<T>(inList, "Enabled", true);
                        }
                        else if ((int?)GetValueByProperty<T>(inList, stringId) == null || (int?)GetValueByProperty<T>(inList, stringId) == 0)
                        {
                            SetValueByProperty<T>(inList, stringId, GetValueByProperty<T>(item, stringId));
                            SetValueByProperty<T>(inList, "Enabled", GetValueByProperty<T>(item, "Enabled"));
                        }
                        else if ((int?)GetValueByProperty<T>(item, stringId) == null || (int?)GetValueByProperty<T>(item, stringId) == 0)
                        {
                            SetValueByProperty<T>(inList, "Enabled", GetValueByProperty<T>(item, "Enabled"));
                        }

                        flag = false;
                    }
                }

                if (flag) listResp.Add(item);
            }

            return listResp;
        }

        private bool IsEqualParamsObject<T>(object objectBase, object toCompare, string[] parameters)
        {
            bool resp = true;

            foreach (var param in parameters)
            {
                if (!(GetValueByProperty<T>(objectBase, param).Equals(GetValueByProperty<T>(toCompare, param))))
                {
                    resp = false;
                    break;
                }
            }

            return resp;
        }

        private object GetValueByProperty<T>(object obj, string propertyName)
        {
            return typeof(T).GetProperty(propertyName).GetValue(obj, null);
        }

        private void SetValueByProperty<T>(object obj, string propertyName, object value)
        {
            typeof(T).GetProperty(propertyName).SetValue(obj, value, null);
        }

        protected List<T> ShuffleList<T>(List<T> list)
        {
            List<T> input = list;
            List<T> output = new List<T>();

            Random bingo = new Random();
            while (input.Count > 0)
            {
                int index = bingo.Next(0, input.Count - 1);
                output.Add(input[index]);
                input.RemoveAt(index);
            }

            return output;
        }

        //Account

        protected LoginViewModel GetCurrentUser() { return this._currentUser; }

        [NonAction]
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //authenticated user
            if (this._currentUser == null)
            {
                HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                filterContext.Result = new RedirectResult(Url.Action("Index", "Home"));
                return;
            }

            //first login 
            if (this._currentUser.Member.User.IsFirstLogin && this._currentUser.Member.User.UserName != _saName)
            {
                filterContext.Result = new RedirectResult(Url.Action("FirstLogin", "Account"));
                return;
            }

            //superuser role
            filterContext.Controller.ViewBag.IsSuperUser = (this._currentUser.Member.User.UserName == _saName);

            if (this._currentUser.Member.User.UserName == _saName)
            {

                if (filterContext.ActionDescriptor.ControllerDescriptor.ControllerName != "Configuration" &&
                    filterContext.ActionDescriptor.ControllerDescriptor.ControllerName != "Application" &&
                    filterContext.ActionDescriptor.ControllerDescriptor.ControllerName != "Member")
                {
                    filterContext.Result = new RedirectResult(Url.Action("SecurityMenu", "Configuration"));
                    return;
                }
            }
            else
            {
                Assembly asm = Assembly.GetExecutingAssembly();
                var isPermission = asm.GetTypes()
                    .AsEnumerable()
                    .Where(type => typeof(Controller).IsAssignableFrom(type) && type.Name.Replace("Controller", "") == filterContext.ActionDescriptor.ControllerDescriptor.ControllerName)
                    .SelectMany(type => type.GetMethods())
                    .Where(method => method.IsPublic && (method.ReturnType.Name == "ActionResult" || method.ReturnType.Name == "Task`1") && method.Name == filterContext.ActionDescriptor.ActionName && method.GetCustomAttribute(typeof(IsPermission)) != null)
                    .Select(x => new { ControllerName = x.DeclaringType.Name.Replace("Controller", ""), MethodName = x.Name })
                    .ToList();

                if (isPermission.Count > 0)
                {
                    List<string> list = this._currentUser.Member.User.Role.Permissions.Select(x => x.Controller + x.Action).ToList();
                    if (!list.Contains(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName + filterContext.ActionDescriptor.ActionName))
                    {
                        if (Request.IsAjaxRequest())
                        {
                            Response.StatusCode = 403;
                            Response.StatusDescription = "Su usuario no posee permisos para continuar con la acción.";
                            filterContext.Result = Json(new { ResponseCode = 403, ResponseText = "Su usuario no posee permisos para continuar con la acción." }, JsonRequestBehavior.AllowGet);
                            return;
                        }
                        else
                        {
                            filterContext.Result = new RedirectResult(Url.Action("Forbidden", "Error"));
                            return;
                        }
                    }
                }
            }

        }

        [NonAction]
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Exception != null)
                filterContext.HttpContext.Trace.Write("(Controller)Exception thrown");

            base.OnActionExecuted(filterContext);
        }

    }

}