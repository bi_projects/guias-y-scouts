﻿using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    //[IsMenu(8, "Consultas", "icon_consultas", null, "caption_security.jpg", "Esta es la descripción para Consultas. Solamente es un texto de prueba, recordar cambiarlo.", "Query")]
    public class QueryController : Controller
    {
        private static string _BaseUrl = System.Configuration.ConfigurationManager.AppSettings["WebApi"].ToString();
        private IApplicationHelper _helper = new ApplicationHelper(_BaseUrl);

        #region Records

        [IsMenu(1, "Registros", null, null, null, null, "Records")]
        [Route("Query/Records/Index")]
        public ActionResult RecordsIndex()
        {
            return View("Records/Index");
        }

        #endregion
    }
}