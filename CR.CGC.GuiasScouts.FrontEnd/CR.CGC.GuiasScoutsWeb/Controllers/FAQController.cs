﻿using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using static System.Net.HttpStatusCode;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    [IsMenu(7, "Ayuda", "icon_help", null, "caption_faq.jpg", "Esta es la descripción para Ayuda. Solamente es un texto de prueba, recordar cambiarlo.", "FAQ")]
    public class FAQController : ApplicationController
    {
        private static string _BaseUrl = System.Configuration.ConfigurationManager.AppSettings["WebApi"].ToString();
        private IApplicationHelper _helper = new ApplicationHelper(_BaseUrl);

        #region Faqs

        [IsMenu(1, "Preguntas Frecuentes", null, null, null, null, "Faqs")]
        [Route("FAQ/Faqs/Index")]
        public ActionResult RecordsIndex()
        {
            return RedirectToAction("FAQS");
        }

        #region View

        [Route("FAQ/Faqs/FaqsView")]
        public ActionResult FAQS()
        {
            Assembly asm = Assembly.GetExecutingAssembly();

            List<Models.MenuModel> controllerlist = asm.GetTypes().AsEnumerable().Where(type => typeof(Controller).IsAssignableFrom(type) && type.GetCustomAttribute(typeof(IsMenu)) != null)
                .Select(x => new Models.MenuModel()
                {
                    NameMenu = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIsMenu() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIsMenu() : x.Name),
                    IconMenu = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIcon() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetIcon() : ""),
                    CodeMenu = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetCode() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetCode() : ""),
                    DescriptionMenu = (((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetDescription() != null ? ((IsMenu)x.GetCustomAttribute(typeof(IsMenu))).GetDescription() : "")
                }).ToList();

            var (ListResponseCode, ListResponseText, List) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ModuleId.ToString()) });
            if (ListResponseCode == OK) List = List.Where(x => x.Enabled).ToList(); 
            else List = new List<BaseCatalog>();

            return View("FAQS/Index", new Models.FaqDetailViewModel() {MenuModules = controllerlist, Modules = List });
        }

        public ActionResult GetGridFaqs(int ModuleId = 0, string TabId = null)
        {
            ViewBag.TabId = TabId;
            var (ResponseCode, ResponseText, faqs) = _helper.GetEntitiesByParams<List<Faq>>("Config", "Faq", new[] { ("Type", $"{ModuleId}") });
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, faqs.Where(x => x.Enabled).ToList(), "~/Views/FAQ/FAQS/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("FAQS/_GridPartial", faqs);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("FAQS/_GridPartial", new List<Faq>());
            }
        }

        #endregion

        #endregion
    }
}