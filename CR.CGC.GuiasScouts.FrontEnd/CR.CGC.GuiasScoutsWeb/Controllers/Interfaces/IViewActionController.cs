﻿/*==========================================================================
Archivo:            IViewActionController
Descripción:        Interfaz de Controlador                      
Autor:              Armando Bustos                           
Fecha de creación:  09-01-2018                                              
Versión:            1.0                                                       
Derechos Reservados BI S.A.                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CR.CGC.GuiasScoutsWeb.Controllers.Interfaces
{
    public interface IViewActionController
    {
        ActionResult Index();
        ActionResult Show(int? id);
        ActionResult Create();
        ActionResult Edit(int? id);
        ActionResult Delete(int id);

    }
}