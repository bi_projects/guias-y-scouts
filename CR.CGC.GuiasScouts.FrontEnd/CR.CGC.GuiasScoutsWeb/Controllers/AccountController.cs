﻿using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        protected readonly string _propertiesFile = "~/.properties";

        public AccountController() { }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #region Custom

        private static string _urlAccount = ConfigurationManager.AppSettings["webAddressSecurity"].ToString() + "Account/";

        private IApplicationHelper _helper = new ApplicationHelper(_urlAccount);

        private const string _saName = Common.Constants.saName;

        #endregion

        #region Views

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult FirstUser()
        {
            //string IPAddressL = "";

            //IPHostEntry Host = default(IPHostEntry);
            //string Hostname = null;
            //Hostname = System.Environment.MachineName;
            //Host = Dns.GetHostEntry(Hostname);
            //foreach (IPAddress IP in Host.AddressList)
            //{
            //    if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            //    {
            //        IPAddressL = Convert.ToString(IP);
            //    }
            //}
            ////IPAddressL = IPAddressL;
            //ViewBag.ipaddress = IPAddressL;

            return View();
        }

        public ActionResult FirstLogin()
        {
            //get model of user
            return View();
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordToken(string token)
        {
            if (token == null)
                return View("Error");
            return View(new ForgotPasswordTokenViewModel() { Token = token });
        }

        #endregion

        #region Methods

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> FirstUser(RegisterViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            User container = new User()
            {
                UserName = _saName,
                Email = model.Email,
                Password = model.Password
            };

            var response = await _helper.CreateObject<List<User>>("FirstUser", container);

            if (response.GetType().Equals(typeof(List<User>)))
            {
                User responseModel = ((List<User>)response).FirstOrDefault();
                if (responseModel.UserName.Equals(_saName))
                {
                    Properties config = new Properties(this._propertiesFile);
                    config.set("isFirstUser", "false");
                    config.Save();
                }

                return RedirectToAction("Login");
            }
            else
            {
                ModelState.AddModelError("Error", (string)response);
                return View(model);
            }

        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid) return View(model);

            var currentUser = new User()
            {
                UserName = model.Email.Contains("@") ? null : model.Email,
                Password = model.Password,
                Email = model.Email.Contains("@") ? model.Email : null
            };

            var response = await _helper.CreateObject<ERContainer>("LogIn", currentUser);
            if (response.GetType().Equals(typeof(ERContainer)))
            {
                Member responseModel = ((ERContainer)response).Members.FirstOrDefault();
                model.Password = null;
                model.Member = responseModel;

                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, responseModel.User.UserName),
                    new Claim(ClaimTypes.Email, responseModel.Email)
                };

                var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, identity);

                CustomSession.CurrentSession.CurrentUser = model;
                (CustomSession.CurrentSession.CatalogConstants, CustomSession.CurrentSession.SocialNetworkConstants, CustomSession.CurrentSession.ListContainer) = Common.Constants.SetConstants();

                if (responseModel.User.IsFirstLogin)
                    return RedirectToAction("FirstLogin");
                else
                    return RedirectToLocal(returnUrl);
            }
            else
            {
                ModelState.AddModelError("Intento de inicio de sesión no válido.", (string)response);
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> FirstLogin(FirstLoginModel model)
        {
            if (!ModelState.IsValid) return View(model);

            model.PersonalInfo = CustomSession.CurrentSession.CurrentUser.Member.User;
            var container = await _helper.CreateObject<ERContainer>("UpdatePassword", new UpdPassModel()
            {
                Email = model.PersonalInfo.Email,
                UserName = model.PersonalInfo.UserName,
                CurrentPassword = model.ActualPassword,
                NewPassword = model.NewPassword
            });

            if (container.GetType().Equals(typeof(ERContainer)))
            {
                User responseModel = ((ERContainer)container).Users.FirstOrDefault();

                if (responseModel.IsFirstLogin)
                {
                    ModelState.AddModelError("Error.", "Ha ocurrido un error.");
                    return View(model);
                }
                else
                {
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                ModelState.AddModelError("Error.", (string)container);
                return View(model);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            User currentUser = new User() { Email = model.Email, Source = model.Source };

            var response = await _helper.CreateObject<User>("ForgotPassword", currentUser);
            if (response.GetType().Equals(typeof(User)))
                return RedirectToAction("ForgotPasswordConfirmation");
            else
            {
                ModelState.AddModelError("Error", (string)response);
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LogOff()
        {
            var response = await _helper.CreateObject<User>("LogOut", CustomSession.CurrentSession.CurrentUser.Member.User);
            if (response.GetType().Equals(typeof(User)))
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPasswordToken(ForgotPasswordTokenViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            User currentUser = new User() { PasswordResetToken = model.Token, Password = model.Password };
            var response = await _helper.CreateObject<User>("ConfirmForgotPassword", currentUser);
            if (response.GetType().Equals(typeof(User)))
            {
                User responseModel = (User)response;
                return RedirectToAction("Login");
            }
            else
            {
                ModelState.AddModelError("Intento de inicio de sesión no válido.", (string)response);
                return View(model);
            }
        }

        #endregion

        #region default

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Requerir que el usuario haya iniciado sesión con nombre de usuario y contraseña o inicio de sesión externo
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // El código siguiente protege de los ataques por fuerza bruta a los códigos de dos factores. 
            // Si un usuario introduce códigos incorrectos durante un intervalo especificado de tiempo, la cuenta del usuario 
            // se bloqueará durante un período de tiempo especificado. 
            // Puede configurar el bloqueo de la cuenta en IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Código no válido.");
                    return View(model);
            }
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // Para obtener más información sobre cómo habilitar la confirmación de cuenta y el restablecimiento de contraseña, visite http://go.microsoft.com/fwlink/?LinkID=320771
                    // Enviar correo electrónico con este vínculo
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirmar cuenta", "Para confirmar la cuenta, haga clic <a href=\"" + callbackUrl + "\">aquí</a>");

                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            // Si llegamos a este punto, es que se ha producido un error y volvemos a mostrar el formulario
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ResetPassword
        public ActionResult ResetPassword()
        {
            return View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            UpdPassModel currentUser = new UpdPassModel() { CurrentPassword = model.CurrentPassword, NewPassword = model.Password, UserName = CustomSession.CurrentSession.CurrentUser.Member.User.UserName, Email = CustomSession.CurrentSession.CurrentUser.Member.User.Email };
            var ResponseResetPassword = await _helper.CreateObject<User>("UpdatePassword", currentUser);
            if (ResponseResetPassword.GetType().Equals(typeof(User)))
            {
                var ResponseLogOut = await _helper.CreateObject<User>("LogOut", CustomSession.CurrentSession.CurrentUser.Member.User);
                if (ResponseLogOut.GetType().Equals(typeof(User)))
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("Intento de inicio de sesión no válido.", (string)ResponseResetPassword);
                return View(model);
            }
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Solicitar redireccionamiento al proveedor de inicio de sesión externo
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generar el token y enviarlo
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Si el usuario ya tiene un inicio de sesión, iniciar sesión del usuario con este proveedor de inicio de sesión externo
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // Si el usuario no tiene ninguna cuenta, solicitar que cree una
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Obtener datos del usuario del proveedor de inicio de sesión externo
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Aplicaciones auxiliares
        // Se usa para la protección XSRF al agregar inicios de sesión externos
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        [NonAction]
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //first user
            Properties config = new Properties(this._propertiesFile);
            if (config.get("isFirstUser") == null)
            {
                config.set("isFirstUser", "true");
                config.Save();
                config.reload(this._propertiesFile);
            }
            if (Convert.ToBoolean(config.get("isFirstUser")) && !(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Account" && filterContext.ActionDescriptor.ActionName == "FirstUser"))
            {
                filterContext.Result = new RedirectResult(Url.Action("FirstUser", "Account"));
                return;
            }

            if (!Convert.ToBoolean(config.get("isFirstUser")) && filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Account" && filterContext.ActionDescriptor.ActionName == "FirstUser")
            {
                filterContext.Result = new RedirectResult(Url.Action("Index", "Home"));
                return;
            }
        }
    }
}