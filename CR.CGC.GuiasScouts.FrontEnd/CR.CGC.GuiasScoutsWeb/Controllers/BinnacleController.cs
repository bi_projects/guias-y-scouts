﻿using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Pluralize.NET;
using static System.Net.HttpStatusCode;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    [IsMenu(5, "Bitácora", "icon_bitacora", null, "caption_binnacle.jpg", "Esta es la descripción para Bitácora. Solamente es un texto de prueba, recordar cambiarlo.", "Binnacle")]
    public class BinnacleController : ApplicationController
    {
        private static string _BaseUrl = System.Configuration.ConfigurationManager.AppSettings["WebApi"].ToString();
        private IApplicationHelper _helper = new ApplicationHelper(_BaseUrl);

        #region Information

        [IsMenu(1, "Información", null, null, null, null, "Information")]
        [Route("Binnacle/Information/Index")]
        public ActionResult InformationIndex()
        {
            return RedirectToAction("Information");
        }

        [Route("Binnacle/Information/InformationList")]
        [IsPermission("Bitácora", PermissionTypes.Other, "Bitácora de Miembro", "InformationList")]
        public ActionResult Information()
        {
            return View("Information/Information");
        }

        public ActionResult GetGridInformationBinnacle(int Number = 1)
        {
            try
            {
                CustomSession.CurrentSession.ListContainer = (ERContainer)_helper.CreateGetAsync<ERContainer>("Members/List/ForMembers", new Dictionary<string, object>());
            }
            catch (Exception ex)
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ex.Message;
                    return Json(new { ResponseCode = 500, ResponseText = ex.Message }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Request/_BinnacleItemPartial", new List<Logbook>());
            }

            var (ResponseCode, ResponseText, records, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<Logbook>>("Binnacle", "Logbook", Number, new[] { ("UserId", CustomSession.CurrentSession.CurrentUser.Member.User.UserId.ToString()), ("LogTypeMember", true.ToString()), ("LogTypeUser", true.ToString()) });
            if (ResponseCode == OK)
            {
                records = records.Where(x => x.LogType.Name == BinnacleConstants.MemberTypeName || x.LogType.Name == BinnacleConstants.UserTypeName).ToList();
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, records, "~/Views/Binnacle/Information/_BinnacleItemPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Information/_BinnacleItemPartial", records);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Information/_BinnacleItemPartial", new List<Logbook>());
            }

        }

        #endregion

        #region Activity

        [IsMenu(2, "Actividades", null, null, null, null, "Activity")]
        [Route("Binnacle/Activity/Index")]
        public ActionResult ActivityIndex()
        {
            return RedirectToAction("Activity");
        }

        [Route("Binnacle/Activity/ActivityList")]
        [IsPermission("Bitácora", PermissionTypes.Other, "Bitácora de Actividades", "ActivityList")]
        public ActionResult Activity()
        {
            return View("Activity/Activity");
        }

        public ActionResult GetGridBinnacle(int Number = 1)
        {
            ViewBag.PageNumber = Number;
            var (ResponseCode, ResponseText, events) = _helper.GetEntitiesList<List<Event>>("Activities", "Event");
            if (ResponseCode == OK)
            {
                events.ForEach(x => { x.EventAssessments = x.EventAssessments.Where(y => y.Enabled).ToList(); });
                ViewBag.EventList = events;
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else ViewBag.EventList = new List<Event>();
            }

            List<Logbook> records;
            int RowCount, NumberPage;
            (ResponseCode, ResponseText, records, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<Logbook>>("Binnacle", "Logbook", Number, new[] { ("UserId", CustomSession.CurrentSession.CurrentUser.Member.User.UserId.ToString()), ("LogTypePublication", true.ToString()), ("LogTypeActivity", true.ToString()) });
            if (ResponseCode == OK)
            {
                records = records.Where(x => x.LogType.Name == BinnacleConstants.PublicationTypeName || x.LogType.Name == BinnacleConstants.ActivityTypeName).ToList();
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, records, "~/Views/Binnacle/Activity/_BinnacleItemPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Activity/_BinnacleItemPartial", records);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Activity/_BinnacleItemPartial", new List<Logbook>());
            }

        }

        #endregion

        #region Request

        [IsMenu(3, "Solicitud", null, null, null, null, "Request")]
        [Route("Binnacle/Request/Index")]
        public ActionResult RequestIndex()
        {
            return RedirectToAction("Requests");
        }

        [Route("Binnacle/Request/RequestList")]
        [IsPermission("Bitácora", PermissionTypes.Other, "Bitácora de Solicitudes", "RequestList")]
        public ActionResult Requests()
        {
            return View("Request/Request");
        }

        public ActionResult GetGridRequestBinnacle(int Number = 1)
        {
            try
            {
                CustomSession.CurrentSession.ListContainer = (ERContainer)_helper.CreateGetAsync<ERContainer>("Members/List/ForMembers", new Dictionary<string, object>());
            }
            catch (Exception ex)
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ex.Message;
                    return Json(new { ResponseCode = 500, ResponseText = ex.Message }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Request/_BinnacleItemPartial", new List<Logbook>());
            }

            var (ResponseCode, ResponseText, records, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<Logbook>>("Binnacle", "Logbook", Number, new[] { ("UserId", CustomSession.CurrentSession.CurrentUser.Member.User.UserId.ToString()), ("LogTypeRequest", true.ToString()) });
            if (ResponseCode == OK)
            {
                records = records.Where(x => x.LogType.Name == BinnacleConstants.RequestTypeName).ToList();
                records.ForEach(x =>
                {
                    x.Description = string.Empty;
                    if (x.LogbookDetails != null && x.LogbookDetails.Count > 0)
                    {
                        if (x.LogbookDetails.First().NewData != null && x.LogbookDetails.First().NewData.Requests.Count > 0)
                        {
                            x.Description = x.LogbookDetails.First().NewData.Requests.First().Name ?? string.Empty;
                        }
                    }
                });
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, records, "~/Views/Binnacle/Request/_BinnacleItemPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Request/_BinnacleItemPartial", records);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Request/_BinnacleItemPartial", new List<Logbook>());
            }

        }

        #endregion

        #region Administrador

        [IsMenu(4, "Administrador", null, null, null, null, "Administrator")]
        [Route("Binnacle/Administrator/Index")]
        public ActionResult AdministratorIndex()
        {
            return RedirectToAction("Administrator");
        }

        [Route("Binnacle/Administrator/AdministratorList")]
        [IsPermission("Bitácora", PermissionTypes.Other, "Bitácora de Administrador", "AdministratorList")]
        public ActionResult Administrator()
        {
            return View("Administrator/Administrator");
        }

        public ActionResult GetGridAdministratorBinnacle(int Number = 1, DateTime? StartDate = null, DateTime? EndingDate = null)
        {
            try
            {
                CustomSession.CurrentSession.ListContainer = (ERContainer)_helper.CreateGetAsync<ERContainer>("Members/List/ForMembers", new Dictionary<string, object>());
            }
            catch (Exception ex)
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ex.Message;
                    return Json(new { ResponseCode = 500, ResponseText = ex.Message }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Administrator/_BinnacleItemPartial", new List<Logbook>());
            }

            if (StartDate != null) StartDate = new DateTime(((DateTime)StartDate).Year, ((DateTime)StartDate).Month, ((DateTime)StartDate).Day, 0, 0, 0);
            if (EndingDate != null) EndingDate = new DateTime(((DateTime)EndingDate).Year, ((DateTime)EndingDate).Month, ((DateTime)EndingDate).Day, 23, 59, 59);
            var (ResponseCode, ResponseText, records, RowCount, NumberPage) = _helper.GetCustomEntitiesByParamsPag<List<Logbook>>("Binnacle/List/ForAdmin", "Logbook", Number, StartDate == null ? new(string, string)[0] : new[] { ("StartDate", (StartDate == null ? null : StartDate.ToString())), ("EndingDate", (EndingDate == null ? null : EndingDate.ToString())) });
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, records, "~/Views/Binnacle/Administrator/_BinnacleItemPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Administrator/_BinnacleItemPartial", records);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Administrator/_BinnacleItemPartial", new List<Logbook>());
            }

        }

        #endregion

        public ActionResult GetDetailItemBinnacle(LogbookDetail item, bool admin = false, Logbook record = null)
        {
            ViewBag.Record = record;
            try
            {
                ERContainer listContainer = CustomSession.CurrentSession.ListContainer;

                List<string> properties = new List<string>();
                List<ItemBinnacleViewModel> model = new List<ItemBinnacleViewModel>();

                ItemBinnacleViewModel insertModel = null;
                object NewData = ((IList)GetValue(item.NewData, new Pluralizer().Pluralize(item.Entity)))[0];
                object OldData = null;
                if (item.OldData != null)
                {
                    OldData = ((IList)GetValue(item.OldData, new Pluralizer().Pluralize(item.Entity)))[0];
                    if (GetValue(OldData, OldData.GetType().GetProperties().Where(x => Attribute.IsDefined(x, typeof(IgnoreBinnaclePropertyAttribute))).First().Name) == null) OldData = null;
                }
                if (OldData == null)
                {
                    insertModel = new ItemBinnacleViewModel()
                    {
                        ItemType = ItemBinnacleConstants.ExternalType,
                        DisplayName = BinnacleConstants.GetBinnacleEntityName(item.Entity),
                        IsExternal = true,
                        ActionType = BinnacleConstants.Insert
                    };
                }

                NewData.GetType().GetProperties().ToList().Where(x => IsComparableProperty(x)).ToList().ForEach(x => { properties.Add(x.Name); });

                foreach (var prop in properties)
                {
                    if (item.ActionType == BinnacleConstants.Update && OldData != null)
                    {
                        if (GetType(NewData, prop).Name != "List`1")
                        {
                            if (IsComparableProperty(NewData, prop))
                            {
                                if ((GetValue(NewData, prop) != null && !GetValue(NewData, prop).Equals(GetValue(OldData, prop))) || (GetValue(OldData, prop) != null && !(GetValue(OldData, prop).Equals(GetValue(NewData, prop)))))
                                {
                                    if ((GetValue(NewData, prop) != null && GetValue(NewData, prop).GetType() == typeof(DateTime)) || (GetValue(OldData, prop) != null && GetValue(OldData, prop).GetType() == typeof(DateTime)))
                                    {
                                        model.Add(new ItemBinnacleViewModel()
                                        {
                                            ItemType = ItemBinnacleConstants.DefaultType,
                                            DisplayName = GetComparableTag(NewData, prop),
                                            OldData = GetValue(OldData, prop) == null ? "__-__-____" : ((DateTime)GetValue(OldData, prop)).ToString("dd-MM-yyyy"),
                                            NewData = GetValue(NewData, prop) == null ? "__-__-____" : ((DateTime)GetValue(NewData, prop)).ToString("dd-MM-yyyy"),
                                            ActionType = BinnacleConstants.Update
                                        });
                                    }
                                    else
                                    {
                                        var modelItem = new ItemBinnacleViewModel()
                                        {
                                            DisplayName = GetComparableTag(NewData, prop),
                                            OldData = GetValue(OldData, prop) == null ? "---" : GetValue(OldData, prop).ToString(),
                                            NewData = GetValue(NewData, prop) == null ? "---" : GetValue(NewData, prop).ToString(),
                                            ActionType = BinnacleConstants.Update
                                        };

                                        if (IsForeign(NewData, prop))
                                        {
                                            modelItem = SetForeignObject(NewData, prop, modelItem, listContainer);

                                            if (GetValue(OldData, prop) != null)
                                            {
                                                modelItem = SetForeignObject(OldData, prop, modelItem, listContainer, true);
                                            }
                                        }

                                        if (Attribute.IsDefined(NewData.GetType().GetProperty(prop), typeof(ImageFilePropertyAttribute))) modelItem.ItemType = ItemBinnacleConstants.ImageType;
                                        else if (Attribute.IsDefined(NewData.GetType().GetProperty(prop), typeof(AttachmentFilePropertyAttribute))) modelItem.ItemType = ItemBinnacleConstants.FileType;
                                        else modelItem.ItemType = ItemBinnacleConstants.DefaultType;
                                        model.Add(modelItem);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (IsComparableProperty(NewData, prop) && GetValue(NewData, prop) != null)
                            {
                                foreach (var detail in NewData.GetType().GetProperty(prop).GetValue(NewData) as IList)
                                {
                                    var modelItem = new ItemBinnacleViewModel()
                                    {
                                        ItemType = ItemBinnacleConstants.ExternalType,
                                        DisplayName = ((DisplayNameBinnaclePropertyAttribute)NewData.GetType().GetProperty(prop).GetCustomAttribute(typeof(DisplayNameBinnaclePropertyAttribute))).DisplayName,
                                        IsExternal = true
                                    };

                                    foreach (var subDetail in detail.GetType().GetProperties().Where(sd => Attribute.IsDefined(sd, typeof(DisplayNameBinnaclePropertyAttribute))).ToList())
                                    {
                                        var modelSubItem = new ItemBinnacleViewModel()
                                        {
                                            DisplayName = ((DisplayNameBinnaclePropertyAttribute)subDetail.GetCustomAttribute(typeof(DisplayNameBinnaclePropertyAttribute))).DisplayName,
                                            IsImageItem = Attribute.IsDefined(subDetail, typeof(ImageFilePropertyAttribute)),
                                            NewData = detail.GetType().GetProperty(subDetail.Name).GetValue(detail).GetType() == typeof(DateTime) ? ((DateTime)detail.GetType().GetProperty(subDetail.Name).GetValue(detail)).ToString("dd-MM-yyyy") : detail.GetType().GetProperty(subDetail.Name).GetValue(detail).ToString(),
                                        };

                                        if (subDetail.GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute)) != null)
                                        {
                                            if (detail.GetType().GetProperty(((DisplayForeignBinnaclePropertyAttribute)subDetail.GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute))).DisplayObject).GetValue(detail) != null)
                                            {
                                                var foreignObjDet = detail.GetType().GetProperty(((DisplayForeignBinnaclePropertyAttribute)subDetail.GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute))).DisplayObject).GetValue(detail);
                                                modelSubItem.NewData = foreignObjDet.GetType().GetProperty(((DisplayForeignBinnaclePropertyAttribute)subDetail.GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute))).DisplayField).GetValue(foreignObjDet).ToString();
                                            }
                                            else
                                            {
                                                foreach (var ForeignItemDet in listContainer.GetType().GetProperty(((DisplayForeignBinnaclePropertyAttribute)subDetail.GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute))).ContainerObject).GetValue(listContainer) as IList)
                                                {
                                                    if (modelSubItem.NewData == ForeignItemDet.GetType().GetProperty(((DisplayForeignBinnaclePropertyAttribute)subDetail.GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute))).ConteinerObjectId).GetValue(ForeignItemDet).ToString())
                                                    {
                                                        modelSubItem.NewData = ForeignItemDet.GetType().GetProperty(((DisplayForeignBinnaclePropertyAttribute)subDetail.GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute))).DisplayField).GetValue(ForeignItemDet).ToString();
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        if (Attribute.IsDefined(subDetail, typeof(ImageFilePropertyAttribute))) modelSubItem.ItemType = ItemBinnacleConstants.ImageType;
                                        else if (Attribute.IsDefined(subDetail, typeof(AttachmentFilePropertyAttribute))) modelSubItem.ItemType = ItemBinnacleConstants.FileType;
                                        else modelSubItem.ItemType = ItemBinnacleConstants.DefaultType;

                                        modelItem.Details.Add(modelSubItem);
                                    }

                                    if ((bool)detail.GetType().GetProperty("Enabled").GetValue(detail))
                                    {
                                        if (OldData.GetType().GetProperty(prop).GetValue(OldData) == null) modelItem.ActionType = BinnacleConstants.Insert;
                                        else
                                        {
                                            bool comparatorFlag = true;
                                            foreach (var comparatorDetail in OldData.GetType().GetProperty(prop).GetValue(OldData) as IList)
                                            {
                                                if (comparatorDetail.GetType().GetProperties().Where(cd => Attribute.IsDefined(cd, typeof(ComparatorFieldBinnaclePropertyAttribute))).First().GetValue(comparatorDetail) != null &&
                                                    comparatorDetail.GetType().GetProperties().Where(cd => Attribute.IsDefined(cd, typeof(ComparatorFieldBinnaclePropertyAttribute))).First().GetValue(comparatorDetail).ToString() == detail.GetType().GetProperties().Where(cd => Attribute.IsDefined(cd, typeof(ComparatorFieldBinnaclePropertyAttribute))).First().GetValue(detail).ToString())
                                                {
                                                    modelItem.ActionType = BinnacleConstants.Update;
                                                    comparatorFlag = false;
                                                    break;
                                                }
                                            }
                                            if (comparatorFlag) modelItem.ActionType = BinnacleConstants.Insert;
                                        }
                                    }
                                    else
                                    {
                                        modelItem.ActionType = BinnacleConstants.Delete;
                                    }

                                    model.Add(modelItem);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (IsComparableProperty(NewData, prop) && GetType(NewData, prop).Name != "List`1" && GetValue(NewData, prop) != null)
                        {
                            if (GetValue(NewData, prop).GetType() == typeof(DateTime))
                            {
                                model.Add(new ItemBinnacleViewModel()
                                {
                                    ItemType = ItemBinnacleConstants.DefaultType,
                                    DisplayName = GetComparableTag(NewData, prop),
                                    NewData = GetValue(NewData, prop) == null ? "__-__-____" : ((DateTime)GetValue(NewData, prop)).ToString("dd-MM-yyyy"),
                                });
                            }
                            else
                            {
                                if (IsDefaultProperty(NewData, prop))
                                {
                                    var modelItem = new ItemBinnacleViewModel()
                                    {
                                        ItemType = ItemBinnacleConstants.DefaultType,
                                        DisplayName = GetComparableTag(NewData, prop),
                                        NewData = GetValue(NewData, prop) == null ? "---" : GetValue(NewData, prop).ToString(),
                                    };

                                    if (IsForeign(NewData, prop))
                                    {
                                        modelItem = SetForeignObject(NewData, prop, modelItem, listContainer);
                                    }
                                    insertModel.Details.Add(modelItem);
                                }
                            }
                        }
                    }
                }

                if (insertModel != null) model.Add(insertModel);

                return PartialView((admin ? "Administrator/_BinnacleDetailItemPartial" : "_BinnacleDetailItemPartial"), model);
            }
            catch (Exception ex)
            {
                return PartialView((admin ? "Administrator/_BinnacleDetailItemPartial" : "_BinnacleDetailItemPartial"), new List<ItemBinnacleViewModel>());
            }
        }

        #region Util

        private object GetValue(object Obj, string Property)
        {
            return Obj.GetType().GetProperty(Property).GetValue(Obj);
        }

        private Type GetType(object Obj, string Property)
        {
            return Obj.GetType().GetProperty(Property).PropertyType;
        }

        private bool IsComparableProperty(PropertyInfo Prop)
        {
            return !Attribute.IsDefined(Prop, typeof(IgnorePropertyAttribute)) && !Attribute.IsDefined(Prop, typeof(IgnoreBinnaclePropertyAttribute));
        }

        private bool IsComparableProperty(object Obj, string Property)
        {
            return Obj.GetType().GetProperty(Property).GetCustomAttribute(typeof(DisplayNameBinnaclePropertyAttribute)) != null;
        }

        private bool IsDefaultProperty(object Obj, string Property)
        {
            return !Attribute.IsDefined(Obj.GetType().GetProperty(Property), typeof(ImageFilePropertyAttribute)) && !Attribute.IsDefined(Obj.GetType().GetProperty(Property), typeof(AttachmentFilePropertyAttribute));
        }

        private string GetComparableTag(object Obj, string Property)
        {
            return ((DisplayNameBinnaclePropertyAttribute)Obj.GetType().GetProperty(Property).GetCustomAttribute(typeof(DisplayNameBinnaclePropertyAttribute))).DisplayName;
        }

        private bool IsForeign(object Obj, string Property)
        {
            return Attribute.IsDefined(Obj.GetType().GetProperty(Property), typeof(DisplayForeignBinnaclePropertyAttribute));
        }

        private string GetForeignTag(object Obj, string Property)
        {
            return ((DisplayForeignBinnaclePropertyAttribute)Obj.GetType().GetProperty(Property).GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute))).DisplayObject;
        }

        private string GetForeignFieldTag(object Obj, string Property)
        {
            return ((DisplayForeignBinnaclePropertyAttribute)Obj.GetType().GetProperty(Property).GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute))).DisplayField;
        }

        private string GetForeignContainerTag(object Obj, string Property)
        {
            return ((DisplayForeignBinnaclePropertyAttribute)Obj.GetType().GetProperty(Property).GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute))).ContainerObject;
        }

        private string GetForeignId(object Obj, string Property)
        {
            return ((DisplayForeignBinnaclePropertyAttribute)Obj.GetType().GetProperty(Property).GetCustomAttribute(typeof(DisplayForeignBinnaclePropertyAttribute))).ConteinerObjectId;
        }

        private ItemBinnacleViewModel SetForeignObject(object Data, string Property, ItemBinnacleViewModel Model, ERContainer ListContainer, bool OldData = false)
        {
            if (GetValue(Data, GetForeignTag(Data, Property)) != null)
            {
                var foreignObj = GetValue(Data, GetForeignTag(Data, Property));

                if (!OldData)
                {
                    if (Model.NewData == GetValue(foreignObj, GetForeignId(Data, Property)).ToString())
                    {
                        Model.NewData = GetValue(foreignObj, GetForeignFieldTag(Data, Property)).ToString();
                        return Model;
                    }
                }
                else
                {
                    if (Model.OldData == GetValue(foreignObj, GetForeignId(Data, Property)).ToString())
                    {
                        Model.OldData = GetValue(foreignObj, GetForeignFieldTag(Data, Property)).ToString();
                        return Model;
                    }
                }
            }

            foreach (var ForeignItem in GetValue(ListContainer, GetForeignContainerTag(Data, Property)) as IList)
            {
                if (!OldData)
                {
                    if (Model.NewData == GetValue(ForeignItem, GetForeignId(Data, Property)).ToString())
                    {
                        Model.NewData = GetValue(ForeignItem, GetForeignFieldTag(Data, Property)).ToString();
                        break;
                    }
                }
                else
                {
                    if (Model.OldData == GetValue(ForeignItem, GetForeignId(Data, Property)).ToString())
                    {
                        Model.OldData = GetValue(ForeignItem, GetForeignFieldTag(Data, Property)).ToString();
                        break;
                    }
                }
            }

            return Model;
        }

        #endregion

    }
}