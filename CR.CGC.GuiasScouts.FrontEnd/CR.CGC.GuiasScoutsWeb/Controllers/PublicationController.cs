﻿using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using static System.Net.HttpStatusCode;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    [IsMenu(1, "Publicaciones", "icon_publicaciones", null, "caption_publication.jpg", "Esta es la descripción para Publicaciones. Solamente es un texto de prueba, recordar cambiarlo.", "Publication")]
    public class PublicationController : ApplicationController
    {
        private static string _BaseUrl = System.Configuration.ConfigurationManager.AppSettings["WebApi"].ToString();
        private IApplicationHelper _helper = new ApplicationHelper(_BaseUrl);

        #region Subscriptions

        [IsMenu(1, "Suscripciones", null, null, null, null, "Subscriptions")]
        [Route("Publication/Subscriptions/Index")]
        public ActionResult SubscriptionsIndex()
        {
            return RedirectToAction("Subscriptions");
        }

        [Route("Publication/Subscriptions/Dashboard")]
        [IsPermission("Suscripción", PermissionTypes.List, null, "SubscriptionList")]
        public ActionResult Subscriptions(string SearchText = null)
        {
            System.Net.HttpStatusCode ResponseCode;
            string ResponseText;
            List<Publication> publications;

            if (SearchText == null)
                (ResponseCode, ResponseText, publications) = _helper.GetEntitiesByParams<List<Publication>>("Publications", "Publication", new[] { ("CurrentUser", CustomSession.CurrentSession.CurrentUser.Member.User.UserId.ToString()) });
            else
            {
                (ResponseCode, ResponseText, publications) = _helper.GetEntitiesByParams<List<Publication>>("Publications", "Publication", new[] { ("SearchText", (SearchText ?? "")), ("CurrentUser", CustomSession.CurrentSession.CurrentUser.Member.User.UserId.ToString()) });
                ViewBag.SearchText = SearchText;
            }

            if (ResponseCode == OK)
            {
                int age = DateTime.Now.Date.AddTicks(-CustomSession.CurrentSession.CurrentUser.Member.BirthDate.Ticks).Year - 1;
                publications = publications.Where(x => x.InterestTheme.MemberInterestThemes.Select(s => s.MemberId).ToList().Contains((int)CustomSession.CurrentSession.CurrentUser.Member.MemberId) && x.Enabled && x.EndDate.Date >= DateTime.Now.Date).ToList();
                List<Publication> publicationsForUser = new List<Publication>();
                publications.ForEach(x =>
                {
                    x.PublicationAgeRanges = x.PublicationAgeRanges.Where(y => y.Enabled).ToList();
                    if (x.PublicationAgeRanges.Count > 0)
                    {
                        bool flag = false;
                        x.PublicationAgeRanges.ForEach(y => { if (y.AgeRange.MinimumAge <= age && age <= y.AgeRange.MaximumAge) flag = true; });
                        if (flag) publicationsForUser.Add(x);
                    }
                    else publicationsForUser.Add(x);
                });

                List<InterestTheme> themes;
                (ResponseCode, ResponseText, themes) = _helper.GetEntitiesList<List<InterestTheme>>("Publications", "InterestTheme");
                if (ResponseCode == OK)
                {
                    themes = ShuffleList(themes.Where(x => !(x.MemberInterestThemes.Select(s => s.MemberId).ToList().Contains((int)CustomSession.CurrentSession.CurrentUser.Member.MemberId)) && x.Enabled).ToList());
                    ViewBag.listThemes = themes.Take(5).ToList();
                }
                else
                    ViewBag.listThemes = new List<InterestTheme>();

                return View("Subscriptions/Dashboard", publicationsForUser);
            }
            else
            {
                return RedirectToAction("ProfileView", "Configuration");
            }
        }

        [Route("Publication/Subscriptions/Content")]
        [IsPermission("Suscripción", PermissionTypes.Other, "Ver contenido de Publicaciones", "SubscriptionViewContent")]
        public async Task<ActionResult> SubscriptionContent(int PublicationId = 0)
        {
            if (PublicationId <= 0) return Subscriptions();

            var (ResponseCode, ResponseText, Publication) = _helper.GetEntity<Publication>("Publications", "Publication", "PublicationId", (int)PublicationId);
            if (ResponseCode == OK)
            {
                Publication.ViewsNumber = Publication.ViewsNumber + 1;
                ERContainer container = new ERContainer()
                {
                    Publications = new List<Publication>() { Publication },
                    CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId,
                    Model = "Publication"
                };
                List<Publication> publications;
                (ResponseCode, ResponseText, publications) = await _helper.CreatePostGetElement<List<Publication>>("Publications/Model/NewObject", container);
                if (ResponseCode == OK)
                    Publication = publications.FirstOrDefault();
                else
                    return RedirectToAction("Subscriptions");

                (ResponseCode, ResponseText, publications) = _helper.GetEntitiesList<List<Publication>>("Publications", "Publication");
                if (ResponseCode == OK)
                {
                    int age = DateTime.Now.Date.AddTicks(-CustomSession.CurrentSession.CurrentUser.Member.BirthDate.Ticks).Year - 1;
                    publications = publications.Where(x => x.Enabled && x.InterestThemeId == Publication.InterestThemeId && x.PublicationId != Publication.PublicationId && x.EndDate.Date >= DateTime.Now.Date).ToList();
                    List<Publication> publicationsForUser = new List<Publication>();
                    publications.ForEach(x =>
                    {
                        x.PublicationAgeRanges = x.PublicationAgeRanges.Where(y => y.Enabled).ToList();
                        if (x.PublicationAgeRanges.Count > 0)
                        {
                            bool flag = false;
                            x.PublicationAgeRanges.ForEach(y => { if (y.AgeRange.MinimumAge <= age && age <= y.AgeRange.MaximumAge) flag = true; });
                            if (flag) publicationsForUser.Add(x);
                        }
                        else publicationsForUser.Add(x);
                    });
                    ViewBag.listPublications = ShuffleList(publicationsForUser);
                }
                else
                    ViewBag.listPublications = new List<Publication>();

                List<InterestTheme> themes;
                (ResponseCode, ResponseText, themes) = _helper.GetEntitiesList<List<InterestTheme>>("Publications", "InterestTheme");
                if (ResponseCode == OK) {
                    themes = ShuffleList(themes.Where(x => !(x.MemberInterestThemes.Select(s => s.MemberId).ToList().Contains((int)CustomSession.CurrentSession.CurrentUser.Member.MemberId)) && x.Enabled).ToList());
                    ViewBag.listThemes = themes.Take(5).ToList();
                }
                else
                    ViewBag.listThemes = new List<InterestTheme>();

                Publication.PublicationAgeRanges = Publication.PublicationAgeRanges.Where(x => x.Enabled).ToList();
                Publication.PublicationFiles = Publication.PublicationFiles.Where(x => x.Enabled).ToList();
                Publication.Body = Publication.Body.Replace("'", "\"");
                return View("Subscriptions/Content", Publication);
            }
            else
            {
                return RedirectToAction("Subscriptions");
            }
        }

        [Route("Publication/Subscriptions/Theme")]
        [IsPermission("Suscripción", PermissionTypes.Other, "Ver publicaciones de un Tema de Interés", "SubscriptionViewTheme")]
        public ActionResult SubscriptionTheme(int InterestThemeId = 0, string SearchText = null)
        {
            if (InterestThemeId <= 0) return Subscriptions();

            var (ResponseCode, ResponseText, Theme) = _helper.GetEntity<InterestTheme>("Publications", "InterestTheme", "InterestThemeId", InterestThemeId);
            if (ResponseCode == OK)
            {
                List<Publication> publications;
                if (SearchText == null)
                    (ResponseCode, ResponseText, publications) = _helper.GetEntitiesList<List<Publication>>("Publications", "Publication");
                else
                {
                    (ResponseCode, ResponseText, publications) = _helper.GetEntitiesByParams<List<Publication>>("Publications", "Publication", new[] { ("SearchText", (SearchText ?? "")) });
                    ViewBag.SearchText = SearchText;
                }

                if (ResponseCode == OK)
                {
                    int age = DateTime.Now.Date.AddTicks(-CustomSession.CurrentSession.CurrentUser.Member.BirthDate.Ticks).Year - 1;
                    publications = publications.Where(x => x.Enabled && x.InterestThemeId == Theme.InterestThemeId && x.EndDate.Date >= DateTime.Now.Date).ToList();
                    List<Publication> publicationsForUser = new List<Publication>();
                    publications.ForEach(x =>
                    {
                        x.PublicationAgeRanges = x.PublicationAgeRanges.Where(y => y.Enabled).ToList();
                        if (x.PublicationAgeRanges.Count > 0)
                        {
                            bool flag = false;
                            x.PublicationAgeRanges.ForEach(y => { if (y.AgeRange.MinimumAge <= age && age <= y.AgeRange.MaximumAge) flag = true; });
                            if (flag) publicationsForUser.Add(x);
                        }
                        else publicationsForUser.Add(x);
                    });

                    ViewBag.listPublications = publicationsForUser;
                }
                else ViewBag.listPublications = new List<Publication>();

                return View("Subscriptions/Theme", Theme);
            }
            else
            {
                return RedirectToAction("Subscriptions");
            }
        }

        [HttpPost]
        public async Task<ActionResult> SavePublicationAssessment(int PublicationId, decimal Assessment)
        {
            if (PublicationId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            PublicationAssessment assessment = new PublicationAssessment()
            {
                PublicationAssessmentId = 0,
                MemberId = (int)CustomSession.CurrentSession.CurrentUser.Member.MemberId,
                PublicationId = PublicationId,
                Assessment = Decimal.Round(Assessment, 1),
                Enabled = true
            };

            ERContainer container = new ERContainer() { PublicationAssessments = new List<PublicationAssessment>() { assessment }, Model = "PublicationAssessment", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Members/Model/NewObject", container);

            if (ResponseCode == OK)
            {
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText = "Guardado exitosamente." });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        #endregion

        #region Records

        [IsMenu(2, "Registros", null, null, null, null, "Records")]
        [Route("Publication/Records/Index")]
        public ActionResult RecordsIndex()
        {
            return RedirectToAction("Publications");
        }

        #region Publications

        [IsMenu(1, "Lista de Publicaciones", null, "Registros", null, null, "PublicationList")]
        [Route("Publication/Records/PublicationList")]
        [IsPermission("Publicación", PermissionTypes.List, null, "PublicationList")]
        public ActionResult Publications()
        {
            return View("Records/Publications/Index");
        }

        public ActionResult GetGridPublications(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, publications, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Publication>>("Publications", "Publication", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                publications.ForEach(x => { x.Body.Replace("'", "\""); });
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, publications, "~/Views/Publication/Records/Publications/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Records/Publications/_GridPartial", publications);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Records/Publications/_GridPartial", new List<Publication>());
            }
        }

        [Route("Publication/Records/NewPublication")]
        [IsPermission("Publicación", PermissionTypes.New, null, "PublicationCreation")]
        public ActionResult NewPublication(int? PublicationId)
        {
            var (ListResponseCode, ListResponseText, ListThemes) = _helper.GetEntitiesList<List<InterestTheme>>("Publications", "InterestTheme");
            if (ListResponseCode == OK) ViewBag.ThemesList = new SelectList(ListThemes.Where(x => x.Enabled).ToList(), "InterestThemeId", "Name");
            else ViewBag.ThemesList = new SelectList(new List<InterestTheme>(), "InterestThemeId", "Name");

            List<AgeRange> ListAgeRanges;
            (ListResponseCode, ListResponseText, ListAgeRanges) = _helper.GetEntitiesList<List<AgeRange>>("Config", "AgeRange");
            if (ListResponseCode == OK)
            {
                foreach (var item in ListAgeRanges) item.Name = item.Name + " / " + item.MinimumAge.ToString() + "-" + item.MaximumAge.ToString();
                ViewBag.AgeRangesList = ListAgeRanges.Where(x => x.Enabled).ToList();
            }
            else
                ViewBag.AgeRangesList = new List<AgeRange>();

            if (PublicationId == null)
            {
                return View("Records/Publications/Create", new Publication());
            }
            else if (PublicationId == 0)
            {
                return View("Records/Publications/Create", new Publication());
            }
            else
            {
                var (ResponseCode, ResponseText, Publication) = _helper.GetEntity<Publication>("Publications", "Publication", "PublicationId", (int)PublicationId);
                if (ResponseCode == OK)
                {
                    Publication.PublicationAgeRanges = Publication.PublicationAgeRanges.Where(x => x.Enabled).ToList();
                    Publication.PublicationFiles = Publication.PublicationFiles.Where(x => x.Enabled).ToList();
                    Publication.Body = Publication.Body.Replace("'", "\"");
                    return View("Records/Publications/Create", Publication);
                }
                else return View("Records/Publications/Create", new Publication());
            }
        }

        public ActionResult GetThemePartial(int InterestThemeId)
        {
            var (ListResponseCode, ListResponseText, Theme) = _helper.GetEntity<InterestTheme>("Publications", "InterestTheme", "InterestThemeId", InterestThemeId);
            var response = Utilities.RenderRazorViewToString(ControllerContext, Theme, "~/Views/Publication/Records/Publications/_ThemePartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPublicationAgeRangePartial(int index)
        {
            var (ListResponseCode, ListResponseText, ListAge) = _helper.GetEntitiesList<List<AgeRange>>("Config", "AgeRange");
            if (ListResponseCode == OK)
            {
                foreach (var item in ListAge) item.Name = item.Name + " / " + item.MinimumAge.ToString() + "-" + item.MaximumAge.ToString();
                ViewBag.AgeRangesList = new SelectList(ListAge.Where(x => x.Enabled).ToList(), "AgeRangeId", "Name");
            }
            else ViewBag.AgeRangesList = new SelectList(new List<AgeRange>(), "AgeRangeId", "Name");

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Publication/Records/Publications/_PublicationAgeRangePartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [Route("Publication/Records/PublicationDetails")]
        [IsPermission("Publicación", PermissionTypes.View, null, "PublicationView")]
        public ActionResult PublicationDetails(int PublicationId = 0)
        {
            if (PublicationId <= 0)
            {
                return Publications();
            }
            else
            {
                var (ResponseCode, ResponseText, publication) = _helper.GetEntity<Publication>("Publications", "Publication", "PublicationId", (int)PublicationId);
                if (ResponseCode == OK)
                {
                    publication.PublicationAgeRanges = publication.PublicationAgeRanges.Where(x => x.Enabled).ToList();
                    publication.PublicationFiles = publication.PublicationFiles.Where(x => x.Enabled).ToList();
                    publication.Body = publication.Body.Replace("'", "\"");
                    return View("Records/Publications/Details", publication);
                }
                else
                    return Publications();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> SavePublication(Publication publication)
        {
            //if (!ModelState.IsValid)
            //{
            //    Response.StatusCode = 400;
            //    Response.StatusDescription = "Datos no válidos.";
            //    return Json(new { ResponseCode = 500, ResponseText = "Datos no válidos." });
            //}
            publication.PublicationSendFiles = publication.PublicationSendFiles.Where(x => x != null).ToList();

            if (publication.PublicationId == null || publication.PublicationId == 0)
            {
                publication.PublicationAgeRanges.ForEach(x => x.Enabled = true);

                if (publication.PublicationSendFiles.Count > 0)
                {
                    foreach (var item in publication.PublicationSendFiles)
                    {
                        Entities.File toSave = Utilities.SaveFile(item, "Publication", "Records");

                        publication.PublicationFiles.Add(new PublicationFile()
                        {
                            PublicationFileId = 0,
                            PublicationId = 0,
                            Name = toSave.FileName,
                            FileLocation = toSave.FileUrl,
                            Enabled = true,
                        });
                    }
                }
            }
            else
            {
                publication.PublicationAgeRanges.ForEach(x => x.PublicationId = (int)publication.PublicationId);

                var (ResponseCodeUpd, ResponseTextUpd, publicationUpd) = _helper.GetEntity<Publication>("Publications", "Publication", "PublicationId", (int)publication.PublicationId);

                if (ResponseCodeUpd == OK)
                {
                    publicationUpd.PublicationAgeRanges = publicationUpd.PublicationAgeRanges.Where(x => x.Enabled).ToList();
                    publication.PublicationAgeRanges = EditChildList<PublicationAgeRange>(publication.PublicationAgeRanges, publicationUpd.PublicationAgeRanges, "PublicationAgeRangeId", new[] { "AgeRangeId" });

                    List<int> exist = publication.PublicationFiles.Select(x => (int)x.PublicationFileId).ToList();
                    publicationUpd.PublicationFiles.ForEach(x => { if (!exist.Contains((int)x.PublicationFileId)) x.Enabled = false; });

                    if (publication.PublicationSendFiles.Count > 0)
                    {
                        foreach (var item in publication.PublicationSendFiles)
                        {
                            Entities.File toSave = Utilities.SaveFile(item, "Publication", "Records");

                            publicationUpd.PublicationFiles.Add(new PublicationFile()
                            {
                                PublicationFileId = 0,
                                PublicationId = (int)publication.PublicationId,
                                Name = toSave.FileName,
                                FileLocation = toSave.FileUrl,
                                Enabled = true,
                            });
                        }
                    }
                    publication.PublicationFiles = publicationUpd.PublicationFiles;
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseTextUpd;
                    return Json(new { ResponseCode = ResponseCodeUpd, ResponseText = ResponseTextUpd });
                }
            }

            if (publication.SendFile != null)
            {
                Entities.File toSave = Utilities.SaveFile(publication.SendFile, "Publication", "Records");
                publication.FileLocation = toSave.FileUrl;
            }

            publication.PublicationSendFiles = null;
            publication.Body = publication.Body.Replace("\"", "'");
            publication.SendFile = null;
            publication.Enabled = true;
            ERContainer container = new ERContainer() { Publications = new List<Publication>() { publication }, Model = "Publication", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText, Publication) = await _helper.CreatePostGetElement<List<Publication>>("Publications/Model/NewObject", container);

            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Publicación", PermissionTypes.Delete, null, "PublicationElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeletePublication(int? PublicationId, List<int> Publications, string Search, int Number = 1)
        {
            if (PublicationId == null && Publications == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (PublicationId != null)
            {
                var (ResponseCode, ResponseText, Publication) = _helper.GetEntity<Publication>("Publications", "Publication", "PublicationId", (int)PublicationId);
                if (ResponseCode == OK)
                {
                    Publication.Enabled = !Publication.Enabled;
                    ERContainer container = new ERContainer() { Publications = new List<Publication>() { Publication }, Model = "Publication", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Publications/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridPublications(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Publication>>("Publications", "Publication");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Publications.Contains((int)x.PublicationId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Publications = list, Model = "Publication", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridPublications(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Publications/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridPublications(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        #endregion

        #endregion

        #region Catalogs

        [IsMenu(3, "Catálogos", null, null, null, null, "Catalogs")]
        [Route("Publication/Catalogs/Index")]
        public ActionResult CatalogsIndex()
        {
            return RedirectToAction("InterestTheme");
        }

        #region InterestTheme option 1

        [IsMenu(1, "Temas de interés", null, "Catálogos", null, null, "InterestThemeList")]
        [Route("Publication/Catalogs/InterestThemeList")]
        [IsPermission("Tema de Interés", PermissionTypes.List, null, "InterestThemeList")]
        public ActionResult InterestTheme()
        {
            return View("Catalogs/InterestTheme/Index");
        }

        public ActionResult GetGridInterestTheme(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, InterestTheme, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<InterestTheme>>("Publications", "InterestTheme", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, InterestTheme, "~/Views/Publication/Catalogs/InterestTheme/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/InterestTheme/_GridPartial", InterestTheme);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/InterestTheme/_GridPartial", new List<InterestTheme>());
            }
        }

        [Route("Publication/Catalogs/InterestTheme/NewInterestTheme")]
        [IsPermission("Tema de Interés", PermissionTypes.New, null, "InterestThemeCreation")]
        public ActionResult NewInterestTheme(int? InterestThemeId)
        {
            if (InterestThemeId == null)
            {
                return View("Catalogs/InterestTheme/Create", new InterestTheme());
            }
            else if (InterestThemeId == 0)
            {
                return View("Catalogs/InterestTheme/Create", new InterestTheme());
            }
            else
            {
                var (ResponseCode, ResponseText, InterestTheme) = _helper.GetEntity<InterestTheme>("Publications", "InterestTheme", "InterestThemeId", (int)InterestThemeId);

                if (ResponseCode == OK)
                    return View("Catalogs/InterestTheme/Create", InterestTheme);
                else
                    return View("Catalogs/InterestTheme/Create", new InterestTheme());
            }
        }

        [Route("Publication/Catalogs/InterestTheme/InterestThemeDetails")]
        [IsPermission("Tema de Interés", PermissionTypes.View, null, "InterestThemeView")]
        public ActionResult InterestThemeDetails(int InterestThemeId = 0)
        {
            if (InterestThemeId <= 0)
            {
                return InterestTheme();
            }
            else
            {
                var (ResponseCode, ResponseText, interestTheme) = _helper.GetEntity<InterestTheme>("Publications", "InterestTheme", "InterestThemeId", (int)InterestThemeId);

                if (ResponseCode == OK)
                    return View("Catalogs/InterestTheme/Details", interestTheme);
                else
                    return InterestTheme();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveInterestTheme(InterestTheme Interest)
        {
            if (!ModelState.IsValid) return View(Interest);

            if (Interest.SendFile != null)
            {
                Entities.File toSave = Utilities.SaveFile(Interest.SendFile, "Publication", "Catalogs");
                Interest.FileLocation = toSave.FileUrl;
            }
            if (Interest.BackgroundSendFile != null)
            {
                Entities.File toSaveBG = Utilities.SaveFile(Interest.BackgroundSendFile, "Publication", "Catalogs");
                Interest.BackgroundFileLocation = toSaveBG.FileUrl;
            }

            Interest.SendFile = null;
            Interest.BackgroundSendFile = null;
            Interest.Enabled = true;
            ERContainer container = new ERContainer() { InterestThemes = new List<InterestTheme>() { Interest }, Model = "InterestTheme", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText, InterestTheme) = await _helper.CreatePostGetElement<List<InterestTheme>>("Publications/Model/NewObject", container);

            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Tema de Interés", PermissionTypes.Delete, null, "InterestThemeElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteInterestTheme(int? InterestThemeId, List<int> InterestThemes, string Search, int Number = 1)
        {
            if (InterestThemeId == null && InterestThemes == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (InterestThemeId != null)
            {
                var (ResponseCode, ResponseText, InterestTheme) = _helper.GetEntity<InterestTheme>("Publications", "InterestTheme", "InterestThemeId", (int)InterestThemeId);
                if (ResponseCode == OK)
                {
                    if (InterestTheme.MemberInterestThemes.Where(x => x.Enabled).ToList().Count > 0 && InterestTheme.Enabled)
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = "El tema de interés posee suscriptores.";
                        return Json(new { ResponseCode = 400, ResponseText = "El tema de interés posee suscriptores." });
                    }

                    InterestTheme.Enabled = !InterestTheme.Enabled;
                    ERContainer container = new ERContainer() { InterestThemes = new List<InterestTheme>() { InterestTheme }, Model = "InterestTheme", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Publications/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridInterestTheme(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<InterestTheme>>("Publications", "InterestTheme");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => InterestThemes.Contains((int)x.InterestThemeId)).ToList();
                    foreach (var item in list)
                    {
                        if (item.MemberInterestThemes.Where(x => x.Enabled).ToList().Count > 0 && item.Enabled)
                        {
                            Response.StatusCode = 400;
                            Response.StatusDescription = "El tema de interés " + item.Name + " posee suscriptores.";
                            return Json(new { ResponseCode = 400, ResponseText = "El tema de interés " + item.Name + " posee suscriptores." });
                        }
                        else
                        {
                            item.Enabled = !item.Enabled;
                        }
                    }

                    ERContainer container = new ERContainer() { InterestThemes = list, Model = "InterestTheme", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridInterestTheme(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Publications/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridInterestTheme(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }
        #endregion

        #endregion

    }
}