﻿using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using static System.Net.HttpStatusCode;

namespace CR.CGC.GuiasScoutsWeb.Controllers
{
    [IsMenu(3, "Actividades", "icon_actividades", null, "caption_activity.jpg", "Esta es la descripción para Actividades. Solamente es un texto de prueba, recordar cambiarlo.", "Activity")]
    public class ActivityController : ApplicationController
    {
        private static string _BaseUrl = System.Configuration.ConfigurationManager.AppSettings["WebApi"].ToString();
        private IApplicationHelper _helper = new ApplicationHelper(_BaseUrl);

        #region Events

        [IsMenu(1, "Eventos", null, null, null, null, "Events")]
        [Route("Activity/Events/Index")]
        public ActionResult EventsIndex()
        {
            return RedirectToAction("Attendances");
        }

        #region Attendance View

        [Route("Activity/Events/AttendancesList")]
        [IsPermission("Asistencia a Evento", PermissionTypes.List, null, "AttendancesList")]
        public ActionResult Attendances(string SearchText = null)
        {
            ViewBag.SearchText = SearchText;
            return View("Events/Attendances/Index");
        }

        public ActionResult GetGridAttendances(string SearchText = null, string TabId = null)
        {
            System.Net.HttpStatusCode ResponseCode;
            string ResponseText;
            List<Event> Events;

            if (SearchText == null)
                (ResponseCode, ResponseText, Events) = _helper.GetEntitiesList<List<Event>>("Activities", "Event");
            else
            {
                (ResponseCode, ResponseText, Events) = _helper.GetEntitiesByParams<List<Event>>("Activities", "Event", new[] { ("SearchText", (SearchText ?? "")) });
                ViewBag.SearchText = SearchText;
            }

            if (ResponseCode == OK)
            {
                int age = DateTime.Now.Date.AddTicks(-CustomSession.CurrentSession.CurrentUser.Member.BirthDate.Ticks).Year - 1;
                Events = Events.Where(x => x.Enabled).ToList();
                List<Event> eventsForUser = new List<Event>();
                Events.ForEach(x =>
                {
                    x.EventAgeRanges = x.EventAgeRanges.Where(y => y.Enabled).ToList();
                    if (x.EventAgeRanges.Count > 0)
                    {
                        bool flag = false;
                        x.EventAgeRanges.ForEach(y => { if (y.AgeRange.MinimumAge <= age && age <= y.AgeRange.MaximumAge) flag = true; });
                        if (flag) eventsForUser.Add(x);
                    }
                    else eventsForUser.Add(x);
                });

                List<MemberEvent> member;
                (ResponseCode, ResponseText, member) = _helper.GetEntitiesList<List<MemberEvent>>("Members", "MemberEvent");
                if (ResponseCode == OK)
                    ViewBag.ListIndividual = member.Where(x => x.MemberId == CustomSession.CurrentSession.CurrentUser.Member.MemberId && x.Enabled).Select(x => x.EventId).ToList();
                else
                    ViewBag.ListIndividual = new List<int>();

                ViewBag.TabId = TabId;
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, eventsForUser, "~/Views/Activity/Events/Attendances/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Events/Attendances/_GridPartial", eventsForUser);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ViewBag.ListIndividual = new List<int>();
                    return PartialView("Events/Attendances/_GridPartial", new List<Event>());
                }
            }

        }

        [Route("Activity/Events/AttendanceContent")]
        [IsPermission("Asistencia a Evento", PermissionTypes.View, null, "AttendanceDetail")]
        public ActionResult AttendanceDetails(int EventId)
        {
            if (EventId <= 0) return RedirectToAction("Attendances");

            var (ResponseCode, ResponseText, Event) = _helper.GetEntity<Event>("Activities", "Event", "EventId", EventId);

            if (ResponseCode == OK)
            {
                Event.EventAgeRanges = Event.EventAgeRanges.Where(x => x.Enabled).ToList();
                Event.EventRoles = Event.EventRoles.Where(x => x.Enabled).ToList();
                Event.MemberEvents = Event.MemberEvents.Where(x => x.Enabled).ToList();
                Event.EventAssessments = Event.EventAssessments.Where(x => x.Enabled).ToList();
                Event.Description = Event.Description.Replace("'", "\"");
                Event.EmailBody = Event.EmailBody.Replace("'", "\"");
                return View("Events/Attendances/Details", Event);
            }
            else return RedirectToAction("Events");
        }

        [IsPermission("Asistencia a Evento", PermissionTypes.New, null, "NewAttendance")]
        [HttpPost]
        public async Task<ActionResult> SaveAttendance(int EventId)
        {
            if (EventId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            MemberEvent Attendance = new MemberEvent()
            {
                MemberEventId = 0,
                MemberId = (int)CustomSession.CurrentSession.CurrentUser.Member.MemberId,
                EventId = EventId,
                Enabled = true
            };

            ERContainer container = new ERContainer() { MemberEvents = new List<MemberEvent>() { Attendance }, Model = "MemberEvent", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Activities/Model/NewObject", container);

            if (ResponseCode == OK)
            {
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText = "Guardado exitosamente." });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Asistencia a Evento", PermissionTypes.Delete, null, "AttendanceDelete")]
        [HttpDelete]
        public async Task<ActionResult> DeleteAttendance(int EventId)
        {
            if (EventId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            var (ResponseCode, ResponseText, attendances) = _helper.GetEntitiesList<List<MemberEvent>>("Activities", "MemberEvent");
            if (ResponseCode == OK)
            {
                MemberEvent attendance = attendances.Where(x => x.EventId == EventId && x.MemberId == CustomSession.CurrentSession.CurrentUser.Member.MemberId).FirstOrDefault();
                if (attendance != null)
                {
                    attendance.Enabled = false;
                    ERContainer container = new ERContainer() { MemberEvents = new List<MemberEvent>() { attendance }, Model = "MemberEvent", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Activities/Model/NewObject", container);
                    if (ResponseCode == OK)
                    {
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText = "Guardado exitosamente." });
                    }
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Ha ocurrido un error buscando el registro.";
                return Json(new { ResponseCode, ResponseText = "Ha ocurrido un error buscando el registro." });
            }

        }

        [HttpPost]
        public async Task<ActionResult> SaveEventAssessment(int EventId, decimal Assessment)
        {
            if (EventId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            EventAssessment assessment = new EventAssessment()
            {
                EventAssessmentId = 0,
                MemberId = (int)CustomSession.CurrentSession.CurrentUser.Member.MemberId,
                EventId = EventId,
                Assessment = Decimal.Round(Assessment, 1),
                Enabled = true
            };

            ERContainer container = new ERContainer() { EventAssessments = new List<EventAssessment>() { assessment }, Model = "EventAssessment", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Activities/Model/NewObject", container);

            if (ResponseCode == OK)
            {
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText = "Guardado exitosamente." });
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        #endregion

        #endregion

        #region Records

        [IsMenu(2, "Registros", null, null, null, null, "Records")]
        [Route("Activity/Records/Index")]
        public ActionResult RecordsIndex()
        {
            return RedirectToAction("Events");
        }

        #region Events

        [Route("Activity/Records/EventsList")]
        [IsPermission("Evento", PermissionTypes.List, null, "EventsList")]
        public ActionResult Events()
        {
            return View("Records/Events/Index");
        }

        public ActionResult GetGridEvents(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, events, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Event>>("Activities", "Event", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                events.ForEach(x => { x.Description.Replace("'", "\""); });
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, events, "~/Views/Activity/Records/Events/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Records/Events/_GridPartial", events);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Records/Events/_GridPartial", new List<Event>());
            }
        }

        [Route("Activity/Records/NewEvent")]
        [IsPermission("Evento", PermissionTypes.New, null, "EventsCreation")]
        public ActionResult NewEvent(int? EventId)
        {
            List<AgeRange> ListAge;
            List<Template> ListTemplate;
            List<EventStatus> ListStatus;
            List<Role> ListRole;
            var (ListResponseCode, ListResponseText, ListCatalog) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityScopeId.ToString()) });
            if (ListResponseCode == OK) ViewBag.ScopeList = new SelectList(ListCatalog.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.ScopeList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            (ListResponseCode, ListResponseText, ListCatalog) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", CustomSession.CurrentSession.CatalogConstants.ActivityTypeId.ToString()) });
            if (ListResponseCode == OK) ViewBag.ActivityTypeList = new SelectList(ListCatalog.Where(x => x.Enabled).ToList(), "BaseCatalogId", "Name");
            else ViewBag.ActivityTypeList = new SelectList(new List<BaseCatalog>(), "BaseCatalogId", "Name");

            (ListResponseCode, ListResponseText, ListAge) = _helper.GetEntitiesList<List<AgeRange>>("Config", "AgeRange");
            if (ListResponseCode == OK)
            {
                foreach (var item in ListAge) item.Name = item.Name + " / " + item.MinimumAge.ToString() + "-" + item.MaximumAge.ToString();
                ViewBag.AgeRangesList = ListAge.Where(x => x.Enabled).ToList();// new SelectList(ListAge.Where(x => x.Enabled).ToList(), "AgeRangeId", "Name");
            }
            else ViewBag.AgeRangesList = new List<AgeRange>();//new SelectList(new List<AgeRange>(), "AgeRangeId", "Name");

            (ListResponseCode, ListResponseText, ListRole) = _helper.GetEntitiesList<List<Role>>("Config", "Role");
            if (ListResponseCode == OK) ViewBag.RolesList = ListRole.Where(x => x.Enabled).ToList();
            else ViewBag.RolesList = new List<Role>();

            (ListResponseCode, ListResponseText, ListStatus) = _helper.GetEntitiesList<List<EventStatus>>("Config", "EventStatus");
            if (ListResponseCode == OK)
            {
                ListStatus.ForEach(x => { x.Name = x.Name + x.Color; });
                ViewBag.StatusList = new SelectList(ListStatus.Where(x => x.Enabled).ToList(), "EventStatusId", "Name");
            }
            else ViewBag.StatusList = new SelectList(new List<EventStatus>(), "EventStatusId", "Name");

            (ListResponseCode, ListResponseText, ListTemplate) = _helper.GetEntitiesList<List<Template>>("Activities", "Template");
            if (ListResponseCode == OK) ViewBag.TemplateList = ListTemplate.Where(x => x.Enabled).ToList();
            else ViewBag.TemplateList = new List<Template>();


            if (EventId == null)
            {
                return View("Records/Events/Create", new Event());
            }
            else if (EventId == 0)
            {
                return View("Records/Events/Create", new Event());
            }
            else
            {
                var (ResponseCode, ResponseText, Event) = _helper.GetEntity<Event>("Activities", "Event", "EventId", (int)EventId);
                if (ResponseCode == OK)
                {
                    Event.EventAgeRanges = Event.EventAgeRanges.Where(x => x.Enabled).ToList();
                    Event.EventRoles = Event.EventRoles.Where(x => x.Enabled).ToList();
                    Event.Requirements = Event.Requirements.Replace("\\n", Environment.NewLine);
                    Event.Place = Event.Place.Replace("\\n", Environment.NewLine);
                    Event.Address = Event.Address.Replace("\\n", Environment.NewLine);
                    Event.Description = Event.Description.Replace("'", "\"");
                    Event.EmailBody = Event.EmailBody.Replace("'", "\"");
                    return View("Records/Events/Create", Event);
                }
                else return View("Records/Events/Create", new Event());
            }
        }

        public ActionResult GetEventAgeRangePartial(int index)
        {
            var (ListResponseCode, ListResponseText, ListAge) = _helper.GetEntitiesList<List<AgeRange>>("Config", "AgeRange");
            if (ListResponseCode == OK)
            {
                foreach (var item in ListAge) item.Name = item.Name + " / " + item.MinimumAge.ToString() + "-" + item.MaximumAge.ToString();
                ViewBag.AgeRangesList = new SelectList(ListAge.Where(x => x.Enabled).ToList(), "AgeRangeId", "Name");
            }
            else ViewBag.AgeRangesList = new SelectList(new List<AgeRange>(), "AgeRangeId", "Name");

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Activity/Records/Events/_EventAgeRangePartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEventRolePartial(int index)
        {
            var (PositionResponseCode, PositionResponseText, ListRole) = _helper.GetEntitiesList<List<Role>>("Config", "Role");
            if (PositionResponseCode == OK) ViewBag.RoleList = new SelectList(ListRole.Where(x => x.Enabled).ToList(), "RoleId", "Name");
            else ViewBag.RoleList = new SelectList(new List<Role>(), "RoleId", "Name");

            var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Activity/Records/Events/_EventRolePartial.cshtml");
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }

        [Route("Activity/Records/GetEventDetails")]
        [IsPermission("Evento", PermissionTypes.View, null, "EventsDetails")]
        public ActionResult GetEventDetails(int EventId)
        {
            if (EventId <= 0) return RedirectToAction("Events");

            var (ResponseCode, ResponseText, Event) = _helper.GetEntity<Event>("Activities", "Event", "EventId", EventId);

            if (ResponseCode == OK)
            {
                Event.EventAgeRanges = Event.EventAgeRanges.Where(x => x.Enabled).ToList();
                Event.EventRoles = Event.EventRoles.Where(x => x.Enabled).ToList();
                Event.Requirements = Event.Requirements.Replace("\\n", Environment.NewLine);
                Event.Place = Event.Place.Replace("\\n", Environment.NewLine);
                Event.Address = Event.Address.Replace("\\n", Environment.NewLine);
                Event.Description = Event.Description.Replace("'", "\"");
                Event.EmailBody = Event.EmailBody.Replace("'", "\"");
                return View("Records/Events/Details", Event);
            }
            else return RedirectToAction("Events");
        }

        [Route("Activity/Records/EventManageInvitations")]
        [IsPermission("Evento", PermissionTypes.Other, "Gestionar Invitaciones", "EventManageInvitations")]
        public ActionResult EventManageInvitations(int EventId)
        {
            if (EventId <= 0) return RedirectToAction("Events");

            EventInvitationContainer Event;
            List<Group> ListGroup;
            List<Section> ListSection;
            List<Position> ListPosition;

            var (ResponseCode, ResponseText, ListMember) = _helper.GetEntitiesList<List<Member>>("Members", "Member");
            if (ResponseCode == OK)
            {
                ListMember.ForEach(x => { x.Names = x.Names + " " + x.Surnames; });
                ViewBag.MemberList = ListMember.Where(x => x.Enabled).ToList();
            }
            else return RedirectToAction("Events");

            (ResponseCode, ResponseText, ListGroup) = _helper.GetEntitiesList<List<Group>>("Members", "Group");
            if (ResponseCode == OK)
            {
                ListGroup.ForEach(x => { x.Name = x.Name + " - " + x.Number.ToString(); });
                ViewBag.GroupList = ListGroup.Where(x => x.Enabled).ToList();
            }
            else return RedirectToAction("Events");

            (ResponseCode, ResponseText, ListSection) = _helper.GetEntitiesList<List<Section>>("Members", "Section");
            if (ResponseCode == OK) ViewBag.SectionList = ListSection.Where(x => x.Enabled).ToList();
            else return RedirectToAction("Events");

            (ResponseCode, ResponseText, ListPosition) = _helper.GetEntitiesList<List<Position>>("Members", "Position");
            if (ResponseCode == OK) ViewBag.PositionList = ListPosition.Where(x => x.Enabled).ToList();
            else return RedirectToAction("Events");

            (ResponseCode, ResponseText, Event) = _helper.GetEntity<EventInvitationContainer>("Activities", "EventInvitationContainer", "EventId", EventId);
            if (ResponseCode == OK)
            {
                Event.EventMemberInvitations = Event.EventMemberInvitations.Where(x => x.Enabled).ToList();
                Event.EventGroupInvitations = Event.EventGroupInvitations.Where(x => x.Enabled).ToList();
                Event.EventSectionInvitations = Event.EventSectionInvitations.Where(x => x.Enabled).ToList();
                Event.EventPositionInvitations = Event.EventPositionInvitations.Where(x => x.Enabled).ToList();

                Event.EventMemberExclusions = Event.EventMemberExclusions.Where(x => x.Enabled).ToList();
                Event.EventGroupExclusions = Event.EventGroupExclusions.Where(x => x.Enabled).ToList();
                Event.EventSectionExclusions = Event.EventSectionExclusions.Where(x => x.Enabled).ToList();
                Event.EventPositionExclusions = Event.EventPositionExclusions.Where(x => x.Enabled).ToList();

                return View("Records/Events/ManageInvitations", Event);
            }
            else return RedirectToAction("Events");
        }

        public ActionResult GetEventMemberPartial(int index, string model)
        {
            ViewBag.ModelName = model;

            var (ResponseCode, ResponseText, ListMember) = _helper.GetEntitiesList<List<Member>>("Members", "Member");
            if (ResponseCode == OK)
            {
                ListMember.ForEach(x => { x.Names = x.Names + " " + x.Surnames; });
                ViewBag.MemberList = new SelectList(ListMember.Where(x => x.Enabled).ToList(), "MemberId", "Names");

                var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Activity/Records/Events/_EventMemberPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetEventGroupPartial(int index, string model)
        {
            ViewBag.ModelName = model;

            var (ResponseCode, ResponseText, ListGroup) = _helper.GetEntitiesList<List<Group>>("Members", "Group");
            if (ResponseCode == OK)
            {
                ListGroup.ForEach(x => { x.Name = x.Name + " - " + x.Number.ToString(); });
                ViewBag.GroupList = new SelectList(ListGroup.Where(x => x.Enabled).ToList(), "GroupId", "Name");

                var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Activity/Records/Events/_EventGroupPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetEventSectionPartial(int index, string model)
        {
            ViewBag.ModelName = model;

            var (ResponseCode, ResponseText, ListSection) = _helper.GetEntitiesList<List<Section>>("Members", "Section");
            if (ResponseCode == OK)
            {
                ViewBag.SectionList = new SelectList(ListSection.Where(x => x.Enabled).ToList(), "SectionId", "Name");

                var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Activity/Records/Events/_EventSectionPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetEventPositionPartial(int index, string model)
        {
            ViewBag.ModelName = model;

            var (ResponseCode, ResponseText, ListPosition) = _helper.GetEntitiesList<List<Position>>("Members", "Position");
            if (ResponseCode == OK)
            {
                ViewBag.PositionList = new SelectList(ListPosition.Where(x => x.Enabled).ToList(), "PositionId", "Name");

                var response = Utilities.RenderRazorViewToString(ControllerContext, index, "~/Views/Activity/Records/Events/_EventPositionPartial.cshtml");
                return Json(new { response }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> SaveEvent(Event events)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Datos no válidos:" + GetErrorMessage(ModelState);
                return Json(new { ResponseCode = 500, ResponseText = "Datos no válidos:" + Environment.NewLine + GetErrorMessage(ModelState) });
            }

            if (events.EventId == null || events.EventId == 0)
            {
                events.EventAgeRanges.ForEach(x => x.Enabled = true);
                events.EventRoles.ForEach(x => x.Enabled = true);
            }
            else
            {
                events.EventAgeRanges.ForEach(x =>
                {
                    x.EventId = (int)events.EventId;
                    if (x.EventAgeRangeId == null || x.EventAgeRangeId == 0) x.Enabled = true;
                });
                events.EventRoles.ForEach(x =>
                {
                    x.EventId = (int)events.EventId;
                    if (x.EventRoleId == null || x.EventRoleId == 0) x.Enabled = true;
                });

                var (ResponseCodeUpd, ResponseTextUpd, eventUpd) = _helper.GetEntity<Event>("Activities", "Event", "EventId", (int)events.EventId);

                if (ResponseCodeUpd == OK)
                {
                    eventUpd.EventAgeRanges = eventUpd.EventAgeRanges.Where(x => x.Enabled).ToList();
                    eventUpd.EventRoles = eventUpd.EventRoles.Where(x => x.Enabled).ToList();

                    events.EventAgeRanges = EditChildList<EventAgeRange>(events.EventAgeRanges, eventUpd.EventAgeRanges, "EventAgeRangeId", new[] { "AgeRangeId" });
                    events.EventRoles = EditChildList<EventRole>(events.EventRoles, eventUpd.EventRoles, "EventRoleId", new[] { "RoleId" });
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseTextUpd;
                    return Json(new { ResponseCode = ResponseCodeUpd, ResponseText = ResponseTextUpd });
                }
            }

            if (events.SendFile != null)
            {
                Entities.File toSave = Utilities.SaveFile(events.SendFile, "Activity", "Records");
                events.EventImage = toSave.FileUrl;
            }

            events.Description = events.Description.Replace("\"", "'");
            events.EmailBody = events.EmailBody.Replace("\"", "'").Replace("\t", "");
            events.SendFile = null;
            events.Enabled = true;

            ERContainer container = new ERContainer() { Events = new List<Event>() { events }, Model = "Event", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText, Event) = await _helper.CreatePostGetElement<List<Event>>("Activities/Model/NewObject", container);

            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveInvitations(EventInvitationContainer container)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Datos no válidos.";
                return Json(new { ResponseCode = 500, ResponseText = "Datos no válidos." });
            }

            container.EventMemberInvitations.ForEach(x =>
            {
                x.EventId = (int)container.EventId;
                if (x.EventMemberInvitationId == null || x.EventMemberInvitationId == 0) x.Enabled = true;
            });
            container.EventGroupInvitations.ForEach(x =>
            {
                x.EventId = (int)container.EventId;
                if (x.EventGroupInvitationId == null || x.EventGroupInvitationId == 0) x.Enabled = true;
            });
            container.EventSectionInvitations.ForEach(x =>
            {
                x.EventId = (int)container.EventId;
                if (x.EventSectionInvitationId == null || x.EventSectionInvitationId == 0) x.Enabled = true;
            });
            container.EventPositionInvitations.ForEach(x =>
            {
                x.EventId = (int)container.EventId;
                if (x.EventPositionInvitationId == null || x.EventPositionInvitationId == 0) x.Enabled = true;
            });

            container.EventMemberExclusions.ForEach(x =>
            {
                x.EventId = (int)container.EventId;
                if (x.EventMemberExclusionId == null || x.EventMemberExclusionId == 0) x.Enabled = true;
            });
            container.EventGroupExclusions.ForEach(x =>
            {
                x.EventId = (int)container.EventId;
                if (x.EventGroupExclusionId == null || x.EventGroupExclusionId == 0) x.Enabled = true;
            });
            container.EventSectionExclusions.ForEach(x =>
            {
                x.EventId = (int)container.EventId;
                if (x.EventSectionExclusionId == null || x.EventSectionExclusionId == 0) x.Enabled = true;
            });
            container.EventPositionExclusions.ForEach(x =>
            {
                x.EventId = (int)container.EventId;
                if (x.EventPositionExclusionId == null || x.EventPositionExclusionId == 0) x.Enabled = true;
            });

            var (ResponseCode, ResponseText, Event) = _helper.GetEntity<EventInvitationContainer>("Activities", "EventInvitationContainer", "EventId", (int)container.EventId);
            if (ResponseCode == OK)
            {
                Event.EventMemberInvitations = Event.EventMemberInvitations.Where(x => x.Enabled).ToList();
                Event.EventGroupInvitations = Event.EventGroupInvitations.Where(x => x.Enabled).ToList();
                Event.EventSectionInvitations = Event.EventSectionInvitations.Where(x => x.Enabled).ToList();
                Event.EventPositionInvitations = Event.EventPositionInvitations.Where(x => x.Enabled).ToList();

                Event.EventMemberExclusions = Event.EventMemberExclusions.Where(x => x.Enabled).ToList();
                Event.EventGroupExclusions = Event.EventGroupExclusions.Where(x => x.Enabled).ToList();
                Event.EventSectionExclusions = Event.EventSectionExclusions.Where(x => x.Enabled).ToList();
                Event.EventPositionExclusions = Event.EventPositionExclusions.Where(x => x.Enabled).ToList();

                container.EventMemberInvitations = EditChildList<EventMemberInvitation>(container.EventMemberInvitations, Event.EventMemberInvitations, "EventMemberInvitationId", new[] { "MemberId" });
                container.EventGroupInvitations = EditChildList<EventGroupInvitation>(container.EventGroupInvitations, Event.EventGroupInvitations, "EventGroupInvitationId", new[] { "GroupId" });
                container.EventSectionInvitations = EditChildList<EventSectionInvitation>(container.EventSectionInvitations, Event.EventSectionInvitations, "EventSectionInvitationId", new[] { "SectionId" });
                container.EventPositionInvitations = EditChildList<EventPositionInvitation>(container.EventPositionInvitations, Event.EventPositionInvitations, "EventPositionInvitationId", new[] { "PositionId" });

                container.EventMemberExclusions = EditChildList<EventMemberExclusion>(container.EventMemberExclusions, Event.EventMemberExclusions, "EventMemberExclusionId", new[] { "MemberId" });
                container.EventGroupExclusions = EditChildList<EventGroupExclusion>(container.EventGroupExclusions, Event.EventGroupExclusions, "EventGroupExclusionId", new[] { "GroupId" });
                container.EventSectionExclusions = EditChildList<EventSectionExclusion>(container.EventSectionExclusions, Event.EventSectionExclusions, "EventSectionExclusionId", new[] { "SectionId" });
                container.EventPositionExclusions = EditChildList<EventPositionExclusion>(container.EventPositionExclusions, Event.EventPositionExclusions, "EventPositionExclusionId", new[] { "PositionId" });

                container.EventMemberInvitations = EditDuplicateList<EventMemberInvitation>(container.EventMemberInvitations, "EventMemberInvitationId", new[] { "EventId", "MemberId" });
                container.EventGroupInvitations = EditDuplicateList<EventGroupInvitation>(container.EventGroupInvitations, "EventGroupInvitationId", new[] { "EventId", "GroupId" });
                container.EventSectionInvitations = EditDuplicateList<EventSectionInvitation>(container.EventSectionInvitations, "EventSectionInvitationId", new[] { "EventId", "SectionId" });
                container.EventPositionInvitations = EditDuplicateList<EventPositionInvitation>(container.EventPositionInvitations, "EventPositionInvitationId", new[] { "EventId", "PositionId" });

                container.EventMemberExclusions = EditDuplicateList<EventMemberExclusion>(container.EventMemberExclusions, "EventMemberExclusionId", new[] { "EventId", "MemberId" });
                container.EventGroupExclusions = EditDuplicateList<EventGroupExclusion>(container.EventGroupExclusions, "EventGroupExclusionId", new[] { "EventId", "GroupId" });
                container.EventSectionExclusions = EditDuplicateList<EventSectionExclusion>(container.EventSectionExclusions, "EventSectionExclusionId", new[] { "EventId", "SectionId" });
                container.EventPositionExclusions = EditDuplicateList<EventPositionExclusion>(container.EventPositionExclusions, "EventPositionExclusionId", new[] { "EventId", "PositionId" });

                List<EventInvitationContainer> RespPost = null;
                ERContainer toSend = new ERContainer() { EventInvitationContainers = new List<EventInvitationContainer>() { container }, Model = "EventInvitationContainer", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                (ResponseCode, ResponseText, RespPost) = await _helper.CreatePostGetElement<List<EventInvitationContainer>>("Activities/Model/NewObject", toSend);
                if (ResponseCode == OK)
                {
                    Event = RespPost.First();
                    if (container.Send)
                    {
                        try
                        {
                            if (ValidateAddresses(Event))
                            {
                                ERContainer emails = new ERContainer()
                                {
                                    Events = new List<Event>()
                                    {
                                        new Event()
                                        {
                                            EventId = Event.EventId
                                        }
                                    },
                                    Model = "Event",
                                    CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId
                                };
                                (ResponseCode, ResponseText) = await _helper.CreatePost("Activities/Model/SendEmails", toSend);
                                if (ResponseCode == OK)
                                {
                                    return Json(new { ResponseCode, ResponseText });
                                }
                                else
                                {
                                    Response.StatusCode = 400;
                                    Response.StatusDescription = ResponseText;
                                    return Json(new { ResponseCode, ResponseText });
                                }
                            }
                            else
                            {
                                Response.StatusCode = 400;
                                Response.StatusDescription = "Las exclusiones ingresadas dan como resultado 0 destinatarios.";
                                return Json(new { ResponseCode = 500, ResponseText = "Las exclusiones ingresadas dan como resultado 0 destinatarios." });
                            }
                        }
                        catch (Exception ex)
                        {
                            Response.StatusCode = 400;
                            Response.StatusDescription = "Ocurrio un error al enviar los correos electrónicos.";
                            return Json(new { ResponseCode = 500, ResponseText = "Ocurrio un error al enviar los correos electrónicos." });
                        }
                    }
                    else
                    {
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText });
                }
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Ocurrió un error modificando las invitaciones.";
                return Json(new { ResponseCode = 500, ResponseText = "Ocurrió un error modificando las invitaciones." });
            }
        }

        [IsPermission("Evento", PermissionTypes.Delete, null, "EventsElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteEvent(int? EventId, List<int> Events, string Search, int Number = 1)
        {
            if (EventId == null && Events == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (EventId != null)
            {
                var (ResponseCode, ResponseText, Event) = _helper.GetEntity<Event>("Activities", "Event", "EventId", (int)EventId);
                if (ResponseCode == OK)
                {
                    Event.Enabled = !Event.Enabled;
                    ERContainer container = new ERContainer() { Events = new List<Event>() { Event }, Model = "Event", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Activities/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridEvents(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Event>>("Activities", "Event");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Events.Contains((int)x.EventId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Events = list, Model = "Event", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridEvents(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Activities/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridEvents(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        private bool ValidateAddresses(EventInvitationContainer Event)
        {
            try
            {
                Event.EventMemberInvitations = Event.EventMemberInvitations.Where(x => x.Enabled).ToList();
                Event.EventGroupInvitations = Event.EventGroupInvitations.Where(x => x.Enabled).ToList();
                Event.EventSectionInvitations = Event.EventSectionInvitations.Where(x => x.Enabled).ToList();
                Event.EventPositionInvitations = Event.EventPositionInvitations.Where(x => x.Enabled).ToList();

                Event.EventMemberExclusions = Event.EventMemberExclusions.Where(x => x.Enabled).ToList();
                Event.EventGroupExclusions = Event.EventGroupExclusions.Where(x => x.Enabled).ToList();
                Event.EventSectionExclusions = Event.EventSectionExclusions.Where(x => x.Enabled).ToList();
                Event.EventPositionExclusions = Event.EventPositionExclusions.Where(x => x.Enabled).ToList();

                var (ResponseCode, ResponseText, ListMember) = _helper.GetEntitiesList<List<Member>>("Members", "Member");
                if (ResponseCode == OK)
                {
                    List<Member> ListMemberBU = new List<Member>(ListMember);
                    List<int> positions = Event.EventPositionInvitations.Select(x => x.PositionId).ToList();
                    List<int> sections = Event.EventSectionInvitations.Select(x => x.SectionId).ToList();
                    List<int> groups = Event.EventGroupInvitations.Select(x => x.GroupId).ToList();
                    List<int> members = Event.EventMemberInvitations.Select(x => x.MemberId).ToList();

                    if (positions.Count > 0 || sections.Count > 0 || groups.Count > 0 || members.Count > 0)
                    {
                        ListMember = ListMember.Where(x =>
                        {
                            x.MemberPositions = x.MemberPositions.Where(y => y.Enabled).ToList();
                            x.MemberGroups = x.MemberGroups.Where(y => y.Enabled).ToList();
                            MemberGroup groupModel = null;
                            if (x.MemberGroups.Count > 0) groupModel = x.MemberGroups.OrderByDescending(y => y.StartDate).First();
                            bool flag = false;

                            positions.ForEach(y => { if (x.MemberPositions.Select(m => m.PositionId).ToList().Contains(y)) flag = true; });
                            sections.ForEach(y => { if (groupModel != null) { if (groupModel.SectionId == y) flag = true; } });
                            groups.ForEach(y => { if (groupModel != null) { if (groupModel.GroupId == y) flag = true; } });

                            return flag;
                        }).ToList();
                    }

                    foreach (var exclusion in Event.EventPositionExclusions) ListMember = ListMember.Where(x => !(x.MemberPositions.Select(y => y.PositionId).ToList().Contains(exclusion.PositionId))).ToList();
                    foreach (var exclusion in Event.EventSectionExclusions) ListMember = ListMember.Where(x => !(x.MemberGroups.Select(y => y.SectionId).ToList().Contains(exclusion.SectionId))).ToList();
                    foreach (var exclusion in Event.EventGroupExclusions) ListMember = ListMember.Where(x => !(x.MemberGroups.Select(y => y.GroupId).ToList().Contains(exclusion.GroupId))).ToList();
                    foreach (var exclusion in Event.EventMemberExclusions) ListMember = ListMember.Where(x => !(x.MemberId == exclusion.MemberId)).ToList();

                    members.ForEach(x =>
                    {
                        if (ListMember.Where(y => y.MemberId == x).ToList().Count == 0) { if (ListMemberBU.Where(z => z.MemberId == x).FirstOrDefault() != null) ListMember.Add(ListMemberBU.Where(z => z.MemberId == x).First()); }
                    });

                    return ListMember.Count > 0;
                }
                else throw new Exception("Ocurrio un error al enviar los correos electrónicos.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #endregion

        #region Catalogs

        [IsMenu(3, "Catálogos", null, null, null, null, "Catalogs")]
        [Route("Activity/Catalogs/Index")]
        public ActionResult CatalogsIndex()
        {
            return RedirectToAction("Templates");
        }

        #region Templates

        [Route("Activity/Catalogs/TemplatesList")]
        [IsPermission("Plantilla", PermissionTypes.List, null, "TemplatesList")]
        public ActionResult Templates()
        {
            return View("Catalogs/Templates/Index");
        }

        public ActionResult GetGridTemplates(string Search, int Number = 1)
        {
            var (ResponseCode, ResponseText, templates, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<Template>>("Activities", "Template", Search, Number);
            ViewBag.Pagination = new PaginationToolsViewModel() { SearchText = SearchText, RowCount = RowCount, NumberPage = NumberPage };
            if (ResponseCode == OK)
            {
                templates.ForEach(x => { x.Body.Replace("'", "\""); });
                if (Request.IsAjaxRequest())
                {
                    var response = Utilities.RenderRazorViewToString(ControllerContext, templates, "~/Views/Activity/Catalogs/Templates/_GridPartial.cshtml");
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Templates/_GridPartial", templates);
            }
            else
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode, ResponseText }, JsonRequestBehavior.AllowGet);
                }
                else return PartialView("Catalogs/Templates/_GridPartial", new List<Template>());
            }
        }

        [Route("Activity/Catalogs/NewTemplate")]
        [IsPermission("Plantilla", PermissionTypes.New, null, "TemplatesCreation")]
        public ActionResult NewTemplate(int? TemplateId)
        {
            if (TemplateId == null)
            {
                return View("Catalogs/Templates/Create", new Template());
            }
            else if (TemplateId == 0)
            {
                return View("Catalogs/Templates/Create", new Template());
            }
            else
            {
                var (ResponseCode, ResponseText, Template) = _helper.GetEntity<Template>("Activities", "Template", "TemplateId", (int)TemplateId);
                if (ResponseCode == OK)
                {
                    Template.Body = Template.Body.Replace("'", "\"");
                    return View("Catalogs/Templates/Create", Template);
                }
                else return View("Catalogs/Templates/Create", new Template());
            }
        }

        public ActionResult GetTemplate(int TemplateId)
        {
            if (TemplateId <= 0)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Datos no válidos.";
                return Json(new { ResponseCode = 500, ResponseText = "Datos no válidos." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var (ResponseCode, ResponseText, response) = _helper.GetEntity<Template>("Activities", "Template", "TemplateId", (int)TemplateId);
                if (ResponseCode == OK)
                {
                    return Json(new { response }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = ResponseText;
                    return Json(new { ResponseCode = 500, ResponseText = ResponseText }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> SaveTemplate(Template template)
        {
            if (!ModelState.IsValid) return View(template);

            template.Body = template.Body.Replace("\"", "'");
            template.Enabled = true;

            ERContainer container = new ERContainer() { Templates = new List<Template>() { template }, Model = "Template", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
            var (ResponseCode, ResponseText, Template) = await _helper.CreatePostGetElement<List<Template>>("Activities/Model/NewObject", container);

            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        [IsPermission("Plantilla", PermissionTypes.Delete, null, "TemplatesElimination")]
        [HttpDelete]
        public async Task<ActionResult> DeleteTemplate(int? TemplateId, List<int> Templates, string Search, int Number = 1)
        {
            if (TemplateId == null && Templates == null)
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Parámetros no válidos.";
                return Json(new { ResponseCode = 400, ResponseText = "Parámetros no válidos." });
            }

            if (TemplateId != null)
            {
                var (ResponseCode, ResponseText, Template) = _helper.GetEntity<Template>("Activities", "Template", "TemplateId", (int)TemplateId);
                if (ResponseCode == OK)
                {
                    Template.Enabled = !Template.Enabled;
                    ERContainer container = new ERContainer() { Templates = new List<Template>() { Template }, Model = "Template", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    (ResponseCode, ResponseText) = await _helper.CreatePost("Activities/Model/NewObject", container);

                    if (ResponseCode == OK)
                        return GetGridTemplates(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "El registro no fue encontrado.";
                    return Json(new { ResponseCode, ResponseText = "El registro no fue encontrado." });
                }
            }
            else
            {
                var (ResponseCode, ResponseText, list) = _helper.GetEntitiesList<List<Template>>("Activities", "Template");
                if (ResponseCode == OK)
                {
                    list = list.Where(x => Templates.Contains((int)x.TemplateId)).ToList();
                    foreach (var item in list) item.Enabled = !item.Enabled;

                    ERContainer container = new ERContainer() { Templates = list, Model = "Template", CurrentUser = CustomSession.CurrentSession.CurrentUser.Member.User.UserId };
                    if (list.Count == 0) return GetGridTemplates(Search, Number);

                    (ResponseCode, ResponseText) = await _helper.CreatePost("Activities/Model/NewObject", container);
                    if (ResponseCode == OK)
                        return GetGridTemplates(Search, Number);
                    else
                    {
                        Response.StatusCode = 400;
                        Response.StatusDescription = ResponseText;
                        return Json(new { ResponseCode, ResponseText });
                    }
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Registros no encontrados.";
                    return Json(new { ResponseCode, ResponseText = "Registros no encontrados." });
                }
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> SendEmailTest(Template template)
        {
            //template.Body = template.Body.Replace("\"", "'");
            EmailContainer toSend = new EmailContainer()
            {
                ListToSend = template.Description,
                mailMessage = new MailMessage()
                {
                    Subject = template.Name + " - Prueba de correo",
                    IsBodyHtml = true,
                    Body = template.Body,
                }
            };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Requests/Email/SendEmail", toSend);


            if (ResponseCode == OK)
                return Json(new { ResponseCode, ResponseText });
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = ResponseText;
                return Json(new { ResponseCode, ResponseText });
            }
        }

        #endregion

        #endregion

    }
}