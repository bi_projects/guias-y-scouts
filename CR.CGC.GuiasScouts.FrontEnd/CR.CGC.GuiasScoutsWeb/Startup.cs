﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CR.CGC.GuiasScoutsWeb.Startup))]
namespace CR.CGC.GuiasScoutsWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
