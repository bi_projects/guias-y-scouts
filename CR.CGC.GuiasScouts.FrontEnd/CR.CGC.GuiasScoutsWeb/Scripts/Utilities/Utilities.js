﻿var pageFunctions = {
    hideElement: function (elem) {
        if (typeof elem != 'undefined') {
            if (elem != null) {
                $(elem).addClass('hide');
            }
        }
    },
    showElement: function (elem) {
        if (typeof elem != 'undefined') {
            if (elem != null) {
                $(elem).removeClass('hide');
            }
        }
    },
    reloadPage: function (force) {
        if (typeof force != 'undefined') {
            if (force != null) {
                location.reload(force);
            }
        }
    },
    unhideElem: function (idElem) {
        var item = document.getElementById(idElem);
        if (item) {
            if (item.className.indexOf("hide") != -1) {
                item.classList.remove("hide");
            } else {
                item.classList.add("hide");
            }
        }
    }
};

//AJAX
var gateway = {
    getData: function (apiUrl, elementId) {
        $.ajax({
            url: apiUrl,
            type: 'GET',
            dataType: 'html',
            async: false,
            cache: false,
            success: function (response) {
                var table = document.getElementById(elementId);
                $(table).html(response);
            },
            error: function (response) {
                return response;
            },
            complete: function (response) {
                return response;
            }
        });
    },
    getData_Callback: function (apiUrl, data, callback, initElems) {
        initElems = initElems || true;
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            url: apiUrl,
            type: 'GET',
            data: data,
            dataType: 'json',
            cache: false,
            success: function (data) {
                callback(data);
            },
            error: function (data) {
                console.log(data);
                try {
                    alertFunctions.basicAlert($('#AlertTitle').val(), data.responseJSON.ResponseText, "error");
                }
                catch (ex) {
                    console.log(ex.message);
                    if (data.statusText != undefined) {
                        alertFunctions.basicAlert($('#AlertTitle').val(), data.statusText, 'error');
                    } else {
                        alertFunctions.basicAlert($('#AlertTitle').val(), "Ha ocurrido un error.", 'error');
                    }
                }
            },
            complete: function () {
                if (initElems) {
                    paginationCustomFunctions.init();
                    reloadElements();
                }
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },
    getDataByData: function (apiUrl, data, formContainerId) {
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            url: apiUrl,
            type: 'GET',
            data: data,
            dataType: 'json',
            cache: false,
            success: function (data) {
                $('#' + formContainerId).html(data.response);
            },
            error: function (data) {
                console.log(data);
                try {
                    alertFunctions.basicAlert($('#AlertTitle').val(), data.responseJSON.ResponseText, "error");
                }
                catch (ex) {
                    console.log(ex.message);
                    if (data.statusText != undefined) {
                        alertFunctions.basicAlert($('#AlertTitle').val(), data.statusText, 'error');
                    } else {
                        alertFunctions.basicAlert($('#AlertTitle').val(), "Ha ocurrido un error.", 'error');
                    }
                }
            },
            complete: function () {
                paginationCustomFunctions.init();
                reloadElements();
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },
    getDataByData_Callback: function (apiUrl, data, formContainerId, callback, initElems) {
        initElems = initElems || true;
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            url: apiUrl,
            type: 'GET',
            data: data,
            dataType: 'json',
            cache: false,
            success: function (data) {
                $('#' + formContainerId).html(data.response);
                callback();
            },
            error: function (data) {
                alertFunctions.basicAlert($('#AlertTitle').val(), data.responseJSON.ResponseText, "error");
            },
            complete: function () {
                if (initElems) {
                    paginationCustomFunctions.init();
                    reloadElements();
                }
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },
    add_GetDataByData: function (apiUrl, data, formContainerId) {
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            url: apiUrl,
            type: 'GET',
            data: data,
            dataType: 'json',
            cache: false,
            success: function (data) {
                $('#' + formContainerId).append(data.response);
            },
            error: function (data) {
                alertFunctions.basicAlert($('#AlertTitle').val(), data.responseJSON.ResponseText, "error");
            },
            complete: function () {
                paginationCustomFunctions.init();
                reloadElements();
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },
    add_GetDataByData_Callback: function (apiUrl, data, formContainerId, callback, callbackComplete) {
        callbackComplete = callbackComplete || null;
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            url: apiUrl,
            type: 'GET',
            data: data,
            dataType: 'json',
            cache: false,
            success: function (data) {
                $('#' + formContainerId).append(data.response);
                callback(data);
            },
            error: function (data) {
                alertFunctions.basicAlert($('#AlertTitle').val(), data.responseJSON.ResponseText, "error");
            },
            complete: function () {
                if (callbackComplete != null) callbackComplete();
                paginationCustomFunctions.init();
                reloadElements();
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },
    deleteData: function (apiUrl, jsonData, alertTitle) {
        $.ajax({
            type: 'DELETE',
            url: apiUrl,
            data: { id: jsonData },
            success: function (response) {
                alertFunctions.basicAlert(alertTitle, response, 'success');
            },
            error: function (response) {
                alertFunctions.basicAlert(alertTitle, response, 'error');
            },
            complete: function (response) {
                //TODO
            }
        });
    },
    editEntity: function (apiUrl, data, formContainerId) {
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            url: apiUrl,
            type: "GET",
            cache: false,
            dataType: 'json',
            data: data,
            success: function (data) {
                $('#' + formContainerId).html(data.response);
            },
            error: function (data) {
                alertFunctions.basicAlert($('#AlertTitle').val(), '¡Error al editar el elemento!', "error");
            },
            complete: function () {
                reloadElements();
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },
    deleteEntity: function (apiUrl, data, formContainerId) {
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'DELETE',
            url: apiUrl,
            data: JSON.stringify(data),
            success: function (data) {
                alertFunctions.basicAlert($('#AlertTitle').val(), '¡Modificación exitosa!', "success");
                $('#' + formContainerId).html(data.response);
            },
            error: function (data) {
                console.log(data);
                try {
                    alertFunctions.basicAlert($('#AlertTitle').val(), data.responseJSON.ResponseText, "error");
                }
                catch (ex) {
                    console.log(ex.message);
                    if (data.statusText != undefined) {
                        alertFunctions.basicAlert($('#AlertTitle').val(), data.statusText, 'error');
                    } else {
                        alertFunctions.basicAlert($('#AlertTitle').val(), "Ha ocurrido un error.", 'error');
                    }
                }
            },
            complete: function () {
                paginationCustomFunctions.init();
                reloadElements();
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },
    deleteEntities: function (apiUrl, listName, formContainerId, gridContainerId) {
        var IDs = [];
        $('tr.is-selected', "#" + gridContainerId).each(function (index, item) { IDs.push($(item).attr('data')); });
        if (IDs.length == 0) {
            alertFunctions.basicAlert($('#AlertTitle').val(), '¡Debe seleccionar por lo menos un registro!', "warning");
            return;
        }
        var jsonToSend = {};
        jsonToSend[listName] = IDs;

        pageFunctions.showElement($('#preloader'));
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'DELETE',
            url: apiUrl,
            data: JSON.stringify(jsonToSend),
            success: function (data) {
                alertFunctions.basicAlert($('#AlertTitle').val(), '¡Modificación exitosa!', "success");
                $('#' + formContainerId).html(data.response);
            },
            error: function (data) {
                console.log(data);
                try {
                    alertFunctions.basicAlert($('#AlertTitle').val(), data.responseJSON.ResponseText, "error");
                }
                catch (ex) {
                    console.log(ex.message);
                    if (data.statusText != undefined) {
                        alertFunctions.basicAlert($('#AlertTitle').val(), data.statusText, 'error');
                    } else {
                        alertFunctions.basicAlert($('#AlertTitle').val(), "Ha ocurrido un error.", 'error');
                    }
                }
            },
            complete: function () {
                paginationCustomFunctions.init();
                reloadElements();
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },

    deleteEntitiesPag: function (apiUrl, listName, formContainerId, gridContainerId, data) {
        var IDs = [];
        $('tr.is-selected', "#" + gridContainerId).each(function (index, item) {
            if ($(item).attr('data') != undefined && $(item).attr('data') != null) {
                IDs.push($(item).attr('data'));
            }
        });

        if (IDs.length == 0) {
            alertFunctions.basicAlert($('#AlertTitle').val(), '¡Debe seleccionar por lo menos un registro!', "warning");
            return;
        }
        var jsonToSend = data;
        jsonToSend[listName] = IDs;

        pageFunctions.showElement($('#preloader'));
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'DELETE',
            url: apiUrl,
            data: JSON.stringify(jsonToSend),
            success: function (data) {
                alertFunctions.basicAlert($('#AlertTitle').val(), '¡Modificación exitosa!', "success");
                $('#' + formContainerId).html(data.response);
            },
            error: function (data) {
                console.log(data);
                try {
                    alertFunctions.basicAlert($('#AlertTitle').val(), data.responseJSON.ResponseText, "error");
                }
                catch (ex) {
                    console.log(ex.message);
                    if (data.statusText != undefined) {
                        alertFunctions.basicAlert($('#AlertTitle').val(), data.statusText, 'error');
                    } else {
                        alertFunctions.basicAlert($('#AlertTitle').val(), "Ha ocurrido un error.", 'error');
                    }
                }
            },
            complete: function () {
                paginationCustomFunctions.init();
                reloadElements();
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },

    deleteEntity_Callback: function (apiUrl, data, callback) {
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'DELETE',
            url: apiUrl,
            data: JSON.stringify(data),
            success: function (data) {
                callback();
            },
            error: function (data) {
                console.log(data);
                try {
                    alertFunctions.basicAlert($('#AlertTitle').val(), data.responseJSON.ResponseText, "error");
                }
                catch (ex) {
                    console.log(ex.message);
                    if (data.statusText != undefined) {
                        alertFunctions.basicAlert($('#AlertTitle').val(), data.statusText, 'error');
                    } else {
                        alertFunctions.basicAlert($('#AlertTitle').val(), "Ha ocurrido un error.", 'error');
                    }
                }
            },
            complete: function () {
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },
    saveEntity_Callback: function (apiUrl, data, callback) {
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: apiUrl,
            data: JSON.stringify(data),
            success: function (data) {
                callback(data);
            },
            error: function (data) {
                console.log(data);
                try {
                    alertFunctions.basicAlert($('#AlertTitle').val(), data.responseJSON.ResponseText, "error");
                }
                catch (ex) {
                    console.log(ex.message);
                    if (data.statusText != undefined) {
                        alertFunctions.basicAlert($('#AlertTitle').val(), data.statusText, 'error');
                    } else {
                        alertFunctions.basicAlert($('#AlertTitle').val(), "Ha ocurrido un error.", 'error');
                    }
                }
            },
            complete: function () {
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },
}

//ALERTS
var alertFunctions = {
    basicAlert: function (title, text, type) {
        swal({
            title: title,
            text: text,
            type: type,
            confirmButtonText: ''//"Aceptar"
        });
    },
    questionAlert: function (title, text, callback) {
        swal({
            title: title,
            text: text,
            showCancelButton: true,
            confirmButtonText: '',//"Aceptar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                callback();
            } else {
                alertFunctions.basicAlert(title, '¡Operación cancelada!', "success");
            }
        });
    },
    callBackAlert: function (title, text, type, callback) {
        swal({
            title: title,
            text: text,
            type: type,
            confirmButtonText: ''//"Aceptar"
        }, function () {
            callback();
        });
    },
}

var partialFunctions = {
    addChild: function (url, parent, callback) {
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            cache: false,
            dataType: 'json',
            success: function (data) {
                $(parent).append(data.response);
                callback();
            },
            error: function () {
                alertFunctions.basicAlert($('#AlertTitle').val(), '¡Error al agregar nuevo elemento!', "error");
            }
        });
    },
    removeChild: function (elem, parentElem) {
        var parent = $(elem).parents(parentElem);
        $(parent).remove();
    },
    orderElements: function (container, childElem, classToFind, modelAttrName) {
        var cont = 0;
        $(container).children(childElem).each(function (index, item) {
            $(item).find('.' + classToFind).each(function (ind, itm) {
                $(itm).attr('name', modelAttrName + '[' + cont.toString() + '].' + $(itm).attr('id'));
            });
            cont++;
        });
    }
}

//FORMS
function OnBegin(validateFunction) {
    if (validateFunction != null) {
        if (this[validateFunction]()) {
            return true;
        } else
            return false;
    } else {
        return true;
    }
}

function OnComplete() {
    pageFunctions.hideElement($('#preloader'));
}

function OnSuccess(res) {
    pageFunctions.hideElement($('#preloader'));
    if (res.ResponseCode == 400) {
        alertFunctions.callBackAlert($('#AlertTitle').val(), res.ResponseText, 'error', function () { location.reload(); });
    }
    else {
        alertFunctions.basicAlert($('#AlertTitle').val(), "Operación realizada con éxito", 'success');
        reloadElements();
    }
}

function OnFailure(res) {
    pageFunctions.hideElement($('#preloader'));
    console.log(res)
    if (res.responseJSON != undefined) {
        alertFunctions.basicAlert($('#AlertTitle').val(), res.responseJSON.ResponseText, 'error');
    } else if (res.statusText != undefined) {
        alertFunctions.basicAlert($('#AlertTitle').val(), res.statusText, 'error');
    } else {
        alertFunctions.basicAlert($('#AlertTitle').val(), "Ha ocurrido un error.", 'error');
    }
}

function deleteRecord(id) {
    alertFunctions.questionAlert($('#ApplicationTitle').val(), 'Are you sure you want to delete the record?', function () { gateway.deleteData($('#url_delete').val(), id, $('#ApplicationTitle').val()); });
}

window.addEventListener('submit', function (e) {
    var form = e.target;
    if (form.dataset.ajax) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var beginName = $(form).attr('OnBegin').toString();
        var funcValName = $(form).attr('validatefunc') != undefined && $(form).attr('validatefunc') != null ? $(form).attr('validatefunc').toString() : null;
        if (!this[beginName](funcValName)) return;

        alertFunctions.questionAlert($('#AlertTitle').val(), $('#VerificationText').val(), function () {
            pageFunctions.showElement($('#preloader'));

            if (form.getAttribute("enctype") === "multipart/form-data") {
                if (form.dataset.ajax) {
                    var xhr = new XMLHttpRequest();
                    xhr.open(form.method, form.action);
                    xhr.onreadystatechange = function () {
                        console.log(xhr)
                        if (xhr.readyState == 4) {
                            pageFunctions.hideElement($('#preloader'));
                            if (xhr.status == 200) {
                                if (form.getAttribute('OnSuccess')) {
                                    alertFunctions.callBackAlert($('#AlertTitle').val(), "Operación realizada con éxito", "success", function () {
                                        window[form.getAttribute('OnSuccess').toString()](xhr.responseText);
                                    });
                                } else {
                                    alertFunctions.basicAlert($('#AlertTitle').val(), "Operación realizada con éxito", "success");
                                    if (form.dataset.ajaxUpdate) {
                                        var updateTarget = document.querySelector(form.dataset.ajaxUpdate);
                                        if (updateTarget) {
                                            updateTarget.innerHTML = xhr.responseText;
                                        }
                                    }
                                }
                            } else {
                                console.log(xhr);
                                try {
                                    var response = JSON.parse(xhr.responseText);
                                    alertFunctions.basicAlert($('#AlertTitle').val(), response.ResponseText, "error");
                                }
                                catch (ex) {
                                    console.log(ex.message);
                                    if (xhr.statusText != undefined) {
                                        alertFunctions.basicAlert($('#AlertTitle').val(), xhr.statusText, 'error');
                                    } else {
                                        alertFunctions.basicAlert($('#AlertTitle').val(), "Ha ocurrido un error.", 'error');
                                    }
                                }
                            }
                        }
                    };
                    xhr.send(new FormData(form));
                }
            } else {
                $(form).trigger(e.type, { 'send': true });
            }
        });
    }
}, true);

//CLIENT CONF
var userConfiguration = {
    setEvents: function () {
        $('.item_theme_user').each(function (index, item) {
            $(item).on('click', function () {
                pageFunctions.showElement($('#preloader'));
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: $('#url_conf_user').val(),
                    data: {
                        Theme: $(item).parent().attr('data-bodyclass'),
                        FontSize: slider.value,
                    },
                    success: function (data) {

                    },
                    error: function (data) {

                    },
                    complete: function () {
                        pageFunctions.hideElement($('#preloader'));
                    }
                });
            });
        });
    },

    saveConf: function () {
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: $('#url_conf_user').val(),
            data: {
                Theme: document.querySelector('body').dataset.theme,
                FontSize: slider.value,
            },
            success: function (data) {

            },
            error: function (data) {

            },
            complete: function () {
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },

    init: function () {
        this.setEvents();
    }
};

userConfiguration.init();

//CLIENT INFO
function getOSName() {
    var OSName = "Desconocido";
    if (window.navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
    if (window.navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
    if (window.navigator.appVersion.indexOf("X11") != -1) OSName = "UNIX";
    if (window.navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";
    return OSName;
}

var browser = function () {
    // Return cached result if avalible, else get result then cache it.
    if (browser.prototype._cachedResult)
        return browser.prototype._cachedResult;

    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;

    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;

    return browser.prototype._cachedResult = isOpera ? 'Opera' : isFirefox ? 'Firefox' : isSafari ? 'Safari' : isChrome ? 'Chrome' : isIE ? 'IE' : isEdge ? 'Edge' : isBlink ? 'Blink' : "Desconocido";
};

//SELECTS
var materialSelects = {
    crateSelects: function () {
        var selects = $('select').not('.select_select2');
        var toSend = [];

        $.each(selects, function (selectInd, select) {
            var options = $(select).children();
            var selectOptionsModel = [];

            $.each(options, function (index, item) {

                if ($(item).attr('value') != '') {
                    var color = null;
                    var text = $(item).text();
                    if ($(item).text().indexOf('#') != -1) {
                        color = $(item).text().substring($(item).text().indexOf('#'), $(item).text().length);
                        text = $(item).text().substring(0, $(item).text().indexOf('#'));
                    }

                    var postalCode = null;
                    if (text.indexOf('+') != -1) {
                        postalCode = text.substring((text.indexOf('+') + 1), (text.indexOf('+') + 6));
                        text = text.substring(0, text.indexOf('+'));
                        $(item).attr('PostalCode', postalCode);
                    }

                    selectOptionsModel.push({
                        Text: text,
                        Id: parseInt($(item).attr('value')),
                        Disabled: false,
                        Color: color,
                    });
                }
            });

            toSend.push({
                Text: $(select).attr('placeholder'),
                Id: $(select).attr('id'),
                Disabled: $(select).attr('selectDisabled') != undefined,
                SelectedId: parseInt($(select).val() == '' ? 0 : $(select).val()),
                TextInBlank: $(select).attr('TextInBlank'),
                iconAlert: $(select).attr('iconAlert'),
                InModal: $(select).attr('InModal'),

                Options: selectOptionsModel,
            });
        });

        if (toSend.length == 0) return;
        pageFunctions.showElement($('#preloader'));
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: "POST",
            url: $('#Url_CreateSelect').val(),
            data: JSON.stringify({ selects: toSend }),
            async: false,
            cache: false,
            success: function (data) {
                if (data.response.length > 0) {
                    $.each(data.response, function (index, item) {
                        $('#' + item.Key + 'SelectDiv').html(item.Value);
                    });
                }
            },
            error: function (data) {
                console.log(data);
            },
            complete: function () {
                pageFunctions.hideElement($('#preloader'));
            }
        });
    },

    activeSelects: function () {
        var nodes = document.querySelectorAll('.mdc-select');
        if (nodes != null) {
            $.each(nodes, function (index, item) {
                var selectElems = new mdc.select.MDCSelect(item);
                selectElems.listen('MDCSelect:change', function () {
                    var id = $(selectElems.root_).attr('id').replace('select', '');
                    $('#' + id).val(selectElems.value.trim()).trigger('change');
                });
            });
        }
    },

    init: function () {
        this.crateSelects();
        this.activeSelects();
    }
}

materialSelects.init();

//COLOR-PICKER
var colorPicker = {
    createPickers: function () {
        var inputPickers = document.querySelectorAll('.input-color-picker');

        if (inputPickers != null) {
            if (inputPickers.length > 0) {
                Array.prototype.forEach.call(inputPickers, function (item) {
                    $(item).spectrum({
                        color: $(item).val() == '' ? '#ff0000' : $(item).val(),
                        showInput: true,
                        cancelText: '',
                        chooseText: '',
                        preferredFormat: 'hex',
                    });
                    $(item).val($(item).val() == '' ? '#ff0000' : $(item).val());
                });
            }
        }
    },

    init: function () {
        this.createPickers();
    }
}

colorPicker.init();

//SLIDERS
var sliderInputs = {
    createSliders: function () {
        var inputs = document.querySelectorAll('.input-slider');

        if (inputs != null) {
            if (inputs.length > 0) {
                Array.prototype.forEach.call(inputs, function (item) {
                    var slider = document.querySelector('#' + item.getAttribute('for'));
                    slider.setAttribute('aria-valuenow', item.value)

                    var instSlider = new mdc.slider.MDCSlider(slider);

                    instSlider.listen('MDCSlider:change', function () {
                        item.value = instSlider.value
                    });
                });
            }
        }
    },

    init: function () {
        this.createSliders();
    }
}

sliderInputs.init();

//FILEINPUTS: IMAGES
var imagesInputs = {
    createInputs: function () {
        var nodes = document.querySelectorAll('.imageInput');

        if (nodes != null) {
            if (nodes.length > 0) {
                Array.prototype.forEach.call(nodes, function (item) {
                    item.addEventListener('change', function () {
                        var fileName = item.value;
                        var indexExt = fileName.lastIndexOf('.') + 1;
                        var fileExt = fileName.substr(indexExt, fileName.length).toLowerCase();

                        if (fileExt != 'jpg' && fileExt != 'png') {
                            document.getElementById(item.getAttribute('for')).setAttribute('src', document.getElementById('Url_DefaultImage').value);
                            item.value = '';
                        }

                        if (item.files && item.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                document.getElementById(item.getAttribute('for')).setAttribute('src', e.target.result);
                            }
                            reader.readAsDataURL(item.files[0]);
                        }
                    });
                });
            }
        }
    },

    downloadFiles: function (url) {
        location.href = url;
    },

    init: function () {
        this.createInputs();
    }
}

imagesInputs.init();

//DATE-PICKER
var dateTimePicker = {
    createDatePickers: function () {
        var inputPickers = document.querySelectorAll('.input-date-picker');

        if (inputPickers != null) {
            if (inputPickers.length > 0) {
                Array.prototype.forEach.call(inputPickers, function (item) {
                    var minDate = $(item).attr('minDate');
                    var maxDate = $(item).attr('maxDate');
                    if (minDate != undefined) minDate = minDate.split('-');
                    if (maxDate != undefined) maxDate = maxDate.split('-');

                    $(item).bootstrapMaterialDatePicker({
                        time: false,
                        clearButton: true,
                        format: 'DD-MM-YYYY',
                        clearButton: false,
                        cancelText: 'Cancelar',
                        okText: 'Seleccionar',
                        minDate: minDate != undefined ? new Date(minDate[2], parseInt(minDate[1]) - 1, minDate[0]) : null,
                        maxDate: maxDate != undefined ? new Date(maxDate[2], parseInt(maxDate[1]) - 1, maxDate[0]) : null,
                    });
                });
            }
        }
    },
    createDateTimePickers: function () {
        var inputPickers = document.querySelectorAll('.input-date-time-picker');

        if (inputPickers != null) {
            if (inputPickers.length > 0) {
                Array.prototype.forEach.call(inputPickers, function (item) {
                    var minDate = $(item).attr('minDate');
                    var maxDate = $(item).attr('maxDate');
                    if (minDate != undefined) minDate = minDate.split('-');
                    if (maxDate != undefined) maxDate = maxDate.split('-');

                    $(item).bootstrapMaterialDatePicker({
                        clearButton: true,
                        format: 'DD-MM-YYYY HH:mm',
                        clearButton: false,
                        cancelText: 'Cancelar',
                        okText: 'Seleccionar',
                        minDate: minDate != undefined ? new Date(minDate[2], parseInt(minDate[1]) - 1, minDate[0]) : null,
                        maxDate: maxDate != undefined ? new Date(maxDate[2], parseInt(maxDate[1]) - 1, maxDate[0]) : null,
                    });
                });
            }
        }
    },

    init: function () {
        this.createDatePickers();
        this.createDateTimePickers();
    }
}

dateTimePicker.init();

function reloadElements() {
    mdc.autoInit()
    materialSelects.init();
    sliderInputs.init();
    colorPicker.init();
    imagesInputs.init();
    dateTimePicker.init();
    componentHandler.upgradeAllRegistered();
    regexInputs.init();
}

//REGEX
var numberPattern = /^\d+$/;
var decimalPattern = /^\d{1,3}\.?\d{0,7}$/;
var phonePattern = /\d|\(|\)|-|\+/;
var LatLongPattern = /\d|-|\./;
var textPattern = /^\"/;
var searchSpecial = '$Backspace$Delete$ArrowLeft$ArrowRight$ArrowUp$ArrowDown$Home$End$Tab$';

var regexInputs = {
    validateKey: function (key) {
        return searchSpecial.indexOf('$' + key + '$') < 0;
    },

    numeric: function () {
        $.each($('input:text.numeric'), function (index, item) {
            $(item).keypress(function (e) {
                if (regexInputs.validateKey(e.key)) {
                    var value = String.fromCharCode(window.event ? event.keyCode : e.which);
                    if (!numberPattern.test(value)) return false;
                }
            });
        });
    },
    text: function () {
        $.each($('input:text'), function (index, item) {
            $(item).keypress(function (e) {
                if (regexInputs.validateKey(e.key)) {
                    var value = String.fromCharCode(window.event ? event.keyCode : e.which)
                    if (textPattern.test(value)) return false;
                }
            });

            $(item).focusout(function (e) {
                $(this).val($(this).val().trim());
            });
        });
    },
    textArea: function () {
        $.each($('textarea'), function (index, item) {
            $(item).keypress(function (e) {
                if (regexInputs.validateKey(e.key)) {
                    var value = String.fromCharCode(window.event ? event.keyCode : e.which)
                    if (textPattern.test(value)) return false;
                }
            });

            $(item).focusout(function (e) {
                $(this).val($(this).val().trim());
            });
        });
    },
    pass: function () {
        $.each($('input:password'), function (index, item) {
            $(item).attr('data-typeInput', 'password');
            $(item).parent().addClass('show_pass');

            $(item).parent().find('.multi_icon').on('mouseenter', function () {
                $(item).attr('type', 'text');
            });
            $(item).parent().find('.multi_icon').on('mouseleave', function () {
                $(item).attr('type', 'password');
            });
            $(item).parent().find('.multi_icon').on('click', function () {
                if ($(item).attr('type') == 'password') {
                    $(item).attr('type', 'text');
                } else {
                    $(item).attr('type', 'password');
                }
            });

            $(item).focusout(function (e) {
                $(this).val($(this).val().trim());
            });
        });
    },
    decimal: function () {
        $.each($('input:text.decimal'), function (index, item) {
            $(item).keyup(function (e) {
                var value = $(this).val();
                if (decimalPattern.exec(value) === null) $(this).val('');
            });
        });
    },
    decimalLat: function () {
        $.each($('input:text.decimalLat'), function (index, item) {
            $(item).keypress(function (e) {
                var value = String.fromCharCode(window.event ? event.keyCode : e.which)
                if (!LatLongPattern.test(value)) return false;
            });

            $(item).mask('g099.0999999', {
                translation: {
                    'g': {
                        pattern: /[-]/,
                        optional: true,
                    },
                }
            });
        });
    },
    money: function () {
        $.each($('input:text.numeric_money'), function (index, item) {
            $(item).keypress(function (e) {
                var value = String.fromCharCode(window.event ? event.keyCode : e.which)
                if (!LatLongPattern.test(value)) return false;
            });

            $(item).mask('099999999.0099');
        });
    },
    phone: function () {
        $.each($('input:text.inputPhone'), function (index, item) {
            $(item).keypress(function (e) {
                var value = String.fromCharCode(e.keyCode)
                if (!phonePattern.test(value)) return false;
            });

            $(item).mask('+(099)0000-0000');
        });
    },

    IdentificationTypeMask: function (elemId, optionValue) {
        $('#' + elemId).unmask();
        $('#' + elemId).removeAttr('pattern');
        switch (optionValue) {
            case $('#IDType_National').val():
                $('#' + elemId).mask('0-0000-0000');
                $('#' + elemId).attr('pattern', '([0-9]{1})+-+([0-9]{4})+-+([0-9]{4})$');
                break;
            case $('#IDType_International').val():
                $('#' + elemId).mask('000000000000');
                $('#' + elemId).attr('pattern', '[0-9]{12}$');
                break;
            default:
                break;
        }
    },

    init: function () {
        this.numeric();
        this.text();
        this.textArea();
        this.pass();
        this.decimal();
        this.decimalLat();
        this.money();
        this.phone();
    }
}

regexInputs.init();

//VALIDATIONS
var validationsForm = {
    text: function (elemId) {
        if ($('#' + elemId).val() == '') {
            $('#' + elemId).parents('.mdc-text-field').addClass('mdc-text-field--invalid');
            return 1;
        } else {
            $('#' + elemId).parents('.mdc-text-field').removeClass('mdc-text-field--invalid');
            return 0;
        }
    },
    select: function (elemId) {
        var parent = $('#select' + elemId).parent();
        if ($('#' + elemId).val() == '') {
            $('#select' + elemId).addClass('mdc-text-field--invalid');
            if ($(parent).hasClass('MemberSearchElem')) {
                console.log($(parent).parent())
                $(parent).parent().siblings('.MemberSearchElemContainer').addClass('mdc-text-field--invalid');
            }
            return 1;
        } else {
            $('#select' + elemId).removeClass('mdc-text-field--invalid');
            if ($(parent).hasClass('MemberSearchElem')) {
                $(parent).parent().siblings('.MemberSearchElemContainer').removeClass('mdc-text-field--invalid');
            }
            return 0;
        }
    },
    image: function (elemFileId, elemUrlId) {
        if ($('#' + elemFileId).val() == '' && $('#' + elemUrlId).val() == '') {
            $('#' + elemFileId + '-helper-text').addClass('mdc-text-field--invalid');
            return 1;
        } else {
            $('#' + elemFileId + '-helper-text').removeClass('mdc-text-field--invalid');
            return 0;
        }
    },
    divContainers: function (elemContainerClass, elemTitleClass, elemBodyClass) {
        var resp = 0;

        if ($('.' + elemContainerClass).length > 0) {
            $('.' + elemContainerClass).each(function (index, item) {
                var titleContainer = $(item).find('.' + elemTitleClass);
                var bodyContainer = $(item).find('.' + elemBodyClass);

                if ($(bodyContainer).find('.mdc-text-field--invalid').length > 0) {
                    $(titleContainer).addClass('mdc-text-field--invalid');

                    var elemDiff = $(item).attr('id').substring(14, $(item).attr('id').length);
                    if ($(item).hasClass('unselected')) {
                        unhide('item' + elemDiff);
                        unhide('name' + elemDiff);
                        unhide('container_item' + elemDiff);
                        unhide('icon_item' + elemDiff);
                    }

                    resp++;
                } else {
                    $(titleContainer).removeClass('mdc-text-field--invalid');
                }
            });
        }

        return resp;
    },
    iqualSelects: function (elemContainerId, message) {
        var resp = 0;
        var values = [];

        $('#' + elemContainerId + ' select').each(function (index, item) {
            if ($(item).val() != null && $(item).val() != '') {
                var flag = false;
                $(values).each(function (ind, itm) {
                    if (itm == parseInt($(item).val())) {
                        $('#select' + $(item).attr('id')).addClass('mdc-text-field--invalid');
                        flag = true;
                        return false;
                    }
                });
                if (flag) {
                    resp++;
                    alertFunctions.basicAlert($('#AlertTitle').val(), message, 'warning')
                } else {
                    values.push(parseInt($(item).val()));
                }
            }
        });

        return resp;
    }
}


//PAGINATION
var searchTextTemporal = null;
var paginationCustomFunctions = {
    loadPagination: function (newPosition) {
        var current = null;
        if (newPosition == null) current = parseInt($('.cap.current').attr('data'));
        else current = newPosition;

        var cant = $('.cap[data]').length;
        var step = Math.ceil(cant / 5);

        $('a', '#paginationContainer').addClass('hide');
        $('.cap[data]').removeClass('firstStep')
        $('.cap[data]').removeClass('finalStep')

        for (var i = 0; i < step; i++) {
            if (current > (i * 5) && current <= ((i + 1) * 5)) {
                $('.cap[data]').each(function (index, item) {
                    var aux = parseInt($(item).attr('data'))
                    if (aux > (i * 5) && aux <= ((i + 1) * 5)) {
                        $(item).removeClass('hide');
                    }
                    if (aux == ((i * 5) + 1)) $(item).addClass('firstStep');
                    if (aux == ((i + 1) * 5)) $(item).addClass('finalStep');
                });

                if (!(i == 0)) $('#BtnMoreBackPagination').removeClass('hide');
                if (!(i == (step - 1))) $('#BtnMoreNextPagination').removeClass('hide');

                if (parseInt($('.cap.current').attr('data')) != 1) $('#BtnBackPagination').removeClass('hide');
                if (parseInt($('.cap.current').attr('data')) != cant) $('#BtnNextPagination').removeClass('hide');

                break;
            }
        }
    },
    chargePaginationButtons: function () {
        $('#BtnMoreBackPagination').on('click', function () {
            var currentAux = parseInt($('.cap.firstStep').attr('data')) - 5;
            paginationCustomFunctions.loadPagination(currentAux);
        });

        $('#BtnMoreNextPagination').on('click', function () {
            var currentAux = parseInt($('.cap.finalStep').attr('data')) + 5;
            paginationCustomFunctions.loadPagination(currentAux);
        });

        $('.cap[data]').on('click', function () {
            var current = parseInt($('.cap.current').attr('data'));
            var pageToSend = parseInt($(this).attr('data'));
            if (current == pageToSend) return;

            gateway.getDataByData($('#Url_Get_Pagination').val(), { Search: $('#inputGridSearch').val(), Number: pageToSend }, $('#Container_Grid_Pagination').val());
        });

        $('#BtnBackPagination').on('click', function () {
            var pageToSend = parseInt($('.cap.current').attr('data')) - 1;
            gateway.getDataByData($('#Url_Get_Pagination').val(), { Search: $('#inputGridSearch').val(), Number: pageToSend }, $('#Container_Grid_Pagination').val());
        });

        $('#BtnNextPagination').on('click', function () {
            var pageToSend = parseInt($('.cap.current').attr('data')) + 1;
            gateway.getDataByData($('#Url_Get_Pagination').val(), { Search: $('#inputGridSearch').val(), Number: pageToSend }, $('#Container_Grid_Pagination').val());
        });
    },
    chargeSearchButtons: function () {
        searchTextTemporal = $('#inputGridSearch').val();

        var inputGrid = document.getElementById('inputGridSearch');
        if (inputGrid) {
            inputGrid.addEventListener('keyup', function (event) {
                event.preventDefault();
                if (event.keyCode === 13) {
                    document.getElementById("SearchDiv").click();
                }
            });
        }

        $('#SearchDiv').on('click', function () {
            if ($('#inputGridSearch').val() == '') return;

            var pageToSend = parseInt($('.cap.current').attr('data'));
            if (isNaN(pageToSend)) {
                pageFunctions.showElement($('#preloader'));
                if ($('#Url_Get_Pagination').val().indexOf('SearchText') != -1)
                    location.href = $('#Url_Get_Pagination').val().replace('-1', $('#inputGridSearch').val());
                else
                    location.href = $('#Url_Get_Pagination').val() + '?SearchText=' + $('#inputGridSearch').val();
            } else {
                gateway.getDataByData($('#Url_Get_Pagination').val(), { Search: $('#inputGridSearch').val(), Number: pageToSend }, $('#Container_Grid_Pagination').val());
            }
        });

        $('#SearchCloseDiv').on('click', function () {
            if (searchTextTemporal != '') {
                var pageToSend = parseInt($('.cap.current').attr('data'));
                if (isNaN(pageToSend)) {
                    pageFunctions.showElement($('#preloader'));
                    if ($('#Url_Get_Pagination').val().indexOf('SearchText') != -1)
                        location.href = $('#Url_Get_Pagination').val().substring(0, ($('#Url_Get_Pagination').val().indexOf('SearchText') - 1));
                    else
                        location.href = $('#Url_Get_Pagination').val();
                } else {
                    gateway.getDataByData($('#Url_Get_Pagination').val(), { Search: null, Number: pageToSend }, $('#Container_Grid_Pagination').val());
                }
            } else {
                $('#inputGridSearch').val('');
            }
        });
    },
    chargeExportButtons: function () {
        var menuEl = document.querySelector('#download-menu');

        if (menuEl != null) {
            var menu = new mdc.menu.MDCMenu(menuEl);
            var menuButtonEl = document.querySelector('#exportButtonDiv');

            menuButtonEl.addEventListener('click', function () {
                menu.open = !menu.open;
            });

            menuEl.addEventListener('MDCMenu:selected', function (evt) {
                console.log(evt.detail);
            });

            menu.quickOpen = true;
        }
    },

    init: function () {
        this.loadPagination();
        this.chargePaginationButtons();
        this.chargeSearchButtons();
        this.chargeExportButtons();
    }
}

paginationCustomFunctions.init();

//NOTIFICATIONS
var Url_Notifications = $('#Url_Notifications').val();
var notificationFunctions = {
    loadNotificationInterval: function () {
        setInterval(function () {
            notificationFunctions.getNotifications();
        }, 60000); //change 300000
    },
    getNotifications: function () {
        $.ajax({
            url: Url_Notifications,
            type: 'GET',
            dataType: 'json',
            cache: false,
            success: function (data) {
                $('#NotificationContainer').html(data.response);
            },
            error: function (data) {
                try {
                    console.log("Notificaciones:" + data.responseJSON.ResponseText);
                }
                catch (ex) {
                    if (data.statusText != undefined) {
                        console.log("Notificaciones:" + data.statusText);
                    } else {
                        console.log("Ha ocurrido un error al obtener las notificaciones.");
                    }
                }
            },
            complete: function () {
                reloadElements();
                notificationFunctions.updateAlertPanel();
            }
        });
    },
    updateAlertPanel: function () {
        var item = document.getElementById('BtnAlert')

        Array.prototype.forEach.call(menuElList, function (el, index) {
            if (item == el[0]) menuElList.splice(index, 1);
        });

        var menuEl = document.querySelector('#' + item.dataset.menuel);
        var menu = new mdc.menu.MDCMenu(menuEl);
        menu.setAnchorCorner(mdc.menu.MDCMenuFoundation.Corner.BOTTOM_START);
        menu.quickOpen = false;
        menuElList.push([item, menuEl, menu]);

        item.addEventListener('click', function () {
            Array.prototype.forEach.call(menuElList, function (el) {
                if (item == el[0]) {
                    item.className = item.className.replace(" active", "");

                    if (el[2].open) el[2].hide();
                    else el[2].show();

                    item.className += " active";
                }
            });
        });

        menuEl.addEventListener('MDCMenu:selected', function (evt) {
            item.className = item.className.replace(" active", "");
            Array.prototype.forEach.call(menuElList, function (el) {
                el[2].root_.className += " mdc-menu--animating-open";
            });
        });

        menuEl.addEventListener('MDCMenu:cancel', function (evt) {
            item.className = item.className.replace(" active", "");
            Array.prototype.forEach.call(menuElList, function (el) {
                el[2].root_.className += " mdc-menu--animating-open";
            });
        });
    },

    init: function () {
        $(document).on('click', '.notificationItem', function () {
            var data_href = $(this).attr('data-href');
            var MemberAlertId = $(this).attr('data-MemberAlertId');

            pageFunctions.showElement($('#preloader'));
            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: $('#Url_SaveNotification').val(),
                data: JSON.stringify({ MemberAlertId: MemberAlertId }),
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    try {
                        console.log("Notificaciones:" + data.responseJSON.ResponseText);
                    }
                    catch (ex) {
                        if (data.statusText != undefined) {
                            console.log("Notificaciones:" + data.statusText);
                        } else {
                            console.log("Ha ocurrido un error al guardar la notificación.");
                        }
                    }
                },
                complete: function () {
                    if (data_href == '' || data_href == '#') {
                        notificationFunctions.getNotifications();
                        pageFunctions.hideElement($('#preloader'));
                    } else {
                        location.href = data_href;
                    }

                }
            });
        });

        this.getNotifications();
        this.loadNotificationInterval();
    }
}

if (Url_Notifications != undefined && Url_Notifications != null) {
    notificationFunctions.init();
}

//SCROLL
var inProcess_scroll = false;
var pageCount_scroll = 2;
var scrollFunctions = {
    loadScroll: function (callback) {
        $(window).scroll(function () {
            if (!inProcess_scroll) {
                if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                    if ($('#ScrollEndPage').val() != undefined) {
                        if ($('#ScrollEndPage').val() == 0) return false;
                    }

                    inProcess_scroll = true;

                    scrollFunctions.loadFirstData(callback);
                }
            }
        });
    },
    loadFirstData: function (callback, clear) {
        clear = clear || false;

        var data = null;

        if ($('#Url_Get_Binnacle').attr('StartDate') != undefined && $('#Url_Get_Binnacle').attr('StartDate') != null) {
            data = {
                Number: pageCount_scroll,
                StartDate: $('#Url_Get_Binnacle').attr('StartDate'),
                EndingDate: $('#Url_Get_Binnacle').attr('EndingDate'),
            };
        } else {
            data = {
                Number: pageCount_scroll,
            };
        }

        if (clear) {
            $('#' + $('#ContentContainer_Scroll').val()).empty();
        }
        pageFunctions.showElement($('#preloader_scroll'));
        $.ajax({
            url: $('#Url_Get_Binnacle').val(),
            type: 'GET',
            data: data,
            dataType: 'json',
            cache: false,
            success: function (data) {
                $('#' + $('#ContentContainer_Scroll').val()).append(data.response);

                pageCount_scroll++;

                if (callback != null) callback();
            },
            error: function (data) {
                try {
                    console.log('Error:' + data.responseJSON.ResponseText);
                }
                catch (ex) {
                    console.log(ex.message);
                    if (data.statusText != undefined) {
                        console.log('Error:' + data.statusText);
                    } else {
                        console.log('Error: **Ha ocurrido un error.**');
                    }
                }
            },
            complete: function () {
                inProcess_scroll = false;

                paginationCustomFunctions.init();
                reloadElements();
                pageFunctions.hideElement($('#preloader_scroll'));
            }
        });
    },

    init: function (callback) {
        callback = callback || null;
        this.loadScroll(callback);
    }
}

//MEMBER SEARCH
var MemberSearchDialog = null;
var MemberSearchFunctions = {
    loadInputSearch: function () {
        MemberSearchDialog = new mdc.dialog.MDCDialog(document.querySelector('#mdc-dialog_MemberSearch'));

        $('#FullName_MemberSearch').select2({
            language: 'es',
            placeholder: 'Seleccione un Miembro',
            minimumInputLength: 4,
            ajax: {
                url: $('#Url_SearchMember').val(),
                dataType: 'json',
                data: function (params) {
                    return { search: params.term };
                },
                processResults: function (data) {
                    $.map(data.response, function (obj) {
                        obj.id = obj.id || obj.MemberId;
                        obj.text = obj.text || obj.Names + ' ' + obj.Surnames;
                    });

                    return { results: data.response };
                },
            },
            escapeMarkup: function (markup) { return markup; },
            templateResult: MemberSearchFunctions.getRepoFormat,
            templateSelection: MemberSearchFunctions.formatRepoSelection
        });
    },
    getRepoFormat: function (repo) {
        if (repo.loading) {
            return repo.text;
        }

        var markup = "<div class='select2-result-repository clearfix pad_l_5 relative min_h_4'>" +
            "<div class='select2-result-repository__avatar wh4 absolute left_0_240_st top_0_240_st'><img class='wh4 circle' src='" + $('#Url_SearchMemberImg').val() + '?FileUrl=' + repo.Photo + "' /></div>" +
            "<div class='select2-result-repository__meta italic pad_tb_05'>" +
            "<div class='select2-result-repository__title bolder fs_12'>" + repo.text + "</div>" +
            "<div class='select2-result-repository__description fs_08'>" + repo.Email + "</div>" +
            "</div></div>";

        return markup;
    },
    formatRepoSelection: function (repo) {
        return  repo.text || repo.Names + ' ' + repo.Surnames;
    },
    loadDialogButtons: function () {
        $(document).on('click', '.BtnMemberSearchDialog', function () {
            var id = $(this).attr('data-KinSelectId');
            $('#Select_MemberSearch').attr('data-KinSelectId', id);

            MemberSearchDialog.show();
        });

        $('#Select_MemberSearch').on('click', function () {
            if ($('#FullName_MemberSearch').val() == '' || $('#FullName_MemberSearch').val() == null || $('#FullName_MemberSearch').val() == undefined) {
                $('#FullName_MemberSearch-helper-text').removeClass('hide');
            } else {
                $('#FullName_MemberSearch-helper-text').addClass('hide');

                $('#' + $('#Select_MemberSearch').attr('data-KinSelectId')).empty();
                $('#' + $('#Select_MemberSearch').attr('data-KinSelectId')).append(new Option($('#FullName_MemberSearch option:selected').text(), $('#FullName_MemberSearch').val(), false, true));
                $('.BtnMemberSearchDialog[data-KinSelectId=' + $('#Select_MemberSearch').attr('data-KinSelectId') + ']').html($('#FullName_MemberSearch option:selected').text());

                MemberSearchDialog.close();
                $('#FullName_MemberSearch').val(null).trigger('change');
            }
        });

        MemberSearchDialog.listen('MDCDialog:cancel', function () {
            $('#FullName_MemberSearch').val(null).trigger('change');
        });
    },

    init: function () {
        this.loadInputSearch();
        this.loadDialogButtons();
    }
}
