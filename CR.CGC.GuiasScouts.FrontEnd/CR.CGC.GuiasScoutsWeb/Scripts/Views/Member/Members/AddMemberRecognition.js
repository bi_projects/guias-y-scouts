﻿$(function () {
    
});

function ValidateMemberRecognition() {
    var resp = 0;
    resp += validationsForm.select('RecognitionId');
    return resp == 0;
}

function OnSuccess_MemberRecognition(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('toSaveMemberRecognition').click();
    });
}

function DeleteMemberRecognition(MemberId, MemberRecognitionId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity_Callback(
                $('#Url_Delete').val(),
                {
                    MemberId: MemberId,
                    MemberRecognitionId: MemberRecognitionId,
                },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), '¡Modificación exitosa!', "success", function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('toSaveMemberRecognition').click();
                    });
                }
            );
        });
}