﻿$(function () {

});

function ValidateProgression() {
    var resp = 0;

    if ($('#SectionId').val() == '') {
        $('#selectSectionId').addClass('mdc-text-field--invalid');
        resp++;
    } else {
        $('#selectSectionId').removeClass('mdc-text-field--invalid');
    }

    if ($('#SystemStatusId').val() == '') {
        $('#selectSystemStatusId').addClass('mdc-text-field--invalid');
        resp++;
    } else {
        $('#selectSystemStatusId').removeClass('mdc-text-field--invalid');
    }

    if ($('#SendFile').val() == '' && $('#FileLocation').val() == '') {
        $('#SendFile-helper-text').addClass('mdc-text-field--invalid');
        resp++;
    } else {
        $('#SendFile-helper-text').removeClass('mdc-text-field--invalid');
    }

    return resp == 0;
}

function OnSuccess_Progression() {
    document.getElementById('DivToList').click();
}