﻿$(function () {

});

$('#PositionTypeId_Young').on('change', function () {
    gateway.editEntity($('#Url_PositionType').val(), { PositionTypeId: parseInt($(this).val()), Type: $('#PositionTypeYoungId').val() }, 'PositionTypeContainerYoung')
});

$('#PositionTypeId_Old').on('change', function () {
    gateway.editEntity($('#Url_PositionType').val(), { PositionTypeId: parseInt($(this).val()), Type: $('#PositionTypeOldId').val() }, 'PositionTypeContainerOld')
});

function ValidatePosition_Young() {
    var resp = 0;

    resp += validationsForm.select('PositionTypeId_Young');

    return resp == 0;
}

function ValidatePosition_Old() {
    var resp = 0;

    resp += validationsForm.select('PositionTypeId_Old');

    return resp == 0;
}

function OnSuccess_Position(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('DivToList').click();
    });
}