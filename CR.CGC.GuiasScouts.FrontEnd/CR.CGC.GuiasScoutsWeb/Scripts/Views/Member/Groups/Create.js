﻿var init_select_location = false;
$(function () {

    if ($('#GroupId').val() != '' && $('#GroupId').val() != '0') {
        init_select_location = true;
        $('#ProvinceId').val($('#ProvinceId_hf').val()).trigger('change');
    }
});

function ValidateGroup() {
    var resp = 0;

    resp += validationsForm.select('SystemStatusId');
    resp += validationsForm.image('SendFile', 'HeadkerchiefImage');
    resp += validationsForm.select('ProvinceId');
    resp += validationsForm.select('CantonId');
    resp += validationsForm.select('DistrictId');
    resp += validationsForm.select('LocalId');
    resp += validationsForm.select('SectorId');

    resp += validationsForm.divContainers('containerForValidation', 'containerTitleForValidation', 'containerBodyForValidation');
    return resp == 0;
}

function OnSuccess_Group(res) {
    pageFunctions.showElement($('#preloader'));
    document.getElementById('DivToList').click();
}

$('#btnAddEmail').on('click', function () {
    var index = $('.EmailContainer').length;
    gateway.add_GetDataByData($('#Url_GroupMail').val(), { index: index }, 'GroupMailContainer');
});

$('#btnAddColor').on('click', function () {
    var index = $('.ColorContainer').length;
    gateway.add_GetDataByData($('#Url_GroupColor').val(), { index: index }, 'GroupColorContainer');
});

$('.social_network_selector').on('click', function () {
    var index = $('.SocialNetworkContainer').length;
    var type = $(this).attr('data');
    $(this).parent().addClass('hide');
    gateway.add_GetDataByData_Callback($('#Url_GroupSocialNetwork').val(), { index: index, type: type }, 'GroupSocialNetworkContainer', function () {
        $('#social_menu_new').removeClass('mdc-menu--open');
        $('#btnAddSocialNetwork').removeClass('active');
        $('.SocialNetworkContainer:last').find('input[type=text]').focus();
    });
});

$(document).on('mouseenter', 'label.icon_input_Email', function () {
    $(this).removeClass('icon_mail');
    $(this).addClass('icon_close_3');
    $(this).addClass('pointer');
});
$(document).on('mouseleave', 'label.icon_input_Email', function () {
    $(this).removeClass('icon_close_3');
    $(this).removeClass('pointer');
    $(this).addClass('icon_mail');
});
$(document).on('focusin', 'input.icon_input_Email', function () {
    $(this).parent().find('label.icon_input_Email').removeClass('icon_mail');
    $(this).parent().find('label.icon_input_Email').addClass('icon_close_3');
});
$(document).on('focusout', 'input.icon_input_Email', function () {
    $(this).parent().find('label.icon_input_Email').addClass('icon_mail');
    $(this).parent().find('label.icon_input_Email').removeClass('icon_close_3');
});

$(document).on('click', 'label.icon_input_Email', function () {
    $(this).parents('div.text-left').remove();

    //order
    var index = 0;
    $('.EmailContainer').each(function (index, item) {
        $(this).find('p.mdc-text-field-helper-text').attr('id', 'GroupEmail_Email_' + index + '-helper-text');
        $(this).find('input[type=hidden]').attr('name', 'GroupEmails[' + index + '].GroupEmailId');
        $(this).find('input[type=hidden]').attr('id', 'GroupEmail_GroupEmailId_' + index);
        $(this).find('label').attr('for', 'GroupEmail_Email_' + index);
        $(this).find('input[type=text]').attr('name', 'GroupEmails[' + index + '].Email');
        $(this).find('input[type=text]').attr('id', 'GroupEmail_Email_' + index);
        $(this).find('input[type=text]').attr('aria-controls', 'GroupEmail_Email_' + index + '-helper-text');

        index++;
    });
});

$(document).on('click', 'span.icon_input_Color', function () {
    $(this).parents('div.ColorContainer').remove();

    //order
    var index = 0;
    $('.ColorContainer').each(function (index, item) {
        $(this).find('div.visualInputColor').attr('id', 'visualInputColor-InputColor_' + index);
        $(this).find('input[type=hidden]').attr('name', 'GroupColors[' + index + '].GroupColorId');
        $(this).find('input[type=hidden]').attr('id', 'GroupColors_GroupColorId_' + index);
        $(this).find('input[type=text]').attr('name', 'GroupColors[' + index + '].Color');
        $(this).find('input[type=text]').attr('id', 'GroupColors_Color_' + index);
        $(this).find('input[type=text]').attr('for', 'visualInputColor-InputColor_' + index);

        index++;
    });
});

$(document).on('mouseenter', 'label.icon_input_Social', function () {
    $(this).removeClass($(this).attr('data-icon'));
    $(this).addClass('icon_close_3');
    $(this).addClass('pointer');
});
$(document).on('mouseleave', 'label.icon_input_Social', function () {
    $(this).removeClass('icon_close_3');
    $(this).removeClass('pointer');
    $(this).addClass($(this).attr('data-icon'));
});
$(document).on('focusin', 'input.icon_input_Social', function () {
    var elem = $(this).parent().find('label.icon_input_Social');
    $(elem).removeClass($(elem).attr('data-icon'));
    $(elem).addClass('icon_close_3');
});
$(document).on('focusout', 'input.icon_input_Social', function () {
    var elem = $(this).parent().find('label.icon_input_Social');
    $(elem).addClass($(elem).attr('data-icon'));
    $(elem).removeClass('icon_close_3');
});

$(document).on('click', 'label.icon_input_Social', function () {
    var type = $(this).parent().find('input[type=hidden][data=type]').val();
    $('.social_network_selector[data=' + type + ']').parent().removeClass('hide');
    $(this).parents('div.text-left').remove();

    //order
    var index = 0;
    $('.SocialNetworkContainer').each(function (index, item) {
        $(this).find('p.mdc-text-field-helper-text').attr('id', 'GroupSocialNetworks_Nickname_' + index + '-helper-text');
        $(this).find('input[type=hidden][data=id]').attr('name', 'GroupSocialNetworks[' + index + '].GroupSocialNetworkId');
        $(this).find('input[type=hidden][data=id]').attr('id', 'GroupSocialNetworks_GroupSocialNetworkId_' + index);
        $(this).find('input[type=hidden][data=type]').attr('name', 'GroupSocialNetworks[' + index + '].Type');
        $(this).find('input[type=hidden][data=type]').attr('id', 'GroupSocialNetworks_Type_' + index);

        $(this).find('label').attr('for', 'GroupSocialNetworks_Nickname_' + index);
        $(this).find('input[type=text]').attr('name', 'GroupSocialNetworks[' + index + '].Nickname');
        $(this).find('input[type=text]').attr('id', 'GroupSocialNetworks_Nickname_' + index);
        $(this).find('input[type=text]').attr('aria-controls', 'GroupSocialNetworks_Nickname_' + index + '-helper-text');

        index++;
    });
});

$(document).on('change', '#ProvinceId', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    gateway.getDataByData_Callback($('#Url_SelectCanton').val(), { ProvinceId: id }, 'CantonContainer', function () {
        if (init_select_location) 
            $('#CantonId').val($('#CantonId_hf').val()).trigger('change');
        else
            $('#CantonId').trigger('change');
    });
});

$(document).on('change', '#CantonId', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    gateway.getDataByData_Callback($('#Url_SelectDistrict').val(), { CantonId: id }, 'DistrictContainer', function () {
        if (init_select_location) {
            $('#DistrictId').val($('#DistrictId_hf').val()).trigger('change');
            init_select_location = false;
        }
        else
            $('#DistrictId').trigger('change');
    });
});

