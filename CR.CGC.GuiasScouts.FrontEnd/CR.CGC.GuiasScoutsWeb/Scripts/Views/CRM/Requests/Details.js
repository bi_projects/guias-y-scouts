﻿$(function () {
    document.getElementById('RequestCommentContainer').scrollTop = document.getElementById('RequestCommentContainer').scrollHeight;
});

function downloadFile(url) {
    imagesInputs.downloadFiles(url);
}

$('#BtnSendComment').on('click', function () {
    gateway.saveEntity_Callback($('#Url_SaveComment').val(),
        {
            comment: {
                RequestCommentId: 0,
                RequestId: $('#RequestId').val(),
                Comment: $('#comment').val(),
                Commentator: $('#CommentatorId').val(),
            }
        },
        function (resp) {
            $('#RequestCommentContainer').html(resp.response);
            $('#comment').val('');
            document.getElementById('RequestCommentContainer').scrollTop = document.getElementById('RequestCommentContainer').scrollHeight;
        });
});
