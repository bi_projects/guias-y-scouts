﻿var init_select_location = false
$(function () {
    if ($('#RequestId').val() != '' && $('#RequestId').val() != 0) {
        init_select_location = true;
        $('#TopicTypeId').val($('#TopicTypeId_hf').val()).trigger('change');
    }

    document.getElementById('RequestCommentContainer').scrollTop = document.getElementById('RequestCommentContainer').scrollHeight;
});

function downloadFile(url) {
    imagesInputs.downloadFiles(url);
}

$('#TopicTypeId').on('change', function () {
    gateway.getDataByData_Callback($('#Url_RequestTopic').val(), { TopicTypeId: $('#TopicTypeId').val() }, 'RequestTopicContainer', function () {
        if (init_select_location) {
            init_select_location = false;
            $('#TopicId').val($('#TopicId_hf').val()).trigger('change');
        }
    });
});

function ValidateRequest() {
    var resp = 0;
    resp += validationsForm.select('RequestStatusId');
    resp += validationsForm.select('PriorityId');
    resp += validationsForm.select('TopicId');

    return resp == 0;
}

function OnSuccess_Request() {
    pageFunctions.showElement($('#preloader'));
    document.getElementById('DivToList').click();
}

$('#BtnSendComment').on('click', function () {
    gateway.saveEntity_Callback($('#Url_SaveComment').val(),
        {
            comment: {
                RequestCommentId: 0,
                RequestId: $('#RequestId').val(),
                Comment: $('#comment').val(),
                Commentator: $('#CommentatorId').val(),
            }
        },
        function (resp) {
            $('#RequestCommentContainer').html(resp.response);
            $('#comment').val('');
            document.getElementById('RequestCommentContainer').scrollTop = document.getElementById('RequestCommentContainer').scrollHeight;
        });
});