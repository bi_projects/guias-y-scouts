﻿$(function () {

});

function DeleteTopic(TopicId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity(
                $('#Url_Delete').val(),
                {
                    TopicId: TopicId,
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                },
                'GridTopics'
            );
        });
}

function DeleteTopics() {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntitiesPag(
                $('#Url_Delete').val(),
                'Topics',
                'GridTopics',
                'GridTopics',
                {
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                }
            );
        });
}
