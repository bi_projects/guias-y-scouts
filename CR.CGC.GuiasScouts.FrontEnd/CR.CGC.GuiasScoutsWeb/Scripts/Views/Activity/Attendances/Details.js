﻿var eventAssessment = null;
$(function () {
    eventAssessment = parseFloat($('#rate_user').attr('data-rateyo_rate'));

    $('#rate_user').rateYo({
        rating: $('#rate_user').attr('data-rateyo_rate'),
        halfStar: true,
        starWidth: "15px"
    }).on('rateyo.set', function (e, data) {
        if (data.rating != eventAssessment) {
            pageFunctions.showElement($('#preloader'));
            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: $('#Url_SaveAssessment').val(),
                data: JSON.stringify({ EventId: $('#EventId').val(), Assessment: data.rating }),
                success: function (resp) {
                    $('#rate_user_visual').html(data.rating);
                    eventAssessment = data.rating;
                },
                error: function (resp) {
                    console.log(resp);
                    $("#rate_user").rateYo("option", "rating", eventAssessment);
                },
                complete: function () {
                    pageFunctions.hideElement($('#preloader'));
                }
            });
        }
    });
});

function addAttendance(EventId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.saveEntity_Callback(
                $('#Url_Add').val(),
                { EventId: EventId },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('Url_toAttendance').click();
                    });
                }
            );
        });
}

function removeAttendance(EventId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity_Callback(
                $('#Url_Delete').val(),
                { EventId: EventId },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Eliminado exitosamente.', 'success', function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('Url_toAttendance').click();
                    });
                }
            );
        });
}