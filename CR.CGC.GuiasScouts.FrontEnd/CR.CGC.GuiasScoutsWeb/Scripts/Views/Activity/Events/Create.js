﻿$(function () {
    $('#Description').summernote({
        airMode: false,
        lang: 'es-ES',
        height: 500,
        tabsize: 2,
        minHeight: null,
        maxHeight: null,
        dialogsInBody: true,
    });

    $('#TemplateBody').summernote({
        airMode: false,
        lang: 'es-ES',
        height: 450,
        tabsize: 2,
        minHeight: null,
        maxHeight: null,
        dialogsInBody: true,
    });
});

var dialogEmail = new mdc.dialog.MDCDialog(document.querySelector('#mdc-dialog_Template'));

document.querySelector('#BtnAddEmail').addEventListener('click', function (evt) {
    dialogEmail.show();
});

function ValidateEvent() {
    var resp = 0;

    resp += validationsForm.image('SendFile', 'EventImage');
    resp += validationsForm.select('EventStatusId');
    resp += validationsForm.text('QuotaLimit');
    resp += validationsForm.text('Name');
    resp += validationsForm.select('ActivityTypeId');
    resp += validationsForm.select('ScopeId');
    resp += validationsForm.text('Cost');
    resp += validationsForm.text('StartDateTime');
    resp += validationsForm.text('EndDateTime');
    resp += validationsForm.select('AgeRangeId');
    resp += validationsForm.text('InscriptionStart');
    resp += validationsForm.text('InscriptionEnd');
    resp += validationsForm.text('Place');
    resp += validationsForm.text('Address');

    $('#EventAgeRangeContainer select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    resp += validationsForm.iqualSelects('EventAgeRangeContainer', 'Existen rangos de edad repetidos.');

    $('#EventRoleContainer select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    resp += validationsForm.iqualSelects('EventRoleContainer', 'Existen roles responsables repetidos.');

    if ($('#Description').summernote('isEmpty')) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'El contenido de la descripción está vacío.', 'warning');
        resp++;
    }

    $('.panel[role=tabpanel]').each(function (index, item) {
        if ($(item).find('.mdc-text-field--invalid').length > 0) {
            $('a.mdc-tab[data-rel=' + $(item).attr('id') + ']').find('.tab_icon_alert').removeClass('hide');
        } else {
            $('a.mdc-tab[data-rel=' + $(item).attr('id') + ']').find('.tab_icon_alert').addClass('hide');
        }
    });

    return resp == 0;
}

function ValidateTemplate() {
    var resp = 0;

    resp += validationsForm.text('Name');
    if (resp > 0) alertFunctions.basicAlert($('#AlertTitle').val(), 'El nombre del evento está vacío.', 'warning');

    if ($('#TemplateBody').summernote('isEmpty')) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'El contenido de la plantilla está vacío.', 'warning');
        resp++;
    }

    return resp == 0;
}

function OnSuccess_Event(res) {
    pageFunctions.showElement($('#preloader'));
    document.getElementById('DivToList').click();
}

$('#FormEmailTemplate').on('submit', function (e) {
    e.preventDefault();

    if ($('#FormEmailTemplate').hasClass('SendEmail_Press')) {
        if (!ValidateTemplate()) return false;

        var address = '';
        if ($('.EmailToSend').length > 0) {
            $('.EmailToSend').each(function (index, item) {
                if (index == 0) address += $(item).html();
                else address += ', ' + $(item).html();
            });
            address += ', ' + $('#Email').val();
        } else {
            address += $('#Email').val();
        }

        gateway.saveEntity_Callback($('#Url_SendEmail').val(),
            {
                template: {
                    TemplateId: 0,
                    Name: $('#Name').val(),
                    Body: $('#TemplateBody').val(),

                    Description: address,
                }
            },
            function (resp) {
                alertFunctions.basicAlert($('#AlertTitle').val(), '¡Realizado con éxito!', "success");
                console.log(resp)
            });
    } else {
        $('#EmailContainer').append('<div class="inline_divs pad_tb_03"><label class="EmailToSend">' + $('#Email').val() + '</label><a class="mar_lr_05 wh1 bck_pos_green icon_close_3 multi_icon pointer removeEmail"></a></div>');

        $('#Email').val('');
        dialogEmail.close();
    }
});

$(document).on('click', '.removeEmail', function () {
    var email = $(this).parent().find('.EmailToSend').text();
    $('#Email').val(email);
    $(this).parent().remove();
});

$('#BtnSendEmail').on('click', function () {
    $('#FormEmailTemplate').addClass('SendEmail_Press');
    $('#FormEmailTemplate').removeClass('ValidateEmail_Press');
});

$('#BtnValidateEmail').on('click', function () {
    $('#FormEmailTemplate').addClass('ValidateEmail_Press');
    $('#FormEmailTemplate').removeClass('SendEmail_Press');
});

$('#BtnPrevEvent').on('click', function () {
    document.getElementById('NavTabTemplate').click();
});

$('#NavTabTemplate').on('click', function () {
    ValidateEvent();

});

$('.TemplateItem').on('click', function () {
    var TemplateId = $(this).attr('data-TemplateId');
    $('.TemplateItem').removeClass('bck_turkey');
    $('.TemplateItemDefault').removeClass('bck_turkey');
    $(this).addClass('bck_turkey');
    

    $('#TemplateId').val(TemplateId).trigger('change');
    $('#TemplateCode').val('');

    gateway.getData_Callback($('#Url_GetTemplate').val(), { TemplateId: TemplateId }, function (data) {
        $('#TemplateBody').summernote('destroy');
        $('#TemplateBody').val(data.response.Body.replace(/\'/g, '"'));
        $('#TemplateBody').summernote({
            airMode: false,
            lang: 'es-ES',
            height: 450,
            tabsize: 2,
            minHeight: null,
            maxHeight: null,
            dialogsInBody: true,
        });

        $('#SectionSelectTemplate').addClass('hide');
        $('#SectionPrevAndSave').removeClass('hide');
    });
});

$('.TemplateItemDefault').on('click', function () {
    var TemplateCode = $(this).attr('data-TemplateId');
    $('.TemplateItemDefault').removeClass('bck_turkey');
    $('.TemplateItem').removeClass('bck_turkey');
    $(this).addClass('bck_turkey');

    $('#TemplateCode').val(TemplateCode);
    $('#TemplateId').val('').trigger('change');

    gateway.getData_Callback($('#Url_GetDefaultTemplate').val(), { TemplateCode: TemplateCode }, function (data) {
        $('#TemplateBody').summernote('destroy');
        $('#TemplateBody').val(data.response);
        $('#TemplateBody').summernote({
            airMode: false,
            lang: 'es-ES',
            height: 450,
            tabsize: 2,
            minHeight: null,
            maxHeight: null,
            dialogsInBody: true,
        });

        $('#SectionSelectTemplate').addClass('hide');
        $('#SectionPrevAndSave').removeClass('hide');
    });
});

$('#BtnCancelSaveEvent').on('click', function () {
    $('#SectionSelectTemplate').removeClass('hide');
    $('#SectionPrevAndSave').addClass('hide');

    $('#TemplateBody').summernote('destroy');
    $('#TemplateBody').val('');
    $('#TemplateBody').summernote({
        airMode: false,
        lang: 'es-ES',
        height: 450,
        tabsize: 2,
        minHeight: null,
        maxHeight: null,
        dialogsInBody: true,
    });
});

$('#BtnVisualSaveEvent').on('click', function () {
    if (ValidateEvent()) {
        $('#EmailBody').val($('#TemplateBody').val());
        document.getElementById('BtnSaveEvent').click();
    } else {
        document.getElementById('NavTabEvent').click();
    }
});

$('#btnAddAgeRange').on('click', function () {
    var index = $('.AgeRangeContainer').length;
    gateway.add_GetDataByData($('#Url_EventAgeRange').val(), { index: index }, 'EventAgeRangeContainer');
});

$(document).on('click', 'a.removeAgeRange', function () {
    $(this).parents('div.AgeRangeContainer').remove();

    //Order
    var index = 0;
    $('.AgeRangeContainer').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'EventAgeRanges[' + index + '].EventAgeRangeId');
        $(this).find('input[type=hidden]').attr('id', 'EventAgeRanges_EventAgeRangeId_' + index);

        $(this).find('select').attr('name', 'EventAgeRanges[' + index + '].AgeRangeId');
        $(this).find('select').attr('id', 'EventAgeRanges_AgeRangeId_' + index);
        $(this).find('div.visualSelectContainer').attr('id', 'EventAgeRanges_AgeRangeId_' + index + 'SelectDiv');

        index++;
    });

    materialSelects.init();
});

$('#btnAddRole').on('click', function () {
    var index = $('.RoleContainer').length;
    gateway.add_GetDataByData($('#Url_EventRole').val(), { index: index }, 'EventRoleContainer');
});

$(document).on('click', 'a.removeRole', function () {
    $(this).parents('div.RoleContainer').remove();

    //Order
    var index = 0;
    $('.RoleContainer').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'EventRoles[' + index + '].EventRoleId');
        $(this).find('input[type=hidden]').attr('id', 'EventRoles_EventRoleId_' + index);

        $(this).find('select').attr('name', 'EventRoles[' + index + '].RoleId');
        $(this).find('select').attr('id', 'EventRoles_RoleId_' + index);
        $(this).find('div.visualSelectContainer').attr('id', 'EventRoles_RoleId_' + index + 'SelectDiv');

        index++;
    });

    materialSelects.init();
});
