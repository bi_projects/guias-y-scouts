﻿$(function () {

});

function OrderChildsElements(ContainerClass, Prefix, ListName, IdElemName, SelectElemName) {
    var index = 0;

    $('.' + ContainerClass).each(function (index, item) {
        $(item).find('input[type=hidden]').attr('name', Prefix + '.' + ListName + '[' + index + '].' + IdElemName);
        $(item).find('input[type=hidden]').attr('id', ListName + '_' + IdElemName + '_' + index);

        $(item).find('select').attr('name', Prefix + '.' + ListName + '[' + index + '].' + SelectElemName);
        $(item).find('select').attr('id', ListName + '_' + SelectElemName + '_' + index);
        $(item).find('div.visualSelectContainer').attr('id', ListName + '_' + SelectElemName + '_' + index + 'SelectDiv');

        index++;
    });

    materialSelects.init();
}

$('.addEventMember').on('click', function () {
    var suffix = $(this).attr('data-suffix_inv');
    var index = $('.MemberContainer' + suffix).length;
    gateway.add_GetDataByData($('#Url_EventMember').val(), { index: index, model: suffix }, 'EventMemberContainer' + suffix);
});

$(document).on('click', 'a.removeMember', function () {
    var suffix = $(this).attr('data-suffix_inv');
    $(this).parents('div.MemberContainer' + suffix).remove();
    OrderChildsElements('MemberContainer' + suffix, 'container', 'EventMember' + suffix + 's', 'EventMember' + suffix + 'Id', 'MemberId', suffix);
});

$('.addEventGroup').on('click', function () {
    var suffix = $(this).attr('data-suffix_inv');
    var index = $('.GroupContainer' + suffix).length;
    gateway.add_GetDataByData($('#Url_EventGroup').val(), { index: index, model: suffix }, 'EventGroupContainer' + suffix);
});

$(document).on('click', 'a.removeGroup', function () {
    var suffix = $(this).attr('data-suffix_inv');
    $(this).parents('div.GroupContainer' + suffix).remove();
    OrderChildsElements('GroupContainer' + suffix, 'container', 'EventGroup' + suffix + 's', 'EventGroup' + suffix + 'Id', 'GroupId', suffix);
});

$('.addEventSection').on('click', function () {
    var suffix = $(this).attr('data-suffix_inv');
    var index = $('.SectionContainer' + suffix).length;
    gateway.add_GetDataByData($('#Url_EventSection').val(), { index: index, model: suffix }, 'EventSectionContainer' + suffix);
});

$(document).on('click', 'a.removeSection', function () {
    var suffix = $(this).attr('data-suffix_inv');
    $(this).parents('div.SectionContainer' + suffix).remove();
    OrderChildsElements('SectionContainer' + suffix, 'container', 'EventSection' + suffix + 's', 'EventSection' + suffix + 'Id', 'SectionId', suffix);
});

$('.addEventPosition').on('click', function () {
    var suffix = $(this).attr('data-suffix_inv');
    var index = $('.PositionContainer' + suffix).length;
    gateway.add_GetDataByData($('#Url_EventPosition').val(), { index: index, model: suffix }, 'EventPositionContainer' + suffix);
});

$(document).on('click', 'a.removePosition', function () {
    var suffix = $(this).attr('data-suffix_inv');
    $(this).parents('div.PositionContainer' + suffix).remove();
    OrderChildsElements('PositionContainer' + suffix, 'container', 'EventPosition' + suffix + 's', 'EventPosition' + suffix + 'Id', 'PositionId', suffix);
});

function ValidateInvitation() {
    var resp = 0;

    resp = ValidateGroupSelect('EventMemberContainerInvitation', resp);
    resp = ValidateGroupSelect('EventGroupContainerInvitation', resp);
    resp = ValidateGroupSelect('EventSectionContainerInvitation', resp);
    resp = ValidateGroupSelect('EventPositionContainerInvitation', resp);

    resp = ValidateGroupSelect('EventMemberContainerExclusion', resp);
    resp = ValidateGroupSelect('EventGroupContainerExclusion', resp);
    resp = ValidateGroupSelect('EventSectionContainerExclusion', resp);
    resp = ValidateGroupSelect('EventPositionContainerExclusion', resp);

    if (resp > 0) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Faltan campos por completar.', 'warning');
        return resp == 0;
    }

    resp = ValidateContainer('EventMemberContainerInvitation', resp);
    resp = ValidateContainer('EventGroupContainerInvitation', resp);
    resp = ValidateContainer('EventSectionContainerInvitation', resp);
    resp = ValidateContainer('EventPositionContainerInvitation', resp);

    if (resp > 0) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen selecciones iguales en destinatarios.', 'warning');
        return resp == 0;
    }

    resp = ValidateContainer('EventMemberContainerExclusion', resp);
    resp = ValidateContainer('EventGroupContainerExclusion', resp);
    resp = ValidateContainer('EventSectionContainerExclusion', resp);
    resp = ValidateContainer('EventPositionContainerExclusion', resp);

    if (resp > 0) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen selecciones iguales en exclusiones.', 'warning');
        return resp == 0;
    }

    if ($('#EventMemberContainerInvitation').find('select').length > 0 && $('#EventMemberContainerExclusion').find('select').length > 0) {
        resp = ValidateAddresseeExclusion('EventMemberContainerInvitation', 'EventMemberContainerExclusion', resp);

        if (resp > 0) {
            alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen Miembros iguales en destinatarios y exclusiones.', 'warning');
            return resp == 0;
        }
    }

    if ($('#EventGroupContainerInvitation').find('select').length > 0 && $('#EventGroupContainerExclusion').find('select').length > 0) {
        resp = ValidateAddresseeExclusion('EventGroupContainerInvitation', 'EventGroupContainerExclusion', resp);

        if (resp > 0) {
            alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen Grupos iguales en destinatarios y exclusiones.', 'warning');
            return resp == 0;
        }
    }

    if ($('#EventSectionContainerInvitation').find('select').length > 0 && $('#EventSectionContainerExclusion').find('select').length > 0) {
        resp = ValidateAddresseeExclusion('EventSectionContainerInvitation', 'EventSectionContainerExclusion', resp);

        if (resp > 0) {
            alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen Secciones iguales en destinatarios y exclusiones.', 'warning');
            return resp == 0;
        }
    }

    if ($('#EventPositionContainerInvitation').find('select').length > 0 && $('#EventPositionContainerExclusion').find('select').length > 0) {
        resp = ValidateAddresseeExclusion('EventPositionContainerInvitation', 'EventPositionContainerExclusion', resp);

        if (resp > 0) {
            alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen Cargos iguales en destinatarios y exclusiones.', 'warning');
            return resp == 0;
        }
    }

    return resp == 0;
}

function ValidateGroupSelect(ContainerId, VarResp) {
    $('#' + ContainerId).find('select').each(function (index, item) {
        VarResp += validationsForm.select($(item).attr('id'));
    });

    return VarResp;
}

$('#BtnSaveInvitations').on('click', function () { $('#Send').val(false); });
$('#BtnSendInvitations').on('click', function (event) {
    if (!ValidateInvitation()) {
        event.preventDefault();
        return;
    }

    if (!ValidateAddressees()) {
        event.preventDefault();

    }

    $('#Send').val(true);
});

function OnSuccess_Invitation(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), "Operación realizada con éxito", 'success', function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('DivToList').click();
    });
}

function ValidateAddressees() {
    var resp = 0;

    if ($('#EventMemberContainerInvitation').find('select').length > 0 && $('#EventMemberContainerExclusion').find('select').length > 0) {
        resp = ValidateAddresseeExclusion('EventMemberContainerInvitation', 'EventMemberContainerExclusion', resp);

        if (resp > 0) {
            alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen Miembros iguales en destinatarios y exclusiones.', 'warning');
            return resp == 0;
        }
    }

    if ($('#EventGroupContainerInvitation').find('select').length > 0 && $('#EventGroupContainerExclusion').find('select').length > 0) {
        resp = ValidateAddresseeExclusion('EventGroupContainerInvitation', 'EventGroupContainerExclusion', resp);

        if (resp > 0) {
            alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen Grupos iguales en destinatarios y exclusiones.', 'warning');
            return resp == 0;
        }
    }

    if ($('#EventSectionContainerInvitation').find('select').length > 0 && $('#EventSectionContainerExclusion').find('select').length > 0) {
        resp = ValidateAddresseeExclusion('EventSectionContainerInvitation', 'EventSectionContainerExclusion', resp);

        if (resp > 0) {
            alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen Secciones iguales en destinatarios y exclusiones.', 'warning');
            return resp == 0;
        }
    }

    if ($('#EventPositionContainerInvitation').find('select').length > 0 && $('#EventPositionContainerExclusion').find('select').length > 0) {
        resp = ValidateAddresseeExclusion('EventPositionContainerInvitation', 'EventPositionContainerExclusion', resp);

        if (resp > 0) {
            alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen Cargos iguales en destinatarios y exclusiones.', 'warning');
            return resp == 0;
        }
    }

    //----------------------------------------------

    //if ($('#EventMemberContainerInvitation').find('select').length == 0) {
    //    if ($('#EventGroupContainerExclusion').find('select').length > 0) {
    //        if (ValidationExclusion('EventGroupContainerExclusion')) {
    //            resp++;
    //            alertFunctions.basicAlert($('#AlertTitle').val(), 'Se han excluido todos los Grupos.', 'warning');
    //            return resp == 0;
    //        }
    //    }

    //    if ($('#EventSectionContainerExclusion').find('select').length > 0) {
    //        if (ValidationExclusion('EventSectionContainerExclusion')) {
    //            resp++;
    //            alertFunctions.basicAlert($('#AlertTitle').val(), 'Se han excluido todos las Secciones.', 'warning');
    //            return resp == 0;
    //        }
    //    }

    //    if ($('#EventPositionContainerExclusion').find('select').length > 0) {
    //        if (ValidationExclusion('EventPositionContainerExclusion')) {
    //            resp++;
    //            alertFunctions.basicAlert($('#AlertTitle').val(), 'Se han excluido todos los cargos.', 'warning');
    //            return resp == 0;
    //        }
    //    }
    //}

    return resp == 0;
}

function ValidateContainer(ContainerClassName, VarResp) {
    var list = [];
    $('#' + ContainerClassName).find('select').each(function (index, item) {
        $(list).each(function (ind, itm) {
            if ($(item).val() == itm) {
                VarResp++;
                $('#select' + $(item).attr('id')).addClass('mdc-text-field--invalid');
                return false;
            }
        });
        list.push($(item).val());
    });

    return VarResp;
}

function ValidateAddresseeExclusion(ContainerInvitation, ContainerExclusion, VarResp) {
    var list = [];
    $('#' + ContainerInvitation).find('select option:selected').each(function (index, item) {
        if ($(item).attr('value') !== '') {
            list.push(parseInt($(item).attr('value')));
        }

    });
    $('#' + ContainerExclusion).find('select option:selected').each(function (index, item) {
        if ($(item).attr('value') !== '') {
            if (list.indexOf(parseInt($(item).attr('value'))) !== -1) {
                VarResp++;
                $('#select' + $(item).parent().attr('id')).addClass('mdc-text-field--invalid');
            }
        }
    });

    return VarResp;
}

function ValidationExclusion(ContainerExclusion) {
    var list = [];
    $('#' + ContainerExclusion).find('select:first').children().each(function (index, item) {
        if ($(item).attr('value') !== '') {
            list.push(parseInt($(item).attr('value')));
        }
    });

    $('#' + ContainerExclusion).find('select option:selected').each(function (index, item) {
        if ($(item).attr('value') !== '') {
            if (list.indexOf(parseInt($(item).attr('value'))) !== -1) {
                list.splice(list.indexOf(parseInt($(item).attr('value'))), 1);
            }
        }
    });

    return list.length == 0;
}