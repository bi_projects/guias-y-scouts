﻿var padTop = null;

$(function () {
    padTop = $('.container_header')[0].offsetHeight;

    scrollFunctions.init();
    $('#BinnacleTable').floatThead({
        position: 'fixed',
        top: padTop,
    });
});

$('#BtnSearchBinnacle').on('click', function () {
    if (!ValidateFilter()) return;

    $('#Url_Get_Binnacle').attr('StartDate', $('#StartDate').val());
    $('#Url_Get_Binnacle').attr('EndingDate', $('#EndingDate').val());
    pageCount_scroll = 1;
    scrollFunctions.loadFirstData(null, true);

    $('#filterContiner').addClass('pad_r_4');
    $('#DivRemoveFilter').removeClass('hide');
});

$('#BtnRemoveFilterBinnacle').on('click', function () {
    $('#StartDate').val('');
    $('#EndingDate').val('');

    $('#Url_Get_Binnacle').removeAttr('StartDate');
    $('#Url_Get_Binnacle').removeAttr('EndingDate');
    pageCount_scroll = 1;
    scrollFunctions.loadFirstData(null, true);

    $('#filterContiner').removeClass('pad_r_4');
    $('#DivRemoveFilter').addClass('hide');
});

function ValidateFilter() {
    var resp = 0;

    resp += validationsForm.text('StartDate');
    resp += validationsForm.text('EndingDate');

    if ($('#StartDate').val() != '' && $('#EndingDate').val()) {
        var startDate = $('#StartDate').val().split('-');
        var endDate = $('#EndingDate').val().split('-');

        if (moment(startDate[2] + '-' + startDate[1] + '-' + startDate[0]).diff(moment(endDate[2] + '-' + endDate[1] + '-' + endDate[0]), 'days') > 0) {
            $('#Date-helper-text').removeClass('hide');
            resp++;
        } else {
            $('#Date-helper-text').addClass('hide');
        }
    }

    return resp == 0;
}

$(document).on('mouseenter', 'label#EndingDate_clear, label#StartDate_clear', function () {
    $(this).removeClass('icon_date');
    $(this).addClass('icon_close_3');
    $(this).addClass('pointer');
});
$(document).on('mouseleave', 'label#EndingDate_clear, label#StartDate_clear', function () {
    $(this).removeClass('icon_close_3');
    $(this).removeClass('pointer');
    $(this).addClass('icon_date');
});
$(document).on('change', '#EndingDate, #StartDate', function () {
    $(this).parents('.mdc-text-field').addClass('mdc-text-field--upgraded');
    $(this).parents('.mdc-text-field').find('.mdc-text-field__label').addClass('mdc-text-field__label--float-above');
});
$(document).on('click', 'label#EndingDate_clear', function () {
    $('#EndingDate').val('');
});
$(document).on('click', 'label#StartDate_clear', function () {
    $('#StartDate').val('');
});
