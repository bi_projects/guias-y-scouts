﻿var publicationAssessment = null;
$(function () {
    publicationAssessment = parseFloat($('#rate_user').attr('data-rateyo_rate'));

    $('#rate_user').rateYo({
        rating: $('#rate_user').attr('data-rateyo_rate'),
        halfStar: true,
        starWidth: "15px"
    }).on('rateyo.set', function (e, data) {
        if (data.rating != publicationAssessment) {
            pageFunctions.showElement($('#preloader'));
            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: $('#Url_SaveAssessment').val(),
                data: JSON.stringify({ PublicationId: $('#PublicationId').val(), Assessment: data.rating }),
                success: function (resp) {
                    $('#rate_user_visual').html(data.rating);
                    publicationAssessment = data.rating;
                },
                error: function (resp) {
                    console.log(resp);
                    $("#rate_user").rateYo("option", "rating", publicationAssessment);
                },
                complete: function () {
                    pageFunctions.hideElement($('#preloader'));
                }
            });
        }
    });
});

function addSubscription(InterestThemeId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.saveEntity_Callback(
                $('#Url_Add').val(),
                { InterestThemeId: InterestThemeId },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('Url_toContent').click();
                    });
                }
            );
        });
}