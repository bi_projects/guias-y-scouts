﻿$(function () {
    $('#Body').summernote({
        airMode: false,
        lang: 'es-ES',
        height: 650,
        tabsize: 2,
        minHeight: null,
        maxHeight: null,
        dialogsInBody: true,
    });
});

function ValidatePublication() {
    var resp = 0;

    resp += validationsForm.text('Title');
    resp += validationsForm.image('SendFile', 'FileLocation');
    resp += validationsForm.select('InterestThemeId');
    //resp += validationsForm.select('AgeRangeId');
    resp += validationsForm.text('StartDate');
    resp += validationsForm.text('EndDate');
    resp += validationsForm.text('Description');
    resp += validationsForm.text('Keywords');

    var startDate = $('#StartDate').val().split('-');
    var endDate = $('#EndDate').val().split('-');

    if (moment(startDate[2] + '-' + startDate[1] + '-' + startDate[0]).diff(moment(endDate[2] + '-' + endDate[1] + '-' + endDate[0]), 'days') >= 0) {
        $('#Date-helper-text').removeClass('hide');
        resp++;
    } else {
        $('#Date-helper-text').addClass('hide');
    }

    $('#PublicationAgeRangeContainer select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    resp += validationsForm.iqualSelects('PublicationAgeRangeContainer', 'Existen rangos de edad repetidos.');

    if ($('#Body').summernote('isEmpty')) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'El contenido de la publicación esta vacío.', 'warning');
        resp++;
    }  

    return resp == 0;
}

function OnSuccess_Publication(res) {
    pageFunctions.showElement($('#preloader'));
    document.getElementById('DivToList').click();
}

$('#BtnPrev').on('click', function () {
    if (!ValidatePublication()) return;

    gateway.getDataByData($('#Url_Theme').val(), { InterestThemeId: $('#InterestThemeId').val() }, 'theme_prev_container');

    $('#sectionBreadCrumbEdit').addClass('hide');
    $('#sectionEdit').addClass('hide');
    $('#sectionBtnEdit').addClass('hide');
    $('#sectionBreadCrumbPrev').removeClass('hide');
    $('#sectionPrev').removeClass('hide');
    $('#sectionBtnPrev').removeClass('hide');

    $('#titlePrev').html($('#Title').val());
    $('#keywordsPrev').html($('#Keywords').val());
    $('#SendFilePrev').attr('src', $('#SendFile-InputView').attr('src'));
    $('#contentPrev').html($('#Body').val());
});

$('#BtnEditPub, #DivToEditPrev').on('click', function () {
    $('#sectionBreadCrumbEdit').removeClass('hide');
    $('#sectionEdit').removeClass('hide');
    $('#sectionBtnEdit').removeClass('hide');
    $('#sectionBreadCrumbPrev').addClass('hide');
    $('#sectionPrev').addClass('hide');
    $('#sectionBtnPrev').addClass('hide');
});

$('#PublicationForm').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});

$('#PublicationForm').on('keypress', 'input:text', function (e) {
    var value = String.fromCharCode(e.keyCode)
    if (textPattern.test(value)) return false;
});

$('#btnAddAgeRange').on('click', function () {
    var index = $('.AgeRangeContainer').length;
    gateway.add_GetDataByData($('#Url_PublicationAgeRange').val(), { index: index }, 'PublicationAgeRangeContainer');
});

$(document).on('click', 'a.removeAgeRange', function () {
    $(this).parents('div.AgeRangeContainer').remove();

    //Order
    var index = 0;
    $('.AgeRangeContainer').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'PublicationAgeRanges[' + index + '].PublicationAgeRangeId');
        $(this).find('input[type=hidden]').attr('id', 'PublicationAgeRanges_PublicationAgeRangeId_' + index);

        $(this).find('select').attr('name', 'PublicationAgeRanges[' + index + '].AgeRangeId');
        $(this).find('select').attr('id', 'PublicationAgeRanges_AgeRangeId_' + index);
        $(this).find('div.visualSelectContainer').attr('id', 'PublicationAgeRanges_AgeRangeId_' + index + 'SelectDiv');

        index++;
    });

    materialSelects.init();
});

//Files

$(document).on('change', '.PublicationSendFile', function () {
    var item = $(this)[0];
    var index = $('.PublicationSendFile').length;

    $('#FileZone').children(':last-child').remove();

    var fileName = item.value;
    var indexExt = fileName.lastIndexOf('.') + 1;
    var fileExt = fileName.substr(indexExt, fileName.length).toLowerCase();

    if (item.files && item.files[0]) {
        if (fileExt == 'jpg' || fileExt == 'png') {
            $('#FileZone').append('<div id="' + item.getAttribute('for') + '" class="relative image_drag mar_05 w5 bck_turkey_light r2 z5 pad_0 FileContainer">' +
                '<div class="bck_body relative circle wh2 overflow_h"><img class="w100p absolute_default scale_15"></div>' +
                '<div class="absolute top_0_240_st right_05_240_st bottom_0_240_st mar_auto block wh1p5 bck_pos_green icon_close_1 multi_icon z1 pointer" onclick="deleteFile(this);"></div>' +
                '<div class="z2 absolute left_-5p5_240_st bottom_100p_240_st w15 bck_black_01 pad_1 box_sizing hover_image_drag trans_05 mar_b_1 r1">' +
                '<img class="w100p">' +
                '<div class="absolute top_100p_240_st left_0_240_st right_0_240_st mar_auto">' +
                '<div class="arrow bck_black_01_arrow"></div>' +
                '</div>' +
                '<div class="absolute_bl_240_st fs_08 pad_1 w100p box_sizing">' +
                '<div class="relative">' +
                '<a class="block ellipsis w100p pad_b_05 pad_t_3 pad_lr_1 text-right italic c_white box_sizing z1 relative cursor_default">' + fileName.replace(/C:\\fakepath\\/i, '') + '</a>' +
                '<div class="vertical_degrade_black absolute_default wh100p opa_50"></div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div >');

            var reader = new FileReader();
            reader.onload = function (e) {
                Array.prototype.forEach.call(document.getElementById(item.getAttribute('for')).querySelectorAll('img'), function (item) {
                    item.setAttribute('src', e.target.result);
                });
            }
            reader.readAsDataURL(item.files[0]);
        } else {
            $('#FileZone').append('<div id="' + item.getAttribute('for') + '" class="btn_ovalo bck_turkey_light relative image_drag inline w100p_240_st wauto_480_st mar_05 FileContainer">' +
                '<div class="absolute top_0_240_st bottom_0_240_st right_05_240_st mar_auto block wh1p5 bck_pos_green icon_close_1 multi_icon z1 pointer" onclick= "deleteFile(this);"></div>' +
                '<div class="pad_r_1_240_st pad_r_2_480_st ellipsis italic fs_09 max_w_10">' + fileName.replace(/C:\\fakepath\\/i, '') + '</div>' +
                '</div>');
        }
    }

    $('#ElemsFileContainer').append('<input type="file" for="PublicationSendFile-InputView_' + index + '" class="hide PublicationSendFile" id="PublicationSendFile_' + index + '" name="PublicationSendFiles[' + index + ']" />');
    $('#FileZone').append('<div class="inline_divs v_mid_all pointer button_drag fileSelector" onclick="document.getElementById(\'PublicationSendFile_' + index + '\').click()"><div class="wh2 bck_turkey_light bck_turkey_light_hover hover_to_green pointer relative circle" ><span class="wh1 center_absolute icon_add multi_icon inline bck_pos_green"></span></div><div class="mar_lr_05 italic c_gray fs_08 trans_05 label_button_drag">Agregue el archivo</div></div>');
});

function deleteFile(button) {
    var elem = $(button).parents('.FileContainer');
    if ($(elem).hasClass('PublicationFile')) {
        $('#ElemsFileContainer').find('input[type=hidden][for=' + $(elem).attr('id') + ']').remove();
        $(elem).remove();

        var index = 0;
        $('.PublicationFile[type=hidden]').each(function (index, item) {
            var ViewId = $(item).attr('for');
            $(item).attr('for', 'PublicationFile-InputView_' + index);
            $(item).attr('id', 'PublicationFile_PublicationFileId_' + index);
            $(item).attr('name', 'PublicationFiles[' + index + '].PublicationFileId');
            $('#' + ViewId).attr('id', 'PublicationFile-InputView_' + index);
        });
    } else {
        var ElemsCont = $('.PublicationSendFile').length;
        $('#ElemsFileContainer').find('input[type=file][for=' + $(elem).attr('id') + ']').remove();
        $(elem).remove();

        var index = 0
        $('.PublicationSendFile').each(function (index, item) {
            var ViewId = $(item).attr('for');
            $(item).attr('for', 'PublicationSendFile-InputView_' + index);
            $(item).attr('id', 'PublicationSendFile_' + index);
            $(item).attr('name', 'PublicationFiles[' + index + ']');
            $('#' + ViewId).attr('id', 'PublicationSendFile-InputView_' + index);

            index++;
            if (index == (ElemsCont - 1)) {
                $('#FileZone').find('.fileSelector').attr('onclick', 'document.getElementById(\'PublicationSendFile_' + (index - 1) + '\').click()');
            }
        });
    }
}


//function handleDragOver(evt) {
//    evt.stopPropagation();
//    evt.preventDefault();
//    evt.dataTransfer.dropEffect = 'copy';

//    document.getElementById('FileZone_drag').classList.remove('hide');
//    document.getElementById('FileZone_drop').classList.remove('hide');
//}

//function handleFileSelect(evt) {
//    evt.stopPropagation();
//    evt.preventDefault();

//    document.getElementById('FileZone_drag').classList.add('hide');
//    document.getElementById('FileZone_drop').classList.add('hide');

//    if (evt.dataTransfer.files.length > 0) {
//        var elem = document.querySelector(".PublicationSendFile:last-child");

//        elem.files = evt.dataTransfer.files;
//        $(elem).trigger('change');
//    }
//}

//function handleDragLeave(evt) {
//    evt.stopPropagation();
//    evt.preventDefault();

//    document.getElementById('FileZone_drag').classList.add('hide');
//    document.getElementById('FileZone_drop').classList.add('hide');
//}

//var FileZone = document.getElementById('FileZoneContainer');
//var FileZone_drop = document.getElementById('FileZone_drop');

//FileZone.addEventListener('dragover', handleDragOver, false);
//FileZone.addEventListener('drop', handleFileSelect, false);
//FileZone_drop.addEventListener('dragleave', handleDragLeave, false);