﻿$(function () {

});

function ToEditPositionType(PositionTypeId) {
    gateway.editEntity($('#Url_ToEdit').val(), { PositionTypeId: PositionTypeId }, 'ToAdd_Section')
}

function ValidatePositionType() {
    var resp = 0;

    resp += validationsForm.select('CategoryId');
    resp += validationsForm.select('GenderId');
    resp += validationsForm.select('AgeRangeId');

    return resp == 0;
}