﻿var panels = null;
$(function () {
    initSubscriptionsTabs();
    SubscriptionFilter();
});

function initSubscriptionsTabs() {
    var dynamicTabBar = new mdc.tabs.MDCTabBar(document.querySelector('#tab-bar-susbscriptions'));
    panels = document.querySelector('.panels');

    dynamicTabBar.tabs.forEach(function (tab) { tab.preventDefaultOnClick = true; });

    dynamicTabBar.listen('MDCTabBar:change', function (item) {
        updatePanel(item.detail.activeTabIndex);
    });
}

function updatePanel(index) {
    var activePanel = panels.querySelector('.panel.active');
    if (activePanel) activePanel.classList.remove('active');

    var newActivePanel = panels.querySelector('.panel:nth-child(' + (index + 1) + ')');
    if (newActivePanel) newActivePanel.classList.add('active');
}

function addSubscription(InterestThemeId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.saveEntity_Callback(
                $('#Url_Add').val(),
                { InterestThemeId: InterestThemeId },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('Url_toSubscriptions').click();
                    });
                }
            );
        });
}

function removeSubscription(InterestThemeId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity_Callback(
                $('#Url_Delete').val(),
                { InterestThemeId: InterestThemeId },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Eliminado exitosamente.', 'success', function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('Url_toSubscriptions').click();
                    });
                }
            );
        });
}

var searchTextTemporal = '';
function SubscriptionFilter() {
    searchTextTemporal = $('#inputGridSearch_Subs').val();

    var inputGrid = document.getElementById('inputGridSearch_Subs');
    if (inputGrid) {
        inputGrid.addEventListener('keyup', function (event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("SearchDiv_Subs").click();
            }
        });
    }

    $('#SearchDiv_Subs').on('click', function () {
        if ($('#inputGridSearch_Subs').val() == '') return;

        gateway.getDataByData_Callback($('#Url_Get_Pagination').val(),
            {
                SearchText: $('#inputGridSearch_Subs').val(),
                TabId: $('#tab-bar-susbscriptions').find('.mdc-tab--active:first').attr('aria-controls'),
            },
            'panels_subscription_container',
            function () {
                initSubscriptionsTabs();
                SubscriptionFilter();
            });
    });

    $('#SearchCloseDiv_Subs').on('click', function () {
        if (searchTextTemporal != '') {
            gateway.getDataByData_Callback($('#Url_Get_Pagination').val(),
                {
                    Search: null,
                    TabId: $('#tab-bar-susbscriptions').find('.mdc-tab--active:first').attr('aria-controls'),
                },
                'panels_subscription_container',
                function () {
                    initSubscriptionsTabs();
                    SubscriptionFilter();
                });
        } else {
            $('#inputGridSearch_Subs').val('');
        }
    });
}
