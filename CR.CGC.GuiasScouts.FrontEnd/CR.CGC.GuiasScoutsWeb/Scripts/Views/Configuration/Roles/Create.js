﻿$(function () {
    
});

$('input:checkbox').on('change', function () {
    var parent = $(this).parents('.list_container_select');

    if ($(this).prop('checked')) $(parent).addClass('active');
    else $(parent).removeClass('active');
});

$('#all_checked').on('click', function () {
    if ($(this).prop('checked'))
        $('.list_container_select').find('input:checkbox').prop('checked', true).trigger('change');
    else
        $('.list_container_select').find('input:checkbox').prop('checked', false).trigger('change');
});

$('#competenceTabLink').click(function (event) {
    ValidateRole();
}); 

function ValidateRole() {
    var resp = 0;

    resp += validationsForm.text('Name');
    resp += validationsForm.text('Description');

    $('.list_container_select').each(function (index, item) {
        if ($(item).find('input:checkbox').prop('checked')) {
            if ($(item).find('select').val() == '') {
                $('#select' + $(item).find('select').attr('id')).addClass('mdc-text-field--invalid');
                resp++;
            } else {
                $('#select' + $(item).find('select').attr('id')).removeClass('mdc-text-field--invalid');
            }
        } else {
            $('#select' + $(item).find('select').attr('id')).removeClass('mdc-text-field--invalid');
        }
    });
    
    $('.panel[role=tabpanel]').each(function (index, item) {
        if ($(item).find('.mdc-text-field--invalid').length > 0) {
            $('a.mdc-tab[aria-controls=' + $(item).attr('id') + ']').find('.tab_icon_alert').removeClass('hide');
        } else {
            $('a.mdc-tab[aria-controls=' + $(item).attr('id') + ']').find('.tab_icon_alert').addClass('hide');
        }
    });

    return resp == 0;
}

function OnSuccess_Role(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), "Operación realizada con éxito", "success", function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('DivToList').click();
    });
}