﻿$(function () {
    init();
});

function init() {
    $('.list_container_checkboxes').each(function (index, item) {
        if ($(item).find('.CommonMethods_container').find('input:checkbox').length == $(item).find('.CommonMethods_container').find('input:checkbox:checked').length) {
            if ($(item).find('.SpecialMethods_container').length > 0) {
                if ($(item).find('.SpecialMethods_container').find('input:checkbox').length == $(item).find('.SpecialMethods_container').find('input:checkbox:checked').length) {
                    $(item).find('.SpecialMethods_arrow_container').click();
                    $(item).find('.cb_entity').prop('checked', true);
                } else {
                    $(item).find('.cb_entity').prop('checked', false);
                }
            } else {
                $(item).find('.cb_entity').prop('checked', true);
            }
        } else {
            $(item).find('.cb_entity').prop('checked', false);
        }
    });

    if ($('.cb_entity').length == $('.cb_entity:checked').length) {
        $('#all_checked').prop('checked', true);
    }
    else {
        $('#all_checked').prop('checked', false);
    }
}

function ValidatePermission() {
    if ($('.list_container_checkboxes').find('input:checkbox:checked').length == 0) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Debe seleccionar al menos un permiso.', 'error');
        return false;
    }
    else {
        return true;
    }
}

function OnSuccess_Permission(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), "Operación realizada con éxito", "success", function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('DivToList').click();
    });
}

$('#all_checked').on('change', function () {
    if ($(this).prop('checked')) {
        $('.cb_entity').prop('checked', true).trigger('change');
    } else {
        $('.cb_entity').prop('checked', false).trigger('change');
    }
});

$('.cb_entity').on('change', function () {
    var elem = $(this);
    if ($(elem).prop('checked')) {
        $(elem).parents('.list_container_checkboxes').find('.SpecialMethods_arrow_container').click();
        $(elem).parents('.list_container_checkboxes').find('.SpecialMethods_container').find('input:checkbox').prop('checked', true).trigger('change');
        $(elem).parents('.list_container_checkboxes').find('.CommonMethods_container').find('input:checkbox').prop('checked', true).trigger('change');
    } else {
        $(elem).parents('.list_container_checkboxes').find('.SpecialMethods_arrow_container').click();
        $(elem).parents('.list_container_checkboxes').find('.SpecialMethods_container').find('input:checkbox').prop('checked', false).trigger('change');
        $(elem).parents('.list_container_checkboxes').find('.CommonMethods_container').find('input:checkbox').prop('checked', false).trigger('change');
    }
});

$('input[type=checkbox]').not('.cb_entity, #all_checked').on('change', function () {
    var elem = $(this);
    if ($(elem).parents('.list_container_checkboxes').find('input[type=checkbox]').not('.cb_entity').length == $(elem).parents('.list_container_checkboxes').find('input[type=checkbox]:checked').not('.cb_entity').length) {
        $(elem).parents('.list_container_checkboxes').find('.cb_entity').prop('checked', true);
    } else {
        $(elem).parents('.list_container_checkboxes').find('.cb_entity').prop('checked', false);
    }
});