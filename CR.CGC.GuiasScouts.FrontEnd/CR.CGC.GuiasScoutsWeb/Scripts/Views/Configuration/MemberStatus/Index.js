﻿$(function () {

});

function DeleteMemberStatus(MemberStatusId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity(
                $('#Url_Delete').val(),
                {
                    MemberStatusId: MemberStatusId,
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                },
                'GridMemberStatuses'
            );
        });
}

function DeleteMemberStatuses() {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntitiesPag(
                $('#Url_Delete').val(),
                'MemberStatuses',
                'GridMemberStatuses',
                'GridMemberStatuses',
                {
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                }
            );
        });
}