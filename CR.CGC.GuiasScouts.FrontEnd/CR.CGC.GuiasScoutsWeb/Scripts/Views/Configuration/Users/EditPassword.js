﻿$(function () {
    
});

function ValidateUserPass() {
    var resp = 0;

    if ($('#Password').val() != $('#PasswordResetToken').val()) {
        resp++;
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Contraseñas no coinciden.', 'warning');
    }

    return resp == 0;
}

function OnSuccess_UserPass(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), "Operación realizada con éxito", "success", function () {
        document.getElementById('DivToList').click();
    });
}