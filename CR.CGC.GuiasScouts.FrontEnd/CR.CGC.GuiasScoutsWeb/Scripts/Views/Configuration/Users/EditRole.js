﻿$(function () {

});

function ValidateUserRole() {
    var resp = 0;

    resp += validationsForm.select('RoleId');

    return resp == 0;
}

function OnSuccess_UserRole(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), "Operación realizada con éxito", "success", function () {
        document.getElementById('DivToList').click();
    });
}