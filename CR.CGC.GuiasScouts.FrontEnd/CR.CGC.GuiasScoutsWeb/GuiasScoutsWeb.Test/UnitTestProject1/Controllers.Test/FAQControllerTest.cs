﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Controllers;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using static System.Net.HttpStatusCode;

namespace UnitTestProject1.Controllers.Test
{
    [TestClass]
    public class FAQControllerTest
    {
        FAQController FAQTest;

        public FAQControllerTest()
        {
            HttpContext.Current = PublicationControllerTest.FakeHttpContext();
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            CustomSession.CurrentSession.CurrentUser = new CR.CGC.GuiasScoutsWeb.Models.LoginViewModel()
            {
                Member = new Member()
                {
                    MemberId = 1,
                    User = new User()
                }
            };

        }

        public Mock<HttpRequestBase> FakeRequest()
        {
            var request = new Mock<HttpRequestBase>();
            // var displaymode = new Mock<IDisplayMode>();
            // Not working - IsAjaxRequest() is static extension method and cannot be mocked
            // request.Setup(x => x.IsAjaxRequest()).Returns(true /* or false */);
            // use this
            request.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            var context = new Mock<HttpContextBase>();
            context.SetupGet(x => x.Request).Returns(request.Object);
            FAQTest = new FAQController();
            FAQTest.ControllerContext = new ControllerContext(context.Object, new RouteData(), FAQTest);
            // MemberTest.ControllerContext.DisplayMode = displaymode.Object;

            return request;
        }

        [TestMethod]
        public void GetGridFaqsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = FAQTest.GetGridFaqs(1, "faq_tab_");
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void FAQSTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = FAQTest.FAQS();
            Assert.IsNotNull(response);
        }
    }
}
