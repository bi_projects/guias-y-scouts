﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using System.Web.WebPages;
using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Controllers;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTestProject1.Controllers.Test
{
    [TestClass]
    public class MemberControllerTest
    {
        MemberController MemberTest;
        Member Miembro;
        MemberRecognition ReconocimientoMiembro;
        MemberGroup GrupoMiembro;
        Group Grupo;
        Section Seccion;
        List<MemberCompetenceEvaluation> ListaEvaluacion;
        Progression Progresion;
        Recognition Reconocimiento;
        Position Cargo;
        Position CargoOld;

        public MemberControllerTest()
        {
            HttpContext.Current = PublicationControllerTest.FakeHttpContext();
            MemberTest = new MemberController();
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            CustomSession.CurrentSession.CurrentUser = new CR.CGC.GuiasScoutsWeb.Models.LoginViewModel()
            {
                Member = new Member() { User = new User() }
            };

            Miembro = new Member()
            {
                Names = "Lester",
                Surnames = "Morales",
                Photo = "TestText",
                IDType = 2,
                IDNumber = "12345647",
                NationalityId = 4,
                BirthDate = DateTime.Now.AddYears(-4),
                GenderId = 2,
                StartDate = DateTime.Now,
                PromiseDate = DateTime.Now,
                Email = "lester.morales@bi.com.ni",
                IsGorS = true,
                IsTutor = false,
                MemberStatusId = 2,
                Address = "TestText",
                DistrictId = 5,
                PostalCode = "10105",
                Workplace = "TestText",
                Enabled = false

            };

            ReconocimientoMiembro = new MemberRecognition()
            {
                MemberId = 6,
                RecognitionId = 3,
                DateAssignment = DateTime.Today,
                Enabled = false
            };

            GrupoMiembro = new MemberGroup()
            {
                MemberId = 5,
                GroupId = 1,
                SectionId = 1,
                ProgressionId = 1,
                StartDate = DateTime.Today,
                Enabled = false
            };

            ListaEvaluacion = new List<MemberCompetenceEvaluation>
            {
                new MemberCompetenceEvaluation()
            {
                MemberId = 5,
                CompetenceId = 1,
                LevelId = 1,
                EvaluationDate = DateTime.Now,
                Enabled = false
            }
            };

            Grupo = new Group()
            {
                Name = "TestText",
                Number = 5,
                FoundationDate = DateTime.Now,
                HeadkerchiefImage = "TestText",
                HeadkerchiefMeaning = "TestText",
                HeadkerchiefDescription = "TestText",
                ChiefMember = null,
                HasABuilding = false,
                DistrictId = 1,
                LocalId = 142,
                Address = "TestText",
                SectorId = 57,
                Latitude = Convert.ToDecimal(0.00000000),
                Longitude = Convert.ToDecimal(0.00000000),
                SystemStatusId = 2,
                Enabled = false
            };

            Seccion = new Section()
            {
                Name = "TestText",
                Description = "",
                SectionTypeId = 2,
                SystemStatusId = 1,
                FileLocation = "TestText",
                Enabled = false
            };

            Progresion = new Progression()
            {
                Name = "TestText",
                SectionId = 2,
                SystemStatusId = 1,
                FileLocation = "TestText",
                Enabled = false
            };

            Reconocimiento = new Recognition()
            {
                Name = "TestText",
                Description = "",
                SystemStatusId = 1,
                FileLocation = "TestText",
                Enabled = false
            };

            Cargo = new Position()
            {
                Name = "TestText",
                Description = "",
                Quantity = 25,
                PositionTypeId = 2,
                Enabled = false
            };

            CargoOld = new Position()
            {
                Name = "CargoOld",
                Description = "",
                Quantity = 25,
                PositionTypeId = 3,
                Enabled = false
            };
        }

        public Mock<HttpRequestBase> FakeRequest()
        {
            var request = new Mock<HttpRequestBase>();
            // var displaymode = new Mock<IDisplayMode>();
            // Not working - IsAjaxRequest() is static extension method and cannot be mocked
            // request.Setup(x => x.IsAjaxRequest()).Returns(true /* or false */);
            // use this
            request.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            var context = new Mock<HttpContextBase>();
            context.SetupGet(x => x.Request).Returns(request.Object);
            MemberTest = new MemberController();
            MemberTest.ControllerContext = new ControllerContext(context.Object, new RouteData(), MemberTest);
            // MemberTest.ControllerContext.DisplayMode = displaymode.Object;

            return request;
        }

        [TestMethod]
        public async Task SaveMemberYoungTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SaveMember_Young(Miembro);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveMember_OldTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SaveMember_Old(Miembro);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveMember_TutorTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SaveMember_Tutor(Miembro);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task DeleteMemberTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.DeleteMember(5,null,string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveMemberRecognitionTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SaveMemberRecognition(ReconocimientoMiembro);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task DeleteMemberRecognitionTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.DeleteMemberRecognition(5,2);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task SaveMemberGroupTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SaveMemberGroup(GrupoMiembro);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task DeleteMemberGroupTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.DeleteMemberGroup(5,2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveMemberCompetenceEvaluationTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SaveMemberCompetenceEvaluation(ListaEvaluacion);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task DeleteMemberCompetenceEvaluationTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.DeleteMemberCompetenceEvaluation(5,DateTime.Today);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridMembersTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = MemberTest.GetGridMembers(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberDetailsTest()
        {
            var response = MemberTest.GetMemberDetails(2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberPositionPartialTest() //Index = 0, Model= Young, Old, Tutor
        {
            FakeRequest();
            var response = MemberTest.GetMemberPositionPartial(0, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberKinshipPartialTest()
        {
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            FakeRequest();
            var response = MemberTest.GetMemberKinshipPartial(0, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberSocialNetworkPartialTest()
        {
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            FakeRequest();
            var response = MemberTest.GetMemberSocialNetworkPartial(0, 1, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberPhoneNumberPartialTest()
        {
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            FakeRequest();
            var response = MemberTest.GetMemberPhoneNumberPartial(0, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberLanguagePartialTest()
        {
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            FakeRequest();
            var response = MemberTest.GetMemberLanguagePartial(0, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberNationalEventPartialTest()
        {
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            FakeRequest();
            var response = MemberTest.GetMemberNationalEventPartial(0, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberInternationalEventPartialTest()
        {
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            FakeRequest();
            var response = MemberTest.GetMemberInternationalEventPartial(0, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberCoursePartialTest()
        {
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            FakeRequest();
            var response = MemberTest.GetMemberCoursePartial(0, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberWorkshopPartialTest()
        {
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            FakeRequest();
            var response = MemberTest.GetMemberWorkshopPartial(0, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberActivityPartialTest()
        {
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            FakeRequest();
            var response = MemberTest.GetMemberActivityPartial(0, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberOtherStudyPartialTest()
        {
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            FakeRequest();
            var response = MemberTest.GetMemberOtherStudyPartial(0, "Young");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberTutorPartialTest()
        {
            var response = MemberTest.GetMemberTutorPartial(2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberTutorDialogPartialTest()
        {
            var response = MemberTest.GetMemberTutorDialogPartial();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewMemberTest()
        {
            var response = MemberTest.NewMember(5);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewMemberRecognitionTest()
        {
            var response = MemberTest.NewMemberRecognition(3, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewMemberGroupTest()
        {
            var response = MemberTest.NewMemberGroup(1, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewMemberCompetenceEvaluationTest()
        {
            var response = MemberTest.NewMemberCompetenceEvaluation(2, DateTime.Now);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void GetSelectProgressionPartialTest()
        {
            var response = MemberTest.GetSelectProgressionPartial(1, false);
            Assert.IsNotNull(response);
        }

        #region Groups
        [TestMethod]
        public void GetGridGroupsTest()
        {
            FakeRequest();
            var response = MemberTest.GetGridGroups(string.Empty,1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void NewGroupTest()
        {
            FakeRequest();
            var response = MemberTest.NewGroup(3);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveGroupTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SaveGroup(Grupo);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task DeleteGroupTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.DeleteGroup(2,null,"",1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGroupMailPartialTest()
        {
            var response = MemberTest.GetGroupMailPartial(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void GetGroupColorPartialTest()
        {
            var response = MemberTest.GetGroupColorPartial(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void GetGroupSocialNetworkPartialTest()
        {
            var response = MemberTest.GetGroupSocialNetworkPartial(1,1);
            Assert.IsNotNull(response);
        }
        #endregion

        #region Sections
        [TestMethod]
        public void GetGridSectionsTest()
        {
            FakeRequest();
            var response = MemberTest.GetGridSections(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetSectionTypePartialTest()
        {
            FakeRequest();
            var response = MemberTest.GetSectionTypePartial(2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewSectionTest()
        {
            FakeRequest();
            var response = MemberTest.NewSection(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task DeleteSectionTest()
        {
            FakeRequest();
            var response = await MemberTest.DeleteSection(5,null,string.Empty,1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task SaveSectionTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SaveSection(Seccion);
            Assert.IsNotNull(response);
        }
        #endregion

        #region Progressions
        [TestMethod]
        public void GetGridProgressionsTest()
        {
            FakeRequest();
            var response = MemberTest.GetGridProgressions(string.Empty, 3);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewProgressionTest()
        {
            FakeRequest();
            var response = MemberTest.NewProgression(2);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task SaveProgressionTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SaveProgression(Progresion);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task DeleteProgressionTest()
        {
            FakeRequest();
            var response = await MemberTest.DeleteProgression(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }
        #endregion

        #region Recognitions
        [TestMethod]
        public void GetGridRecognitionsTest()
        {
            FakeRequest();
            var response = MemberTest.GetGridRecognitions(string.Empty,3);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void NewRecognitionTest()
        {
            FakeRequest();
            var response = MemberTest.NewRecognition(3);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task SaveRecognitionTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SaveRecognition(Reconocimiento);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task DeleteRecognitionTest()
        {
            FakeRequest();
            var response = await MemberTest.DeleteRecognition(4, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }
        #endregion

        #region Positions
        [TestMethod]
        public void GetGridPositionsTest()
        {
            FakeRequest();
            var response = MemberTest.GetGridPositions(string.Empty, 1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void NewPositionTest()
        {
            FakeRequest();
            var response = MemberTest.NewPosition(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void GetPositionTypePartialTest()
        {
            FakeRequest();
            var response = MemberTest.GetPositionTypePartial(3,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SavePosition_YoungTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SavePosition_Young(Cargo);
            Assert.IsNotNull(response);
        }
       
        [TestMethod]
        public async Task SavePosition_OldTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.SavePosition_Old(CargoOld);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeletePositionTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await MemberTest.DeletePosition(4, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }
        #endregion

        [TestMethod]
        public void GetSelectCantonPartialTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = MemberTest.GetSelectCantonPartial(4, null, null, false, false);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetSelectDistrictPartialTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = MemberTest.GetSelectDistrictPartial(1, null, null, false, false);
            Assert.IsNotNull(response);
        }
    }
}
