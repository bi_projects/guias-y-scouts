﻿using System;
using System.Web;
using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Controllers;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Controllers.Test
{
    [TestClass]
    public class ErrorControllerTest
    {
        ErrorController ErrorEntity;

        public ErrorControllerTest()
        {
            HttpContext.Current = PublicationControllerTest.FakeHttpContext();
            ErrorEntity = new ErrorController();
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            CustomSession.CurrentSession.CurrentUser = new CR.CGC.GuiasScoutsWeb.Models.LoginViewModel()
            {
                Email = "erica.argueta@bi.com.ni",
                Password = "Argueta11*",
                RememberMe = false,
                Member = new Member()
                {
                    MemberId = 1,
                    User = new User()
                }
            };
        }

        [TestMethod]
        public void IndexTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = ErrorEntity.Index(0);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NotFoundTest()
        {
            var response = ErrorEntity.NotFound();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void BadRequestTest()
        {
            var response = ErrorEntity.BadRequest();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void ForbiddenTest()
        {
            var response = ErrorEntity.Forbidden();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void ServerErrorTest()
        {
            var response = ErrorEntity.ServerError();
            Assert.IsNotNull(response);
        }
    }
}
