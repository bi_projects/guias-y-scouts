﻿using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Controllers;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTestProject1.Controllers.Test
{
    [TestClass]
    public class PublicationControllerTest
    {
        PublicationController PublicationEntity;
        ConfigurationController ConfigTest;
        InterestTheme TemaInteres;
        private Publication Prueba;

        public PublicationControllerTest()
        {
            HttpContext.Current = FakeHttpContext();
            PublicationEntity = new PublicationController();
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            CustomSession.CurrentSession.CurrentUser = new CR.CGC.GuiasScoutsWeb.Models.LoginViewModel()
            {
                Member = new Member()
                {
                    MemberId = 2,
                    User = new User()
                }
            };

            TemaInteres = new InterestTheme()
            {
                Name = "Prueba",
                FileLocation = "TestText",
                BackgroundFileLocation = "TestText",
                Description = "TestText",
                Enabled = true
            };

            Prueba = new Publication()
            {
                Title = "Prueba",
                InterestThemeId = 3,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                Description = "TestText",
                FileLocation = "TestText",
                Body = "",
                Keywords = "TestText",
                Assessment = Convert.ToDecimal(2.5),
                ViewsNumber = Convert.ToInt32(2),
                Enabled = true
            };
        }

        public static HttpContext FakeHttpContext()
        {
            var httpRequest = new HttpRequest("", "http://madrigal.eastus.cloudapp.azure.com/siigscr/", "");
            var stringWriter = new StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponce);

            var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(),
                                                                 new HttpStaticObjectsCollection(), 10, true,
                                                                 HttpCookieMode.AutoDetect,
                                                                 SessionStateMode.InProc, false);

            /* httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                                                      BindingFlags.NonPublic | BindingFlags.Instance,
                                                      null, CallingConventions.Standard,
                                                      new[] { typeof(HttpSessionStateContainer) },
                                                      null)
                                                 .Invoke(new object[] { sessionContainer }); */
            SessionStateUtility.AddHttpSessionStateToContext(httpContext, sessionContainer);

            return httpContext;
        }

        public Mock<HttpRequestBase> FakeRequest()
        {
            var request = new Mock<HttpRequestBase>();
            // Not working - IsAjaxRequest() is static extension method and cannot be mocked
            // request.Setup(x => x.IsAjaxRequest()).Returns(true /* or false */);
            // use this
            request.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            var context = new Mock<HttpContextBase>();
            context.SetupGet(x => x.Request).Returns(request.Object);
            PublicationEntity = new PublicationController();
            PublicationEntity.ControllerContext = new ControllerContext(context.Object, new RouteData(), PublicationEntity);

            return request;
        }

        public Mock<HttpResponseBase> FakeResponse()
        {

            var response = new Mock<HttpResponseBase>();
            var context = new Mock<HttpContextBase>();

            context.SetupGet(x => x.Response).Returns(response.Object);

            PublicationEntity = new PublicationController
            {
                ControllerContext = new ControllerContext(context.Object, new RouteData(), PublicationEntity)
            };

            return response;
        }

        [TestMethod]
        public void SubscriptionsTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = PublicationEntity.Subscriptions("Ciencia");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SubscriptionContentTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = await PublicationEntity.SubscriptionContent(2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void SubscriptionThemeTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = PublicationEntity.SubscriptionTheme(1, null);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SavePublicationAssessmentTest()
        {
            FakeHttpContext();
            FakeRequest();
            FakeResponse();
            decimal assess = 2.5m;
            var response = await PublicationEntity.SavePublicationAssessment(4, assess);
            Assert.IsNotNull(response);
        }


        #region Publications
        [TestMethod]
        public void GetGridPublicationsTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = PublicationEntity.GetGridPublications(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetThemePartialTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = PublicationEntity.GetThemePartial(2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetPublicationAgeRangePartialTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = PublicationEntity.GetPublicationAgeRangePartial(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void PublicationDetailsTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = PublicationEntity.PublicationDetails(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewPublicationTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = PublicationEntity.NewPublication(null);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SavePublicationTest() // Preguntar si una publicacion puede ingresarse con el mismo nombre
        {
            FakeHttpContext();
            FakeRequest();
            var response = await PublicationEntity.SavePublication(Prueba);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeletePublicationTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = await PublicationEntity.DeletePublication(23, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }
        #endregion

        #region InterestTheme
        [TestMethod]
        public void GetGridInterestThemeTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = PublicationEntity.GetGridInterestTheme(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void InterestThemeTest()
        {
            var response = PublicationEntity.InterestTheme();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewInterestThemeTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = PublicationEntity.NewInterestTheme(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void InterestThemeDetailsTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = PublicationEntity.InterestThemeDetails(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveInterestThemeTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = await PublicationEntity.SaveInterestTheme(TemaInteres); //Nombre duplicado 
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteInterestThemeTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = await PublicationEntity.DeleteInterestTheme(1, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }
        #endregion
    }
}
