﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Controllers;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTestProject1.Controllers.Test
{
    [TestClass]
    public class ActivityControllerTest
    {
        ActivityController ActivityEntity;
        Template Plantilla;
        EventInvitationContainer Evento;
        Event EventoPrueba;

        public ActivityControllerTest()
        {
            HttpContext.Current = PublicationControllerTest.FakeHttpContext();
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            CustomSession.CurrentSession.CurrentUser = new CR.CGC.GuiasScoutsWeb.Models.LoginViewModel()
            {
                Member = new Member()
                {
                    MemberId = 2,
                    User = new User()
                }
            };

            Plantilla = new Template()
            {
                Name = "TestText",
                Description = "TestText",
                Body = "erica.argueta@bi.com.ni",
                Enabled = true,
                
            };

            Evento = new EventInvitationContainer()
            {
                EmailBody = "TestText",
                TemplateCode = "TestText",
                TemplateId = 1,
                EventId = 1,
                Send = true,
                Name = "TestText",
                EventImage = "TestText",
                Description = "TestText",
                StartDateTime = DateTime.Now,
                EndDateTime = DateTime.Now,
                QuotaLimit = 10,
                ScopeId = 1,
                ActivityTypeId = 1,
                Cost = Convert.ToDecimal(200),
                ItIsFundable = false,
                InscriptionStart = DateTime.Now,
                InscriptionEnd = DateTime.Now,
                Requirements = "TestText",
                Place = "TestText",
                Address = "TestText",
                Longitude = Convert.ToDecimal(13.5),
                Latitude = Convert.ToDecimal(21.3),

            };

            EventoPrueba = new Event()
            {
                EmailBody = "TestText",
                TemplateCode = "TestText",
                TemplateId = 1,
                EventStatusId = 1,
                Enabled = true,
                Guests = 10,
                Place = "TestText",
                EventId = 1,   
                Name = "TestText",
                EventImage = "TestText",
                StartDateTime = DateTime.Now,
                EndDateTime = DateTime.Now,
                QuotaLimit = 10,
                ScopeId = 1,
                ActivityTypeId = 1,
                Cost = Convert.ToDecimal(200),
                ItIsFundable = false,
                InscriptionStart = DateTime.Now,
                InscriptionEnd = DateTime.Now,
                Requirements = "TestText",
                Address = "TestText",
                Longitude = Convert.ToDecimal(13.5),
                Latitude = Convert.ToDecimal(21.3),

            };
        }

        public Mock<HttpRequestBase> FakeRequest()
        {
            var request = new Mock<HttpRequestBase>();
            // Not working - IsAjaxRequest() is static extension method and cannot be mocked
            // request.Setup(x => x.IsAjaxRequest()).Returns(true /* or false */);
            // use this
            request.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            var context = new Mock<HttpContextBase>();
            context.SetupGet(x => x.Request).Returns(request.Object);
            ActivityEntity = new ActivityController();
            ActivityEntity.ControllerContext = new ControllerContext(context.Object, new RouteData(), ActivityEntity);

            return request;
        }

        public Mock<HttpResponseBase> FakeResponse()
        {

            var response = new Mock<HttpResponseBase>();
            var context = new Mock<HttpContextBase>();

            context.SetupGet(x => x.Response).Returns(response.Object);

            ActivityEntity = new ActivityController
            {
                ControllerContext = new ControllerContext(context.Object, new RouteData(), ActivityEntity)
            };

            return response;
        }

        #region Attendance View
        [TestMethod]
        public void AttendancesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.Attendances();
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void GetGridAttendancesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetGridAttendances();
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void AttendanceDetailsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.AttendanceDetails(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void SaveAttendanceTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.SaveAttendance(1);
            Assert.IsNotNull(response);
        }
        
         [TestMethod]
        public void DeleteAttendanceTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.DeleteAttendance(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void SaveEventAssessmentTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.SaveEventAssessment(1, Convert.ToDecimal(2.5));
            Assert.IsNotNull(response);
        }

        #endregion

        #region Records Events
        [TestMethod]
        public void RecordsIndexTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.RecordsIndex();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void EventsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.Events();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridEventsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetGridEvents(string.Empty,1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void NewEventTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.NewEvent(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void GetEventAgeRangePartialTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetEventAgeRangePartial(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void GetEventRolePartialTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetEventRolePartial(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void GetEventDetailsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetEventDetails(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void EventManageInvitationsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.EventManageInvitations(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void GetEventMemberPartialTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetEventMemberPartial(1,string.Empty);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetEventGroupPartialTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetEventGroupPartial(1, string.Empty);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetEventSectionPartialTest() 
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetEventSectionPartial(1, string.Empty);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetEventPositionPartialTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetEventPositionPartial(1, string.Empty);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveEventTest()//** HELP
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ActivityEntity.SaveEvent(EventoPrueba);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveInvitationsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.SaveInvitations(Evento);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task DeleteEventTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.DeleteEvent(1,null,string.Empty,1);
            Assert.IsNotNull(response);
        }
        

        #endregion

        #region Catalogs

        [TestMethod]
        public void CatalogsIndexTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.CatalogsIndex();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void TemplatesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.Templates();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridTemplatesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetGridTemplates(string.Empty,1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void NewTemplateTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.NewTemplate(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetTemplateTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ActivityEntity.GetTemplate(1);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task SaveTemplateTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ActivityEntity.SaveTemplate(Plantilla);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public async Task DeleteTemplateTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ActivityEntity.DeleteTemplate(1,null,string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SendEmailTestTest() // HELP
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ActivityEntity.SendEmailTest(Plantilla);
            Assert.IsNotNull(response);
        }
        
        #endregion
    }
}
