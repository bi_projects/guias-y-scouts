﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Controllers;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTestProject1.Controllers.Test
{
    [TestClass]
    public class CRMControllerTest
    {
        PublicationController PublicationEntity;
        CRMController CRMEntity;
        Request Solicitud;
        RequestComment ComentarioSolicitud;
        Priority Prioridad;
        Complexity Complejidad;
        Topic Tematica;

        public CRMControllerTest()
        {
            HttpContext.Current = PublicationControllerTest.FakeHttpContext();
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            CustomSession.CurrentSession.CurrentUser = new CR.CGC.GuiasScoutsWeb.Models.LoginViewModel()
            {
                Member = new Member()
                {
                    MemberId = 1,
                    User = new User()
                    {
                        UserId = 2
                    }
                }
            };
            PublicationEntity = new PublicationController();
            CRMEntity = new CRMController();

            Solicitud = new Request()
            {
                TopicId = 1,
                Name = "TestText",
                Description = "TestText",
                CreationDate = DateTime.Now,
                AttentionDate = DateTime.Now,
                PriorityId = 1,
                ComplexityId = 1,
                RequestingUser = 1,
                AssignedTo = 2,
                RequestStatusId = 1,
                Progress = 0,
                Qualification = 0,
                QualifyingComment = "TestText",
                Enabled = true
            };

            ComentarioSolicitud = new RequestComment()
            {
                RequestId = 2,
                Comment = "TestText",
                Commentator = 2,
                DateTimeComment = DateTime.Now,
                Enabled = true
            };

            Prioridad = new Priority()
            {
                Name = "TestText",
                Description = "TestText",
                Enabled = true
            };

            Complejidad = new Complexity()
            {
                Name = "TestText",
                Description = "TestText",
                Enabled = true
            };

            Tematica = new Topic()
            {
                TypeId = 1,
                Name = "TestText",
                Description = "TestText",
                Enabled = true
            };
        }

        public Mock<HttpRequestBase> FakeRequest()
        {
            var request = new Mock<HttpRequestBase>();
            // Not working - IsAjaxRequest() is static extension method and cannot be mocked
            // request.Setup(x => x.IsAjaxRequest()).Returns(true /* or false */);
            // use this
            request.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            var context = new Mock<HttpContextBase>();
            context.SetupGet(x => x.Request).Returns(request.Object);
            CRMEntity.ControllerContext = new ControllerContext(context.Object, new RouteData(), CRMEntity);

            return request;
        }

        #region Requests
        [TestMethod]
        public void RecordsIndexTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.RecordsIndex();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void RequestsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.Requests();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridRequestsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = CRMEntity.GetGridRequests(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewRequestTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = CRMEntity.NewRequest(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetRequestTopicPartialTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = CRMEntity.GetRequestTopicPartial(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void RequestDetailsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = CRMEntity.RequestDetails(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveRequestTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await CRMEntity.SaveRequest(Solicitud);
            Assert.IsNotNull(response);
        }

        /*  [TestMethod]
          public async Task DeleteRequestsTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await CRMEntity.DeleteRequests(2,null,string.Empty,1);
              Assert.IsNotNull(response);
          }*/
        #endregion

        #region Management
        [TestMethod]
        public void ManagementIndexTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.ManagementIndex();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void AssignedTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.Assigned();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void ManagementTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.Management();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void EditAssignedTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.EditAssigned(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void AssignRequestTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.AssignRequest(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void SaveAssignRequestTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.AssignRequest(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void SaveAssignedTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.SaveAssigned(Solicitud);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void SaveRequestCommentTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.SaveRequestComment(ComentarioSolicitud);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Catalogs

        #region Priorities
        [TestMethod]
        public void PrioritiesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.Priorities();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridPrioritiesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = CRMEntity.GetGridPriorities(string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewPriorityTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.NewPriority(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetPriorityTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = CRMEntity.GetPriority(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetPrioritiesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.GetPriorities();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void SavePriorityTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.SavePriority(Prioridad);
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void DeletePrioritiesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.DeletePriorities(1,null,string.Empty,1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Complexities
        [TestMethod]
        public void ComplexitiesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.Complexities();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridComplexitiesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = CRMEntity.GetGridComplexities(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewComplexityTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.NewComplexity(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetComplexityTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = CRMEntity.GetComplexity(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetComplexitiesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.GetComplexities();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void SaveComplexityTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.SaveComplexity(Complejidad);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void DeleteComplexitiesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.DeleteComplexities(1, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Topics
        [TestMethod]
        public void TopicsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.Topics();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridTopicsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = CRMEntity.GetGridTopics(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void NewTopicTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.NewTopic(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetTopicTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = CRMEntity.GetTopic(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetTopicsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.GetTopics();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void SaveTopicTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.SaveTopic(Tematica);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void DeleteTopicsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            var response = CRMEntity.DeleteTopics(1, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #endregion
    }
}
