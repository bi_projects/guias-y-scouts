﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Controllers;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using static System.Net.HttpStatusCode;

namespace UnitTestProject1.Controllers.Test
{
    [TestClass]
    public class ConfigurationControllerTest
    {
        ConfigurationController ConfigTest;
        Competence Competencia;
        CompetenceLevel NivelCompetencia;
        MemberStatus EstadoMiembro;
        AgeRange RangoEdad;
        RequestStatus EstadoSolicitud;
        SectionType TipoSeccion;
        PositionType TipoCargo;
        BaseCatalog Genero;
        BaseCatalog NivelIdioma;
        BaseCatalog TipoActividad;
        BaseCatalog TipoSolicitud;
        BaseCatalog Sector;
        BaseCatalog Curso;
        BaseCatalog Taller;
        BaseCatalog Actividad;
        BaseCatalog EventoNacional;
        BaseCatalog EventoInternacional;
        Faq Pregunta;
        SendFaqViewModel FaqModel;
        ProfileViewModel Perfil;
        Member Miembro;
        Role Rol;
        User Usuario;
        Member MiembroPrueba;
        User UsuarioPrueba;
        
        PermissionRequestViewModel Permiso;
        List<MemberAlertSetting> Alerta;

        public ConfigurationControllerTest()
        {
            HttpContext.Current = PublicationControllerTest.FakeHttpContext();
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            CustomSession.CurrentSession.CurrentUser = new CR.CGC.GuiasScoutsWeb.Models.LoginViewModel()
            {
                Member = new Member()
                {
                    MemberId = 13,
                    User = new User()
                    {
                        Email = "armando.bustos@bi.com.ni",
                        UserName = "ajbustos"
                    }
                }
            };
            ConfigTest = new ConfigurationController();

            Competencia = new Competence()
            {
                Name = "TestText",
                Type = 2,
                Description = "TestText",
                Enabled = true
            };

            NivelCompetencia = new CompetenceLevel()
            {
                Name = "TestText",
                Level = 12,
                Description = "TestText",
                Enabled = true,
            };

            EstadoMiembro = new MemberStatus()
            {
                Name = "TestText",
                Description = "TestText",
                Color = "#b4b4b4",
                Enabled = true
            };

            RangoEdad = new AgeRange()
            {
                Name = "TestText",
                Description = "TestText",
                MinimumAge = 13,
                MaximumAge = 23,
                Enabled = true
            };

            EstadoSolicitud = new RequestStatus()
            {
                Name = "TestText",
                Description = "TestText",
                Color = "#F000000",
                Enabled = true
            };

            TipoSeccion = new SectionType()
            {
                SectionTypeId = 1,
                Name = "TestText",
                AgeRangeId = 2,
                Description = "TestText",
                Enabled = true
            };

            TipoCargo = new PositionType()
            {
                Name = "TestText",
                CategoryId = 2,
                GenderId = 3,
                AgeRangeId = 2,
                Description = "TestText",
                Enabled = true
            };

            Genero = new BaseCatalog()
            {
                Name = "TestText",
                Description = "TestText",
                ItemOf = 2,
                Enabled = true,
            };

            NivelIdioma = new BaseCatalog()
            {
                Name = "TestText",
                Description = "TestText",
                ItemOf = Convert.ToInt32(5),
                Enabled = true,
            };

            TipoActividad = new BaseCatalog()
            {
                Name = "TestText",
                Description = "TestText",
                ItemOf = 9,
                Enabled = true,
            };

            TipoSolicitud = new BaseCatalog()
            {
                Name = "TestText",
                Description = "TestText",
                ItemOf = 12,
                Enabled = true,
            };

            Sector = new BaseCatalog()
            {
                Name = "TestText",
                Description = "TestText",
                ItemOf = 4,
                Enabled = true,
            };

            Curso = new BaseCatalog()
            {
                Name = "TestText",
                Description = "TestText",
                ItemOf = 21,
                Enabled = true,
            };

            Taller = new BaseCatalog()
            {
                Name = "TestText",
                Description = "TestText",
                ItemOf = 22,
                Enabled = true,
            };

            Actividad = new BaseCatalog()
            {
                Name = "TestText",
                Description = "TestText",
                ItemOf = 23,
                Enabled = true,
            };

            EventoNacional = new BaseCatalog()
            {
                Name = "TestText",
                Description = "TestText",
                ItemOf = 19,
                Enabled = true,
            };

            EventoInternacional = new BaseCatalog()
            {
                Name = "TestText",
                Description = "TestText",
                ItemOf = 20,
                Enabled = true,
            };

            Pregunta = new Faq()
            {
                Type = 2,
                Question = "TestText",
                Answer = "TestText",
                Enabled = true
            };

            FaqModel = new SendFaqViewModel()
            {
                FaqModuleId = 2
            };

            Perfil = new ProfileViewModel()
            {
                ActualPassword = "Abc1234.",
                NewPassword = "Abc1234*",
                ConfirmPassword = "Abc1234*",
                Member = new Member()
                {
                    MemberId = 13
                }
            };

            Rol = new Role() {
                Name = "TestText",
                Description = "TestText",
                Enabled  = true
            };

            Usuario = new User
            {
                UserId = 1,
                RoleId = 2,
                UserName = "TestText",
                Email = "TestText",
                Password = "TestText",
                CurrentAccess = null,
                CurrentIp = "TestText",
                PasswordResetToken = "TestText",
                DateLastAccess = null,
                LastIp = "TestText",
                Theme = "TestText",
                FontSize = 2,
                Active = false,
                Enabled = true,
                CreationDate = DateTime.Now,
                ModifiedDate = null,
                IsFirstLogin = true
            };

            Permiso = new PermissionRequestViewModel()
            {
                RoleId = 1,
                Permissions = new List<Permission>()
            };

            Alerta = new List<MemberAlertSetting>()
            {

            };
        }

        public Mock<HttpRequestBase> FakeRequest()
        {
            var request = new Mock<HttpRequestBase>();
            // var displaymode = new Mock<IDisplayMode>();
            // Not working - IsAjaxRequest() is static extension method and cannot be mocked
            // request.Setup(x => x.IsAjaxRequest()).Returns(true /* or false */);
            // use this
            request.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });

            var context = new Mock<HttpContextBase>();
            context.SetupGet(x => x.Request).Returns(request.Object);
            ConfigTest = new ConfigurationController();
            ConfigTest.ControllerContext = new ControllerContext(context.Object, new RouteData(), ConfigTest);
            // MemberTest.ControllerContext.DisplayMode = displaymode.Object;

            return request;
        }

        public Mock<HttpResponseBase> FakeResponse()
        {
            
            var response = new Mock<HttpResponseBase>();
            var context = new Mock<HttpContextBase>();
            
            context.SetupGet(x => x.Response).Returns(response.Object);

            ConfigTest = new ConfigurationController();
            ConfigTest.ControllerContext = new ControllerContext(context.Object, new RouteData(), ConfigTest);

            return response;
        }

        #region Security
        
          #region Roles
          [TestMethod]
          public void GetGridRolesTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.GetGridRoles(string.Empty,1);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void GetRoleCompetenciesTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.GetRoleCompetencies(Rol);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void NewRoleTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.NewRole(2);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void PermissionListTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.PermissionList(1);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public async Task SaveRoleTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await ConfigTest.SaveRole(Rol);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public async Task DeleteRoleTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await ConfigTest.DeleteRole(1,null,string.Empty,1);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public async Task SavePermissionTest() //**
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await ConfigTest.SavePermission(Permiso);
              Assert.IsNotNull(response);
          }
          #endregion 

          #region Users

        [TestMethod]
          public void GetGridUsersTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.GetGridRoles(string.Empty, 1);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void EditRoleTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.EditRole(1);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void EditPasswordTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.EditPassword(1);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public async Task SaveUserRoleTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await ConfigTest.SaveUserRole(Usuario);
              Assert.IsNotNull(response);
          } //**

          [TestMethod]
          public async Task SaveUserPassTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await ConfigTest.SaveUserPass(Usuario);
              Assert.IsNotNull(response);
          } //**

          [TestMethod]
          public async Task SaveUserConfTest() 
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await ConfigTest.SaveUserConf(Usuario);
              Assert.IsNotNull(response);
          } //**

          [TestMethod]
          public async Task DeleteUserTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await ConfigTest.DeleteUser(2, null, string.Empty, 1);
              Assert.IsNotNull(response);
          }

        #endregion

        #endregion

        #region Alerts
        [TestMethod]
        public void AlertsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.Alerts();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void SaveAlertTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.SaveAlert(Alerta);
            Assert.IsNotNull(response);
        }

        #region Notifications
        [TestMethod]
        public void NotificationsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.Notifications();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridNotificationsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridNotifications(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task GetNotificationsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.GetNotifications();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveNotificationTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            FakeResponse();
            var response = await ConfigTest.SaveNotification(1);
            Assert.IsNotNull(response);
        }

        #endregion

        #endregion

        #region Profile Actions

        [TestMethod]
          public void ProfileMenuTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.ProfileMenu();
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void ProfileEditTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.ProfileEdit();
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void ProfileViewTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.ProfileView();
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public async Task SaveProfileTest() //Preguntar
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await ConfigTest.SaveProfile(Perfil);
              Assert.IsNotNull(response);
          }

          #endregion
           
        #region Publications

        [TestMethod]
          public void NewSubscriptionTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.NewSubscription(string.Empty);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void GetGridSubscriptionsTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.GetGridSubscriptions(string.Empty, "panel-all");
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public async Task SaveMemberInterestThemeTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              FakeResponse();
              var response = await ConfigTest.SaveMemberInterestTheme(7);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public async Task DeleteMemberInterestThemeTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              FakeResponse();
              var response = await ConfigTest.DeleteMemberInterestTheme(7);
              Assert.IsNotNull(response);
          }
          #endregion
          
        #region FAQs

          [TestMethod]
          public void NewFAQTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.NewFAQ(1,2);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void GetGridFAQsTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.GetGridFAQs(string.Empty, 1);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void GetFAQSTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.GetFAQS(1);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public void GetFAQS_AJAXTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = ConfigTest.GetFAQS_AJAX(3);
              Assert.IsNotNull(response);
          }
        
          [TestMethod]
          public void GetFAQTest()
            {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetFAQ(FaqModel);
            Assert.IsNotNull(response);
            }

        [TestMethod]
          public async Task SaveFAQTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await ConfigTest.SaveFAQ(Pregunta);
              Assert.IsNotNull(response);
          }

          [TestMethod]
          public async Task DeleteFAQTest()
          {
              PublicationControllerTest.FakeHttpContext();
              FakeRequest();
              var response = await ConfigTest.DeleteFAQ(27);
              Assert.IsNotNull(response);
          }
          #endregion

        #region Catalogs

        #region Competences
        [TestMethod]
        public void NewCompetenceTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewCompetence(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetCompetenceTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetCompetence(2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridCompetencesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridCompetences(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveCompetenceTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveCompetence(Competencia);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteCompetenceTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteCompetence(3, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }
        #endregion

        #region Competences Levels
        [TestMethod]
        public void NewCompetenceLevelTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewCompetenceLevel(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetCompetenceLevelTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetCompetenceLevel(2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridCompetenciesLevelsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridCompetenciesLevels(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveCompetenceLevelTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveCompetenceLevel(NivelCompetencia);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteCompetenceLevelTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteCompetenceLevel(12, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Member Status
        [TestMethod]
        public void NewMemberStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewMemberStatus(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetMemberStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetMemberStatus(2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridMemberStatusesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridMemberStatuses(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveMemberStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveMemberStatus(EstadoMiembro);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteMemberStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteMemberStatus(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Language Levels 
        [TestMethod]
        public void NewLanguageLevelTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewLanguageLevel(1);
            Assert.IsNotNull(response);
        } 

        [TestMethod]
        public void GetGridLanguageLevelsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridLanguageLevels(string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetLanguageLevelTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridMemberStatuses(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveLanguageLevelTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveLanguageLevel(NivelIdioma);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteLanguageLevelTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteLanguageLevel(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion*/

        #region Age Ranges
        [TestMethod]
        public void NewAgeRangeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewAgeRange(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetAgeRangeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetAgeRange(2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridAgeRangesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridAgeRanges(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveAgeRangeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveAgeRange(RangoEdad);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteAgeRangeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteAgeRange(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Activity Types
        [TestMethod]
        public void NewActivityTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewActivityType(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridActivityTypesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridActivityTypes(string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetActivityTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetActivityType(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveActivityTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveActivityType(TipoActividad);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteActivityTypesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteActivityTypes(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

       /* #region Activity Statuses
        [TestMethod]
        public void NewActivityStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewActivityStatus(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridActivityStatusesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridActivityStatuses(2);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetActivityStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetActivityStatus(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveActivityStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveActivityStatus(EstadoActividad);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteActivityStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteActivityStatus(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion*/

        #region Request Types
        [TestMethod]
        public void NewRequestTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewRequestType(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridRequestTypesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridRequestTypes(string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetRequestTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetRequestType(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveRequestTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveRequestType(TipoSolicitud);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteRequestTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteRequestType(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Request Statuses
        [TestMethod]
        public void NewRequestStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewRequestType(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridRequestStatusesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridRequestStatuses(string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetRequestStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetRequestStatus(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveRequestStatusTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveRequestStatus(EstadoSolicitud);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteRequestStatusesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteRequestStatuses(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Sector
        [TestMethod]
        public void NewSectorTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewSector(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridSectorsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridSectors(string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetSectorTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetSector(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveSectorTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveSector(Sector);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteSectorTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteSector(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Section Types
        [TestMethod]
        public void NewSectionTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewSectionType(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridSectionTypesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridSectionTypes(string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetSectionTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetSectionType(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveSectionTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveSectionType(TipoSeccion);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteSectionTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteSectionType(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Position Types
        [TestMethod]
        public void NewPositionTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewPositionType(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridPositionTypesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridPositionTypes(string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetPositionTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetPositionType(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SavePositionTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SavePositionType(TipoCargo);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeletePositionTypeTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeletePositionType(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Genders
        [TestMethod]
        public void NewGenderTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewGender(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridGendersTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridGenders(string.Empty,1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGenderTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGender(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveGenderTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveGender(Genero);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteGendersTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteGenders(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Courses
        [TestMethod]
        public void NewCourseTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewCourse(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridCoursesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridCourses(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetCourseTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetCourse(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveCourseTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveCourse(Curso);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteCoursesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteCourses(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Workshops
        [TestMethod]
        public void NewWorkshopTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewWorkshop(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridWorkshopsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridWorkshops(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetWorkshopTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetWorkshop(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveWorkshopTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveWorkshop(Taller);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteWorkshopsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteWorkshops(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Activities
        [TestMethod]
        public void NewActivityTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewActivity(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridActivitiesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridActivities(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetActivityTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetWorkshop(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveActivityTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveActivity(Actividad);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteActivitiesTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteActivities(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region National Event
        [TestMethod]
        public void NewNationalEventTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewNationalEvent(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridNationalEventsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridNationalEvents(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetNationalEventTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetNationalEvent(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveNationalEventTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveNationalEvent(EventoNacional);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteNationalEventsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteNationalEvents(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region International Event
        [TestMethod]
        public void NewInternationalEventTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.NewInternationalEvent(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetGridInternationalEventsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetGridInternationalEvents(string.Empty, 1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetInternationalEventTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = ConfigTest.GetInternationalEvent(1);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task SaveInternationalEventTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.SaveInternationalEvent(EventoInternacional);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task DeleteInternationalEventsTest()
        {
            PublicationControllerTest.FakeHttpContext();
            FakeRequest();
            var response = await ConfigTest.DeleteInternationalEvents(2, null, string.Empty, 1);
            Assert.IsNotNull(response);
        }

        #endregion


        #endregion


    }
}
