﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using CR.CGC.GuiasScoutsWeb.Common;
using CR.CGC.GuiasScoutsWeb.Controllers;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTestProject1.Controllers.Test
{
    [TestClass]
    public class BinnacleControllerTest
    {
        BinnacleController BitacoraEntity;

        public BinnacleControllerTest()
        {
            HttpContext.Current = PublicationControllerTest.FakeHttpContext();
            CustomSession.CurrentSession.CatalogConstants = new CatalogConstants();
            CustomSession.CurrentSession.CurrentUser = new CR.CGC.GuiasScoutsWeb.Models.LoginViewModel()
            {
                Member = new Member()
                {
                    MemberId = 2,
                    User = new User()
                }
            };
        }

        public static HttpContext FakeHttpContext()
        {
            var httpRequest = new HttpRequest("", "http://madrigal.eastus.cloudapp.azure.com/siigscr/", "");
            var stringWriter = new StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponce);

            var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(),
                                                                 new HttpStaticObjectsCollection(), 10, true,
                                                                 HttpCookieMode.AutoDetect,
                                                                 SessionStateMode.InProc, false);

            /* httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                                                      BindingFlags.NonPublic | BindingFlags.Instance,
                                                      null, CallingConventions.Standard,
                                                      new[] { typeof(HttpSessionStateContainer) },
                                                      null)
                                                 .Invoke(new object[] { sessionContainer }); */
            SessionStateUtility.AddHttpSessionStateToContext(httpContext, sessionContainer);

            return httpContext;
        }

        public Mock<HttpRequestBase> FakeRequest()
        {
            var request = new Mock<HttpRequestBase>();
            // Not working - IsAjaxRequest() is static extension method and cannot be mocked
            // request.Setup(x => x.IsAjaxRequest()).Returns(true /* or false */);
            // use this
            request.SetupGet(x => x.Headers).Returns(new System.Net.WebHeaderCollection { { "X-Requested-With", "XMLHttpRequest" } });
           // request.SetupGet(x => x.Headers["X-Requested-With"]).Returns("XMLHttpRequest");

            var context = new Mock<HttpContextBase>();
            context.SetupGet(x => x.Request).Returns(request.Object);
            BitacoraEntity = new BinnacleController();
            BitacoraEntity.ControllerContext = new ControllerContext(context.Object, new RouteData(), BitacoraEntity);

            return request;
        }

        public Mock<HttpResponseBase> FakeResponse()
        {

            var response = new Mock<HttpResponseBase>();
            var context = new Mock<HttpContextBase>();

            context.SetupGet(x => x.Response).Returns(response.Object);

            BitacoraEntity = new BinnacleController
            {
                ControllerContext = new ControllerContext(context.Object, new RouteData(), BitacoraEntity)
            };

            return response;
        }

        #region Information

        [TestMethod]
        public void InformationIndexTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = BitacoraEntity.InformationIndex();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void InformationTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = BitacoraEntity.Information();
            Assert.IsNotNull(response);
        }

        #endregion

        #region Activity

        [TestMethod]
        public void ActivityIndexTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = BitacoraEntity.ActivityIndex();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void ActivityTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = BitacoraEntity.Activity();
            Assert.IsNotNull(response);
        }
        
        [TestMethod]
        public void GetGridBinnacleTest() //HELP
        {
            FakeHttpContext();
            FakeRequest();
            FakeResponse();
            var response = BitacoraEntity.GetGridBinnacle(1);
            Assert.IsNotNull(response);
        }

        #endregion

        #region Request
        [TestMethod]
        public void RequestIndexTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = BitacoraEntity.RequestIndex();
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void RequestsTest()
        {
            FakeHttpContext();
            FakeRequest();
            var response = BitacoraEntity.Requests();
            Assert.IsNotNull(response);
        }
        #endregion
    }
}
