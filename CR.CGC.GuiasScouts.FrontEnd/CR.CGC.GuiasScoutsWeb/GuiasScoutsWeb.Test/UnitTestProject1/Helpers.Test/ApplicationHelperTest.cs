﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CR.CGC.GuiasScoutsWeb.Implementacion;
using CR.CGC.GuiasScoutsWeb.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using Entities;
using static System.Net.HttpStatusCode;

namespace UnitTestProject1.Helpers.Test
{
    [TestClass]
    public class ApplicationHelperTest
    {
        private static string _BaseUrl;
        private IApplicationHelper _helper;
        private BaseCatalog entityTest;
        private User currentUser;


        public ApplicationHelperTest()
        {
            _BaseUrl = ConfigurationManager.AppSettings["WebApi"].ToString();
            _helper = new ApplicationHelper(_BaseUrl);
            entityTest = new BaseCatalog()
            {
                BaseCatalogId = 0,
                Name = "TestText",
                Description = "TestText",
                ItemOf = 4,
                Enabled = true
            };
        }

        [TestMethod]
        public async Task CreateObjectTest()
        {

            currentUser = new User()
            {
                UserName = "eargueta",
                Email = "erica.argueta@bi.com.ni",
                Password = "Argueta11*"
            };

            var response = await _helper.CreateObject<ERContainer>("Security/Account/LogIn", currentUser);

            Assert.IsTrue(response.GetType().Equals(typeof(ERContainer)));           
        }

        [TestMethod]
        public async Task CreatePostTest()
        {
           
            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { entityTest }, Model = "BaseCatalog" };
            var (ResponseCode, ResponseText) = await _helper.CreatePost("Config/Model/NewObject", container);

            Assert.AreEqual(ResponseCode, OK);

        }

        [TestMethod]
        public async Task CreatePostGetElementTest()
        {

            ERContainer container = new ERContainer() { BaseCatalogs = new List<BaseCatalog>() { entityTest }, Model = "BaseCatalogs" };
            var (ResponseCode, ResponseText, ResponseObject) = await _helper.CreatePostGetElement<List<BaseCatalog>>("Config/Model/NewObject", container);

            Assert.AreEqual(ResponseCode, OK);
            Assert.IsNotNull(ResponseObject);
        }


        [TestMethod]
        public void GetEntitiesListTest()
        {
            var (ListResponseCode, ListResponseText, ListResponseObject) = _helper.GetEntitiesList<List<BaseCatalog>>("Config", "BaseCatalog");

            Assert.AreEqual(ListResponseCode, OK);
            Assert.IsNotNull(ListResponseObject);
        }

        [TestMethod]
        public void GetEntitiesListPagTest()
        {
            var Number = 1;
            var (ResponseCode, ResponseText, ResponseObject, SearchText, RowCount, NumberPage) = _helper.GetEntitiesListPag<List<BaseCatalog>>("Config", "BaseCatalog", "", Number);


            Assert.AreEqual(ResponseCode, OK);
            Assert.AreEqual(NumberPage,Number);
            Assert.IsNotNull(ResponseObject);
        }
        
        [TestMethod]
        public void GetEntityTest()
        {
            var (ResponseCode, ResponseText, ResponseObject) = _helper.GetEntity<BaseCatalog>("Config", "BaseCatalog", "BaseCatalogId", 18);

            Assert.AreEqual(ResponseCode, OK);
            Assert.IsNotNull(ResponseObject);
        }

        [TestMethod]
        public void GetEntitiesByParamsTest()
        {
            var (ResponseCode, ResponseText, ResponseObject) = _helper.GetEntitiesByParams<List<BaseCatalog>>("Config", "BaseCatalog", new[] { ("ItemOf", "4") });

            Assert.AreEqual(ResponseCode, OK);
            Assert.IsNotNull(ResponseObject);
        }

        [TestMethod]
        public void GetEntitiesByParamsPagTest()
        {
            var (ResponseCode, ResponseText, ResponseObject, SearchText, RowCount, NumberPage) = _helper.GetEntitiesByParamsPag<List<BaseCatalog>>("Config", "BaseCatalog", "", 1 , new[] { ("ItemOf", "1") });

            Assert.AreEqual(ResponseCode, OK);
            Assert.IsNotNull(ResponseObject);
        }

        [TestMethod]
        public void GetEntitiesAsyncTest()
        {
            
            Dictionary<string, object> attr = new Dictionary<string, object>() { { "Model", "BaseCatalog" }, { "SearchText", "" }, { "PageNumber", 1 }, { "PageSize", 10 } };
            var response = _helper.GetEntitiesAsync<List<BaseCatalog>>("Config/Model/GetObjects", attr);

            Assert.AreEqual(response.ResponseCode, OK);

        }
    }
}
