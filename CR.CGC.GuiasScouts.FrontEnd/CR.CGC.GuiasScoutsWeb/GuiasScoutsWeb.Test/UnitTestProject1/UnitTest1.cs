﻿using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using CR.CGC.GuiasScoutsWeb.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTestProject1
{
    [TestClass]
    public class UtilitiesTest
    {
        public UtilitiesTest()
        {
            HttpContext.Current = FakeHttpContext();
        }

        public static HttpContext FakeHttpContext()
        {
            var httpRequest = new HttpRequest("", "http://madrigal.eastus.cloudapp.azure.com/siigscr/", "");
            var stringWriter = new StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponce);

            var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(),
                                                                 new HttpStaticObjectsCollection(), 10, true,
                                                                 HttpCookieMode.AutoDetect,
                                                                 SessionStateMode.InProc, false);

            /* httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                                                      BindingFlags.NonPublic | BindingFlags.Instance,
                                                      null, CallingConventions.Standard,
                                                      new[] { typeof(HttpSessionStateContainer) },
                                                      null)
                                                 .Invoke(new object[] { sessionContainer }); */
            SessionStateUtility.AddHttpSessionStateToContext(httpContext, sessionContainer);

            return httpContext;
        }



        [TestMethod]
        public void TestMethod1()
        {
        }

    }
}
