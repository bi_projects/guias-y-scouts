﻿using System;
using System.Collections.Generic;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Models.Test
{
    [TestClass]
    public class RoleCompetenceViewModelTest
    {
        [TestMethod]
        public void RoleCompetenceViewModelEntity()
        {
            Role Role = new Role();
            List<Competence> Competencies = new List<Competence>();
            List<CompetenceLevel> Levels = new List<CompetenceLevel>();

            var entidad = new RoleCompetenceViewModel()
            {
                Role = Role,
                Competencies = Competencies,
                Levels = Levels
            };

            Assert.IsNotNull(Role);
            Assert.IsNotNull(Competencies);
            Assert.IsNotNull(Levels);
        }
    }
}