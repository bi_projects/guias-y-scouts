﻿using System;
using System.Collections.Generic;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Models.Test
{
    [TestClass]
    public class PermissionRequestViewModelTest
    {
        [TestMethod]
        public void PermissionRequestViewModelEntity()
        {
            int RoleId = 2;
            List<Permission> Permissions = new List<Permission>();

            var entidad = new PermissionRequestViewModel()
            {
                RoleId = RoleId,
                Permissions = Permissions
            };

            Assert.AreEqual(entidad.RoleId, RoleId);
            Assert.IsNotNull(Permissions);
        }
    }
}
