﻿using System;
using System.Collections.Generic;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Models.Test
{
    [TestClass]
    public class MenuModelTest
    {
        [TestMethod]
        public void MenuModelEntity()
        {
            string NameMenu = "TestText";
            string UrlMenu = "TestText";
            string IconMenu = "TestText";
            string CodeMenu = "TestText";

            string ImageMenu = "TestText";
            string DescriptionMenu = "TestText";

            List<MenuModel> SubMenus = new List<MenuModel>();

            var entidad = new MenuModel()
            {
                NameMenu = NameMenu,
                UrlMenu = UrlMenu,
                IconMenu = IconMenu,
                CodeMenu = CodeMenu,

                ImageMenu = ImageMenu,
                DescriptionMenu = DescriptionMenu,
                SubMenus = SubMenus
            };

            Assert.AreEqual(entidad.NameMenu, NameMenu);
            Assert.AreEqual(entidad.UrlMenu, UrlMenu);
            Assert.AreEqual(entidad.IconMenu, IconMenu);
            Assert.AreEqual(entidad.CodeMenu, CodeMenu);

            Assert.AreEqual(entidad.ImageMenu, ImageMenu);
            Assert.AreEqual(entidad.DescriptionMenu, DescriptionMenu);
            Assert.IsNotNull(SubMenus);
        }
    }

    [TestClass]
    public class PermissionModelTest
    {
        [TestMethod]
        public void PermissionModelEntity()
        {
            string EntityName = "TestText";

            List<PermissionItemModel> CommonMethods = new List<PermissionItemModel>();
            List<PermissionItemModel> SpecialMethods = new List<PermissionItemModel>();

            var entidad = new PermissionModel()
            {
                EntityName = EntityName,
                CommonMethods = CommonMethods,
                SpecialMethods = SpecialMethods
            };

            Assert.AreEqual(entidad.EntityName, EntityName);
            
            Assert.IsNotNull(CommonMethods);
            Assert.IsNotNull(SpecialMethods);
        }
    }

    [TestClass]
    public class PermissionItemModelTest
    {
        [TestMethod]
        public void PermissionItemModelEntity()
        {
            string Controller = "TestText";
            string Action = "TestText";
            string Name = "TestText";
            string Description = "TestText";

            var entidad = new PermissionItemModel()
            {
                Controller = Controller,
                Action = Action,
                Name = Name,
                Description = Description
            };

           Assert.AreEqual(entidad.Controller, Controller);
           Assert.AreEqual(entidad.Action, Action);
           Assert.AreEqual(entidad.Name, Name);
           Assert.AreEqual(entidad.Description, Description);
        }
    }

}
