﻿using System;
using System.Collections.Generic;
using CR.CGC.GuiasScoutsWeb.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Models.Test
{
    [TestClass]
    public class UserRoleTest
    {
        [TestMethod]
        public void UserRoleEntity()
        {
            int UserRoleId = 2;
            int UserId = 5;
            int RoleId = 6;
            bool Enabled = true;
            List<PermisoModel> Permisos = new List<PermisoModel>();

            var entidad = new UserRole()
            {
                UserRoleId = UserRoleId,
                UserId = UserId,
                RoleId = RoleId,
                Enabled = Enabled,
                Permisos = Permisos
            };

            Assert.AreEqual(entidad.UserRoleId, UserRoleId);
            Assert.AreEqual(entidad.UserId, UserId);
            Assert.AreEqual(entidad.RoleId, RoleId);
            Assert.AreEqual(entidad.Enabled, Enabled);
            Assert.AreEqual(entidad.Permisos, Permisos);
        }
    }

    [TestClass]
    public class RolModelTest
    {
        [TestMethod]
        public void RolModelEntity()
        {
            int IdRol = 2;
            string NombreRol = "TestText";
            
            List<PermisoModel> Permisos = new List<PermisoModel>();
            List<PermissionModel> VisualPermissions = new List <PermissionModel>();

            var entidad = new RolModel()
            {
                IdRol = IdRol,
                NombreRol = NombreRol,
                Permisos = Permisos,
                VisualPermissions = VisualPermissions
            };

            Assert.AreEqual(entidad.IdRol, IdRol);
            Assert.AreEqual(entidad.NombreRol, NombreRol);

            Assert.AreEqual(entidad.Permisos, Permisos);
            Assert.AreEqual(entidad.VisualPermissions, VisualPermissions);
        }
    }

    [TestClass]
    public class PermisoModelTest
    {
        [TestMethod]
        public void PermisoModelEntity()
        {
            string NombrePermiso = "TestText";
            string NombreController = "TestText";

            var entidad = new PermisoModel()
            {
                NombrePermiso = NombrePermiso,
                NombreController = NombreController

            };

            Assert.AreEqual(entidad.NombrePermiso, NombrePermiso);
            Assert.AreEqual(entidad.NombreController, NombreController);
        }
    }

}
