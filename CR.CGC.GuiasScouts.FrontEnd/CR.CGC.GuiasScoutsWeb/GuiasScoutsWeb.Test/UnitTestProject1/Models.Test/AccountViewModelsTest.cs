﻿using CR.CGC.GuiasScoutsWeb.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web.Mvc;
using Entities;

namespace UnitTestProject1.Models.Test
{
    [TestClass]
    public class ExternalLoginConfirmationViewModelTest
    {
        [TestMethod]
        public void ExternalLoginConfirmationViewModelEntity()
        {
            string EmailTest = "TestText";

            var entidad = new ExternalLoginConfirmationViewModel()
            {
                Email = EmailTest
            };

        Assert.AreEqual(entidad.Email,EmailTest);
        }
    }

    [TestClass]
    public class ExternalLoginListViewModelTest
    {
        [TestMethod]
        public void ExternalLoginListViewModelEntity()
        {
            string ReturnUrl = "TestText";

            var entidad = new ExternalLoginListViewModel()
            {
                ReturnUrl = ReturnUrl
            };

            Assert.AreEqual(entidad.ReturnUrl, ReturnUrl);
        }
    }

    [TestClass]
    public class SendCodeViewModelTest
    {
        [TestMethod]
        public void SendCodeViewModelEntity()
        {
            string SelectedProvider = "TestText";
            ICollection<SelectListItem> Providers = new List<SelectListItem>();
            string ReturnUrl = "TestText";
            bool RememberMe = true;

            var entidad = new SendCodeViewModel()
            {
                SelectedProvider = SelectedProvider,
                Providers = Providers,
                ReturnUrl = ReturnUrl,
                RememberMe = RememberMe
            };

            Assert.AreEqual(entidad.SelectedProvider, SelectedProvider);
            Assert.IsNotNull(Providers);
            Assert.AreEqual(entidad.ReturnUrl,ReturnUrl);
            Assert.AreEqual(entidad.RememberMe,RememberMe);
        }
    }

    [TestClass]
    public class VerifyCodeViewModelTest
    {
        [TestMethod]
        public void VerifyCodeViewModelEntity()
        {
            string Provider = "TestText";
            string Code = "TestText";
            string ReturnUrl = "TestText";
            bool RememberBrowser = true;
            bool RememberMe = true;

            var entidad = new VerifyCodeViewModel()
            {
                Provider = Provider,
                Code = Code,
                ReturnUrl = ReturnUrl,
                RememberBrowser = RememberBrowser,
                RememberMe = RememberMe
            };

            Assert.AreEqual(entidad.Provider, Provider);
            Assert.AreEqual(entidad.Code, Code);
            Assert.AreEqual(entidad.ReturnUrl, ReturnUrl);
            Assert.AreEqual(entidad.RememberBrowser,RememberBrowser);
            Assert.AreEqual(entidad.RememberMe, RememberMe);
        }
    }

  [TestClass]
    public class ForgotViewModelTest
    {
        [TestMethod]
        public void ForgotViewModelEntity()
        {
            string Email = "TestText";

            var entidad = new ForgotViewModel()
            {
                Email = Email
            };

            Assert.AreEqual(entidad.Email, Email);
        }
    }

    [TestClass]
    public class LoginViewModelTest
    {
       [TestMethod]
        public void LoginViewModelEntity()
        {
            string Email = "TestText";
            string Password = "TestText";
            bool RememberMe = true;
            Member Member = new Member();

            var entidad = new LoginViewModel()
            {
                Email = Email,
                Password = Password,
                RememberMe = RememberMe,
                Member = Member
            };

            Assert.AreEqual(entidad.Email, Email);
            Assert.AreEqual(entidad.Password, Password);
            Assert.AreEqual(entidad.RememberMe, RememberMe);
            Assert.IsNotNull(Member);
        }
    }

    [TestClass]
    public class RegisterViewModelTest
    {
        [TestMethod]
        public void RegisterViewModelEntity()
        {
            string Email = "TestText";
            string Password = "TestText";
            string ConfirmPassword = "TestText";

            var entidad = new RegisterViewModel()
            {
                Email = Email,
                Password = Password,
                ConfirmPassword = ConfirmPassword
            };

            Assert.AreEqual(entidad.Email, Email);
            Assert.AreEqual(entidad.Password, Password);
            Assert.AreEqual(entidad.ConfirmPassword, ConfirmPassword);
        }
    }

    [TestClass]
    public class FirstLoginModelTest
    {
        [TestMethod]
        public void FirstLoginModelEntity()
        {
            string ActualPassword = "TestText";
            string NewPassword = "TestText";
            string ConfirmPassword = "TestText";
            User PersonalInfo = new User();

            var entidad = new FirstLoginModel()
            {
                ActualPassword = ActualPassword,
                NewPassword = NewPassword,
                ConfirmPassword = ConfirmPassword,
                PersonalInfo = PersonalInfo
            };

            Assert.AreEqual(entidad.ActualPassword, ActualPassword);
            Assert.AreEqual(entidad.NewPassword, NewPassword);
            Assert.AreEqual(entidad.ConfirmPassword, ConfirmPassword);
            Assert.AreEqual(entidad.PersonalInfo, PersonalInfo);
        }
    }

    [TestClass]
    public class ForgotPasswordTokenViewModelTest
    {
        [TestMethod]
        public void ForgotPasswordTokenViewModelEntity()
        {
            string Password = "TestText";
            string ConfirmPassword = "TestText";
            string Token = "TestText";
           
            var entidad = new ForgotPasswordTokenViewModel()
            {
                Password = Password,
                ConfirmPassword = ConfirmPassword,
                Token = Token,
            };

            Assert.AreEqual(entidad.Password, Password);
            Assert.AreEqual(entidad.ConfirmPassword, ConfirmPassword);
            Assert.AreEqual(entidad.Token, Token);
        }
    }

    [TestClass]
    public class ResetPasswordViewModelTest
    {
        [TestMethod]
        public void ResetPasswordViewModelEntity()
        {
            string Password = "TestText";
            string ConfirmPassword = "TestText";
            string CurrentPassword = "TestText";

            var entidad = new ResetPasswordViewModel()
            {
                CurrentPassword = CurrentPassword,
                Password = Password,
                ConfirmPassword = ConfirmPassword
            };

            Assert.AreEqual(entidad.CurrentPassword, CurrentPassword);
            Assert.AreEqual(entidad.Password, Password);
            Assert.AreEqual(entidad.ConfirmPassword, ConfirmPassword);
        }
    }

    [TestClass]
    public class ForgotPasswordViewModelTest
    {
        [TestMethod]
        public void ForgotPasswordViewModelEntity()
        {
            string Email = "TestText";
            string Source = "TestText";

            var entidad = new ForgotPasswordViewModel()
            {
                Email = Email,
                Source = Source
            };

            Assert.AreEqual(entidad.Email, Email);
            Assert.AreEqual(entidad.Source, Source);
        }
    }

}
