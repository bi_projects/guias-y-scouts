﻿using System;
using System.Collections.Generic;
using System.Media;
using System.Web.Mvc;
using CR.CGC.GuiasScoutsWeb.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Models.Test
{
    [TestClass]
    public class SelectViewModelTest
    {
        [TestMethod]
        public void SelectViewModelEntity()
        {
            string Text = "TestText";
            string Id = "TestText";
            bool Disabled = true;
            int SelectedId = 3;
            bool iconAlert = false;
            List<SelectOptionViewModel> Options = new List<SelectOptionViewModel>();

            var entidad = new SelectViewModel()
            {
                Text = Text,
                Id = Id,
                Disabled = Disabled,
                SelectedId = SelectedId,
                iconAlert = iconAlert,
                Options = Options
            };

            Assert.AreEqual(entidad.Text,Text);
            Assert.AreEqual(entidad.Id,Id);
            Assert.AreEqual(entidad.Disabled,Disabled);
            Assert.AreEqual(entidad.SelectedId,SelectedId);
            Assert.AreEqual(entidad.iconAlert,iconAlert);
            Assert.AreEqual(entidad.Options,Options);
        }
    }

    public class SelectOptionViewModelTest
    {
        [TestMethod]
        public void SelectOptionViewModelEntity()
        {
            string Text = "TestText";
            int Id = 4;
            bool Disabled = true;
            string Color = "TestText";
            
            var entidad = new SelectOptionViewModel()
            {
                Text = Text,
                Id = Id,
                Disabled = Disabled,
                Color = Color
            };

            Assert.AreEqual(entidad.Text, Text);
            Assert.AreEqual(entidad.Id, Id);
            Assert.AreEqual(entidad.Disabled, Disabled);
            Assert.AreEqual(entidad.Color, Color);

        }
    }

    public class ResponseQueryTest
    {
        [TestMethod]
        public void ResponseQueryEntity()
        {
            string Key = "TestText";
            string Value = "TestText";

            var entidad = new ResponseQuery()
            {
                Key = Key,
                Value = Value
            };

            Assert.AreEqual(entidad.Key, Key);
            Assert.AreEqual(entidad.Value, Value);
        }
    }

    public class TableToolsViewModelTest
    {
        [TestMethod]
        public void TableToolsViewModelEntity()
        {
            string DeleteFunction = "TestText";
            string DeleteTooltip = "TestText";
            string UrlAdd = "TestText";
            string AddTooltip = "TestText";

            string SearchText = "TestText";
            string UrlGet = "TestText";
            string GridContainerId = "TestText";

            var entidad = new TableToolsViewModel()
            {
                DeleteFunction = DeleteFunction,
                DeleteTooltip = DeleteTooltip,
                UrlAdd = UrlAdd,
                AddTooltip = AddTooltip,

                SearchText = SearchText,
                UrlGet = UrlGet,
                GridContainerId = GridContainerId
            };

            Assert.AreEqual(entidad.DeleteFunction, DeleteFunction);
            Assert.AreEqual(entidad.DeleteTooltip, DeleteTooltip);
            Assert.AreEqual(entidad.UrlAdd, UrlAdd);
            Assert.AreEqual(entidad.AddTooltip, AddTooltip);
            Assert.AreEqual(entidad.SearchText, SearchText);
            Assert.AreEqual(entidad.UrlGet, UrlGet);
            Assert.AreEqual(entidad.GridContainerId, GridContainerId);
        }
    }

    public class PaginationToolsViewModelTest
    {
        [TestMethod]
        public void PaginationToolsViewModelEntity()
        {
            int RowCount = 10;
            int NumberPage = 20;
            int ActualRows = 100;
      
            string SearchText = "TestText";
         
            var entidad = new PaginationToolsViewModel()
            {
                RowCount = RowCount,
                NumberPage = NumberPage,
                ActualRows = ActualRows,

                SearchText = SearchText
            };

            Assert.AreEqual(entidad.RowCount, RowCount);
            Assert.AreEqual(entidad.NumberPage, NumberPage);
            Assert.AreEqual(entidad.ActualRows, ActualRows);
            Assert.AreEqual(entidad.SearchText, SearchText);
        }
    }

    [TestClass]
    public class SocialNetworkViewModelTest
    {
        [TestMethod]
        public void SocialNetworkViewModelEntity()
        {
            int Index = 2;
            int TypeId = 3;

            var entidad = new SocialNetworkViewModel()
            {
                Index = Index,
                TypeId = TypeId
            };

            Assert.AreEqual(entidad.Index, Index);
            Assert.AreEqual(entidad.TypeId, TypeId);
        }
    }

    [TestClass]
    public class SelectLocationViewModelTest
    {
        [TestMethod]
        public void SelectLocationViewModelEntity()
        {
            SelectList SelectList = new SelectList(new List<string>());
            string Model = "TestText";
            string ElemId = "TestText";
 
            var entidad = new SelectLocationViewModel()
            {
                Model = Model,
                ElemId = ElemId
            };

            Assert.AreEqual(entidad.Model, Model);
            Assert.AreEqual(entidad.ElemId, ElemId);
            Assert.IsNotNull(SelectList);
        }
    }
}
