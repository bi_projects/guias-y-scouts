﻿using System;
using System.Collections.Generic;
using CR.CGC.GuiasScoutsWeb.Models;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Models.Test
{
    [TestClass]
    public class PermissionViewModelTest
    {
        [TestMethod]
        public void PermissionViewModelEntity()
        {
            Role Role = new Role();
            List<PermissionModel> PermissionsView = new List<PermissionModel>();
            List<Permission> Permissions = new List<Permission>();

            var entidad = new PermissionViewModel()
            {
                Role = Role,
                PermissionsView = PermissionsView,
                Permissions = Permissions
            };

            Assert.AreEqual(entidad.Role, Role);
            Assert.IsNotNull(Permissions);
            Assert.IsNotNull(PermissionsView);
        }
    }
}
