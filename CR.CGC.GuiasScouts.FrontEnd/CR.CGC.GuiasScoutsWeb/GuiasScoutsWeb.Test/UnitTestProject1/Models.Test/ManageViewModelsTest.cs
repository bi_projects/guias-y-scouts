﻿using System;
using CR.CGC.GuiasScoutsWeb.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web.Mvc;
using Entities;

namespace UnitTestProject1.Models.Test
{
    [TestClass]
    public class ManageViewModelsTest
    {
        [TestMethod]
        public void IndexViewModelEntity()
        {
            bool HasPassword = true;
           // IList<UserLoginInfo> Logins = new IList<UserLoginInfo>();
            string PhoneNumber = "TestText";
            bool TwoFactor = false;
            bool BrowserRemembered = false;

            var entidad = new IndexViewModel()
            {
                HasPassword = HasPassword,
                PhoneNumber = PhoneNumber,
                TwoFactor = TwoFactor,
                BrowserRemembered = BrowserRemembered
            };

            Assert.AreEqual(entidad.HasPassword, HasPassword);
          //  Assert.IsNotNull(Providers);
            Assert.AreEqual(entidad.PhoneNumber, PhoneNumber);
            Assert.AreEqual(entidad.TwoFactor, TwoFactor);
            Assert.AreEqual(entidad.BrowserRemembered, BrowserRemembered);
        }

        [TestMethod]
        public void FactorViewModelTest()
        {
            string Purpose = "TestText";

            var entidad = new FactorViewModel()
            {
                Purpose = Purpose
            };

            Assert.AreEqual(entidad.Purpose, Purpose);
        }

        [TestMethod]
        public void SetPasswordViewModelTest()
        {
            string NewPassword = "TestText";
            string ConfirmPassword = "TestText";

            var entidad = new SetPasswordViewModel()
            {
                NewPassword = NewPassword,
                ConfirmPassword = ConfirmPassword
            };

            Assert.AreEqual(entidad.NewPassword, NewPassword);
            Assert.AreEqual(entidad.ConfirmPassword, ConfirmPassword);
        }

        [TestMethod]
        public void ChangePasswordViewModelTest()
        {
            string OldPassword = "TestText";
            string NewPassword = "TestText";
            string ConfirmPassword = "TestText";

            var entidad = new ChangePasswordViewModel()
            {
                OldPassword = OldPassword,
                NewPassword = NewPassword,
                ConfirmPassword = ConfirmPassword
            };

            Assert.AreEqual(entidad.OldPassword, OldPassword);
            Assert.AreEqual(entidad.NewPassword, NewPassword);
            Assert.AreEqual(entidad.ConfirmPassword, ConfirmPassword);
        }

        [TestMethod]
        public void AddPhoneNumberViewModelTest()
        {
            string Number = "TestText";

            var entidad = new AddPhoneNumberViewModel()
            {
                Number = Number
            };

            Assert.AreEqual(entidad.Number, Number);
        }

        [TestMethod]
        public void VerifyPhoneNumberViewModelTest()
        {
            string Code = "TestText";
            string PhoneNumber = "TestText";
            

            var entidad = new VerifyPhoneNumberViewModel()
            {
                Code = Code,
                PhoneNumber = PhoneNumber
            };

            Assert.AreEqual(entidad.Code, Code);
            Assert.AreEqual(entidad.PhoneNumber, PhoneNumber);
        }

        [TestMethod]
        public void ConfigureTwoFactorViewModelTest()
        {
            string SelectedProvider = "TestText";
            ICollection<System.Web.Mvc.SelectListItem> Providers = new List<SelectListItem>();


            var entidad = new ConfigureTwoFactorViewModel()
            {
                SelectedProvider = SelectedProvider,
                Providers = Providers
            };

            Assert.AreEqual(entidad.SelectedProvider, SelectedProvider);
            Assert.AreEqual(entidad.Providers, Providers);
        }
    }
}
