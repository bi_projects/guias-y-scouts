﻿using System;
using CR.CGC.GuiasScoutsWeb.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Models.Test
{
    [TestClass]
    public class FaqListViewModelTest
    {
        [TestMethod]
        public void FaqListViewModelEntity()
        {
            int FaqModuleId = 2;
            string ModuleName = "TestText";
            int FaqsCount = 3;

            var entidad = new FaqListViewModel()
            {
                FaqModuleId = FaqModuleId,
                ModuleName = ModuleName,
                FaqsCount = FaqsCount
            };

            Assert.AreEqual(entidad.FaqModuleId, FaqModuleId);
            Assert.AreEqual(entidad.ModuleName, ModuleName);
            Assert.AreEqual(entidad.FaqsCount, FaqsCount);
        }
    }

    public class SendFaqViewModelTest
    {
        [TestMethod]
        public void SendFaqViewModelEntity()
        {
            int FaqModuleId = 3;
            int? FaqId = 4;

            var entidad = new SendFaqViewModel()
            {
                FaqModuleId = FaqModuleId,
                FaqId = FaqId
            };

            Assert.AreEqual(entidad.FaqModuleId, FaqModuleId);
            Assert.AreEqual(entidad.FaqId, FaqId);
        }
    }
}
