﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CR.CGC.GuiasScoutsWeb.Entities
{
    public class File
    {
        public string FileName { get; set; }
        public string FileUrl { get; set; }
    }
}