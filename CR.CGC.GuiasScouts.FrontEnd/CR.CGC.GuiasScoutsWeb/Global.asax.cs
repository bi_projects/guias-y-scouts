﻿using CR.CGC.GuiasScoutsWeb.Models.CustomBinderModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace CR.CGC.GuiasScoutsWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ModelBinders.Binders.Add(typeof(DateTime), new CustomDateBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new CustomDateBinder());

            ModelBinders.Binders.Add(typeof(decimal), new CustomDecimalBinder());
            ModelBinders.Binders.Add(typeof(decimal?), new CustomDecimalBinder());

            //ModelBinders.Binders.Add(typeof(float), new SingleModelBinder());
            //ModelBinders.Binders.Add(typeof(float?), new SingleModelBinder());

            //ModelBinders.Binders.Add(typeof(double), new DoubleModelBinder());
            //ModelBinders.Binders.Add(typeof(double?), new DoubleModelBinder());

            /// Tipo de claim usado para generar la identidad del usuario en sesion
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
        }

    }
}
